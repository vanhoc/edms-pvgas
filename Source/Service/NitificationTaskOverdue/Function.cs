﻿
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;


namespace NitificationTaskOverdue
{
    using System;
    using System.Configuration;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Net.Mail;
    using System.IO;
    using System.Net;
    using System.Data;
    using System.Web;
    using System.Data.SqlClient;
    using System.ServiceProcess;
    public  class Function
    {


        private readonly UserService userService;
        private readonly ObjectAssignedUserService objectAssgnedUserService;
        private readonly ConrespondenceService documentService;
        private readonly HolidayConfigService holidayConfigService;
        private readonly HashSet<DateTime> Holidays = new HashSet<DateTime>();
        public Function()
        {
            this.userService = new UserService();
            this.objectAssgnedUserService = new ObjectAssignedUserService();
            this.documentService = new ConrespondenceService();
            this.holidayConfigService = new HolidayConfigService();

            var holidayList = this.holidayConfigService.GetAll();
            foreach (var holidayConfig in holidayList)
            {
                for (DateTime i = holidayConfig.FromDate.GetValueOrDefault(); i < holidayConfig.ToDate.GetValueOrDefault(); i = i.AddDays(1))
                {
                    this.Holidays.Add(i);
                }
            }
        }
        public void GetAllTaskOverdue()
        {
            var listTask = this.objectAssgnedUserService.GetAllOverDueTask();
            var listUser = listTask.Select(t => t.UserID.GetValueOrDefault()).Distinct().ToList();
            foreach(var obj in listUser)
            {
              if(!this.Holidays.Contains(DateTime.Now))  this.SendNotification(listTask.Where(t => t.UserID == obj).ToList(), this.userService.GetByID(obj));
            }
        }


        private void SendNotification(List<ObjectAssignedUser> assignWorkingUser, User assignUserObj)
        {
            try
            {
                // Implement send mail function
                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                };


                var message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;

                message.Subject = "Task Overdue in workflow process";

                // Generate email body You has &nbsp;#Count# &nbsp; task overdue on EDMS System until today.<br />
                var bodyContent = @"<div style=‘text-align: center;’> 
                                    <span class=‘Apple-tab-span’>Dear All,&nbsp;</span><br />
                                  <<<< FOR OVERDUE >>>><br/><br/>
                                    <p style=‘text-align: center;’><strong><span style=‘font-size: 18px;’>
                                    
                                    REMINDER, you have #Count# the following document past due. Please action by due date.
                                    </span></strong></p><br/><br/>

                                       <table border='1' cellspacing='0'>
                                       <tr>
                                       <th style='text-align:center; width:40px'>No.</th>
                                       <th style='text-align:center; width:330px'>Current Workflow</th>
                                       <th style='text-align:center; width:330px'>Current Workflow Step</th>
                                       <th style='text-align:center; width:330px'>Document.No Title</th>
                                       <th style='text-align:center; width:330px'>Title Title</th>
                                       <th style='text-align:center; width:200px'>Received Date</th>
                                       <th style='text-align:center; width:200px'>Due Date</th>
                                       </tr>";
                var count = 0;

                foreach (var document in assignWorkingUser)
                {

                    count += 1;

                    bodyContent += @"<tr>
                                               <td>" + count + @"</td>
                                               <td>" + document.WorkflowName + @"</td>
                                               <td>"
                                   + document.CurrentWorkflowStepName + @"</td>
                                               <td>"
                                   + document.ObjectNumber + @"</td>
                                               <td>"
                                   + document.ObjectTitle + @"</td>
                                               <td>"
                                   + document.ReceivedDate.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
                                        <td>"
                                   + document.PlanCompleteDate.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm:ss") + @"</td>";

                }

                var st = ConfigurationManager.AppSettings["WebAddress"] + @"/ToDoListPage.aspx";
                bodyContent += @" </table>
                                       <br/>
                                       <span><br />
                                    &nbsp;This link to access&nbsp;:&nbsp; <a href='" + st + "'>" + st + "</a>" +
                             @" <br/>   EDMS WORKFLOW NOTIFICATION </br>
                        [THIS IS SYSTEM GENERATED NOTIFICATION PLEASE DO NOT REPLY]";
                message.Body = bodyContent.Replace("#Count#", count.ToString());
                if (!string.IsNullOrEmpty(assignUserObj.Email))
                {
                    message.To.Add(assignUserObj.Email);
                }

                foreach (var userObj in this.userService.GetAllByDC())
                {

                    if (!string.IsNullOrEmpty(userObj.Email)) message.CC.Add(userObj.Email);

                }
                message.Bcc.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                smtpClient.Send(message);
                TextWriter file = new StreamWriter(ConfigurationManager.AppSettings["File"], true);
                file.WriteLine("Sent email to " + assignUserObj.Email + " .Time: " + DateTime.Now);
                file.Close();
            }
            catch { }
        }

        public void GetCorresponedenceOverDue()
        {
            var listDocuments = this.documentService.GetAll().Where(t =>!t.IsDelete.GetValueOrDefault() && t.AnswerRequestDate != null && t.AnswerRequestDate.GetValueOrDefault().Date <= DateTime.Now.AddDays(1).Date && string.IsNullOrEmpty(t.Reply)).ToList();
            if (listDocuments.Count > 0 && !this.Holidays.Contains(DateTime.Now))
            {
                try
                {
                    // Implement send mail function
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };


                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;

                    message.Subject = "Correspondence Documents Overdue _ " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

                    // Generate email body You has &nbsp;#Count# &nbsp; task overdue on EDMS System until today.<br />
                    var bodyContent = @"<div style=‘text-align: center;’> 
                                    <span class=‘Apple-tab-span’>Dear All,&nbsp;</span><br />
                                  <<<< FOR CORRESPONDENCE OVERDUE >>>><br/><br/>
                                    <p style=‘text-align: center;’><strong><span style=‘font-size: 18px;’>
                                    
                                    REMINDER, There are #Count# documents that are past due or nearly overdue. Please action by date of Answer Request .
                                    </span></strong></p><br/><br/>

                                       <table border='1' cellspacing='0'>
                                       <tr>
                                       <th style='text-align:center; width:40px'>No.</th>
                                       <th style='text-align:center; width:330px'>M-Number</th>
                                       <th style='text-align:center; width:330px'>Issue Date</th>
                                       <th style='text-align:center; width:330px'>Answer Request</th>
                                       <th style='text-align:center; width:330px'>Document Title</th>
                                       <th style='text-align:center; width:200px'>Discipline</th>
                                       <th style='text-align:center; width:200px'>Cos type </th>
                                        <th style='text-align:center; width:200px'>From Unit</th>
                                       <th style='text-align:center; width:200px'>To Unit</th>
                                       </tr>";
                    var count = 0;

                    foreach (var document in listDocuments)
                    {

                        count += 1;

                        bodyContent += @"<tr>
                                               <td>" + count + @"</td>
                                               <td>" + document.DocumentNumber + @"</td>
                                               <td>" + document.IssueDate.GetValueOrDefault().ToString("dd/MM/yyyy") + @"</td>
                                               <td>" + document.AnswerRequestDate.GetValueOrDefault().ToString("dd/MM/yyyy") + @"</td>
                                               <td>" + document.Title + @"</td>
                                               <td>" + document.DisciplineName + @"</td>
                                               <td>" + document.DocumentTypeName + @"</td>
                                               <td>" + document.FromName + @"</td>
                                               <td>" + document.ToName + @"</td>";

                    }

                    var st = ConfigurationManager.AppSettings["WebAddress"] + @"/Controls/Document/CorrespondenceList.aspx";
                    bodyContent += @" </table>
                                       <br/>
                                       <span><br />
                                    &nbsp;This link to access&nbsp;:&nbsp; <a href='" + st + "'>" + st + "</a>" +
                                 @" <br/>   EDMS WORKFLOW NOTIFICATION </br>
                        [THIS IS SYSTEM GENERATED NOTIFICATION PLEASE DO NOT REPLY]";
                    message.Body = bodyContent.Replace("#Count#", count.ToString());

                    foreach (var userObj in this.userService.GetAllByDC())
                    {

                        if (!string.IsNullOrEmpty(userObj.Email)) message.To.Add(userObj.Email);

                    }
                    message.Bcc.Add(ConfigurationManager.AppSettings["EmailAccount"]);
                    smtpClient.Send(message);
                    TextWriter file = new StreamWriter(ConfigurationManager.AppSettings["File"], true);
                    file.WriteLine("Sent email Over-due Correspondence .Time: " + DateTime.Now);
                    file.Close();
                }
                catch { }
            }
        }
    }
}
