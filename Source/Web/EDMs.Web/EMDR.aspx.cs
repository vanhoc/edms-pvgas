﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Data;
using System.Drawing;
using EDMs.Business.Services.Scope;

namespace EDMs.Web
{
	using System;
	using System.Collections.Generic;
	using System.Configuration;
    using System.Web;
    using System.IO;
	using System.Linq;
	using System.Net;
	using System.Net.Mail;
	using System.ServiceProcess;
	using System.Text;
	using System.Web.Hosting;
	using System.Web.UI;
	using System.Web.UI.WebControls;

	using Aspose.Cells;

	using EDMs.Business.Services.Document;
	using EDMs.Business.Services.Library;
	using EDMs.Business.Services.Security;
	using EDMs.Data.Entities;
	using EDMs.Web.Utilities;
	using EDMs.Web.Utilities.Sessions;

	using Telerik.Web.UI;

	using CheckBox = System.Web.UI.WebControls.CheckBox;
	using Label = System.Web.UI.WebControls.Label;
	using TextBox = System.Web.UI.WebControls.TextBox;

	/// <summary>
	/// Class customer
	/// </summary>
	public partial class EMDR : Page
	{
		

		/// <summary>
		/// The permission service.
		/// </summary>
		private readonly PermissionService permissionService = new PermissionService();

		/// <summary>
		/// The revision service.
		/// </summary>
		private readonly RevisionService revisionService = new RevisionService();

		/// <summary>
		/// The document type service.
		/// </summary>
		private readonly DocumentTypeService documentTypeService = new DocumentTypeService();


		private readonly NotificationRuleService notificationRuleService = new NotificationRuleService();

		private readonly GroupDataPermissionService groupDataPermissionService = new GroupDataPermissionService();

		private readonly UserService userService = new UserService();

		private readonly AttachFileService attachFileService = new AttachFileService();

		private readonly AttachFilesPackageService attachFilesPackageService = new AttachFilesPackageService();

		private readonly ProjectCodeService scopeProjectService = new  ProjectCodeService();

		//private readonly PackageService packageService = new PackageService();

		private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

		private readonly DisciplineService disciplineService = new DisciplineService();

		private readonly RoleService roleService = new RoleService();

		private readonly TemplateManagementService templateManagementService = new TemplateManagementService();

		private readonly PermissionDisciplineService permissionDisciplineService = new PermissionDisciplineService();

		private readonly ContractorService contractorService = new ContractorService();

		private readonly CommentResponseService commentResponseService = new CommentResponseService();

		private readonly DocumentNumberingService documentNumberingService = new DocumentNumberingService();

		private readonly OriginatorService originatorService = new OriginatorService();

		private readonly StatusService statusService = new StatusService();

		protected const string ServiceName = "EDMSFolderWatcher";
        private readonly PackageService packageService = new PackageService();
        public static RadTreeNode editedNode = null;
        private readonly DocumentStatuService DocumentStatusService= new DocumentStatuService();
        private readonly CodeService codeSerVice = new CodeService();
        private readonly CategoryService categoryService = new CategoryService();

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";

		/// <summary>
		/// The list folder id.
		/// </summary>
		//private List<int> listFolderId = new List<int>();

		/// <summary>
		/// The page_ load.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		protected void Page_Load(object sender, EventArgs e)
		{
			this.Title = ConfigurationManager.AppSettings.Get("AppName");
			Session.Add("SelectedMainMenu", "Document Management");

			if (!Page.IsPostBack)
			{
				this.LoadObjectTree();
				Session.Add("IsListAll", false);

				if (!UserSession.Current.User.Role.IsAdmin.GetValueOrDefault())
				{
					this.CustomerMenu.Items[0].Visible = false;
					this.CustomerMenu.Items[1].Visible = false;
					foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
					{
						if (item.Value == "Adminfunc")
						{
							item.Visible = false;
						}
					}

					this.CustomerMenu.Items[3].Visible = false;

					this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
					this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
					this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;
				}

				if (UserSession.Current.User.Role.IsAdmin.GetValueOrDefault())
				{
					this.IsFullPermission.Value = "true";
				}


				//this.LoadListPanel();
				//this.LoadSystemPanel();
			}
		}

		/// <summary>
		/// The rad tree view 1_ node click.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		//protected void radTreeFolder_NodeClick(object sender, RadTreeNodeEventArgs e)
		//{
		//	var folder = this.folderService.GetById(Convert.ToInt32(e.Node.Value));
		//	var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
		//	temp.NavigateUrl = ConfigurationSettings.AppSettings.Get("ServerName") + folder.DirName;


		//	////var originalURL = @"\\" + ConfigurationSettings.AppSettings.Get("ServerName") + @"\" + folder.DirName.Replace(@"/", @"\");
		//	////var tempURI = new Uri(originalURL);/////

		//	////var temp = (RadToolBarButton)this.CustomerMenu.FindItemByText("View explorer");
		//	////temp.NavigateUrl = tempURI.AbsoluteUri;

		//	var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
		//	this.LoadDocuments(true, isListAll);
		//}

		/// <summary>
		/// Load all document by folder
		/// </summary>
		/// <param name="isbind">
		/// The isbind.
		/// </param>
		protected void LoadDocuments(bool isbind = false, bool isListAll = false)
		{
			var cbShowAll = (CheckBox)this.CustomerMenu.Items[4].FindControl("ckbShowAll");
			var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
			var docList = new List<DocumentPackage>();
			if (this.rtvDiscipline.SelectedNode != null)
			{
                if(this.rtvDiscipline.SelectedNode.ParentNode!= null)
                {
                    var selectedNodesId =
                       this.rtvDiscipline.SelectedNode.GetAllNodes().Select(t => Convert.ToInt32(t.Value)).ToList();
                    selectedNodesId.Insert(0, Convert.ToInt32(rtvDiscipline.SelectedNode.Value));
                    var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                    docList = this.documentPackageService.GetAllByPackage(category.PackageId.GetValueOrDefault(), cbShowAll.Checked)
                    .Where(t=> selectedNodesId.Contains(t.CategoryID.GetValueOrDefault()))
                    .OrderBy(t => t.CategoryID.GetValueOrDefault()).ThenBy(t => t.DocNo).ThenBy(t => t.RevisionName)
                    .ToList();
                    this.lbPakageId.Value = category.PackageId.ToString();
                }
                else
                {
                    docList = this.documentPackageService.GetAllByPackage(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value), cbShowAll.Checked)
                                        .OrderBy(t => t.CategoryID.GetValueOrDefault()).ThenBy(t => t.DocNo).ThenBy(t => t.RevisionName)
                                        .ToList();
                    this.lbPakageId.Value = this.rtvDiscipline.SelectedNode.Value;
                }
			

				//var txtDisciplineComplete = this.CustomerMenu.Items[3].FindControl("txtDisciplineComplete") as RadNumericTextBox;
				//var txtDisciplineWeight = this.CustomerMenu.Items[3].FindControl("txtDisciplineWeight") as RadNumericTextBox;

				//if (txtDisciplineComplete != null)
				//{
				//	double? complete = 0;
				//	complete = docList.Aggregate(complete, (current, t) => current + (t.Complete * t.Weight) / 100);
				//	txtDisciplineComplete.Value = complete;
				//}

				//if (txtDisciplineWeight != null)
				//{
				//	double? weight = 0;
				//	weight = docList.Aggregate(weight, (current, t) => current + t.Weight);
				//	txtDisciplineWeight.Value = weight;
				//}
			}
			else
			{
				docList = this.documentPackageService.GetAllByProject(projectId, cbShowAll.Checked)
					.OrderBy(t => t.PackageId).ThenBy(t=> t.CategoryID).ThenBy(t => t.DocNo).ThenBy(t=> t.RevisionName)
                        .ToList();

				this.CustomerMenu.Items[3].Visible = false;
			}
            //if (cbShowAll.Checked == true)
            //{
            //    docList=docList.OrderBy(t => t.PackageId).ThenBy(t => t.CategoryID).ThenBy(t => t.DocNo)
            //}
			this.grdDocument.DataSource = docList;
		}
        private void ExportEMDRReport()
        {


            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook(filePath + @"Template\EMDR_S-Curve_PPM_Template.xlsm");
            var dataSheet = workbook.Worksheets[0];
            var scurveSheet = workbook.Worksheets[1];
            var ppmSheet = workbook.Worksheets[2];

            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            /// var docList = this.documentPackageService.GetAllByProject(projectId,true).Where(t => t.IsEMDR.GetValueOrDefault() && t.PhaseName=="DD" && t.ContractorId==5).ToList();

            var docList = this.documentPackageService.GetAllByProject(projectObj.ID, false).Where(t => t.IsEMDR.GetValueOrDefault()).ToList();
            if (this.rtvDiscipline.SelectedNode.ParentNode != null)
            {
                var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                docList = docList.Where(t => t.PackageId == category.PackageId)
                .ToList();
                dataSheet.Name = category.PackageName;
            }
            else
            {
                docList = docList = docList.Where(t => t.PackageId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).ToList();
                dataSheet.Name = this.rtvDiscipline.SelectedNode.Text;
            }

            // var unitRowList = new List<string>();
            var disciplineRowList = new List<int>();
            var docRowList = new List<string>();

            var rowCount = 0;
            var countDoc = 0;
            var totalDisciplineManhour = 0.0;
            totalDisciplineManhour = docList.Sum(t => t.Manhours.GetValueOrDefault());

            dataSheet.Cells["M16"].PutValue(totalDisciplineManhour);

            var st = workbook.CreateStyle();
            st.IsTextWrapped = true;
            st.HorizontalAlignment = TextAlignmentType.Center;
            st.Number = 1;

            var flag = new StyleFlag();
            flag.All = true;
            flag.NumberFormat = true;
            var ppmCount = 0;

            var dtDoc = new DataTable();
            dtDoc.Columns.AddRange(new[]
            {

                new DataColumn("NoIndex", typeof (String)),
                new DataColumn("Empty1", typeof (String)),
                new DataColumn("empty2", typeof (String)),
                new DataColumn("DocNo", typeof (String)),
                new DataColumn("DocTitle", typeof (String)),
                new DataColumn("DocType", typeof (String)),
                new DataColumn("DisCipline", typeof (String)),
                new DataColumn("Empty3", typeof (String)),
                new DataColumn("empty4", typeof (String)),
                new DataColumn("Empty5", typeof (String)),
                new DataColumn("empty6", typeof (String)),
                new DataColumn("empty7", typeof (String)),
                new DataColumn("Manhour", typeof (Double)),
                new DataColumn("Weight", typeof (Double)),
                new DataColumn("PlanStart", typeof (DateTime)),
                new DataColumn("ForeStart", typeof (DateTime)),
                new DataColumn("ActualSatrt", typeof (DateTime)),
                new DataColumn("PlanIDC", typeof (DateTime)),
                new DataColumn("ForeIDC", typeof (DateTime)),
                new DataColumn("ActualIDC", typeof (DateTime)),
                new DataColumn("PlanIFRSAIPEM", typeof (DateTime)),
                new DataColumn("ForeIFRSAIPEM", typeof (DateTime)),
              new DataColumn("ActualIFRSAIPEM", typeof (DateTime)),
                new DataColumn("PlanIFREXM", typeof (DateTime)),
                new DataColumn("ForeIFREXM", typeof (DateTime)),
              new DataColumn("ActualIFREXM", typeof (DateTime)),
                new DataColumn("PlanIFD", typeof (DateTime)),
                new DataColumn("ForeIFD", typeof (DateTime)),
              new DataColumn("ActualIFD", typeof (DateTime)),
                new DataColumn("PlanFINAL", typeof (DateTime)),
                new DataColumn("ForeFINAL", typeof (DateTime)),
              new DataColumn("ActualFINAL", typeof (DateTime)),
              new DataColumn("empty8", typeof (String)),
            });

            dataSheet.Cells["E11"].PutValue(DateTime.Now);
            dataSheet.Cells["AR16"].PutValue("4");
            var countDiscipline = 0;
            List<int> ListRowDiscipline = new List<int>();
            rowCount += 1;
            var docGroupByDiscipline = docList.GroupBy(t => t.DisciplineName);
            foreach (var docTableByDiscipline in docGroupByDiscipline.OrderBy(t => t.Key.ToString()))
            {

                countDiscipline += 1;
                ListRowDiscipline.Add(rowCount + 16 - 1);
                var disNumber = Int32.Parse(docTableByDiscipline.Key.ToString());
                dataSheet.Cells["AR" + (16 + rowCount)].PutValue("3");
                dataSheet.Cells["A" + (16 + rowCount)].PutValue(Utility.ToRoman(disNumber));
                dataSheet.Cells["D" + (16 + rowCount)].PutValue(this.disciplineService.GetByName(docTableByDiscipline.Key.ToString()).FullName);

                var DisciplineManhour = 0.0;
                DisciplineManhour = docTableByDiscipline.ToList().Sum(t => t.Manhours.GetValueOrDefault());
                dataSheet.Cells["N" + (16 + rowCount)].Formula = "=M" + (16 + rowCount) + "/$M$16";//.PutValue(docTableByDiscipline.ToList().Sum(t => t.Weight.GetValueOrDefault() / 100));//

                dataSheet.Cells["M" + (16 + rowCount)].PutValue(DisciplineManhour);

                //put Weekly Progress Dis
                ppmSheet.Cells["N" + (10 + ppmCount)].PutValue("0");
                ppmSheet.Cells["A" + (10 + ppmCount)].PutValue(Utility.ToRoman(disNumber));
                ppmSheet.Cells["B" + (10 + ppmCount)].PutValue(this.disciplineService.GetByName(docTableByDiscipline.Key.ToString()).FullName);


                ppmSheet.Cells["C" + (10 + ppmCount)].Formula = "=EMDR!N$" + (16 + rowCount);

                ppmSheet.Cells["D" + (10 + ppmCount)].Formula = "=EMDR!AH$" + (16 + rowCount);
                ppmSheet.Cells["E" + (10 + ppmCount)].Formula = "=EMDR!AI$" + (16 + rowCount);
                ppmSheet.Cells["F" + (10 + ppmCount)].Formula = "=E" + (10 + ppmCount) + "-D" + (10 + ppmCount);

                ppmSheet.Cells["G" + (10 + ppmCount)].Formula = "=J" + (10 + ppmCount) + "-D" + (10 + ppmCount);
                ppmSheet.Cells["H" + (10 + ppmCount)].Formula = "=K" + (10 + ppmCount) + "-E" + (10 + ppmCount);
                ppmSheet.Cells["I" + (10 + ppmCount)].Formula = "=L" + (10 + ppmCount) + "-F" + (10 + ppmCount);

                ppmSheet.Cells["J" + (10 + ppmCount)].Formula = "=EMDR!AJ$" + (16 + rowCount);
                ppmSheet.Cells["K" + (10 + ppmCount)].Formula = "=EMDR!AK$" + (16 + rowCount);
                ppmSheet.Cells["L" + (10 + ppmCount)].Formula = "=K" + (10 + ppmCount) + "-J" + (10 + ppmCount);

                ppmSheet.Cells["M" + (10 + ppmCount)].Formula = "=EMDR!AM$" + (16 + rowCount);

                ppmCount += 1;


                disciplineRowList.Add(rowCount);

                rowCount += 1;
                dtDoc.Rows.Clear();
                // Count doc by status

                foreach (var docObj in docTableByDiscipline.OrderBy(t => t.DocumentTypeName))
                {
                    countDoc += 1;
                    var dataRow = dtDoc.NewRow();
                    dataRow["NoIndex"] = countDoc;

                    dataRow["DocNo"] = docObj.DocNo;
                    dataRow["DocTitle"] = docObj.DocTitle;
                    dataRow["DocType"] = docObj.DocumentTypeName;
                    dataRow["DisCipline"] = docObj.DisciplineName;

                    dataRow["Manhour"] = docObj.Manhours != null ? docObj.Manhours.GetValueOrDefault() : 0.0;
                    dataRow["Weight"] = DisciplineManhour==0? 0.0:(((docObj.Manhours != null && docObj.Manhours!= 0) ? docObj.Manhours.GetValueOrDefault() : 0.0) / DisciplineManhour);// docObj.Weight != null ? docObj.Weight.GetValueOrDefault() / 100 : 0;//

                    if (docObj.StartPlan != null) dataRow["PlanStart"] = docObj.StartPlan;
                    if (docObj.StartRevised != null) dataRow["ForeStart"] = docObj.StartRevised;
                    if (docObj.StartActual != null) dataRow["ActualSatrt"] = docObj.StartActual;
                    if (docObj.IDCPlan != null) dataRow["PlanIDC"] = docObj.IDCPlan;
                    if (docObj.IDCRevised != null) dataRow["ForeIDC"] = docObj.IDCRevised;
                    if (docObj.IDCACtual != null) dataRow["ActualIDC"] = docObj.IDCACtual;
                    if (docObj.IFRPlan != null) dataRow["PlanIFRSAIPEM"] = docObj.IFRPlan;
                    if (docObj.IFRRevised != null) dataRow["ForeIFRSAIPEM"] = docObj.IFRRevised;
                    if (docObj.IFRActual != null) dataRow["ActualIFRSAIPEM"] = docObj.IFRActual;
                    if (docObj.ReIFRPlan != null) dataRow["PlanIFREXM"] = docObj.ReIFRPlan;
                    if (docObj.ReIFRRevised != null) dataRow["ForeIFREXM"] = docObj.ReIFRRevised;
                    if (docObj.ReIFRActual != null) dataRow["ActualIFREXM"] = docObj.ReIFRActual;
                    if (docObj.IFAPlan != null) dataRow["PlanIFD"] = docObj.IFAPlan;
                    if (docObj.IFARevised != null) dataRow["ForeIFD"] = docObj.IFARevised;
                    if (docObj.IFAActual != null) dataRow["ActualIFD"] = docObj.IFAActual;
                    if (docObj.AFCPlan != null) dataRow["PlanFINAL"] = docObj.AFCPlan;
                    if (docObj.AFCRevised != null) dataRow["ForeFINAL"] = docObj.AFCRevised;
                    if (docObj.AFCActual != null) dataRow["ActualFINAL"] = docObj.AFCActual;
                    dtDoc.Rows.Add(dataRow);
                    docRowList.Add(disciplineRowList[disciplineRowList.Count - 1] + "$" + rowCount.ToString());
                    rowCount += 1;
                }
                dataSheet.Cells.ImportDataTable(dtDoc, false, 15 + rowCount - dtDoc.Rows.Count, 0, dtDoc.Rows.Count, dtDoc.Columns.Count, false);



            }
            var STFormula1 = "=";

            var SumWeight = "=";
            for (int i = 0; i < rowCount; i++)
            {
                if (disciplineRowList.Contains(i))
                {
                    SumWeight += "N" + (16 + i) + "+";
                    STFormula1 += "AH" + (16 + i) + "*N" + (16 + i) + "+";

                    var docOfDis = docRowList.Where(t => t.Split('$')[0] == i.ToString()).Select(t => Convert.ToInt32(t.Split('$')[1])).ToList();
                    if (docOfDis.Count > 0)
                    {
                        //if (docOfDis.Count > 1)
                        //{
                        dataSheet.Cells["Q" + (16 + i)].Formula = "=COUNTIFS($F" + (16 + i + 1) + ":$F" + (16 + i + docOfDis.Count) + ",\"<>ACT\",Q" + (16 + i + 1) + ":Q" + (16 + i + docOfDis.Count) + ",\"<>\")";

                        dataSheet.Cells["T" + (16 + i)].Formula = "=COUNTIFS($F" + (16 + i + 1) + ":$F" + (16 + i + docOfDis.Count) + ",\"<>ACT\",T" + (16 + i + 1) + ":T" + (16 + i + docOfDis.Count) + ",\"<>\")";

                        dataSheet.Cells["W" + (16 + i)].Formula = "=COUNTIFS($F" + (16 + i + 1) + ":$F" + (16 + i + docOfDis.Count) + ",\"<>ACT\",W" + (16 + i + 1) + ":W" + (16 + i + docOfDis.Count) + ",\"<>\")";

                        dataSheet.Cells["Z" + (16 + i)].Formula = "=COUNTIFS($F" + (16 + i + 1) + ":$F" + (16 + i + docOfDis.Count) + ",\"<>ACT\",Z" + (16 + i + 1) + ":Z" + (16 + i + docOfDis.Count) + ",\"<>\")";

                        dataSheet.Cells["AC" + (16 + i)].Formula = "=COUNTIFS($F" + (16 + i + 1) + ":$F" + (16 + i + docOfDis.Count) + ",\"<>ACT\",AC" + (16 + i + 1) + ":AC" + (16 + i + docOfDis.Count) + ",\"<>\")";

                        dataSheet.Cells["AF" + (16 + i)].Formula = "=COUNTIFS($F" + (16 + i + 1) + ":$F" + (16 + i + docOfDis.Count) + ",\"<>ACT\",AF" + (16 + i + 1) + ":AF" + (16 + i + docOfDis.Count) + ",\"<>\")";

                        dataSheet.Cells["AH" + (16 + i)].Formula = "=SUMPRODUCT(AH" + (16 + i + 1) + ":AH" + (16 + i + docOfDis.Count) + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";
                        dataSheet.Cells["AI" + (16 + i)].Formula = "=SUMPRODUCT(AI" + (16 + i + 1) + ":AI" + (16 + i + docOfDis.Count) + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";
                        dataSheet.Cells["AJ" + (16 + i)].Formula = "=SUMPRODUCT(AJ" + (16 + i + 1) + ":AJ" + (16 + i + docOfDis.Count) + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";
                        dataSheet.Cells["AK" + (16 + i)].Formula = "=SUMPRODUCT(AK" + (16 + i + 1) + ":AK" + (16 + i + docOfDis.Count) + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";
                        dataSheet.Cells["AL" + (16 + i)].Formula = "=AK" + (16 + i) + "-AJ" + (16 + i) + "";
                        dataSheet.Cells["AM" + (16 + i)].Formula = "=SUMPRODUCT(AM" + (16 + i + 1) + ":AM" + (16 + i + docOfDis.Count) + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";
                        // }

                        //Formula for plan, forecast, actual Discipline
                        for (int j = 1; j <= 49; j++)
                        {
                            var disciplinePlan = "=SUMPRODUCT(" + dataSheet.Cells[15 + i + 1, 46 + j].Name + ":" + dataSheet.Cells[15 + i + docOfDis.Count, 46 + j].Name + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";


                            var disciplineForecast = "=SUMPRODUCT(" + dataSheet.Cells[15 + i + 1, 46 + (2 + 49) + j].Name + ":" + dataSheet.Cells[15 + i + docOfDis.Count, 46 + (2 + 49) + j].Name + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";

                            var disciplineActual = "=SUMPRODUCT(" + dataSheet.Cells[15 + i + 1, 46 + (2 + 49) * 2 + j].Name + ":" + dataSheet.Cells[15 + i + docOfDis.Count, 46 + (2 + 49) * 2 + j].Name + ",$N$" + (16 + i + 1) + ":$N$" + (16 + i + docOfDis.Count) + ")";


                            dataSheet.Cells[15 + i, 46 + j].Formula = disciplinePlan;
                            dataSheet.Cells[15 + i, 46 + (2 + 49) + j].Formula = disciplineForecast;
                            dataSheet.Cells[15 + i, 46 + (2 + 49) * 2 + j].Formula = disciplineActual;
                        }

                        foreach (var docrow in docOfDis)
                        {
                            i += 1;


                            var docPreWeekPlan = "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK(O" + (16 + i) + "),ISBLANK(AD" + (16 + i) + ")),0,(MIN($E$11-7,AD" + (16 + i) + ")-O" + (16 + i) + ")/(AD" + (16 + i) + "-O" + (16 + i) + ")),0),1),MAX(IF(AND(O" + (16 + i) + "<>\"\",O" + (16 + i) + "<=$E$11-7),O$15,0),IF(AND(R" + (16 + i) + "<>\"\",R" + (16 + i) + "<=$E$11-7),R$15,0),IF(AND(U" + (16 + i) + "<>\"\",U" + (16 + i) + "<=$E$11-7),U$15,0),IF(AND(X" + (16 + i) + "<>\"\",X" + (16 + i) + "<=$E$11-7),X$15,0),IF(AND(AA" + (16 + i) + "<>\"\",AA" + (16 + i) + "<=E$11-7),AA$15,0),IF(AND(AD" + (16 + i) + "<>\"\",AD" + (16 + i) + "<=E$11-7),AD$15,0)))";

                            var docPreWeekActual = "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK(O" + (16 + i) + "),ISBLANK(AD" + (16 + i) + ")),0,(MIN($E$11-7,AD" + (16 + i) + ")-O" + (16 + i) + ")/(AD" + (16 + i) + "-O" + (16 + i) + ")),0),1),MAX(IF(AND(Q" + (16 + i) + "<>\"\",Q" + (16 + i) + "<=$E$11-7),Q$15,0),IF(AND(T" + (16 + i) + "<>\"\",T" + (16 + i) + "<=$E$11-7),T$15,0),IF(AND(W" + (16 + i) + "<>\"\",W" + (16 + i) + "<=$E$11-7),W$15,0),IF(AND(Z" + (16 + i) + "<>\"\",Z" + (16 + i) + "<=$E$11-7),Z$15,0),IF(AND(AC" + (16 + i) + "<>\"\",AC" + (16 + i) + "<=E$11-7),AC$15,0),IF(AND(AF" + (16 + i) + "<>\"\",AF" + (16 + i) + "<=E$11-7),AF$15,0)))";

                            var docCurrPlan = "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK(O" + (16 + i) + "),ISBLANK(AD" + (16 + i) + ")),0,(MIN($E$11,AD" + (16 + i) + ")-O" + (16 + i) + ")/(AD" + (16 + i) + "-O" + (16 + i) + ")),0),1),MAX(IF(AND(O" + (16 + i) + "<>\"\",O" + (16 + i) + "<=$E$11),O$15,0),IF(AND(R" + (16 + i) + "<>\"\",R" + (16 + i) + "<=$E$11),R$15,0),IF(AND(U" + (16 + i) + "<>\"\",U" + (16 + i) + "<=$E$11),U$15,0),IF(AND(X" + (16 + i) + "<>\"\",X" + (16 + i) + "<=$E$11),X$15,0),IF(AND(AA" + (16 + i) + "<>\"\",AA" + (16 + i) + "<=E$11),AA$15,0),IF(AND(AD" + (16 + i) + "<>\"\",AD" + (16 + i) + "<=E$11),AD$15,0)))";

                            var docCurrActual = "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK(O" + (16 + i) + "),ISBLANK(AD" + (16 + i) + ")),0,(MIN($E$11,AD" + (16 + i) + ")-O" + (16 + i) + ")/(AD" + (16 + i) + "-O" + (16 + i) + ")),0),1),MAX(IF(AND(Q" + (16 + i) + "<>\"\",Q" + (16 + i) + "<=$E$11),Q$15,0),IF(AND(T" + (16 + i) + "<>\"\",T" + (16 + i) + "<=$E$11),T$15,0),IF(AND(W" + (16 + i) + "<>\"\",W" + (16 + i) + "<=$E$11),W$15,0),IF(AND(Z" + (16 + i) + "<>\"\",Z" + (16 + i) + "<=$E$11),Z$15,0),IF(AND(AC" + (16 + i) + "<>\"\",AC" + (16 + i) + "<=E$11),AC$15,0),IF(AND(AF" + (16 + i) + "<>\"\",AF" + (16 + i) + "<=E$11),AF$15,0)))";

                            var docCurrVariati = "=AK" + (16 + i) + "-AJ" + (16 + i);

                            var docNextWeekPlan = "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK(O" + (16 + i) + "),ISBLANK(AD" + (16 + i) + ")),0,(MIN($E$11+7,AD" + (16 + i) + ")-O" + (16 + i) + ")/(AD" + (16 + i) + "-O" + (16 + i) + ")),0),1),MAX(IF(AND(O" + (16 + i) + "<>\"\",O" + (16 + i) + "<=$E$11+7),O$15,0),IF(AND(R" + (16 + i) + "<>\"\",R" + (16 + i) + "<=$E$11+7),R$15,0),IF(AND(U" + (16 + i) + "<>\"\",U" + (16 + i) + "<=$E$11+7),U$15,0),IF(AND(X" + (16 + i) + "<>\"\",X" + (16 + i) + "<=$E$11+7),X$15,0),IF(AND(AA" + (16 + i) + "<>\"\",AA" + (16 + i) + "<=E$11+7),AA$15,0),IF(AND(AD" + (16 + i) + "<>\"\",AD" + (16 + i) + "<=E$11+7),AD$15,0)))";

                            var StartusDoc //= "=IF(F" + (16 + i) + "<>\"ACT\",IF(AQ" + (16 + i) + "<6,VLOOKUP(AQ" + (16 + i) + ",param!$A$2:$C$7,ROUNDDOWN(INDIRECT(\"RC[-\"&25-3*ROUNDDOWN(AQ" + (16 + i) + ",0)&\"]\",FALSE)/$E$11,0)+2,FALSE),\"\"),\"\")";
                            = "=IF(AQ"+(16+i)+ "<7,VLOOKUP(AQ" + (16 + i) + ",param!$A$2:$C$7,ROUNDDOWN(INDIRECT(\"RC[-\"&27-3*ROUNDDOWN(AQ" + (16 + i) + ",0)&\"]\",FALSE)/$E$11,0)+2,FALSE),\"\")";

                            var preMil// = "=MAX(IF(ISBLANK(Q" + (16 + i) + ")=FALSE,1,0),IF(ISBLANK(T" + (16 + i) + ")=FALSE,2,0),IF(ISBLANK(W" + (16 + i) + ")=FALSE,3,0),IF(ISBLANK(Z" + (16 + i) + ")=FALSE,4,0),IF(ISBLANK(AC" + (16 + i) + ")=FALSE,5,0),IF(ISBLANK(AF" + (16 + i) + ")=FALSE,6,0),IF(F" + (16 + i) + "=\"ACT\",7,0))";
                            = "=MAX(IF(AND(ISBLANK(Q" + (16 + i) + ")=FALSE,Q" + (16 + i) + "<=$E$11),1,0),IF(AND(ISBLANK(T" + (16 + i) + ")=FALSE,T" + (16 + i) + "<=$E$11),2,0),IF(AND(ISBLANK(W" + (16 + i) + ")=FALSE,W" + (16 + i) + "<=$E$11),3,0),IF(AND(ISBLANK(Z" + (16 + i) + ")=FALSE,Z" + (16 + i) + "<=$E$11),4,0),IF(AND(ISBLANK(AC" + (16 + i) + ")=FALSE,AC" + (16 + i) + "<=$E$11),5,0),IF(AND(ISBLANK(AF" + (16 + i) + ")=FALSE,AF" + (16 + i) + "<=$E$11),6,0))";
                            var currMil// = "=MIN(IF(AND(ISBLANK(O" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<1),1,7),IF(AND(ISBLANK(R" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<2),2,7),IF(AND(ISBLANK(U" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<3),3,7),IF(AND(ISBLANK(X" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<4),4,7),IF(AND(ISBLANK(AA" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<5),5,7),IF(AND(ISBLANK(AD" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<6),6,7))";

                                        = "=MIN(IF(AND(ISBLANK(O" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<1),1,7),IF(AND(ISBLANK(R" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<2),2,7),IF(AND(ISBLANK(U" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<3),3,7),IF(AND(ISBLANK(X" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<4),4,7),IF(AND(ISBLANK(AA" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<5),5,7),IF(AND(ISBLANK(AD" + (16 + i) + ")=FALSE,AP" + (16 + i) + "<6),6,7))";

                            dataSheet.Cells["AH" + (16 + i)].Formula = docPreWeekPlan;
                            dataSheet.Cells["AI" + (16 + i)].Formula = docPreWeekActual;
                            dataSheet.Cells["AJ" + (16 + i)].Formula = docCurrPlan;
                            dataSheet.Cells["AK" + (16 + i)].Formula = docCurrActual;
                            dataSheet.Cells["AL" + (16 + i)].Formula = docCurrVariati;
                            dataSheet.Cells["AM" + (16 + i)].Formula = docNextWeekPlan;
                            dataSheet.Cells["AN" + (16 + i)].Formula = StartusDoc;
                            dataSheet.Cells["AP" + (16 + i)].Formula = preMil;
                            dataSheet.Cells["AQ" + (16 + i)].Formula = currMil;

                            for (int j = 1; j <= 49; j++)
                            {
                                var docPlan =
                                "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK($O" + (16 + i) + "),ISBLANK($AD" + (16 + i) + ")),0,(MIN(" + dataSheet.Cells[13, 46 + j].Name + ",$AD" + (16 + i) + ")-$O" + (16 + i) + ")/($AD" + (16 + i) + "-$O" + (16 + i) + ")),0),1),MAX(IF(AND($O" + (16 + i) + "<>\"\",$O" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + j].Name + "),$O$15,0),IF(AND($R" + (16 + i) + "<>\"\",$R" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + j].Name + "),$R$15,0),IF(AND($U" + (16 + i) + "<>\"\",$U" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + j].Name + "),$U$15,0),IF(AND($X" + (16 + i) + "<>\"\",$X" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + j].Name + "),$X$15,0),IF(AND($AA" + (16 + i) + "<>\"\",$AA" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + j].Name + "),$AA$15,0),IF(AND($AD" + (16 + i) + "<>\"\",$AD" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + j].Name + "),$AD$15,0)))";

                                var docForecast =
                                   "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK($O" + (16 + i) + "),ISBLANK($AD" + (16 + i) + ")),0,(MIN(" + dataSheet.Cells[13, 46 + (2 + 49) + j].Name + ",$AD" + (16 + i) + ")-$O" + (16 + i) + ")/($AD" + (16 + i) + "-$O" + (16 + i) + ")),0),1),MAX(IF(AND($P" + (16 + i) + "<>\"\",$P" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) + j].Name + "),$P$15,0),IF(AND($S" + (16 + i) + "<>\"\",$S" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) + j].Name + "),$S$15,0),IF(AND($V" + (16 + i) + "<>\"\",$V" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) + j].Name + "),$V$15,0),IF(AND($Y" + (16 + i) + "<>\"\",$Y" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) + j].Name + "),$Y$15,0),IF(AND($AB" + (16 + i) + "<>\"\",$AB" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) + j].Name + "),$AB$15,0),IF(AND($AE" + (16 + i) + "<>\"\",$AE" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) + j].Name + "),$AE$15,0)))";

                                var docActual =
                                  "=IF($F" + (16 + i) + "=\"ACT\",MIN(MAX(IF(OR(ISBLANK($O" + (16 + i) + "),ISBLANK($AD" + (16 + i) + ")),0,(MIN(" + dataSheet.Cells[13, 46 + (2 + 49) * 2 + j].Name + ",$AD" + (16 + i) + ")-$O" + (16 + i) + ")/($AD" + (16 + i) + "-$O" + (16 + i) + ")),0),1),MAX(IF(AND($Q" + (16 + i) + "<>\"\",$Q" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) * 2 + j].Name + "),$Q$15,0),IF(AND($T" + (16 + i) + "<>\"\",$T" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) * 2 + j].Name + "),$T$15,0),IF(AND($W" + (16 + i) + "<>\"\",$W" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) * 2 + j].Name + "),$W$15,0),IF(AND($Z" + (16 + i) + "<>\"\",$Z" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) * 2 + j].Name + "),$Z$15,0),IF(AND($AC" + (16 + i) + "<>\"\",$AC" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) * 2 + j].Name + "),$AC$15,0),IF(AND($AF" + (16 + i) + "<>\"\",$AF" + (16 + i) + "<=" + dataSheet.Cells[13, 46 + (2 + 49) * 2 + j].Name + "),$AF$15,0)))";


                                dataSheet.Cells[15 + i, 46 + j].Formula = docPlan;
                                dataSheet.Cells[15 + i, 46 + (2 + 49) + j].Formula = docForecast;
                                dataSheet.Cells[15 + i, 46 + (2 + 49) * 2 + j].Formula = docActual;
                            }
                        }
                    }
                }
            }

            for (int j = 1; j <= 49; j++)
            {
                var STFormulaPlan = "=";
                var STFormulaFore = "=";
                var STFormulaActual = "=";
                foreach (var i in disciplineRowList)
                {
                    STFormulaPlan += dataSheet.Cells[16 + i - 1, 46 + j].Name + "*N" + (16 + i) + "+";
                    STFormulaFore += dataSheet.Cells[16 + i - 1, 46 + (2 + 49) + j].Name + "*N" + (16 + i) + "+";
                    STFormulaActual += dataSheet.Cells[16 + i - 1, 46 + (2 + 49) * 2 + j].Name + "*N" + (16 + i) + "+";
                }

                dataSheet.Cells[15, 46 + j].Formula = STFormulaPlan.Substring(0, STFormulaPlan.Length - 1);
                dataSheet.Cells[15, 46 + (2 + 49) + j].Formula = STFormulaFore.Substring(0, STFormulaFore.Length - 1);
                dataSheet.Cells[15, 46 + (2 + 49) * 2 + j].Formula = STFormulaActual.Substring(0, STFormulaActual.Length - 1);

                //--- Scurve
                scurveSheet.Cells[22, 1 + j].Formula = "=EMDR!" + dataSheet.Cells[14, 46 + j].Name;
                scurveSheet.Cells[23, 1 + j].Formula = "=EMDR!" + dataSheet.Cells[15, 46 + j].Name;

                scurveSheet.Cells[24, 1 + j].Formula = "=EMDR!" + dataSheet.Cells[14, 46 + (2 + 49) + j].Name;
                scurveSheet.Cells[25, 1 + j].Formula = "=EMDR!" + dataSheet.Cells[15, 46 + (2 + 49) + j].Name;

                scurveSheet.Cells[26, 1 + j].Formula = "=IF(" + scurveSheet.Cells[20, 1 + j].Name + "<=EMDR!$E$11,EMDR!" + dataSheet.Cells[14, 46 + (2 + 49) * 2 + j].Name + ",#N/A)";
                scurveSheet.Cells[27, 1 + j].Formula = "=IF(" + scurveSheet.Cells[20, 1 + j].Name + "<=EMDR!$E$11,EMDR!" + dataSheet.Cells[15, 46 + (2 + 49) * 2 + j].Name + ",#N/A)";
                scurveSheet.Cells[28, 1 + j].Formula = "=IF(" + scurveSheet.Cells[20, 1 + j].Name + "<=EMDR!$E$11," + scurveSheet.Cells[27, 1 + j].Name + "-" + scurveSheet.Cells[25, 1 + j].Name + ",\"\")";
                //---
            }
            SumWeight = SumWeight.Substring(0, SumWeight.Length - 1);
            STFormula1 = STFormula1.Substring(0, STFormula1.Length - 1);
            dataSheet.Cells["N16"].Formula = SumWeight;

            dataSheet.Cells["Q16"].Formula = SumWeight.Replace("N", "Q");

            dataSheet.Cells["T16"].Formula = SumWeight.Replace("N", "T");

            dataSheet.Cells["W16"].Formula = SumWeight.Replace("N", "W");

            dataSheet.Cells["Z16"].Formula = SumWeight.Replace("N", "Z");

            dataSheet.Cells["AC16"].Formula = SumWeight.Replace("N", "AC");

            dataSheet.Cells["AF16"].Formula = SumWeight.Replace("N", "AF");

            dataSheet.Cells["AH16"].Formula = STFormula1;
            dataSheet.Cells["AI16"].Formula = STFormula1.Replace("AH", "AI");
            dataSheet.Cells["AJ16"].Formula = STFormula1.Replace("AH", "AJ");
            dataSheet.Cells["AK16"].Formula = STFormula1.Replace("AH", "AK");
            dataSheet.Cells["AL16"].Formula = "=AK16-AJ16";
            dataSheet.Cells["AM16"].Formula = STFormula1.Replace("AH", "AM");


            //----- PPM
            ppmSheet.Cells["N" + (10 + ppmCount)].PutValue("4");

            ppmSheet.Cells["B" + (10 + ppmCount)].PutValue("Total : ");
            Aspose.Cells.Style style = ppmSheet.Cells["B" + (10 + ppmCount)].GetStyle();
            style.Font.IsBold = true;
            ppmSheet.Cells["B" + (10 + ppmCount)].SetStyle(style);

            ppmSheet.Cells["C" + (10 + ppmCount)].Formula = "=EMDR!N$16";

            ppmSheet.Cells["D" + (10 + ppmCount)].Formula = "=EMDR!AH$16";
            ppmSheet.Cells["E" + (10 + ppmCount)].Formula = "=EMDR!AI$16";
            ppmSheet.Cells["F" + (10 + ppmCount)].Formula = "=E" + (10 + ppmCount) + "-D" + (10 + ppmCount);

            ppmSheet.Cells["G" + (10 + ppmCount)].Formula = "=J" + (10 + ppmCount) + "-D" + (10 + ppmCount);
            ppmSheet.Cells["H" + (10 + ppmCount)].Formula = "=K" + (10 + ppmCount) + "-E" + (10 + ppmCount);
            ppmSheet.Cells["I" + (10 + ppmCount)].Formula = "=L" + (10 + ppmCount) + "-F" + (10 + ppmCount);

            ppmSheet.Cells["J" + (10 + ppmCount)].Formula = "=EMDR!AJ$16";
            ppmSheet.Cells["K" + (10 + ppmCount)].Formula = "=EMDR!AK$16";
            ppmSheet.Cells["L" + (10 + ppmCount)].Formula = "=K" + (10 + ppmCount) + "-J" + (10 + ppmCount);

            ppmSheet.Cells["M" + (10 + ppmCount)].Formula = "=EMDR!AM$16";
            //------
            dataSheet.Cells["AR15"].PutValue(rowCount);
            ppmSheet.Cells["N9"].PutValue(disciplineRowList.Count);
            var filename = "PV GAS_ EMDR Report _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";
            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();

        }

        private void DocumentIssuedThisWeek()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGAS_Documents Issued This Week.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            DateTime startAtMonday = DateTime.Now.AddDays(DayOfWeek.Monday - DateTime.Now.DayOfWeek);
            DateTime startAtSunday = startAtMonday.AddDays(6);
            var docList = this.documentPackageService.DocumentIssuedThisWeekForIn(startAtMonday, startAtSunday, projectObj.ID).ToList();
            // dataSheet.Cells["H5"].PutValue("From " + startAtMonday.ToShortDateString() + " To " + startAtSunday.ToShortDateString());
            if (this.rtvDiscipline.SelectedNode.ParentNode != null)
            {
                var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                docList = docList.Where(t => t.PackageId == category.PackageId)
                .ToList();
                dataSheet.Name = category.PackageName;
            }
            else
            {
                docList = docList = docList.Where(t => t.PackageId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).ToList();
                dataSheet.Name = this.rtvDiscipline.SelectedNode.Text;
            }
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                new DataColumn("NoIndex", typeof (String)),
                 new DataColumn("Discipline", typeof(String)),
                new DataColumn("DocNo", typeof(String)),
                 new DataColumn("DocTitle", typeof(String)),
                 new DataColumn("Rev", typeof(String)),
                 new DataColumn("StatusCM", typeof(String)),
                 new DataColumn("IssueFor", typeof(String)),
                new DataColumn("TransDate", typeof (DateTime)),
                new DataColumn("TransNo", typeof (String)) });
            var countDoc = 0;
            var groupbycategoty = docList.OrderBy(t => t.CategoryID).GroupBy(t => t.CategoryName);
            foreach (var lisdoc in groupbycategoty)
            {
                var dataRows = dtEngDoc.NewRow();
                dataRows["DocNo"] = lisdoc.Key;
                dtEngDoc.Rows.Add(dataRows);
                var loop = lisdoc.OrderBy(t => t.DocNo).ToList();
                for (int i = 0; i < loop.Count(); i++)
                {
                    countDoc += 1;
                    var dataRow = dtEngDoc.NewRow();
                    dataRow["NoIndex"] = countDoc.ToString();
                    dataRow["DocNo"] = loop[i].DocNo;
                    dataRow["DocTitle"] = loop[i].DocTitle;
                    dataRow["Rev"] = loop[i].RevisionName;
                    dataRow["Discipline"] = loop[i].DisciplineFullName.Substring(4, loop[i].DisciplineFullName.IndexOf('(') - 5);
                    dataRow["TransNo"] = loop[i].IncomingTransNo;
                    dataRow["TransDate"] = loop[i].IncomingTransDate.GetValueOrDefault();

                    var issuefor = loop[i].RevisionName == "1" ? "IFR" : (loop[i].RevisionName.Contains("AC") ? "AFC" : (loop[i].RevisionName == "2" ? "Re-IFR" : ""));
                    dataRow["IssueFor"] = issuefor;
                    if (issuefor == "Re-IFR" || issuefor == "IFR")
                    {

                        if (loop[i].DeadlineTransOut != null && loop[i].OutgoingTransDate == null && loop[i].DeadlineTransOut.GetValueOrDefault() <= DateTime.Now)
                        {
                            dataRow["StatusCM"] = "WFC" + loop[i].RevisionName;
                        }
                        else if (loop[i].DeadlineTransOut != null && loop[i].OutgoingTransDate == null && loop[i].DeadlineTransOut.GetValueOrDefault() > DateTime.Now)
                        {
                            dataRow["StatusCM"] = "Delay" + loop[i].RevisionName;

                        }
                    }
                    dtEngDoc.Rows.Add(dataRow);
                }
            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 4, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
          //  dataSheet.Cells["I7"].PutValue(docList.Count);
            var rb = dataSheet.Cells.CreateRange(4, 0, 5 + dtEngDoc.Rows.Count, dtEngDoc.Columns.Count);
            var style = workbook.CreateStyle();
            style.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.LeftBorder].Color = Color.Black;
            style.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.RightBorder].Color = Color.Black;
            style.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.TopBorder].Color = Color.Black;
            style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.BottomBorder].Color = Color.Black;
            var flag = new StyleFlag();

            flag.Borders = true;
            rb.ApplyStyle(style, flag);
            var filename = "PV GAS_ Documents Issued This Week_" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentStatus()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PV GAS_Documents Issued and commented status.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            DateTime startAtMonday = DateTime.Now.AddDays(DayOfWeek.Monday - DateTime.Now.DayOfWeek);
            DateTime startAtSunday = startAtMonday.AddDays(6);
            var docList = this.documentPackageService.GetAllByProject(projectId,false).Where(t => t.OutgoingTransId != null).OrderByDescending(t => t.CreatedDate.GetValueOrDefault()).ToList();
            var FulldocList = this.documentPackageService.GetAllByProject(projectId,true).Where(t => t.IsEMDR.GetValueOrDefault()).ToList();
            dataSheet.Cells["I5"].PutValue("Cut-off:  " + DateTime.Now.ToShortDateString());
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                new DataColumn("NoIndex", typeof (String)),
                new DataColumn("DocNo", typeof(String)),
                 new DataColumn("DocTitle", typeof(String)),
                 new DataColumn("Rev", typeof(String)),
                 new DataColumn("Dis", typeof(String)),
                 new DataColumn("CurrentCode", typeof(String)),
                  new DataColumn("IssuedFor", typeof(String)),
                new DataColumn("TransDate", typeof (String)),
                new DataColumn("Remark", typeof (String)) });
            var countDoc = 0;
            List<string> LisdocExit = new List<string>();
            for (int i = 0; i < docList.Count; i++)
            {
                countDoc += 1;
                if (!LisdocExit.Any(t => t == docList[i].DocNo))
                {
                    var dataRow = dtEngDoc.NewRow();
                    dataRow["NoIndex"] = countDoc.ToString();
                    dataRow["DocNo"] = docList[i].DocNo;
                    dataRow["DocTitle"] = docList[i].DocTitle;
                    dataRow["Rev"] = docList[i].RevisionName;
                    dataRow["Dis"] = docList[i].DisciplineName;
                    if (!string.IsNullOrEmpty(docList[i].DocReviewStatusCode))
                    {
                        dataRow["CurrentCode"] = docList[i].DocReviewStatusCode;
                    }
                    else
                    {
                        var DocAllRev = FulldocList.Where(t => t.DocNo == docList[i].DocNo && t.ID != docList[i].ID).OrderByDescending(t => t.CreatedDate).ToList();
                        if (DocAllRev.Count > 0)
                        {
                            dataRow["CurrentCode"] = DocAllRev[0].DocReviewStatusCode;
                        }
                    }
                    dataRow["IssuedFor"] = docList[i].CodeName; // this.revisionSchemaService.GetById(docList[i].RevisionSchemaId.GetValueOrDefault()).Description;
                    dataRow["TransDate"] = docList[i].OutgoingTransDate.GetValueOrDefault().ToShortDateString();
                    dataRow["Remark"] = docList[i].Remarks;
                    dtEngDoc.Rows.Add(dataRow);
                    LisdocExit.Add(docList[i].DocNo);
                }
            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 7, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, false);
            dataSheet.Cells["J7"].PutValue(docList.Count);
            var rb = dataSheet.Cells.CreateRange(7, 0, 8 + dtEngDoc.Rows.Count, dtEngDoc.Columns.Count);
            var style = workbook.CreateStyle();
            style.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.LeftBorder].Color = Color.Black;
            style.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.RightBorder].Color = Color.Black;
            style.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.TopBorder].Color = Color.Black;
            style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.BottomBorder].Color = Color.Black;
            var flag = new StyleFlag();

            flag.Borders = true;
            rb.ApplyStyle(style, flag);
            var filename = "PV GAS_Documents Issued and commented status_" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentLateISsued()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PV GAS_Documents be late Issued.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);

            var docList = this.documentPackageService.GetAllByProject(projectObj.ID,false).Where(t => t.IsEMDR.GetValueOrDefault() && t.RevisionName=="1"
            && (t.IFRActual == null && t.IFRPlan != null && t.IFRPlan.GetValueOrDefault().Date < DateTime.Now.Date)).ToList();
            if (this.rtvDiscipline.SelectedNode.ParentNode != null)
            {
                var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                docList = docList.Where(t => t.PackageId == category.PackageId)
                .ToList();
                dataSheet.Name = category.PackageName;
            }
            else
            {
                docList = docList = docList.Where(t => t.PackageId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).ToList();
                dataSheet.Name = this.rtvDiscipline.SelectedNode.Text;
            }
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                new DataColumn("NoIndex", typeof (String)),
                new DataColumn("Dis", typeof(String)),
                new DataColumn("DocNo", typeof(String)),
                 new DataColumn("DocTitle", typeof(String)),
                 new DataColumn("IssuedPlan", typeof(DateTime)) }
               );
            var countDoc = 0;

            var groupbycategoty = docList.OrderBy(t => t.CategoryID).GroupBy(t => t.CategoryName);
            foreach (var lisdoc in groupbycategoty)
            {
                var dataRows = dtEngDoc.NewRow();
                dataRows["DocNo"] = lisdoc.Key;
                dtEngDoc.Rows.Add(dataRows);
                var loop = lisdoc.OrderBy(t => t.DocNo).ToList();
                for (int i = 0; i < loop.Count(); i++)
                {
                    countDoc += 1;

                    var dataRow = dtEngDoc.NewRow();
                    dataRow["NoIndex"] = countDoc.ToString();
                    dataRow["DocNo"] = loop[i].DocNo;
                    dataRow["DocTitle"] = loop[i].DocTitle;

                    dataRow["Dis"] = loop[i].DisciplineFullName.Substring(4, loop[i].DisciplineFullName.IndexOf('(') - 5);

                    if(loop[i].IFRPlan!= null) dataRow["IssuedPlan"] = loop[i].IFRPlan.GetValueOrDefault();

                    dtEngDoc.Rows.Add(dataRow);

                }
            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 4, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, false);
            var rb = dataSheet.Cells.CreateRange(4, 0, 5 + dtEngDoc.Rows.Count, dtEngDoc.Columns.Count);
            var style = workbook.CreateStyle();
            style.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.LeftBorder].Color = Color.Black;
            style.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.RightBorder].Color = Color.Black;
            style.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.TopBorder].Color = Color.Black;
            style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.BottomBorder].Color = Color.Black;
            var flag = new StyleFlag();

            flag.Borders = true;
            rb.ApplyStyle(style, flag);
            var filename = "PVGAS_Documents Late Issued_" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentLateCommented()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PV GAS_Documents be late commented.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            // DateTime startAtMonday = DateTime.Now.AddDays(DayOfWeek.Monday - DateTime.Now.DayOfWeek);
            // DateTime startAtSunday = startAtMonday.AddDays(6);
            var docList = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
             && t.IncomingTransId != null && t.DeadlineTransOut != null
             && !t.RevisionName.Contains("AC")
             && t.DeadlineTransOut.GetValueOrDefault().Date < DateTime.Now.Date
             && t.OutgoingTransDate == null).OrderBy(t => t.CategoryID.GetValueOrDefault()).ToList();
            // dataSheet.Cells["K5"].PutValue("Cut-off:  " + DateTime.Now.ToShortDateString());
            if (this.rtvDiscipline.SelectedNode.ParentNode != null)
            {
                var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                docList = docList.Where(t => t.PackageId == category.PackageId)
                // .Where(t => t.DisciplineId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value))
                .ToList();
                dataSheet.Name = category.PackageName;
            }
            else
            {
                docList = docList = docList.Where(t => t.PackageId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).ToList();
                dataSheet.Name = this.rtvDiscipline.SelectedNode.Text;
            }
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                new DataColumn("NoIndex", typeof (String)),
                  new DataColumn("Dis", typeof(String)),
                new DataColumn("DocNo", typeof(String)),
                 new DataColumn("DocTitle", typeof(String)),
                  new DataColumn("Rev", typeof(String)),
                 new DataColumn("IssuedFor", typeof(String)),
                new DataColumn("Date", typeof (DateTime)),
                 new DataColumn("TransIssue", typeof (String)),
                  });
            var countDoc = 0;
            List<string> LisdocExit = new List<string>();
            var groupbycategoty = docList.OrderBy(t=> t.CategoryID).GroupBy(t => t.CategoryName);
            foreach (var lisdoc in groupbycategoty) {
                var dataRows = dtEngDoc.NewRow();
                dataRows["DocNo"] = lisdoc.Key;
                dtEngDoc.Rows.Add(dataRows);
                var loop = lisdoc.OrderBy(t=> t.DocNo).ToList();
                for (int i = 0; i < loop.Count(); i++)
            {
                countDoc += 1;
                
                    var dataRow = dtEngDoc.NewRow();
                    dataRow["NoIndex"] = countDoc.ToString();
                    dataRow["DocNo"] = loop[i].DocNo;
                    dataRow["DocTitle"] = loop[i].DocTitle;
                    dataRow["Rev"] = loop[i].RevisionName;
                    dataRow["Dis"] = loop[i].DisciplineFullName.Substring(4, loop[i].DisciplineFullName.IndexOf('(') - 5);
                    dataRow["TransIssue"] = loop[i].IncomingTransNo;
                   if(loop[i].IncomingTransDate!= null) dataRow["Date"] = loop[i].IncomingTransDate.GetValueOrDefault();
                    var issuefor = loop[i].RevisionName == "1" ? "IFR" : (loop[i].RevisionName.Contains("AC") ?"":"Re-IFR" );
                    dataRow["IssuedFor"] = issuefor;
                    dtEngDoc.Rows.Add(dataRow);
                
            } }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 4, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, false);

            var rb = dataSheet.Cells.CreateRange(4, 0, 5 + dtEngDoc.Rows.Count, dtEngDoc.Columns.Count);
            var style = workbook.CreateStyle();
            style.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.LeftBorder].Color = Color.Black;
            style.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.RightBorder].Color = Color.Black;
            style.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.TopBorder].Color = Color.Black;
            style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.BottomBorder].Color = Color.Black;
            var flag = new StyleFlag();

            flag.Borders = true;
            rb.ApplyStyle(style, flag);
            var filename = "PVGAS_Documents be late commented_" + DateTime.Now.ToString("ddMMyyyy") + ".xlsx";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void StatusDocumentSendToNCSP()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PV GAS_Documents_Infor_ send to NCSP OWNER.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);

            var docList = this.documentPackageService.GetAllByProject(projectObj.ID, false).Where(t => t.IsEMDR.GetValueOrDefault() 
            && !string.IsNullOrEmpty(t.NCS_markup)
             && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")).ToList();
            if (this.rtvDiscipline.SelectedNode.ParentNode != null)
            {
                var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                docList = docList.Where(t => t.PackageId == category.PackageId)
                .ToList();
                dataSheet.Name = category.PackageName;
            }
            else
            {
                docList = docList = docList.Where(t => t.PackageId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).ToList();
                dataSheet.Name = this.rtvDiscipline.SelectedNode.Text;
            }
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                new DataColumn("DocNo", typeof(String)),
                new DataColumn("DocTitle", typeof(String)),
                new DataColumn("Dis", typeof(String)),
               new DataColumn("Start", typeof(DateTime)),
               new DataColumn("IDC", typeof(DateTime)),
               new DataColumn("IFR", typeof(DateTime)),
               new DataColumn("Code2", typeof(DateTime)),
               new DataColumn("Code1", typeof(DateTime)),
                 new DataColumn("AFC", typeof(DateTime)), }
               );
            var countDoc = 0;

            var groupbycategoty = docList.OrderBy(t => t.CategoryID).GroupBy(t => t.CategoryName);
            foreach (var lisdoc in groupbycategoty)
            {
                var dataRows = dtEngDoc.NewRow();
                dataRows["DocNo"] = lisdoc.Key;
                dtEngDoc.Rows.Add(dataRows);
                var loop = lisdoc.OrderBy(t => t.DocNo).ToList();
                for (int i = 0; i < loop.Count(); i++)
                {
                    countDoc += 1;

                    var dataRow = dtEngDoc.NewRow();
                  //  dataRow["NoIndex"] = countDoc.ToString();
                    dataRow["DocNo"] = loop[i].DocNo;
                    dataRow["DocTitle"] = loop[i].DocTitle;

                    dataRow["Dis"] = loop[i].DisciplineFullName.Substring(4, loop[i].DisciplineFullName.IndexOf('(') - 5);

                    if(loop[i].StartActual != null) dataRow["Start"] = loop[i].StartActual.GetValueOrDefault();
                    if(loop[i].IDCACtual != null) dataRow["IDC"] = loop[i].IDCACtual.GetValueOrDefault();
                    if(loop[i].IFRActual != null) dataRow["IFR"] = loop[i].IFRActual.GetValueOrDefault();
                    if(loop[i].ReIFRActual != null) dataRow["Code2"] = loop[i].ReIFRActual.GetValueOrDefault();
                    if(loop[i].IFAActual != null) dataRow["Code1"] = loop[i].IFAActual.GetValueOrDefault();
                    if (loop[i].AFCActual != null) dataRow["AFC"] = loop[i].AFCActual.GetValueOrDefault();

                    dtEngDoc.Rows.Add(dataRow);

                }
            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 5, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, false);
            var rb = dataSheet.Cells.CreateRange(5, 0, 6 + dtEngDoc.Rows.Count, dtEngDoc.Columns.Count);
            var style = workbook.CreateStyle();
            style.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.LeftBorder].Color = Color.Black;
            style.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.RightBorder].Color = Color.Black;
            style.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.TopBorder].Color = Color.Black;
            style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.BottomBorder].Color = Color.Black;
            var flag = new StyleFlag();

            flag.Borders = true;
            rb.ApplyStyle(style, flag);
            var filename = "PVGAS_Documents Status Send To NCSP Owner_" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentLateResponse()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PV GAS_Documents be late Response.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            DateTime startAtMonday = DateTime.Now.AddDays(DayOfWeek.Monday - DateTime.Now.DayOfWeek);
            DateTime startAtSunday = startAtMonday.AddDays(6);
            var docList = this.documentPackageService.GetAllByProject(projectObj.ID,false).Where(t => t.IsEMDR.GetValueOrDefault()
            && t.DeadlineClientResponse != null
            && t.DeadlineTransOut.GetValueOrDefault().Date < DateTime.Now.Date).OrderByDescending(t => t.CreatedDate.GetValueOrDefault()).ToList();
            //var FulldocList = this.documentPackageService.GetAllByProject(projectObj.ID, true).Where(t => t.IsEMDR.GetValueOrDefault()).ToList();
            dataSheet.Cells["K5"].PutValue("Cut-off:  " + DateTime.Now.ToShortDateString());
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                new DataColumn("NoIndex", typeof (String)),
                new DataColumn("DocNo", typeof(String)),
                 new DataColumn("DocTitle", typeof(String)),
                 new DataColumn("Rev", typeof(String)),
                 new DataColumn("Dis", typeof(String)),
                 new DataColumn("TransIn", typeof(String)),
                  new DataColumn("DateReceived", typeof(String)),
                new DataColumn("Duedate", typeof (String)),
                 new DataColumn("Overdue", typeof (String)),
                new DataColumn("Remark", typeof (String)) });
            var countDoc = 0;
            List<string> LisdocExit = new List<string>();
            for (int i = 0; i < docList.Count; i++)
            {
                countDoc += 1;
                if (!LisdocExit.Any(t => t == docList[i].DocNo))
                {
                    var dataRow = dtEngDoc.NewRow();
                    dataRow["NoIndex"] = countDoc.ToString();
                    dataRow["DocNo"] = docList[i].DocNo;
                    dataRow["DocTitle"] = docList[i].DocTitle;
                    dataRow["Rev"] = docList[i].RevisionName;
                    dataRow["Dis"] = docList[i].DisciplineName;
                    dataRow["Duedate"] = docList[i].DeadlineClientResponse.GetValueOrDefault().ToShortDateString();
                    dataRow["OverDue"] = (DateTime.Now - docList[i].DeadlineClientResponse.GetValueOrDefault()).Days;
                    if (docList[i].OutgoingTransId != null)
                    {
                        dataRow["TransIn"] = docList[i].OutgoingTransNo;
                        dataRow["DateReceived"] = docList[i].OutgoingTransDate.GetValueOrDefault().ToShortDateString();
                    }
                    //else
                    //{
                    //    var DocAllRev = FulldocList.Where(t => t.DocNo == docList[i].DocNo && t.ID != docList[i].ID).OrderByDescending(t => t.CreatedDate).ToList();
                    //    if (DocAllRev.Count > 0)
                    //    {
                    //        dataRow["TransIn"] = DocAllRev[0].IncomingTransNo;
                    //        dataRow["DateReceived"] = DocAllRev[0].IncomingTransDate.GetValueOrDefault().ToShortDateString();
                    //    }
                    //    //dataRow["OverDue"] = (DateTime.Now - docList[i].DueDateReceive.GetValueOrDefault()).Days;
                    //}

                    dataRow["Remark"] = docList[i].Remarks;
                    LisdocExit.Add(docList[i].DocNo);
                    dtEngDoc.Rows.Add(dataRow);
                }
            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 7, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, false);
            dataSheet.Cells["L7"].PutValue(docList.Count);

            var rb = dataSheet.Cells.CreateRange(7, 0, 8 + docList.Count, 10);
            var style = workbook.CreateStyle();
            style.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.LeftBorder].Color = Color.Black;
            style.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.RightBorder].Color = Color.Black;
            style.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.TopBorder].Color = Color.Black;
            style.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;
            style.Borders[BorderType.BottomBorder].Color = Color.Black;
            var flag = new StyleFlag();

            flag.Borders = true;
            rb.ApplyStyle(style, flag);
            var filename = "PVGAS_Documents be late Response_" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentSendKDNOperator()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGASTransmittalTemplate_KDN_Operator.xlsm");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            var docList = this.documentPackageService.GetAllByProject(projectObj.ID, true).Where(t => t.IsEMDR.GetValueOrDefault()
             && !string.IsNullOrEmpty(t.KDN_markup)
             && (t.KDN_markup== "I" || t.KDN_markup == "R"|| t.KDN_markup == "A")
             && (t.DNB_FinalCode== "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
             && ((t.DNV_markup=="R" && t.DNV_FinalCode=="C")|| (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
             && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderByDescending(t => t.CreatedDate.GetValueOrDefault()).ToList();
         
            dataSheet.Cells["C5"].PutValue(DateTime.Now.Date);
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                     new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String))});
            var countDoc = 0;

            foreach (var docobj in docList)
            {
                countDoc += 1;
              
                    var dataRow = dtEngDoc.NewRow();
                    dataRow["Index"] = countDoc;
                    dataRow["DocTitle"] = docobj.DocTitle;
                    dataRow["DocNo"] = docobj.DocNo;
                    dataRow["RevisionName"] = docobj.RevisionName;
                  
                   
                    dtEngDoc.Rows.Add(dataRow);
                
            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 20, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
            for (int i = 0; i < dtEngDoc.Rows.Count; i++)
            {
                dataSheet.Cells.Merge(20 + i, 1, 1, 2);
                dataSheet.Cells.Merge(20 + i, 3, 1, 2);
             
            }
            var filename = "PVGAS_Documents be Send To KDN _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }

        private void DocumentSendNCSROSNEFTPERENCO()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGASTransmittalTemplate_NCS_Operator.xlsm");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);
            var docList = this.documentPackageService.GetAllByProject(projectObj.ID, true).Where(t => t.IsEMDR.GetValueOrDefault()
             && !string.IsNullOrEmpty(t.NCS_markup)
             && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")
             && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
             && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
              && string.IsNullOrEmpty(t.NCS_Operator_OutgoingTransNo)).OrderByDescending(t => t.CreatedDate.GetValueOrDefault()).ToList();

            dataSheet.Cells["C5"].PutValue(DateTime.Now.Date);
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                     new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String))});
            var countDoc = 0;

            foreach (var docobj in docList)
            {
                countDoc += 1;

                var dataRow = dtEngDoc.NewRow();
                dataRow["Index"] = countDoc;
                dataRow["DocTitle"] = docobj.DocTitle;
                dataRow["DocNo"] = docobj.DocNo;
                dataRow["RevisionName"] = docobj.RevisionName;


                dtEngDoc.Rows.Add(dataRow);

            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 20, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
            for (int i = 0; i < dtEngDoc.Rows.Count; i++)
            {
                dataSheet.Cells.Merge(20 + i, 1, 1, 2);
                dataSheet.Cells.Merge(20 + i, 3, 1, 2);

            }
            var filename = "PVGAS_Documents be Send To NCS ROSNEFT PERENCO _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentSubmitStatus()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PV GAS_Documents Submit Status.xls");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            var projectId = this.ddlProject.SelectedItem != null ? Convert.ToInt32(this.ddlProject.SelectedValue) : 0;
            var projectObj = this.scopeProjectService.GetById(projectId);

            var docList = this.documentPackageService.GetAllByProject(projectObj.ID, false).Where(t => t.IsEMDR.GetValueOrDefault()).ToList();
            if (this.rtvDiscipline.SelectedNode.ParentNode != null)
            {
                var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                docList = docList.Where(t => t.PackageId == category.PackageId)
                .ToList();
                dataSheet.Name = category.PackageName;
            }
            else
            {
                docList = docList = docList.Where(t => t.PackageId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).ToList();
                dataSheet.Name = this.rtvDiscipline.SelectedNode.Text;
            }
            var groupbycategoty = docList.OrderBy(t => t.CategoryID).GroupBy(t => t.CategoryName);
            DateTime startAtMonday = DateTime.Now.AddDays(DayOfWeek.Monday - DateTime.Now.DayOfWeek);
            DateTime startAtSunday = startAtMonday.AddDays(6);
            DateTime YeMonday = startAtMonday.AddDays(-7);
            DateTime YeSunday = startAtSunday.AddDays(-7);
            DateTime today = DateTime.Now;
            int rows = 6;
            int rowsDNV = 13;
            dataSheet.Cells["K4"].PutValue("TÍCH LŨY ĐẾN " + DateTime.Now.ToString("dd/MM/yyyy"));
            foreach (var lisdoc in groupbycategoty)
            {
                rows += 1;
               // var loop = lisdoc.ToList();
                dataSheet.Cells["A"+rows].PutValue(lisdoc.Key);
                dataSheet.Cells["B" + rows].PutValue(lisdoc.Count());
                dataSheet.Cells["C" + rows].PutValue(lisdoc.Count(t=> t.IFRActual != null && t.IFRActual.GetValueOrDefault().Date >= YeMonday.Date && t.IFRActual.GetValueOrDefault().Date <= YeSunday.Date));
                dataSheet.Cells["D" + rows].PutValue(lisdoc.Count(t => t.AFCActual != null && t.AFCActual.GetValueOrDefault().Date >= YeMonday.Date && t.AFCActual.GetValueOrDefault().Date <= YeSunday.Date));
                dataSheet.Cells["E" + rows].PutValue(lisdoc.Count(t => t.IFRPlan != null && t.IFRPlan.GetValueOrDefault().Date >= startAtMonday.Date && t.IFRPlan.GetValueOrDefault().Date <= startAtSunday.Date));
                dataSheet.Cells["F" + rows].PutValue(lisdoc.Count(t => t.IFRActual != null && t.IFRActual.GetValueOrDefault().Date >= startAtMonday.Date && t.IFRActual.GetValueOrDefault().Date <= startAtSunday.Date));
                dataSheet.Cells["G" + rows].Formula = "=F" + rows + "- E" + rows;
                dataSheet.Cells["H" + rows].PutValue(lisdoc.Count(t => t.AFCPlan != null && t.AFCPlan.GetValueOrDefault().Date >= startAtMonday.Date && t.AFCPlan.GetValueOrDefault().Date <= startAtSunday.Date));
                dataSheet.Cells["I" + rows].PutValue(lisdoc.Count(t => t.AFCActual != null && t.AFCActual.GetValueOrDefault().Date >= startAtMonday.Date && t.AFCActual.GetValueOrDefault().Date <= startAtSunday.Date));
                dataSheet.Cells["J" + rows].Formula = "=I" + rows + "- H" + rows;
                //den ngay
                dataSheet.Cells["K" + rows].PutValue(lisdoc.Count(t => t.IFRPlan != null && t.IFRPlan.GetValueOrDefault().Date <= today.Date));
                dataSheet.Cells["L" + rows].PutValue(lisdoc.Count(t => t.IFRActual != null  && t.IFRActual.GetValueOrDefault().Date <= today.Date));
                dataSheet.Cells["M" + rows].Formula = "=L" + rows + "- K" + rows;
                dataSheet.Cells["N" + rows].PutValue(lisdoc.Count(t => t.AFCPlan != null && t.AFCPlan.GetValueOrDefault().Date <= today.Date));
                dataSheet.Cells["O" + rows].PutValue(lisdoc.Count(t => t.AFCActual != null && t.AFCActual.GetValueOrDefault().Date <= today.Date));
                dataSheet.Cells["P" + rows].Formula = "=O" + rows + "- N" + rows;

                var docsDNV = lisdoc.Where(t=>!string.IsNullOrEmpty(t.DNV_markup)).ToList();
                dataSheet.Cells["A" + rowsDNV].PutValue(lisdoc.Key);
                var sum = docsDNV.Count();
                dataSheet.Cells["B" + rowsDNV].PutValue(sum);
                var IFRYeSunday = docsDNV.Count(t => t.RevisionName=="1" && t.IFRActual != null && t.IFAActual.GetValueOrDefault().Date <= YeSunday.Date && t.OutgoingTransDate_CA != null);
                var AFCYeSunday = docsDNV.Count(t => t.RevisionName.Contains("AC") && t.AFCActual != null && t.AFCActual.GetValueOrDefault().Date <= YeSunday.Date && t.OutgoingTransDate_CA != null);
               dataSheet.Cells["C" + rowsDNV].PutValue(IFRYeSunday);
                dataSheet.Cells["D" + rowsDNV].PutValue(AFCYeSunday);
                dataSheet.Cells["E" + rowsDNV].PutValue(sum- (IFRYeSunday+ AFCYeSunday));
                dataSheet.Cells["F" + rowsDNV].PutValue(docsDNV.Count(t => !t.RevisionName.Contains("AC") && t.OutgoingTransDate_CA != null && t.OutgoingTransDate_CA.GetValueOrDefault().Date >= startAtMonday.Date && t.OutgoingTransDate_CA.GetValueOrDefault().Date <= startAtSunday.Date));
                dataSheet.Cells["G" + rowsDNV].PutValue(docsDNV.Count(t => t.RevisionName.Contains("AC") && t.OutgoingTransDate_CA != null && t.OutgoingTransDate_CA.GetValueOrDefault().Date >= startAtMonday.Date && t.OutgoingTransDate_CA.GetValueOrDefault().Date <= startAtSunday.Date));

                var IFRSunday = docsDNV.Count(t => !t.RevisionName.Contains("AC") && t.IFRActual != null && t.IFAActual.GetValueOrDefault().Date <= startAtSunday.Date && t.OutgoingTransDate_CA != null);
                var AFCSunday = docsDNV.Count(t => t.RevisionName.Contains("AC") && t.AFCActual != null && t.AFCActual.GetValueOrDefault().Date <= startAtSunday.Date && t.OutgoingTransDate_CA != null);
                dataSheet.Cells["H" + rowsDNV].PutValue(IFRSunday);
                dataSheet.Cells["I" + rowsDNV].PutValue(AFCSunday);
                dataSheet.Cells["J" + rowsDNV].PutValue(sum - (IFRSunday + AFCSunday));
                dataSheet.Cells["K" + rowsDNV].PutValue(docsDNV.Count(t =>t.DNV_FinalCode=="C"));
                dataSheet.Cells["L" + rowsDNV].PutValue(docsDNV.Count(t => t.DNV_FinalCode == "CI"));
                dataSheet.Cells["M" + rowsDNV].PutValue(docsDNV.Count(t => t.DNV_FinalCode == "O"));
                dataSheet.Cells.InsertRow(rows);
                rowsDNV += 2;
                dataSheet.Cells.InsertRow(rowsDNV);
            }
            rows += 1;
            dataSheet.Cells["A" + rows].PutValue("Tổng Cộng");
           dataSheet.Cells["B" + rows].Formula = "=Sum(B7:B"+(rows-1)+")";
            dataSheet.Cells["C" + rows].Formula = "=Sum(C7:C" + (rows - 1) + ")";
            dataSheet.Cells["D" + rows].Formula = "=Sum(D7:D" + (rows - 1) + ")";
            dataSheet.Cells["E" + rows].Formula = "=Sum(E7:E" + (rows - 1) + ")";
            dataSheet.Cells["F" + rows].Formula = "=Sum(F7:F" + (rows - 1) + ")";
            dataSheet.Cells["G" + rows].Formula = "=Sum(G7:G" + (rows - 1) + ")";
            dataSheet.Cells["H" + rows].Formula = "=Sum(H7:H" + (rows - 1) + ")";
            dataSheet.Cells["I" + rows].Formula = "=Sum(I7:I" + (rows - 1) + ")";
            dataSheet.Cells["J" + rows].Formula = "=Sum(J7:J" + (rows - 1) + ")";
            //den ngay
            dataSheet.Cells["K" + rows].Formula = "=Sum(K7:K" + (rows - 1) + ")";
            dataSheet.Cells["L" + rows].Formula = "=Sum(L7:L" + (rows - 1) + ")";
            dataSheet.Cells["M" + rows].Formula = "=Sum(M7:M" + (rows - 1) + ")";
            dataSheet.Cells["N" + rows].Formula = "=Sum(N7:N" + (rows - 1) + ")";
            dataSheet.Cells["O" + rows].Formula = "=Sum(O7:O" + (rows - 1) + ")";
            dataSheet.Cells["P" + rows].Formula = "=Sum(P7:P" + (rows - 1) + ")";
            //var rbBuld = dataSheet.Cells.CreateRange(rows, 0, 1, 15);

            dataSheet.Cells["A" + rowsDNV].PutValue("Tổng Cộng");
            dataSheet.Cells["B" + rowsDNV].Formula = "=Sum(B"+(rows+6) +":B" + (rowsDNV - 1) + ")";
            dataSheet.Cells["C" + rowsDNV].Formula = "=Sum(C"+(rows+6) +":C" + (rowsDNV - 1) + ")";
            dataSheet.Cells["D" + rowsDNV].Formula = "=Sum(D"+(rows+6) +":D" + (rowsDNV - 1) + ")";
            dataSheet.Cells["E" + rowsDNV].Formula = "=Sum(E"+(rows+6) +":E" + (rowsDNV - 1) + ")";
            dataSheet.Cells["F" + rowsDNV].Formula = "=Sum(F"+(rows+6) +":F" + (rowsDNV - 1) + ")";
            dataSheet.Cells["G" + rowsDNV].Formula = "=Sum(G"+(rows+6) +":G" + (rowsDNV - 1) + ")";
            dataSheet.Cells["H" + rowsDNV].Formula = "=Sum(H"+(rows+6) +":H" + (rowsDNV - 1) + ")";
            dataSheet.Cells["I" + rowsDNV].Formula = "=Sum(I"+(rows+6) +":I" + (rowsDNV - 1) + ")";
            dataSheet.Cells["J" + rowsDNV].Formula = "=Sum(J"+(rows+6) +":J" + (rowsDNV - 1) + ")";
            dataSheet.Cells["K" + rowsDNV].Formula = "=Sum(K"+(rows+6) +":K" + (rowsDNV - 1) + ")";
            dataSheet.Cells["L" + rowsDNV].Formula = "=Sum(L"+(rows+6) +":L" + (rowsDNV - 1) + ")";
            dataSheet.Cells["M" + rowsDNV].Formula = "=Sum(M" + (rows + 6) + ":M" + (rowsDNV - 1) + ")";
           
            var filename = "PVGAS_Documents Submit Status_" + DateTime.Now.ToString("ddMMyyyy") + ".xls";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
		{
            if (e.Argument == "Rebind")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ExportScrve") {
                //var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Exports") + @"\";

                //this.Download_File(filePath + @"EMDR2 PVGAS_DEMO.xlsm");
                //string filename = "PVGAS_Report EMDR_" + DateTime.Today.ToBinary() + ".xlsm";
                this.ExportEMDRReport();
            }
            else if (e.Argument == "DocumentIssuedThisWeek")
            {
                this.DocumentIssuedThisWeek();
            }
            else if (e.Argument == "ExportEMDRReport_New")
            {
                this.ExportCMDRDataFile();
            }
            else if (e.Argument == "DocumentStatus")
            {
                this.DocumentStatus();
            }
            else if (e.Argument == "LateIssued")
            {
                this.DocumentLateISsued();
            }
            else if(e.Argument== "StatusSubmitDocuments")
            {
                this.DocumentSubmitStatus();
            }
            else if (e.Argument == "LateCommented")
            {
                this.DocumentLateCommented();
            }
            else if (e.Argument == "LateResponse")
            {
                this.DocumentLateResponse();
            }
            else if(e.Argument== "StatusDocumentNCSP")
            {
                this.StatusDocumentSendToNCSP();
            }
            else if (e.Argument == "SendKDNOperator")
            {
                this.DocumentSendKDNOperator();
            }
            else if(e.Argument== "SendNCSROSNEFTPERENCO")
            {
                this.DocumentSendNCSROSNEFTPERENCO();
            }
            else if (e.Argument == "DeleteAllDoc")
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = new Guid(selectedItem.GetDataKeyValue("ID").ToString());
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        if (docObj.ParentId == null)
                        {
                            docObj.IsDelete = true;
                            this.documentPackageService.Update(docObj);
                        }
                        else
                        {
                            var listRelateDoc =
                                this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                            if (listRelateDoc != null)
                            {
                                foreach (var objDoc in listRelateDoc)
                                {
                                    objDoc.IsDelete = true;
                                    this.documentPackageService.Update(objDoc);
                                }
                            }
                        }
                    }
                }

                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ClearEMDRData")
            {
                var listDocPack = this.documentPackageService.GetAll();
                foreach (var documentPackage in listDocPack)
                {
                    this.documentPackageService.Delete(documentPackage);
                }

                var attachFilePackage = this.attachFilesPackageService.GetAll();
                foreach (var attachFilesPackage in attachFilePackage)
                {
                    var filePath = Server.MapPath(attachFilesPackage.FilePath);
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }

                    this.attachFilesPackageService.Delete(attachFilesPackage);
                }

                this.grdDocument.Rebind();
            }
            else if (e.Argument == "ExportMasterList")
            {
                var projectId = !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                    ? Convert.ToInt32(this.ddlProject.SelectedValue)
                    : 0;
                var projectObj = this.scopeProjectService.GetById(projectId);

                var listDisciplineInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
                    ? this.disciplineService.GetAllDisciplineOfProject(projectId).OrderBy(t => t.Name).ToList()
                    : this.disciplineService.GetAllDisciplineInPermission(UserSession.Current.User.Id, projectId).OrderBy(t => t.Name).ToList();

                var filePath = Server.MapPath("Exports") + @"\";
                if (listDisciplineInPermission.Count > 0)
                {
                    var workbook = new Workbook();
                    workbook.Open(filePath + @"Template\NPK_MasterListTemplate.xlsm");
                    var revisionList = this.revisionService.GetAllByProject(projectId);
                    //var docNumneringList = this.documentNumberingService.GetAllByProject(projectId).OrderBy(t => t.Name).ToList();
                    var originatorList = this.originatorService.GetAllByProject(projectId).OrderBy(t => t.Name).ToList();
                    var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId).OrderBy(t => t.Name).ToList();
                    //var docTypeList = this.documentTypeService.GetAll().OrderBy(t => t.Name).ToList();

                    var sheets = workbook.Worksheets;
                    var tempDataSheet = sheets[0];
                    var rangeRevisionList = tempDataSheet.Cells.CreateRange("A1", "A" + (revisionList.Count == 0 ? 1 : revisionList.Count));
                    //var rangeDocNumberingList = tempDataSheet.Cells.CreateRange("B1", "B" + (docNumneringList.Count == 0 ? 1 : docNumneringList.Count));
                    var rangeProject = tempDataSheet.Cells.CreateRange("C1", "C1");

                    var rangeOriginatorList = tempDataSheet.Cells.CreateRange("D1", "D" + (originatorList.Count == 0 ? 1 : originatorList.Count));
                    var rangeDisciplineList = tempDataSheet.Cells.CreateRange("E1", "E" + (disciplineList.Count == 0 ? 1 : disciplineList.Count));
                    //var rangeDocTypeList = tempDataSheet.Cells.CreateRange("F1", "F" + (docTypeList.Count == 0 ? 1 : docTypeList.Count));



                    rangeRevisionList.Name = "RevisionList";
                    rangeDisciplineList.Name = "DisciplineList";
                    rangeProject.Name = "ProjectCode";
                    rangeOriginatorList.Name = "OriginatorList";


                    for (int j = 0; j < revisionList.Count; j++)
                    {
                        rangeRevisionList[j, 0].PutValue(revisionList[j].Name);
                    }

                    rangeProject[0, 0].PutValue(projectObj.Name);


                    for (int j = 0; j < originatorList.Count; j++)
                    {
                        rangeOriginatorList[j, 0].PutValue(originatorList[j].Name);
                    }

                    for (int j = 0; j < disciplineList.Count; j++)
                    {
                        rangeDisciplineList[j, 0].PutValue(disciplineList[j].Name);
                    }


                    //for (int i = 0; i < listDisciplineInPermission.Count; i++)
                    //{
                        sheets.AddCopy("Sheet1");
                    sheets[2].Name = "EMDR";// listDisciplineInPermission[0].Name;
                        sheets[ 2].Cells["A1"].PutValue(this.ddlProject.SelectedValue);
                        sheets[ 2].Cells["D1"].PutValue(this.ddlProject.SelectedItem.Text);
                      //  sheets[ 2].Cells["D3"].PutValue("Discipline: " + listDisciplineInPermission[0].FullName);
                        sheets[ 2].Cells["A2"].PutValue(listDisciplineInPermission[0].ID);

                        var validations = sheets[2].Validations;
                        this.CreateValidation(rangeRevisionList.Name, validations, 5, 3000, 4, 4);
                        //this.CreateValidation(rangeDocNumberingList.Name, validations, 5, 3000, 5, 5);
                        //this.CreateValidation(rangeProject.Name, validations, 5, 3000, 6, 6);
                        //this.CreateValidation(rangeDisciplineList.Name, validations, 5, 3000, 5, 5);
                        //this.CreateValidation(rangeDocTypeList.Name, validations, 5, 3000, 9, 9);
                        //this.CreateValidation(rangeOriginatorList.Name, validations, 5, 3000, 6, 6);
                  //  }

                    workbook.Worksheets.RemoveAt(1);
                    workbook.Worksheets[0].IsVisible = false;

                    var filename = this.ddlProject.SelectedItem.Text.Split(',')[0] + "$" + "MasterListTemplate.xls";
                    workbook.Save(filePath + filename);
                    this.Download_File(filePath + filename);
                }
            }
            else if (e.Argument == "ExportEMDRReport")
            {
                var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                var docList = new List<DocumentPackage>();
                var projectName = string.Empty;
                var dtFull = new DataTable();
                var projectID = Convert.ToInt32(this.ddlProject.SelectedValue);
                var contractorList = this.contractorService.GetAllByProject(projectID);
                dtFull.Columns.AddRange(new[]
                {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Department", typeof (String)),
                    new DataColumn("Start", typeof (String)),
                    new DataColumn("Planned", typeof (String)),
                    new DataColumn("RevName", typeof (String)),
                    new DataColumn("RevPlanned", typeof (String)),
                    new DataColumn("RevActual", typeof (String)),
                    new DataColumn("RevCommentCode", typeof (String)),
                    new DataColumn("Complete", typeof (Double)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("OutgoingNo", typeof (String)),
                    new DataColumn("OutgoingDate", typeof (String)),
                    new DataColumn("IncomingNo", typeof (String)),
                    new DataColumn("IncomingDate", typeof (String)),
                    new DataColumn("ICANo", typeof (String)),
                    new DataColumn("ICADate", typeof (String)),
                    new DataColumn("ICAReviewCode", typeof (String)),
                    new DataColumn("Notes", typeof (String)),
                    new DataColumn("IsEMDR", typeof (String)),
                    new DataColumn("HasAttachFile", typeof (String)),
                });

                foreach (var contractor in contractorList)
                {

                }

                var dtDiscipline = new DataTable();
                dtDiscipline.Columns.AddRange(new[]
                {
                    new DataColumn("DocId", typeof (String)),
                    new DataColumn("NoIndex", typeof (String)),
                    new DataColumn("DocNo", typeof (String)),
                    new DataColumn("DocTitle", typeof (String)),
                    new DataColumn("Start", typeof (DateTime)),
                    new DataColumn("RevName", typeof (String)),
                    new DataColumn("RevPlanned", typeof (DateTime)),
                    new DataColumn("RevActual", typeof (DateTime)),
                    new DataColumn("Complete", typeof (Double)),
                    new DataColumn("Weight", typeof (Double)),
                    new DataColumn("Department", typeof (String)),
                    new DataColumn("Notes", typeof (String)),
                    new DataColumn("IsEMDR", typeof (String)),
                    new DataColumn("HasAttachFile", typeof (String)),
                    new DataColumn("OutgoingNo", typeof (String)),
                    new DataColumn("OutgoingDate", typeof (String)),
                });
                projectName = this.ddlProject.SelectedItem.Text;

                if (this.rtvDiscipline.SelectedNode != null)
                {

                    var templateManagement = this.templateManagementService.GetSpecial(1, projectID);
                    if (templateManagement != null)
                    {
                        workbook.Open(Server.MapPath(templateManagement.FilePath));

                        var sheets = workbook.Worksheets;

                        var DisciplineId = Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value);
                        var Discipline = this.disciplineService.GetById(DisciplineId);
                        if (Discipline != null)
                        {
                            sheets[0].Name = Discipline.Name;
                            sheets[0].Cells["C7"].PutValue(Discipline.Name);
                            sheets[0].Cells["B7"].PutValue(this.ddlProject.SelectedValue + "," + DisciplineId);
                            sheets[0].Cells["M4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                            docList =
                                this.documentPackageService.GetAllEMDRByDiscipline(DisciplineId, false)
                                    .OrderBy(t => t.DocNo)
                                    .ToList();

                            var count = 1;

                            var listDocumentTypeId =
                                docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();
                            foreach (var documentTypeId in listDocumentTypeId)
                            {
                                var documentType = this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                var dataRow = dtDiscipline.NewRow();
                                dataRow["DocId"] = -1;
                                dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                dtDiscipline.Rows.Add(dataRow);

                                var listDocByDocType = docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                foreach (var document in listDocByDocType)
                                {
                                    dataRow = dtDiscipline.NewRow();
                                    dataRow["DocId"] = document.ID;
                                    dataRow["NoIndex"] = count;
                                    dataRow["DocNo"] = document.DocNo;
                                    dataRow["DocTitle"] = document.DocTitle;
                                    dataRow["Start"] = (object)document.StartDate ?? DBNull.Value;
                                    dataRow["RevName"] = document.RevisionName;
                                    dataRow["RevPlanned"] = (object)document.RevisionPlanedDate ?? DBNull.Value;
                                    dataRow["RevActual"] = (object)document.RevisionActualDate ?? DBNull.Value;
                                    dataRow["Complete"] = document.Complete / 100;
                                    dataRow["Weight"] = document.Weight / 100;
                                    dataRow["Department"] = document.DeparmentName;
                                    dataRow["Notes"] = document.Notes;
                                    dataRow["IsEMDR"] = document.IsEMDR.GetValueOrDefault() ? "x" : string.Empty;
                                    dataRow["HasAttachFile"] = document.HasAttachFile ? "x" : string.Empty;
                                    dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                    dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                        ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                        : string.Empty;
                                    count += 1;
                                    dtDiscipline.Rows.Add(dataRow);
                                }
                            }

                            sheets[0].Cells["A7"].PutValue(dtDiscipline.Rows.Count);

                            sheets[0].Cells.ImportDataTable(dtDiscipline, false, 7, 1, dtDiscipline.Rows.Count, 16, true);
                            sheets[1].Cells.ImportDataTable(dtDiscipline, false, 7, 1, dtDiscipline.Rows.Count, 16, true);

                            sheets[0].Cells[7 + dtDiscipline.Rows.Count, 2].PutValue("Total");

                            var txtDisciplineComplete =
                                this.CustomerMenu.Items[3].FindControl("txtDisciplineComplete") as RadNumericTextBox;
                            var txtDisciplineWeight =
                                this.CustomerMenu.Items[3].FindControl("txtDisciplineWeight") as RadNumericTextBox;
                            if (txtDisciplineComplete != null)
                            {
                                sheets[0].Cells[7 + dtDiscipline.Rows.Count, 9].PutValue(txtDisciplineComplete.Value / 100);
                            }

                            if (txtDisciplineWeight != null)
                            {
                                sheets[0].Cells[7 + dtDiscipline.Rows.Count, 10].PutValue(txtDisciplineWeight.Value / 100);
                            }
                            sheets[0].AutoFitRows();
                            sheets[1].IsVisible = false;

                            var filename = projectName + " - " + Discipline.Name + " EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.Download_File(filePath + filename);
                        }
                    }
                }
                else
                {
                    var listDisciplineInPermission = UserSession.Current.User.Id == 1
                        ? this.disciplineService.GetAllDisciplineOfProject(Convert.ToInt32(this.ddlProject.SelectedValue))
                            .OrderBy(t => t.ID)
                            .ToList()
                        : this.disciplineService.GetAllDisciplineInPermission(UserSession.Current.User.Id,
                            !string.IsNullOrEmpty(this.ddlProject.SelectedValue)
                                ? Convert.ToInt32(this.ddlProject.SelectedValue)
                                : 0)
                            .OrderBy(t => t.ID).ToList();

                    if (listDisciplineInPermission.Count > 0)
                    {
                        var templateManagement = this.templateManagementService.GetSpecial(2,
                            projectID);
                        if (templateManagement != null)
                        {
                            workbook.Open(Server.MapPath(templateManagement.FilePath));

                            var totalDoc = 0;
                            var totalDocIssues = 0;
                            var totalDocRev0Issues = 0;
                            var totalDocRev1Issues = 0;
                            var totalDocRev2Issues = 0;
                            var totalDocRev3Issues = 0;
                            var totalDocRev4Issues = 0;
                            var totalDocRev5Issues = 0;
                            var totalDocRevIssues = 0;

                            var totalDocDontIssues = 0;

                            var sheets = workbook.Worksheets;
                            var wsSummary = sheets[0];
                            wsSummary.Cells.InsertRows(7, listDisciplineInPermission.Count - 1);

                            for (int i = 0; i < listDisciplineInPermission.Count; i++)
                            {
                                dtFull.Rows.Clear();

                                sheets.AddCopy(1);

                                sheets[i + 2].Name = listDisciplineInPermission[i].Name;
                                sheets[i + 2].Cells["V4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
                                sheets[i + 2].Cells["C7"].PutValue(listDisciplineInPermission[i].Name);

                                // Add hyperlink
                                var linkName = listDisciplineInPermission[i].Name;
                                wsSummary.Cells["B" + (7 + i)].PutValue(linkName);
                                wsSummary.Hyperlinks.Add("B" + (7 + i), 1, 1,
                                    "'" + listDisciplineInPermission[i].Name + "'" + "!D7");

                                docList =
                                    this.documentPackageService.GetAllEMDRByDiscipline(
                                        listDisciplineInPermission[i].ID, false).OrderBy(t => t.DocNo).ToList();
                                var docListHasAttachFile = docList.Where(t => t.HasAttachFile);
                                var wgDoc = docList.Count;
                                var wgDocIssues = docListHasAttachFile.Count();
                                var wgDocRev0Issues = docListHasAttachFile.Count(t => t.RevisionName == "0");
                                var wgDocRev1Issues = docListHasAttachFile.Count(t => t.RevisionName == "1");
                                var wgDocRev2Issues = docListHasAttachFile.Count(t => t.RevisionName == "2");
                                var wgDocRev3Issues = docListHasAttachFile.Count(t => t.RevisionName == "3");
                                var wgDocRev4Issues = docListHasAttachFile.Count(t => t.RevisionName == "4");
                                var wgDocRev5Issues = docListHasAttachFile.Count(t => t.RevisionName == "5");
                                var wgTotalDocRev = wgDocRev0Issues + wgDocRev1Issues + wgDocRev2Issues +
                                                    wgDocRev3Issues + wgDocRev4Issues + wgDocRev5Issues;
                                var wgDocDontIssues = wgDoc - wgDocIssues;

                                totalDoc += wgDoc;
                                totalDocIssues += wgDocIssues;
                                totalDocRev0Issues += wgDocRev0Issues;
                                totalDocRev1Issues += wgDocRev1Issues;
                                totalDocRev2Issues += wgDocRev2Issues;
                                totalDocRev3Issues += wgDocRev3Issues;
                                totalDocRev4Issues += wgDocRev4Issues;
                                totalDocRev5Issues += wgDocRev5Issues;
                                totalDocRevIssues += wgTotalDocRev;
                                totalDocDontIssues = totalDoc - totalDocIssues;

                                wsSummary.Cells["C" + (7 + i)].PutValue(wgDoc);
                                wsSummary.Cells["D" + (7 + i)].PutValue(wgDocDontIssues);
                                wsSummary.Cells["E" + (7 + i)].PutValue(wgDocRev0Issues);
                                wsSummary.Cells["F" + (7 + i)].PutValue(wgDocRev1Issues);
                                wsSummary.Cells["G" + (7 + i)].PutValue(wgDocRev2Issues);
                                wsSummary.Cells["H" + (7 + i)].PutValue(wgDocRev3Issues);
                                wsSummary.Cells["I" + (7 + i)].PutValue(wgDocRev4Issues);
                                wsSummary.Cells["J" + (7 + i)].PutValue(wgDocRev5Issues);
                                wsSummary.Cells["K" + (7 + i)].PutValue(wgTotalDocRev);
                                wsSummary.Cells["L" + (7 + i)].PutValue(wgTotalDocRev);


                                var count = 1;

                                var listDocumentTypeId =
                                    docList.Select(t => t.DocumentTypeId).Distinct().OrderBy(t => t).ToList();

                                double? complete = 0;
                                double? weight = 0;

                                foreach (var documentTypeId in listDocumentTypeId)
                                {
                                    var documentType =
                                        this.documentTypeService.GetById(documentTypeId.GetValueOrDefault());

                                    var dataRow = dtFull.NewRow();
                                    dataRow["NoIndex"] = documentType != null ? documentType.FullName : string.Empty;
                                    dtFull.Rows.Add(dataRow);

                                    var listDocByDocType =
                                        docList.Where(t => t.DocumentTypeId == documentTypeId).ToList();
                                    foreach (var document in listDocByDocType)
                                    {
                                        dataRow = dtFull.NewRow();
                                        dataRow["DocId"] = document.ID;
                                        dataRow["NoIndex"] = count;
                                        dataRow["DocNo"] = document.DocNo;
                                        dataRow["DocTitle"] = document.DocTitle;
                                        dataRow["Department"] = document.DeparmentName;
                                        dataRow["Start"] = document.StartDate != null
                                            ? document.StartDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["Planned"] = document.PlanedDate != null
                                            ? document.PlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevName"] = document.RevisionName;
                                        dataRow["RevPlanned"] = document.RevisionPlanedDate != null
                                            ? document.RevisionPlanedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevActual"] = document.RevisionActualDate != null
                                            ? document.RevisionActualDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["RevCommentCode"] = document.RevisionCommentCode;
                                        dataRow["Complete"] = document.Complete / 100;
                                        dataRow["Weight"] = document.Weight / 100;
                                        dataRow["OutgoingNo"] = document.OutgoingTransNo;
                                        dataRow["OutgoingDate"] = document.OutgoingTransDate != null
                                            ? document.OutgoingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["IncomingNo"] = document.IncomingTransNo;
                                        dataRow["IncomingDate"] = document.IncomingTransDate != null
                                            ? document.IncomingTransDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICANo"] = document.ICAReviewOutTransNo;
                                        dataRow["ICADate"] = document.ICAReviewReceivedDate != null
                                            ? document.ICAReviewReceivedDate.Value.ToString("dd/MM/yyyy")
                                            : string.Empty;
                                        dataRow["ICAReviewCode"] = document.ICAReviewCode;
                                        dataRow["Notes"] = document.Notes;
                                        dataRow["IsEMDR"] = document.IsEMDR.GetValueOrDefault() ? "x" : string.Empty;
                                        dataRow["HasAttachFile"] = document.HasAttachFile ? "x" : string.Empty;

                                        count += 1;
                                        dtFull.Rows.Add(dataRow);

                                        complete += (document.Complete / 100) * (document.Weight / 100);
                                        weight += document.Weight / 100;
                                    }
                                }

                                sheets[i + 2].Cells["A7"].PutValue(dtFull.Rows.Count);
                                sheets[i + 2].Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, 23, true);

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 2].PutValue("Total");

                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 12].PutValue(complete);
                                sheets[i + 2].Cells[7 + dtFull.Rows.Count, 13].PutValue(weight);

                            }

                            wsSummary.Cells["H4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));

                            wsSummary.Cells["C" + (7 + listDisciplineInPermission.Count)].PutValue(totalDoc);
                            wsSummary.Cells["D" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocDontIssues);
                            wsSummary.Cells["E" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRev0Issues);
                            wsSummary.Cells["F" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRev1Issues);
                            wsSummary.Cells["G" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRev2Issues);
                            wsSummary.Cells["H" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRev3Issues);
                            wsSummary.Cells["I" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRev4Issues);
                            wsSummary.Cells["J" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRev5Issues);
                            wsSummary.Cells["K" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRevIssues);
                            wsSummary.Cells["L" + (7 + listDisciplineInPermission.Count)].PutValue(totalDocRevIssues);

                            sheets[1].IsVisible = false;

                            var filename = projectName + " - " + "EMDR Report " +
                                           DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                            workbook.Save(filePath + filename);
                            this.Download_File(filePath + filename);
                        }
                    }
                }
            }
            else if (e.Argument == "UpdatePackageStatus")
            {
                var txtPackageComplete =
                    this.CustomerMenu.Items[2].FindControl("txtPackageComplete") as RadNumericTextBox;
                var txtPackageWeight = this.CustomerMenu.Items[2].FindControl("txtPackageWeight") as RadNumericTextBox;

                var packageobj = this.packageService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                if (packageobj != null)
                {
                    if (txtPackageComplete != null)
                    {
                        packageobj.Complete = txtPackageComplete.Value.GetValueOrDefault();
                    }

                    if (txtPackageWeight != null)
                    {
                        packageobj.Weight = txtPackageWeight.Value.GetValueOrDefault();
                    }

                    this.packageService.Update(packageobj);
                }
            }
            else if (e.Argument.Contains("DeleteRev"))
            {
                string st = e.Argument.ToString();
                var docId = new Guid(st.Replace("DeleteRev_", string.Empty));

                var docObj = this.documentPackageService.GetById(docId);
                var listRelateDoc =
                    this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                if (docObj != null && listRelateDoc.Count > 1)
                {

                    docObj.IsDelete = true;
                    docObj.IsLeaf = false;
                    this.documentPackageService.Update(docObj);
                    docId = new Guid("0");
                    listRelateDoc =
                        this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
                    if (listRelateDoc != null)
                    {
                        foreach (var objDoc in listRelateDoc)
                        {
                            if (docObj.CreatedDate < objDoc.CreatedDate)
                            {
                                docId = objDoc.ID;
                                docObj = objDoc;
                            }
                        }
                    }
                    if (docId != new Guid("0"))
                    {
                        docObj.IsLeaf = true;
                        this.documentPackageService.Update(docObj);
                        this.grdDocument.Rebind();
                    }
                }
                else
                {
                    Response.Write(
                        "<script>window.alert('Can not be reduced, because this document is only one version.')</script>");
                }
            }
            else if (e.Argument == "DownloadMulti")
            {
                //var serverTotalDocPackPath =
                //    Server.MapPath("~/Exports/DocPack/" + DateTime.Now.ToBinary() + "_DocPack.rar");
                //var docPack = ZipPackage.CreateFile(serverTotalDocPackPath);

                //foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                //{
                //    var cboxSelected = (CheckBox) item["IsSelected"].FindControl("IsSelected");
                //    if (cboxSelected.Checked)
                //    {
                //        var docId = Convert.ToInt32(item.GetDataKeyValue("ID"));

                //        var name = (Label) item["Index1"].FindControl("lblName");
                //        var serverDocPackPath =
                //            Server.MapPath("~/Exports/DocPack/" + name.Text + "_" +
                //                           DateTime.Now.ToString("ddMMyyyhhmmss") + ".rar");

                //        var attachFiles = this.attachFileService.GetAllByDocId(docId);

                //        var temp = ZipPackage.CreateFile(serverDocPackPath);

                //        foreach (var attachFile in attachFiles)
                //        {
                //            if (File.Exists(Server.MapPath(attachFile.FilePath)))
                //            {
                //                temp.Add(Server.MapPath(attachFile.FilePath));
                //            }
                //        }

                //        docPack.Add(serverDocPackPath);
                //    }
                //}

                //this.Download_File(filePath + filename);

            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "SendNotification")
            {
                var listDisciplineId = new List<int>();
                var listSelectedDoc = new List<Document>();
                var count = 0;
                foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                {
                    var cboxSelected = (CheckBox)item["IsSelected"].FindControl("IsSelected");
                    if (cboxSelected.Checked)
                    {
                        count += 1;
                        var docItem = new Document();
                        var disciplineId = item["DisciplineID"].Text != @"&nbsp;"
                            ? item["DisciplineID"].Text
                            : string.Empty;
                        if (!string.IsNullOrEmpty(disciplineId) && disciplineId != "0")
                        {
                            listDisciplineId.Add(Convert.ToInt32(disciplineId));

                            docItem.ID = count;
                            docItem.DocumentNumber = item["DocumentNumber"].Text != @"&nbsp;"
                                ? item["DocumentNumber"].Text
                                : string.Empty;
                            docItem.Title = item["Title"].Text != @"&nbsp;"
                                ? item["Title"].Text
                                : string.Empty;
                            docItem.RevisionName = item["Revision"].Text != @"&nbsp;"
                                ? item["Revision"].Text
                                : string.Empty;
                            docItem.FilePath = item["FilePath"].Text != @"&nbsp;"
                                ? item["FilePath"].Text
                                : string.Empty;
                            docItem.DisciplineID = Convert.ToInt32(disciplineId);
                            listSelectedDoc.Add(docItem);
                        }
                    }
                }

                listDisciplineId = listDisciplineId.Distinct().ToList();

                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    Credentials =
                        new NetworkCredential(UserSession.Current.User.Email,
                            Utility.Decrypt(UserSession.Current.User.HashCode))
                };

                foreach (var disciplineId in listDisciplineId)
                {
                    var notificationRule = this.notificationRuleService.GetAllByDiscipline(disciplineId);

                    if (notificationRule != null)
                    {
                        var message = new MailMessage();
                        message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                        message.Subject = "Test send notification from EDMs";
                        message.BodyEncoding = new UTF8Encoding();
                        message.IsBodyHtml = true;
                        message.Body = @"******<br/>
										Dear users,<br/><br/>

										Please be informed that the following documents are now available on the BDPOC Document Library System for your information.<br/><br/>

										<table border='1' cellspacing='0'>
											<tr>
												<th style='text-align:center; width:40px'>No.</th>
												<th style='text-align:center; width:350px'>Document number</th>
												<th style='text-align:center; width:350px'>Document title</th>
												<th style='text-align:center; width:60px'>Revision</th>
											</tr>";

                        if (!string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listUserId =
                                notificationRule.ReceiverListId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            foreach (var userId in listUserId)
                            {
                                var user = this.userService.GetByID(userId);
                                if (user != null)
                                {
                                    message.To.Add(new MailAddress(user.Email));
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(notificationRule.ReceiveGroupId) &&
                                 string.IsNullOrEmpty(notificationRule.ReceiverListId))
                        {
                            var listGroupId =
                                notificationRule.ReceiveGroupId.Split(';').Select(t => Convert.ToInt32(t)).ToList();
                            var listUser = this.userService.GetSpecialListUser(listGroupId);
                            foreach (var user in listUser)
                            {
                                message.To.Add(new MailAddress(user.Email));
                            }
                        }

                        var subBody = string.Empty;
                        foreach (var document in listSelectedDoc)
                        {
                            var port = ConfigurationSettings.AppSettings.Get("DocLibPort");
                            if (document.DisciplineID == disciplineId)
                            {
                                subBody += @"<tr>
								<td>" + document.ID + @"</td>
								<td><a href='http://" + Server.MachineName +
                                           (!string.IsNullOrEmpty(port) ? ":" + port : string.Empty)
                                           + document.FilePath + "' download='" + document.DocumentNumber + "'>"
                                           + document.DocumentNumber + @"</a></td>
								<td>"
                                           + document.Title + @"</td>
								<td>"
                                           + document.RevisionName + @"</td>";
                            }
                        }


                        message.Body += subBody + @"</table>
										<br/><br/>
										Thanks and regards,<br/>
										******";

                        smtpClient.Send(message);
                    }
                }
            }

		}

		/// <summary>
		/// The rad grid 1_ on need data source.
		/// </summary>
		/// <param name="source">
		/// The source.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
		{
			var isListAll = this.Session["IsListAll"] != null && Convert.ToBoolean(this.Session["IsListAll"]);
			this.LoadDocuments(false, isListAll);
		}

		/// <summary>
		/// The grd khach hang_ delete command.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
		{
			var item = (GridDataItem)e.Item;
			var docId = new Guid(item.GetDataKeyValue("ID").ToString());
			var docObj = this.documentPackageService.GetById(docId);
			if (docObj != null)
			{
				if (docObj.ParentId == null)
				{
					docObj.IsDelete = true;
					this.documentPackageService.Update(docObj);
				}
				else
				{
					var listRelateDoc = this.documentPackageService.GetAllRelatedDocument(docObj.ParentId.GetValueOrDefault());
					if (listRelateDoc != null)
					{
						foreach (var objDoc in listRelateDoc)
						{
							objDoc.IsDelete = true;
							this.documentPackageService.Update(objDoc);
						}
					}
				}
			}
		}

		/// <summary>
		/// The grd document_ item command.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
		{
			if (e.CommandName == "RebindGrid")
			{

			}
			if (e.CommandName == RadGrid.RebindGridCommandName)
			{
				this.CustomerMenu.Items[3].Visible = false;
				this.rtvDiscipline.UnselectAllNodes();
				this.grdDocument.Rebind();
			}
			else if (e.CommandName == RadGrid.ExportToExcelCommandName)
			{

			}
		}

		/// <summary>
		/// The grd document_ item data bound.
		/// </summary>
		/// <param name="sender">
		/// The sender.
		/// </param>
		/// <param name="e">
		/// The e.
		/// </param>
		protected void grdDocument_ItemDataBound(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridFilteringItem)
			{

				////Populate Filters by binding the combo to datasource
				//var filteringItem = (GridFilteringItem)e.Item;
				//var myRadComboBox = (RadComboBox)filteringItem.FindControl("RadComboBoxCustomerProgramDescription");

				//myRadComboBox.DataSource = myDataSet;
				//myRadComboBox.DataTextField = "CustomerProgramDescription";
				//myRadComboBox.DataValueField = "CustomerProgramDescription";
				//myRadComboBox.ClearSelection();
				//myRadComboBox.DataBind();
			}
			if (e.Item is GridDataItem)
			{
				var item = e.Item as GridDataItem;
				if (item["IsHasAttachFile"].Text == "True")
				{
					item.BackColor = Color.Aqua;
					item.BorderColor = Color.Aqua;
                    item["No"].ToolTip = "Is Has AttachFile";
                }

                try
                {    ////// set color for overdue
                    var deadline = new DateTime();
                    var datetrans = item["DeadlineTransOut"].Text;
                   var flag= Utility.ConvertStringToDateTime(datetrans, ref deadline);
                    var Rev = item["Rev"].Text;
                    var transOut = item["OutgoingTransNo"].Text;
                    var transOutdate = item["OutgoingTransDate"].Text;
                    var UotDate = new DateTime();
                    var flagout = Utility.ConvertStringToDateTime(transOutdate, ref UotDate);
                    var today = DateTime.Now.Date;

                    var datetransin = new DateTime();
                    var flargtranin = Utility.ConvertStringToDateTime(item["IncomingTransDate"].Text, ref datetransin);
                    if (flag && flargtranin && (deadline.Date < today) && !Rev.Contains("AC") && !flagout)
                    {
                        item["No"].BackColor = Color.Red;
                        item["No"].BorderColor = Color.Red;
                        item["No"].ToolTip = "Late commented";
                    }
                    else if (deadline.Date == today)
                    {
                        item["No"].BackColor = System.Drawing.ColorTranslator.FromHtml("#FFB90F");
                        item["No"].BorderColor = System.Drawing.ColorTranslator.FromHtml("#FFB90F");
                        item["No"].ToolTip  = "Need send comment";
                    }
                }
                catch { }
            }

			//if (e.Item is GridEditableItem && e.Item.IsInEditMode)
			//{
			//	var item = e.Item as GridEditableItem;
			//	var lbldocNo = item.FindControl("lbldocNo") as Label;
			//	var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
			//	var ddlDepartment = item.FindControl("ddlDepartment") as RadComboBox;
			//	var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
			//	var txtPlanedDate = item.FindControl("txtPlanedDate") as RadDatePicker;

			//	var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
			//	var txtRevisionPlanedDate = item.FindControl("txtRevisionPlanedDate") as RadDatePicker;
			//	var txtRevisionActualDate = item.FindControl("txtRevisionActualDate") as RadDatePicker;
			//	var txtRevisionCommentCode = item.FindControl("txtRevisionCommentCode") as TextBox;

			//	var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
			//	var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

			//	var txtOutgoingTransNo = item.FindControl("txtOutgoingTransNo") as TextBox;
			//	var txtOutgoingTransDate = item.FindControl("txtOutgoingTransDate") as RadDatePicker;

			//	var txtIncomingTransNo = item.FindControl("txtIncomingTransNo") as TextBox;
			//	var txtIncomingTransDate = item.FindControl("txtIncomingTransDate") as RadDatePicker;

			//	var txtICAReviewOutTransNo = item.FindControl("txtICAReviewOutTransNo") as TextBox;
			//	var txtICAReviewReceivedDate = item.FindControl("txtICAReviewReceivedDate") as RadDatePicker;
			//	var txtICAReviewCode = item.FindControl("txtICAReviewCode") as TextBox;

			//	var cbIsEMDR = item.FindControl("cbIsEMDR") as CheckBox;


			//	var listRevision = this.revisionService.GetAll();
			//	listRevision.Insert(0, new Revision() { ID = 0 });
			//	if (ddlRevision != null)
			//	{
			//		ddlRevision.DataSource = listRevision;
			//		ddlRevision.DataTextField = "Name";
			//		ddlRevision.DataValueField = "ID";
			//		ddlRevision.DataBind();
			//	}

			//	if (txtStartDate != null)
			//	{
			//		txtStartDate.DatePopupButton.Visible = false;
			//	}

			//	if (txtPlanedDate != null)
			//	{
			//		txtPlanedDate.DatePopupButton.Visible = false;
			//	}

			//	if (txtRevisionPlanedDate != null)
			//	{
			//		txtRevisionPlanedDate.DatePopupButton.Visible = false;
			//	}

			//	if (txtRevisionActualDate != null)
			//	{
			//		txtRevisionActualDate.DatePopupButton.Visible = false;
			//	}

			//	if (txtOutgoingTransDate != null)
			//	{
			//		txtOutgoingTransDate.DatePopupButton.Visible = false;
			//	}

			//	if (txtIncomingTransDate != null)
			//	{
			//		txtIncomingTransDate.DatePopupButton.Visible = false;
			//	}

			//	if (txtICAReviewReceivedDate != null)
			//	{
			//		txtICAReviewReceivedDate.DatePopupButton.Visible = false;
			//	}



			//	var docNo = (item.FindControl("DocNo") as HiddenField).Value;
			//	var docTitle = (item.FindControl("DocTitle") as HiddenField).Value;
			//	var deparmentId = (item.FindControl("DeparmentId") as HiddenField).Value;
			//	var startDate = (item.FindControl("StartDate") as HiddenField).Value;
			//	var planedDate = (item.FindControl("PlanedDate") as HiddenField).Value;
			//	var revisionId = (item.FindControl("RevisionId") as HiddenField).Value;
			//	var revisionPlanedDate = (item.FindControl("RevisionPlanedDate") as HiddenField).Value;
			//	var revisionActualDate = (item.FindControl("RevisionActualDate") as HiddenField).Value;
			//	var revisionCommentCode = (item.FindControl("RevisionCommentCode") as HiddenField).Value;
			//	var complete = (item.FindControl("Complete") as HiddenField).Value;
			//	var weight = (item.FindControl("Weight") as HiddenField).Value;

			//	var OutgoingTransNo = (item.FindControl("OutgoingTransNo") as HiddenField).Value;
			//	var OutgoingTransDate = (item.FindControl("OutgoingTransDate") as HiddenField).Value;
			//	var IncomingTransNo = (item.FindControl("IncomingTransNo") as HiddenField).Value;
			//	var IncomingTransDate = (item.FindControl("IncomingTransDate") as HiddenField).Value;
			//	var ICAReviewOutTransNo = (item.FindControl("ICAReviewOutTransNo") as HiddenField).Value;
			//	var ICAReviewReceivedDate = (item.FindControl("ICAReviewReceivedDate") as HiddenField).Value;
			//	var ICAReviewCode = (item.FindControl("ICAReviewCode") as HiddenField).Value;


			//	var isEMDR = (item.FindControl("IsEMDR") as HiddenField).Value;

			//	if (!string.IsNullOrEmpty(startDate))
			//	{
			//		txtStartDate.SelectedDate = Convert.ToDateTime(startDate);
			//	}

			//	if (!string.IsNullOrEmpty(planedDate))
			//	{
			//		txtPlanedDate.SelectedDate = Convert.ToDateTime(planedDate);
			//	}

			//	if (!string.IsNullOrEmpty(revisionPlanedDate))
			//	{
			//		txtRevisionPlanedDate.SelectedDate = Convert.ToDateTime(revisionPlanedDate);
			//	}

			//	if (!string.IsNullOrEmpty(revisionActualDate))
			//	{
			//		txtRevisionActualDate.SelectedDate = Convert.ToDateTime(revisionActualDate);
			//	}

			//	lbldocNo.Text = docNo;
			//	txtDocTitle.Text = docTitle;

			//	var departmentList = this.roleService.GetAll(false);

			//	if (ddlDepartment != null)
			//	{
			//		departmentList.Insert(0, new Role { Id = 0 });
			//		ddlDepartment.DataSource = departmentList;
			//		ddlDepartment.DataTextField = "Name";
			//		ddlDepartment.DataValueField = "Id";
			//		ddlDepartment.DataBind();

			//		ddlDepartment.SelectedValue = deparmentId;
			//	}

			//	ddlRevision.SelectedValue = revisionId;
			//	txtRevisionCommentCode.Text = revisionCommentCode;
			//	txtComplete.Value = Convert.ToDouble(complete);
			//	txtWeight.Value = Convert.ToDouble(weight);

			//	txtOutgoingTransNo.Text = OutgoingTransNo;
			//	if (!string.IsNullOrEmpty(OutgoingTransDate))
			//	{
			//		txtOutgoingTransDate.SelectedDate = Convert.ToDateTime(OutgoingTransDate);
			//	}

			//	txtIncomingTransNo.Text = IncomingTransNo;
			//	if (!string.IsNullOrEmpty(IncomingTransDate))
			//	{
			//		txtIncomingTransDate.SelectedDate = Convert.ToDateTime(IncomingTransDate);
			//	}

			//	txtICAReviewOutTransNo.Text = ICAReviewOutTransNo;
			//	txtICAReviewCode.Text = ICAReviewCode;
			//	if (!string.IsNullOrEmpty(ICAReviewReceivedDate))
			//	{
			//		txtICAReviewReceivedDate.SelectedDate = Convert.ToDateTime(ICAReviewReceivedDate);
			//	}

			//	cbIsEMDR.Checked = Convert.ToBoolean(isEMDR);
			//}
		}

		//protected void radTreeFolder_NodeExpand(object sender, RadTreeNodeEventArgs e)
		//{
		//	PopulateNodeOnDemand(e, TreeNodeExpandMode.ServerSideCallBack);
		//}

		protected void grdDocument_UpdateCommand(object sender, GridCommandEventArgs e)
		{
			//if (e.Item is GridEditableItem && e.Item.IsInEditMode)
			//{
			//	var item = e.Item as GridEditableItem;
			//	var lbldocNo = item.FindControl("lbldocNo") as Label;
			//	var txtDocTitle = item.FindControl("txtDocTitle") as TextBox;
			//	var ddlDepartment = item.FindControl("ddlDepartment") as RadComboBox;
			//	var txtStartDate = item.FindControl("txtStartDate") as RadDatePicker;
			//	var txtPlanedDate = item.FindControl("txtPlanedDate") as RadDatePicker;

			//	var ddlRevision = item.FindControl("ddlRevision") as RadComboBox;
			//	var txtRevisionPlanedDate = item.FindControl("txtRevisionPlanedDate") as RadDatePicker;
			//	var txtRevisionActualDate = item.FindControl("txtRevisionActualDate") as RadDatePicker;
			//	var txtRevisionCommentCode = item.FindControl("txtRevisionCommentCode") as TextBox;

			//	var txtComplete = item.FindControl("txtComplete") as RadNumericTextBox;
			//	var txtWeight = item.FindControl("txtWeight") as RadNumericTextBox;

			//	var txtOutgoingTransNo = item.FindControl("txtOutgoingTransNo") as TextBox;
			//	var txtOutgoingTransDate = item.FindControl("txtOutgoingTransDate") as RadDatePicker;

			//	var txtIncomingTransNo = item.FindControl("txtIncomingTransNo") as TextBox;
			//	var txtIncomingTransDate = item.FindControl("txtIncomingTransDate") as RadDatePicker;

			//	var txtICAReviewOutTransNo = item.FindControl("txtICAReviewOutTransNo") as TextBox;
			//	var txtICAReviewReceivedDate = item.FindControl("txtICAReviewReceivedDate") as RadDatePicker;
			//	var txtICAReviewCode = item.FindControl("txtICAReviewCode") as TextBox;

			//	var cbIsEMDR = item.FindControl("cbIsEMDR") as CheckBox;

			//	var docId = new Guid(item.GetDataKeyValue("ID").ToString());

			//	var objDoc = this.documentPackageService.GetById(docId);

			//	var currentRevision = objDoc.RevisionId;
			//	var newRevision = Convert.ToInt32(ddlRevision.SelectedValue);
			//	var department = this.roleService.GetByID(Convert.ToInt32(ddlDepartment.SelectedValue));

			//	var projectid = Convert.ToInt32(this.ddlProject.SelectedValue);
			//	var projectname = this.ddlProject.SelectedItem.Text;

			//	if (newRevision > currentRevision)
			//	{
			//		var docObjNew = new DocumentPackage();
			//		docObjNew.ProjectId = projectid;
			//		docObjNew.ProjectName = projectname;
			//		docObjNew.DisciplineId = objDoc.DisciplineId;
			//		docObjNew.DisciplineName = objDoc.DisciplineName;
			//		docObjNew.DocNo = lbldocNo.Text;
			//		docObjNew.DocTitle = txtDocTitle.Text.Trim();
			//		docObjNew.DeparmentId = objDoc.DeparmentId;
			//		docObjNew.DeparmentName = objDoc.DeparmentName;
			//		docObjNew.StartDate = txtStartDate.SelectedDate;

			//		docObjNew.PlanedDate = txtPlanedDate.SelectedDate;
			//		docObjNew.RevisionId = newRevision;
			//		docObjNew.RevisionName = ddlRevision.SelectedItem.Text;
			//		docObjNew.RevisionActualDate = txtRevisionActualDate.SelectedDate;
			//		docObjNew.RevisionCommentCode = txtRevisionCommentCode.Text.Trim();
			//		docObjNew.RevisionPlanedDate = txtRevisionPlanedDate.SelectedDate;
			//		docObjNew.Complete = txtComplete.Value.GetValueOrDefault();
			//		docObjNew.Weight = txtWeight.Value.GetValueOrDefault();
			//		docObjNew.OutgoingTransNo = txtOutgoingTransNo.Text.Trim();
			//		docObjNew.OutgoingTransDate = txtOutgoingTransDate.SelectedDate;
			//		docObjNew.IncomingTransNo = txtIncomingTransNo.Text.Trim();
			//		docObjNew.IncomingTransDate = txtIncomingTransDate.SelectedDate;
			//		docObjNew.ICAReviewOutTransNo = txtICAReviewOutTransNo.Text.Trim();
			//		docObjNew.ICAReviewCode = txtICAReviewCode.Text.Trim();
			//		docObjNew.ICAReviewReceivedDate = txtICAReviewReceivedDate.SelectedDate;

			//		docObjNew.DocumentTypeId = objDoc.DocumentTypeId;
			//		docObjNew.DocumentTypeName = objDoc.DocumentTypeName;
			//		docObjNew.DisciplineId = objDoc.DisciplineId;
			//		docObjNew.DisciplineName = objDoc.DisciplineName;
			//		docObjNew.PackageId = objDoc.PackageId;
			//		docObjNew.PackageName = objDoc.PackageName;
			//		docObjNew.Notes = string.Empty;
			//		docObjNew.PlatformId = objDoc.PlatformId;
			//		docObjNew.PlatformName = objDoc.PlatformName;

			//		docObjNew.IsLeaf = true;
			//		docObjNew.IsEMDR = cbIsEMDR.Checked;
			//		docObjNew.ParentId = objDoc.ParentId ?? objDoc.ID;
			//		docObjNew.CreatedBy = UserSession.Current.User.Id;
			//		docObjNew.CreatedDate = DateTime.Now;

			//		this.documentPackageService.Insert(docObjNew);

			//		objDoc.IsLeaf = false;
			//	}
			//	else
			//	{
			//		////objDoc.DocNo = lbldocNo.Text;
			//		objDoc.DocTitle = txtDocTitle.Text.Trim();
			//		////objDoc.DeparmentName = department != null ? department.FullName : string.Empty;
			//		////objDoc.DeparmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
			//		objDoc.StartDate = txtStartDate.SelectedDate;
			//		objDoc.PlanedDate = txtPlanedDate.SelectedDate;
			//		objDoc.RevisionId = newRevision;
			//		objDoc.RevisionName = ddlRevision.SelectedItem.Text;
			//		objDoc.RevisionActualDate = txtRevisionActualDate.SelectedDate;
			//		objDoc.RevisionCommentCode = txtRevisionCommentCode.Text.Trim();
			//		objDoc.RevisionPlanedDate = txtRevisionPlanedDate.SelectedDate;
			//		objDoc.Complete = txtComplete.Value.GetValueOrDefault();
			//		objDoc.Weight = txtWeight.Value.GetValueOrDefault();
			//		////objDoc.OutgoingTransNo = txtOutgoingTransNo.Text.Trim();
			//		////objDoc.OutgoingTransDate = txtOutgoingTransDate.SelectedDate;
			//		////objDoc.IncomingTransNo = txtIncomingTransNo.Text.Trim();
			//		////objDoc.IncomingTransDate = txtIncomingTransDate.SelectedDate;
			//		////objDoc.ICAReviewOutTransNo = txtICAReviewOutTransNo.Text.Trim();
			//		////objDoc.ICAReviewCode = txtICAReviewCode.Text.Trim();
			//		////objDoc.ICAReviewReceivedDate = txtICAReviewReceivedDate.SelectedDate;

			//		objDoc.IsEMDR = cbIsEMDR.Checked;
			//	}

			//	objDoc.LastUpdatedBy = UserSession.Current.User.Id;
			//	objDoc.LastUpdatedDate = DateTime.Now;

			//	this.documentPackageService.Update(objDoc);
			//}
		}

		protected void ckbEnableFilter_OnCheckedChanged(object sender, EventArgs e)
		{
			this.grdDocument.AllowFilteringByColumn = ((CheckBox)sender).Checked;
			this.grdDocument.Rebind();
		}

		//protected void radTreeFolder_OnNodeDataBound(object sender, RadTreeNodeEventArgs e)
		//{
		//	e.Node.ImageUrl = "Images/folderdir16.png";
		//}

		//private void PopulateNodeOnDemand(RadTreeNodeEventArgs e, TreeNodeExpandMode expandMode)
		//{
		//	var categoryId = this.lblCategoryId.Value;
		//	var folderPermission =
		//		this.groupDataPermissionService.GetByRoleId(UserSession.Current.User.RoleId.GetValueOrDefault()).Where(
		//			t => t.CategoryIdList == categoryId).Select(t => Convert.ToInt32(t.FolderIdList)).ToList();

		//	var listFolChild = this.folderService.GetAllByParentId(Convert.ToInt32(e.Node.Value), folderPermission);
		//	foreach (var folderChild in listFolChild)
		//	{
		//		var nodeFolder = new RadTreeNode();
		//		nodeFolder.Text = folderChild.Name;
		//		nodeFolder.Value = folderChild.ID.ToString();
		//		nodeFolder.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
		//		nodeFolder.ImageUrl = "Images/folderdir16.png";
		//		e.Node.Nodes.Add(nodeFolder);
		//	}

		//	e.Node.Expanded = true;
		//}

		private void CreateValidation(string formular, ValidationCollection objValidations, int startRow, int endRow, int startColumn, int endColumn)
		{
			// Create a new validation to the validations list.
			Validation validation = objValidations[objValidations.Add()];

			// Set the validation type.
			validation.Type = Aspose.Cells.ValidationType.List;

			// Set the operator.
			validation.Operator = OperatorType.None;

			// Set the in cell drop down.
			validation.InCellDropDown = true;

			// Set the formula1.
			validation.Formula1 = "=" + formular;

			// Enable it to show error.
			validation.ShowError = true;

			// Set the alert type severity level.
			validation.AlertStyle = ValidationAlertType.Stop;

			// Set the error title.
			validation.ErrorTitle = "Error";

			// Set the error message.
			validation.ErrorMessage = "Please select item from the list";

			// Specify the validation area.
			CellArea area;
			area.StartRow = startRow;
			area.EndRow = endRow;
			area.StartColumn = startColumn;
			area.EndColumn = endColumn;

			// Add the validation area.
			validation.AreaList.Add(area);

			////return validation;
		}

		private void LoadObjectTree()
		{
			var projectInPermission = this.scopeProjectService.GetAll().OrderBy(t => t.Name).ToList();

			this.ddlProject.DataSource = projectInPermission;
			this.ddlProject.DataTextField = "FullName";
			this.ddlProject.DataValueField = "ID";
			this.ddlProject.DataBind();
			if (this.ddlProject.SelectedItem != null)
			{
				var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
				this.lblProjectId.Value = projectId.ToString();

                var packagelist = this.packageService.GetAllPackageOfProject(projectId).OrderBy(t => t.ID).ToList();
                foreach(var item in packagelist)
                {
                    RadTreeNode node = new RadTreeNode();
                    node.Text = item.Name;
                    node.Value = item.ID.ToString();
                    node.ImageUrl = "~/Images/package.png";
                    node.Target = "Package";
                    node.ToolTip="Package : " + item.Name;
                    var categorylist = this.categoryService.GetAllOfProjectAndPAckage(projectId,item.ID);
                    if (categorylist.Count()>0)
                    {
                        foreach (var folderChild in categorylist.Where(t=> t.ParentId==null).ToList())
                        {
                            var nodeFolder = new RadTreeNode();
                            nodeFolder.Text = folderChild.Name;
                            nodeFolder.Value = folderChild.ID.ToString();
                            nodeFolder.ImageUrl = "~/Images/listmenu.png";
                            nodeFolder.Target = "Category";
                            nodeFolder.ToolTip = "Category : " + folderChild.Name;
                            node.Nodes.Add(nodeFolder);
                            this.BindNoteToRadTreeNode(ref nodeFolder, categorylist, folderChild.ID);
                        }
                    }

                    this.rtvDiscipline.Nodes.Add(node);
                }
                RadTreeNode temp = this.rtvDiscipline.Nodes[0];
                if (temp != null) temp.Selected = true;


            }
		}

        private void BindNoteToRadTreeNode(ref RadTreeNode Node,List<Category> ListCategory, int categories)
        {
            var listnode = ListCategory.Where(t => t.ParentId == categories).ToList();
            if (!listnode.Any()) { return; }
            foreach (var folderChild in listnode)
            {
                var nodeFolder = new RadTreeNode();
                nodeFolder.Text = folderChild.Name;
                nodeFolder.Value = folderChild.ID.ToString();
               // nodeFolder.ExpandMode = TreeNodeExpandMode.ServerSideCallBack;
                nodeFolder.ImageUrl = "~/Images/listmenu.png";
                nodeFolder.Target = "Category";
                nodeFolder.ToolTip = "Category : " + folderChild.Name;
                Node.Nodes.Add(nodeFolder); this.BindNoteToRadTreeNode(ref nodeFolder, ListCategory, folderChild.ID);
            }
        }
        /// <summary>
        /// The repair list.
        /// </summary>
        /// <param name="listOptionalTypeDetail">
        /// The list optional type detail.
        /// </param>
        //private void RepairList(ref List<OptionalTypeDetail> listOptionalTypeDetail)
        //{
        //	var temp = listOptionalTypeDetail.Where(t => t.ParentId != null).Select(t => t.ParentId).Distinct().ToList();
        //	var temp2 = listOptionalTypeDetail.Select(t => t.ID).ToList();
        //	var tempList = new List<OptionalTypeDetail>();
        //	foreach (var x in temp)
        //	{
        //		if (!temp2.Contains(x.Value))
        //		{
        //			tempList.AddRange(listOptionalTypeDetail.Where(t => t.ParentId == x.Value).ToList());
        //		}
        //	}

        //	var listOptionalType = tempList.Where(t => t.OptionalTypeId != null).Select(t => t.OptionalTypeId).Distinct().ToList();

        //	foreach (var optionalTypeId in listOptionalType)
        //	{
        //		var optionalType = this.optionalTypeService.GetById(optionalTypeId.Value);
        //		var tempOptTypeDetail = new OptionalTypeDetail() { ID = optionalType.ID * 9898, Name = optionalType.Name + "s" };
        //		listOptionalTypeDetail.Add(tempOptTypeDetail);
        //		////tempList.Add(tempOptTypeDetail);
        //		OptionalType type = optionalType;
        //		foreach (var optionalTypeDetail in tempList.Where(t => t.OptionalTypeId == type.ID).ToList())
        //		{
        //			optionalTypeDetail.ParentId = tempOptTypeDetail.ID;
        //		}
        //	}
        //}

        protected void ddlProject_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
		{
			////this.CustomerMenu.Items[2].Visible = false;
			int projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
			this.lblProjectId.Value = projectId.ToString();
			this.CustomerMenu.Items[3].Visible = false;

            //var listDisciplineInPermission = UserSession.Current.User.Role.IsAdmin.GetValueOrDefault()
            //	? this.disciplineService.GetAllDisciplineOfProject(projectId).OrderBy(t => t.Name).ToList()
            //	: this.disciplineService.GetAllDisciplineInPermission(UserSession.Current.User.Id, !string.IsNullOrEmpty(this.ddlProject.SelectedValue) ? projectId : 0)
            //	.OrderBy(t => t.Name).ToList();

            //this.rtvDiscipline.DataSource = listDisciplineInPermission;
            //this.rtvDiscipline.DataTextField = "Name";
            //this.rtvDiscipline.DataValueField = "ID";
            //this.rtvDiscipline.DataFieldID = "ID";
            //this.rtvDiscipline.DataBind();
            this.rtvDiscipline.Nodes.Clear();
            if (this.ddlProject.SelectedItem != null)
            {
                
                this.lblProjectId.Value = projectId.ToString();

                var packagelist = this.packageService.GetAllPackageOfProject(projectId).OrderBy(t => t.ID).ToList();
                foreach (var item in packagelist)
                {
                    RadTreeNode node = new RadTreeNode();
                    node.Text = item.Name;
                    node.Value = item.ID.ToString();
                    node.ImageUrl = "~/Images/package.png";
                    node.Target = "Package";

                    var categorylist = this.categoryService.GetAllOfProjectAndPAckage(projectId, item.ID);
                    if (categorylist.Count() > 0)
                    {
                        foreach (var folderChild in categorylist.Where(t => t.ParentId == null).ToList())
                        {
                            var nodeFolder = new RadTreeNode();
                            nodeFolder.Text = folderChild.Name;
                            nodeFolder.Value = folderChild.ID.ToString();
                            nodeFolder.ImageUrl = "~/Images/listmenu.png";
                            nodeFolder.Target = "Category";
                            node.Nodes.Add(nodeFolder);
                            this.BindNoteToRadTreeNode(ref nodeFolder, categorylist, folderChild.ID);
                        }
                    }

                    this.rtvDiscipline.Nodes.Add(node);
                }

            }
            this.grdDocument.Rebind();


		}

		protected void grdDocument_Init(object sender, EventArgs e)
		{
		}

		protected void grdDocument_DataBound(object sender, EventArgs e)
		{
		}

		protected void rtvDiscipline_NodeClick(object sender, RadTreeNodeEventArgs e)
		{
			if (UserSession.Current.User.Role.IsAdmin.GetValueOrDefault() || this.permissionDisciplineService.IsFullPermission(UserSession.Current.User.Id,
				Convert.ToInt32(e.Node.Value)))
			{
				this.CustomerMenu.Items[0].Visible = true;
				this.CustomerMenu.Items[1].Visible = true;
				foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
				{
					if (item.Value == "Adminfunc")
					{
						item.Visible = true;
					}
				}
				this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = true;
				this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = true;
				this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = true;

				this.IsFullPermission.Value = "true";
			}
			else
			{
				this.CustomerMenu.Items[0].Visible = false;
				this.CustomerMenu.Items[1].Visible = false;
				foreach (RadToolBarButton item in ((RadToolBarDropDown)this.CustomerMenu.Items[2]).Buttons)
				{
					if (item.Value == "Adminfunc")
					{
						item.Visible = false;
					}
				}

				this.grdDocument.MasterTableView.GetColumn("IsSelected").Visible = false;
				this.grdDocument.MasterTableView.GetColumn("EditColumn").Visible = false;
				this.grdDocument.MasterTableView.GetColumn("DeleteColumn").Visible = false;

				this.IsFullPermission.Value = "false";
			}

			this.grdDocument.CurrentPageIndex = 0;
			this.grdDocument.Rebind();
		}

		protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
		{
			if (e.Item is GridFilteringItem)
			{
				var filterItem = (GridFilteringItem)e.Item;
				var selectedProperty = new List<string>();

				var ddlFilterRev = (RadComboBox)filterItem.FindControl("ddlFilterRev");
			}
		}

		protected DateTime? SetPublishDate(GridItem item)
		{
			if (item.OwnerTableView.GetColumn("Index27").CurrentFilterValue == string.Empty)
			{
				return new DateTime?();
			}
			else
			{
				return DateTime.Parse(item.OwnerTableView.GetColumn("Index27").CurrentFilterValue);
			}
		}

		/// <summary>
		/// The bind tree view combobox.
		/// </summary>
		/// <param name="optionalType">
		/// The optional type.
		/// </param>
		/// <param name="ddlObj">
		/// The ddl obj.
		/// </param>
		/// <param name="rtvName">
		/// The rtv name.
		/// </param>
		/// <param name="listOptionalTypeDetailFull">
		/// The list optional type detail full.
		/// </param>
		//private void BindTreeViewCombobox(int optionalType, RadComboBox ddlObj, string rtvName, IEnumerable<OptionalTypeDetail> listOptionalTypeDetailFull)
		//{
		//	var rtvobj = (RadTreeView)ddlObj.Items[0].FindControl(rtvName);
		//	if (rtvobj != null)
		//	{
		//		var listOptionalTypeDetail = listOptionalTypeDetailFull.Where(t => t.OptionalTypeId == optionalType).ToList();
		//		this.RepairList(ref listOptionalTypeDetail);

		//		rtvobj.DataSource = listOptionalTypeDetail;
		//		rtvobj.DataFieldParentID = "ParentId";
		//		rtvobj.DataTextField = "Name";
		//		rtvobj.DataValueField = "ID";
		//		rtvobj.DataFieldID = "ID";
		//		rtvobj.DataBind();
		//	}
		//}

		protected void rtvDiscipline_NodeDataBound(object sender, RadTreeNodeEventArgs e)
		{
			//e.Node.ImageUrl = @"Images/discipline.png";
		}

		protected void ddlProject_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
		{
			e.Item.ImageUrl = @"Images/project.png";
		}

		protected void btnSave_Click(object sender, EventArgs e)
		{

		}

		private void InitGridContextMenu(int projectId)
		{
			var contractorList = this.contractorService.GetAllByProject(projectId).OrderBy(t => t.TypeID).ThenBy(t => t.Name);
			foreach (var contractor in contractorList)
			{
				if (contractor.TypeID == 1)
				{
					//this.grdDocument.MasterTableView.ColumnGroups[0].HeaderText = contractor.Name + " - PVCFC";
				}

				this.radMenu.Items.Add(new RadMenuItem()
				{
					Text = (contractor.TypeID == 1 ? "Response from ": "Comment from ") + contractor.Name,
					ImageUrl = "~/Images/comment1.png",
					Value = contractor.TypeID == 1 ? "Response_" + contractor.ID : "Comment_" + contractor.ID,
					//NavigateUrl = "~/Controls/Document/CommentResponseForm.aspx?contId=" + contractor.ID
				});
			}
		}

        private void ExportCMDRDataFile()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGAS_CMDRDataFileTemplate.xlsm");

            var dataSheet = workbook.Worksheets[2];
            var duplicateSheet = workbook.Worksheets[3];
            var tempSheet = workbook.Worksheets[0];
            var countCol = 11;
            var totalColAdded = 0;
            var projectName = this.ddlProject.SelectedItem != null
                ? this.ddlProject.SelectedItem.Text.Replace("&", "-")
                : string.Empty;
            var dtFull = new DataTable();
            var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
            var projectObj = this.scopeProjectService.GetById(projectId);
            //var revisionList = this.revisionService.GetAllByProject(projectId);
            //var originatorList = this.originatorService.GetAllByProject(projectId).OrderBy(t => t.Name).ToList();
            //var disciplineList = this.disciplineService.GetAllDisciplineOfProject(projectId).OrderBy(t => t.Name).ToList();
            //var docTypeList = this.documentTypeService.GetAll().OrderBy(t => t.Name).ToList();
            var reviewcode = this.codeSerVice.GetAll().Where(t => t.TypeId == 2).ToList();
            var reviewcodeCA = this.codeSerVice.GetAll().Where(t => t.TypeId == 1).ToList();
            var statusList = this.DocumentStatusService.GetAll().OrderBy(t => t.Name).ToList();

            var rangereviewcode = tempSheet.Cells.CreateRange("A1", "A" + (reviewcode.Count == 0 ? 1 : reviewcode.Count));
            var rangereviewcodeCA = tempSheet.Cells.CreateRange("C1", "C" + (reviewcodeCA.Count == 0 ? 1 : reviewcodeCA.Count));

            var rangestatusList = tempSheet.Cells.CreateRange("D1", "D" + (statusList.Count == 0 ? 1 : statusList.Count));



            rangereviewcodeCA.Name = "rangereviewcodeCA";
            //	rangeOriginatorList.Name = "OriginatorList";
            rangestatusList.Name = "StatusList";
            rangereviewcode.Name = "rangereviewcode";


            for (int j = 0; j < reviewcode.Count; j++)
            {
                rangereviewcode[j, 0].PutValue(reviewcode[j].Name);
            }
            for (int j = 0; j < reviewcodeCA.Count; j++)
            {
                rangereviewcodeCA[j, 0].PutValue(reviewcodeCA[j].Name);
            }

            for (int j = 0; j < statusList.Count; j++)
            {
                rangestatusList[j, 0].PutValue(statusList[j].Name);
            }

            //for (int j = 0; j < docTypeList.Count; j++)
            //{
            //	rangeDocTypeList[j, 0].PutValue(docTypeList[j].Name);
            //}

            //for (int j = 0; j < statusList.Count; j++)
            //{
            //	rangeStatusList[j, 0].PutValue(statusList[j].Name);
            //}

            var filename = projectName.Split(',')[0].Trim() + "_" + "Phu Luc 01 S-Curved Data File_Cut off - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsm";
            dataSheet.Name = "All";
            var docList = this.documentPackageService.GetAllByProject(projectId, false).OrderBy(t=> t.PackageId).ToList();
            if (this.rtvDiscipline.SelectedNode != null)
            {
               
                if (this.rtvDiscipline.SelectedNode.ParentNode != null)
                { var category = this.categoryService.GetById(Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value));
                    docList = docList.Where(t => t.PackageId == category.PackageId)
                    // .Where(t => t.DisciplineId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value))
                    .OrderBy(t => t.DocNo)
                    .ToList();
                    dataSheet.Name = category.PackageName;
                }
                else
                {
                    docList = docList = docList.Where(t => t.PackageId == Convert.ToInt32(this.rtvDiscipline.SelectedNode.Value)).OrderBy(t => t.DocNo)
                                        .ToList();
                    dataSheet.Name = this.rtvDiscipline.SelectedNode.Text;
                }

             //   filename = Utility.RemoveSpecialCharacterFileName(this.rtvDiscipline.SelectedNode.Text) + "(" + projectName.Split('-')[0].Trim() + ")" + "_" + "CMDR Data File_Cut off - " + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsm";
            }
                dataSheet.Cells["E1"].PutValue(this.ddlProject.Text);
                dataSheet.Cells["W4"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));

                dtFull.Columns.AddRange(new[]
                {
                new DataColumn("DocId", typeof (String)),
                new DataColumn("NoIndex", typeof (String)),
                new DataColumn("DocNo", typeof (String)),
                new DataColumn("DocTitle", typeof (String)),
                new DataColumn("RevName", typeof (String)),
                new DataColumn("PlanStart", typeof (DateTime)),
                new DataColumn("ForeStart", typeof (DateTime)),
                new DataColumn("ActualSatrt", typeof (DateTime)),
                new DataColumn("PlanIDC", typeof (DateTime)),
                new DataColumn("ForeIDC", typeof (DateTime)),
                new DataColumn("ActualIDC", typeof (DateTime)),
                new DataColumn("PlanIFRSAIPEM", typeof (DateTime)),
                new DataColumn("ForeIFRSAIPEM", typeof (DateTime)),
                new DataColumn("ActualIFRSAIPEM", typeof (DateTime)),
                new DataColumn("PlanIFREXM", typeof (DateTime)),
                new DataColumn("ForeIFREXM", typeof (DateTime)),
                new DataColumn("ActualIFREXM", typeof (DateTime)),
                new DataColumn("PlanIFD", typeof (DateTime)),
                new DataColumn("ForeIFD", typeof (DateTime)),
                new DataColumn("ActualIFD", typeof (DateTime)),
                new DataColumn("PlanFINAL", typeof (DateTime)),
                new DataColumn("ForeFINAL", typeof (DateTime)),
                new DataColumn("ActualFINAL", typeof (DateTime)),
                new DataColumn("Manhour", typeof (double)),
                new DataColumn("Weight", typeof (double)),
                new DataColumn("RevDate", typeof (DateTime)),
                new DataColumn("Status", typeof (String)),

                new DataColumn("ReviewCode", typeof (String)),
                new DataColumn("Remark", typeof (String)),
                new DataColumn("CodeCA", typeof (String)),
                new DataColumn("TransInDate", typeof (DateTime)),
                new DataColumn("TransInNo", typeof (String)),
                new DataColumn("TransOutDate", typeof (DateTime)),
                new DataColumn("TransOutNo", typeof (String)),

                new DataColumn("KDN_O_CodeCA", typeof (String)),
                new DataColumn("KDN_O_TransInDate", typeof (DateTime)),
                new DataColumn("KDN_O_TransInNo", typeof (String)),
                new DataColumn("KDN_O_TransOutDate", typeof (DateTime)),
                new DataColumn("KDN_O_TransOutNo", typeof (String)),

                  new DataColumn("NCS_O_CodeCA", typeof (String)),
                new DataColumn("NCS_O_TransInDate", typeof (DateTime)),
                new DataColumn("NCS_O_TransInNo", typeof (String)),
                new DataColumn("NCS_O_TransOutDate", typeof (DateTime)),
                new DataColumn("NCS_O_TransOutNo", typeof (String)),

            });


                var docGroupByDisciplineList = docList.GroupBy(t => t.CategoryName);
                foreach (var docGroupByDiscipline in docGroupByDisciplineList)
                {
                    var disciplineRowCount = 1;
                    var docListOfDiscipline = docGroupByDiscipline.ToList();
                    var dataRow = dtFull.NewRow();
                    dataRow["DocNo"] = docGroupByDiscipline.Key;
                    dtFull.Rows.Add(dataRow);


                    foreach (var docObj in docListOfDiscipline)
                    {
                        dataRow = dtFull.NewRow();
                        dataRow["DocId"] = docObj.ID;
                        dataRow["NoIndex"] = disciplineRowCount;
                        dataRow["DocNo"] = docObj.DocNo;
                        dataRow["DocTitle"] = docObj.DocTitle;
                        dataRow["RevName"] = docObj.RevisionName;
                        dataRow["Manhour"] = docObj.Manhours != null ? docObj.Manhours.GetValueOrDefault() : 0;
                        dataRow["Weight"] = docObj.Weight != null ? docObj.Weight.GetValueOrDefault() / 100 : 0;
                        dataRow["Status"] = docObj.CodeName;

                        dataRow["ReviewCode"] = docObj.DocReviewStatusCode;
                        dataRow["Remark"] = docObj.Remarks;
                        if (docObj.RevDate != null) dataRow["RevDate"] = docObj.RevDate;
                        if (docObj.StartPlan != null) dataRow["PlanStart"] = docObj.StartPlan;
                        if (docObj.StartRevised != null) dataRow["ForeStart"] = docObj.StartRevised;
                        if (docObj.StartActual != null) dataRow["ActualSatrt"] = docObj.StartActual;
                        if (docObj.IDCPlan != null) dataRow["PlanIDC"] = docObj.IDCPlan;
                        if (docObj.IDCRevised != null) dataRow["ForeIDC"] = docObj.IDCRevised;
                        if (docObj.IDCACtual != null) dataRow["ActualIDC"] = docObj.IDCACtual;
                        if (docObj.IFRPlan != null) dataRow["PlanIFRSAIPEM"] = docObj.IFRPlan;
                        if (docObj.IFRRevised != null) dataRow["ForeIFRSAIPEM"] = docObj.IFRRevised;
                        if (docObj.IFRActual != null) dataRow["ActualIFRSAIPEM"] = docObj.IFRActual;
                        if (docObj.ReIFRPlan != null) dataRow["PlanIFREXM"] = docObj.ReIFRPlan;
                        if (docObj.ReIFRRevised != null) dataRow["ForeIFREXM"] = docObj.ReIFRRevised;
                        if (docObj.ReIFRActual != null) dataRow["ActualIFREXM"] = docObj.ReIFRActual;
                        if (docObj.IFAPlan != null) dataRow["PlanIFD"] = docObj.IFAPlan;
                        if (docObj.IFARevised != null) dataRow["ForeIFD"] = docObj.IFARevised;
                        if (docObj.IFAActual != null) dataRow["ActualIFD"] = docObj.IFAActual;
                        if (docObj.AFCPlan != null) dataRow["PlanFINAL"] = docObj.AFCPlan;
                        if (docObj.AFCRevised != null) dataRow["ForeFINAL"] = docObj.AFCRevised;
                        if (docObj.AFCActual != null) dataRow["ActualFINAL"] = docObj.AFCActual;

                        dataRow["CodeCA"] = docObj.ResponseCACode;
                        dataRow["TransInNo"] = docObj.IncomingTransNo_CA;
                        if (docObj.IncomingTransDate_CA != null) dataRow["TransInDate"] = docObj.IncomingTransDate_CA;
                        dataRow["TransOutNo"] = docObj.OutgoingTransNo_CA;
                        if (docObj.OutgoingTransDate_CA != null) dataRow["TransOutDate"] = docObj.OutgoingTransDate_CA;

                        //kdn
                        dataRow["KDN_O_CodeCA"] = docObj.KDN_Operator_ResponseCode;
                        dataRow["KDN_O_TransInNo"] = docObj.KDN_Operator_IncomingTransNo;
                        if (docObj.KDN_Operator_IncomingTransDate != null) dataRow["KDN_O_TransInDate"] = docObj.KDN_Operator_IncomingTransDate;
                        dataRow["KDN_O_TransOutNo"] = docObj.KDN_Operator_OutgoingTransNo;
                        if (docObj.KDN_Operator_OutgoingTransDate != null) dataRow["KDN_O_TransOutDate"] = docObj.KDN_Operator_OutgoingTransDate;
                        //ncs
                        dataRow["NCS_O_CodeCA"] = docObj.NCS_Operator_ResponseCode;
                        dataRow["NCS_O_TransInNo"] = docObj.NCS_Operator_IncomingTransNo;
                        if (docObj.NCS_Operator_IncomingTransDate != null) dataRow["NCS_O_TransInDate"] = docObj.NCS_Operator_IncomingTransDate;
                        dataRow["NCS_O_TransOutNo"] = docObj.NCS_Operator_OutgoingTransNo;
                        if (docObj.NCS_Operator_OutgoingTransDate != null) dataRow["NCS_O_TransOutDate"] = docObj.NCS_Operator_OutgoingTransDate;
                        disciplineRowCount += 1;
                        dtFull.Rows.Add(dataRow);
                    }
                }
                dataSheet.Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, dtFull.Columns.Count, false);
                duplicateSheet.Cells.ImportDataTable(dtFull, false, 7, 1, dtFull.Rows.Count, dtFull.Columns.Count, false);

                var validations = dataSheet.Validations;
                this.CreateValidation(rangestatusList.Name, validations, 7, 3000, 27, 27);
                this.CreateValidation(rangereviewcodeCA.Name, validations, 7, 3000, 30, 30);
                this.CreateValidation(rangereviewcode.Name, validations, 7, 3000, 28, 28);
                this.CreateValidation(rangereviewcodeCA.Name, validations, 7, 3000, 35, 35);
                this.CreateValidation(rangereviewcodeCA.Name, validations, 7, 3000, 40, 40);

                dataSheet.Cells["A7"].PutValue(dtFull.Rows.Count + 7);
                dataSheet.Cells["A1"].PutValue(projectId);


                workbook.Save(filePath + filename);
                this.Download_File(filePath + filename);

            }
        
		private void Download_File(string FilePath)
		{
			Response.ContentType = ContentType;
			Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
			Response.WriteFile(FilePath);
			Response.End();
		}

		protected void ckbShowAll_CheckedChange(object sender, EventArgs e)
		{
			this.grdDocument.Rebind();
		}
	}
}