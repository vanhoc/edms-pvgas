﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Workflow;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using Telerik.Web.UI;
using System.Linq;
using System.Collections.Generic;

namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ObjectWorkflowDetailPage : Page
    {

        private readonly WorkflowStepService wfStepService = new WorkflowStepService();
        private readonly WorkflowService wfService = new WorkflowService();
        private readonly WorkflowDetailService wfDetailService = new WorkflowDetailService();
        private readonly ScopeProjectService projectService = new ScopeProjectService();
        private readonly ObjectWorkflowDetailService ObjWfDetailService = new ObjectWorkflowDetailService();
        private readonly TemplateWorkflowDetailService templateWFDetailService = new TemplateWorkflowDetailService();
        private readonly DistributionMatrixService matrixService= new DistributionMatrixService();
        private readonly DistributionMatrixDetailService matrixDetailService= new DistributionMatrixDetailService();

        private readonly ContractorTransmittalDocFileService contractorTransmittalDocFileService= new ContractorTransmittalDocFileService();
        private readonly DQRETransmittalService dqreTransmittalService= new DQRETransmittalService();
        private readonly AttachDocToTransmittalService attachDocToTransmittalService= new AttachDocToTransmittalService();

        private readonly HashSet<DateTime> Holidays = new HashSet<DateTime>();

        private readonly RoleService roleService= new RoleService();
        private readonly ObjectWorkflowDetailService objectWorkflowDetailService= new ObjectWorkflowDetailService();
        private readonly HolidayConfigService holidayConfigService= new HolidayConfigService();
        private readonly IntergrateParamConfigService configService= new IntergrateParamConfigService();

        private readonly DQREDocumentService documentService= new DQREDocumentService();
        private readonly UserService userService= new UserService();
        private readonly ObjectAssignedWorkflowService objAssignedWfService= new ObjectAssignedWorkflowService();
        private readonly ObjectAssignedUserService objAssignedUserService= new ObjectAssignedUserService();

        private readonly CustomizeWorkflowDetailService customizeWorkflowDetailService = new CustomizeWorkflowDetailService();

        private int WorkflowId
        {
            get
            {
                return Convert.ToInt32(this.Request.QueryString["wfId"]);
            }
        }
        private new Guid ObjId
        {
            get
            {
                return new Guid(Request.QueryString["objId"]);
            }
        }
        private Data.Entities.Workflow WorkflowObj
        {
            get { return this.wfService.GetById(this.WorkflowId); }
        }
        //private Data.Entities.DQREDocument ObjectDocument
        //{
        //    get { return this.dqreDocumentService.GetById(this.ObjId); }
        //}
        private ScopeProject ProjectObj
        {
            get { return this.projectService.GetById(this.WorkflowObj.ProjectID.GetValueOrDefault()); }
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            if (!this.IsPostBack)
            {
                var wfObj = this.wfService.GetById(this.WorkflowId);
                if (wfObj != null)
                {
                    this.txtWorkflowName.Text = wfObj.Name;
                }

                if (!this.customizeWorkflowDetailService.GetAllByObjId(this.ObjId).Any() && !this.customizeWorkflowDetailService.GetAllByTransId(this.ObjId).Any())
                {
                    var originalWorkflowDetails = this.wfDetailService.GetAllByWorkflow(this.WorkflowId);
                    var customizeWfFrom = this.Request.QueryString["customizeWfFrom"];
                    foreach (var wfDetail in originalWorkflowDetails)
                    {
                        var customizeWfDetail = new CustomizeWorkflowDetail()
                        {
                            WorkflowID = wfDetail.WorkflowID,
                            WorkflowName = wfDetail.WorkflowName,
                            CurrentWorkflowStepID = wfDetail.CurrentWorkflowStepID,
                            CurrentWorkflowStepName = wfDetail.CurrentWorkflowStepName,
                            StepDefinitionID = wfDetail.StepDefinitionID,
                            StepDefinitionName = wfDetail.StepDefinitionName,
                            Duration = wfDetail.Duration,
                            AssignTitleIds = wfDetail.AssignTitleIds,
                            AssignUserIDs = wfDetail.AssignUserIDs,
                            ReviewUserIds = wfDetail.ReviewUserIds,
                            ConsolidateUserIds = wfDetail.ConsolidateUserIds,
                            CommentUserIds = wfDetail.CommentUserIds,
                            ApproveUserIds = wfDetail.ApproveUserIds,
                            InformationOnlyUserIDs = wfDetail.InformationOnlyUserIDs,
                            ManagementUserIds = wfDetail.ManagementUserIds,
                            AssignRoleIDs = wfDetail.AssignRoleIDs,
                            InformationOnlyRoleIDs = wfDetail.InformationOnlyRoleIDs,
                            DistributionMatrixIDs = wfDetail.DistributionMatrixIDs,
                            Recipients = wfDetail.Recipients,
                            NextWorkflowStepID = wfDetail.NextWorkflowStepID,
                            NextWorkflowStepName = wfDetail.NextWorkflowStepName,
                            RejectWorkflowStepID = wfDetail.RejectWorkflowStepID,
                            RejectWorkflowStepName = wfDetail.RejectWorkflowStepName,
                            CreatedBy = wfDetail.CreatedBy,
                            CreatedDate = wfDetail.CreatedDate,
                            UpdatedBy = wfDetail.UpdatedBy,
                            UpdatedDate = wfDetail.UpdatedDate,
                            ProjectID = wfDetail.ProjectID,
                            ProjectName = wfDetail.ProjectName,
                            IsFirst = wfDetail.IsFirst,
                            CanReject = wfDetail.CanReject,
                            IsOnlyWorkingDay = wfDetail.IsOnlyWorkingDay,
                            IsCanCreateOutgoingTrans = wfDetail.IsCanCreateOutgoingTrans,
                            ActionApplyCode = wfDetail.ActionApplyCode,
                            ActionApplyName  =wfDetail.ActionApplyName,
                            
                        };

                        if (customizeWfFrom == "Trans")
                        {
                            customizeWfDetail.IncomingTransId = this.ObjId;
                        }
                        else
                        {
                            customizeWfDetail.ObjectId = this.ObjId;
                        }

                        this.customizeWorkflowDetailService.Insert(customizeWfDetail);
                    }
                }

                //this.divMessage.Visible = false;
                //var firstStep = this.wfStepService.GetAllByWorkflow(WorkflowId).FirstOrDefault(t => t.IsFirst.GetValueOrDefault());
                //if (firstStep != null)
                //{
                //    this.txtFirstStep.Text = firstStep.WorkflowName;
                //}
                //DeleteTemplateWFDetail(this.templateWFDetailService.GetAllByWorkflow(this.WorkflowId, UserSession.Current.User.Id));
                //InsertTemplateWFDetail(this.wfDetailService.GetAllByWorkflow(this.WorkflowId));
            }
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var customizeWfFrom = this.Request.QueryString["customizeWfFrom"];
            if (customizeWfFrom == "Trans")
            {
                this.grdDocument.DataSource = this.customizeWorkflowDetailService.GetAllByWorkflowAndTrans(this.WorkflowId,
                    this.ObjId);
            }
            else
            {
                this.grdDocument.DataSource = this.customizeWorkflowDetailService.GetAllByWorkflowAndObj(this.WorkflowId,
                    this.ObjId);
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }

        protected void grdDocument_OnBatchEditCommand(object sender, GridBatchEditingEventArgs e)
        {
            foreach (GridBatchEditingCommand command in e.Commands)
            {
                Hashtable newValues = command.NewValues;
                var customizeWFDetailID = Convert.ToInt32(newValues["ID"].ToString());
                var customizeWFDetailObj = this.customizeWorkflowDetailService.GetById(customizeWFDetailID);

                if (customizeWFDetailObj != null)
                {
                    //var wfStepDefineID = Convert.ToInt32(newValues["StepDefinitionID"].ToString());
                    //var isOnlyWorkingDay = Convert.ToBoolean(newValues["IsOnlyWorkingDay"].ToString());
                    //var nextWorkflowStepId = Convert.ToInt32(newValues["NextWorkflowStepID"].ToString());
                    //var nextWorkflowStepObj = this.wfStepService.GetById(nextWorkflowStepId);
                    //var rejectWorkflowStepId = Convert.ToInt32(newValues["RejectWorkflowStepID"].ToString());
                    //var rejectWorkflowStepObj = this.wfStepService.GetById(rejectWorkflowStepId);

                    //wfDetailObj.StepDefinitionID = wfStepDefineID;
                    //wfDetailObj.StepDefinitionName = Utility.WorkflowStepDefine[wfStepDefineID];

                    //customizeWFDetailObj.NextWorkflowStepID = nextWorkflowStepObj != null
                    //    ? nextWorkflowStepObj.ID
                    //    : 0;
                    //customizeWFDetailObj.NextWorkflowStepName = nextWorkflowStepObj != null
                    //    ? nextWorkflowStepObj.Name
                    //    : string.Empty;

                    //customizeWFDetailObj.RejectWorkflowStepID = rejectWorkflowStepObj != null
                    //    ? rejectWorkflowStepObj.ID
                    //    : 0;
                    //customizeWFDetailObj.RejectWorkflowStepName = rejectWorkflowStepObj != null
                    //    ? rejectWorkflowStepObj.Name
                    //    : string.Empty;

                    customizeWFDetailObj.Duration = !string.IsNullOrEmpty(newValues["Duration"].ToString()) 
                                        ? Convert.ToInt32(newValues["Duration"].ToString())
                                        : (double?) null;
                    customizeWFDetailObj.IsOnlyWorkingDay = true;

                    ////wfDetailObj.AssignRoleIDs = string.Empty;
                    ////wfDetailObj.AssignUserIDs = string.Empty;
                    ////wfDetailObj.InformationOnlyRoleIDs = string.Empty;
                    ////wfDetailObj.InformationOnlyUserIDs = string.Empty;
                    ////wfDetailObj.DistributionMatrixIDs = string.Empty;

                    this.customizeWorkflowDetailService.Update(customizeWFDetailObj);
                }
            }
        }

        protected void grdDocument_OnPreRender(object sender, EventArgs e)
        {
            //var wfStepList = this.wfStepService.GetAllByWorkflow(this.WorkflowId);

            //var ddlAcceptStep = (this.grdDocument.MasterTableView.GetBatchEditorContainer("NextWorkflowStepID") as Panel).FindControl("ddlAcceptStep") as RadComboBox;
            //var ddlRejectStep = (this.grdDocument.MasterTableView.GetBatchEditorContainer("RejectWorkflowStepID") as Panel).FindControl("ddlRejectStep") as RadComboBox;

            //if (ddlAcceptStep != null && ddlRejectStep != null)
            //{
            //    wfStepList.Insert(0, new WorkflowStep() {ID = 0});
            //    ddlAcceptStep.DataSource = wfStepList;
            //    ddlAcceptStep.DataTextField = "Name";
            //    ddlAcceptStep.DataValueField = "ID";
            //    ddlAcceptStep.DataBind();

            //    ddlRejectStep.DataSource = wfStepList;
            //    ddlRejectStep.DataTextField = "Name";
            //    ddlRejectStep.DataValueField = "ID";
            //    ddlRejectStep.DataBind();
            //}
        }
        protected void btnReload_Click(object sender, EventArgs e)
        {
            if (this.customizeWorkflowDetailService.GetAllByObjId(this.ObjId).Any() || this.customizeWorkflowDetailService.GetAllByTransId(this.ObjId).Any())
            {
                var customizeWfFrom = this.Request.QueryString["customizeWfFrom"];
                var ObjectList = new List<CustomizeWorkflowDetail>();
                if (customizeWfFrom == "Trans")
                {
                    ObjectList = this.customizeWorkflowDetailService.GetAllByTransId(this.ObjId);
                }
                else
                {
                    ObjectList = this.customizeWorkflowDetailService.GetAllByObjId(this.ObjId);
                }

                foreach (var item in ObjectList)
                {
                    this.customizeWorkflowDetailService.Delete(item);
                }

                var originalWorkflowDetails = this.wfDetailService.GetAllByWorkflow(this.WorkflowId);
                foreach (var wfDetail in originalWorkflowDetails)
                {
                    var customizeWfDetail = new CustomizeWorkflowDetail()
                    {
                        WorkflowID = wfDetail.WorkflowID,
                        WorkflowName = wfDetail.WorkflowName,
                        CurrentWorkflowStepID = wfDetail.CurrentWorkflowStepID,
                        CurrentWorkflowStepName = wfDetail.CurrentWorkflowStepName,
                        StepDefinitionID = wfDetail.StepDefinitionID,
                        StepDefinitionName = wfDetail.StepDefinitionName,
                        Duration = wfDetail.Duration,
                        AssignTitleIds = wfDetail.AssignTitleIds,
                        AssignUserIDs = wfDetail.AssignUserIDs,
                        ReviewUserIds = wfDetail.ReviewUserIds,
                        ConsolidateUserIds = wfDetail.ConsolidateUserIds,
                        CommentUserIds = wfDetail.CommentUserIds,
                        ApproveUserIds = wfDetail.ApproveUserIds,
                        InformationOnlyUserIDs = wfDetail.InformationOnlyUserIDs,
                        ManagementUserIds = wfDetail.ManagementUserIds,
                        AssignRoleIDs = wfDetail.AssignRoleIDs,
                        InformationOnlyRoleIDs = wfDetail.InformationOnlyRoleIDs,
                        DistributionMatrixIDs = wfDetail.DistributionMatrixIDs,
                        Recipients = wfDetail.Recipients,
                        NextWorkflowStepID = wfDetail.NextWorkflowStepID,
                        NextWorkflowStepName = wfDetail.NextWorkflowStepName,
                        RejectWorkflowStepID = wfDetail.RejectWorkflowStepID,
                        RejectWorkflowStepName = wfDetail.RejectWorkflowStepName,
                        CreatedBy = wfDetail.CreatedBy,
                        CreatedDate = wfDetail.CreatedDate,
                        UpdatedBy = wfDetail.UpdatedBy,
                        UpdatedDate = wfDetail.UpdatedDate,
                        ProjectID = wfDetail.ProjectID,
                        ProjectName = wfDetail.ProjectName,
                        IsFirst = wfDetail.IsFirst,
                        CanReject = wfDetail.CanReject,
                        IsOnlyWorkingDay = wfDetail.IsOnlyWorkingDay,
                        IsCanCreateOutgoingTrans = wfDetail.IsCanCreateOutgoingTrans,
                        ActionApplyCode = wfDetail.ActionApplyCode,
                        ActionApplyName = wfDetail.ActionApplyName,

                    };

                    if (customizeWfFrom == "Trans")
                    {
                        customizeWfDetail.IncomingTransId = this.ObjId;
                    }
                    else
                    {
                        customizeWfDetail.ObjectId = this.ObjId;
                    }

                    this.customizeWorkflowDetailService.Insert(customizeWfDetail);
                }
                this.grdDocument.Rebind();
            }
        }

    }
}