﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.Hosting;
using System.IO;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Scope;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.WMS;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using System.Configuration;
using Aspose.Cells;
namespace EDMs.Web.Controls.Workflow
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AttachWorkflow : Page
    {
        private readonly WorkflowService wfService;
        private readonly UserService userService;
        private readonly ObjectAssignedWorkflowService objAssignedWfService;
        private readonly ObjectAssignedUserService objAssignedUserService;
        private readonly WorkflowStepService wfStepService;
        private readonly WorkflowDetailService wfDetailService;
        private readonly HolidayConfigService holidayConfigService;
        private readonly DocumentPackageService documentService;
        private readonly DistributionMatrixService matrixService;
        private readonly DistributionMatrixDetailService matrixDetailService;
        private readonly ContractorTransmittalDocFileService contractorTransmittalDocFileService;
        private readonly PVGASTransmittalService PVGASTransmittalService;
        private readonly AttachDocToTransmittalService attachDocToTransmittalService;
        private readonly HashSet<DateTime> holidays = new HashSet<DateTime>();
        private readonly ChangeRequestService changeRequestService;
        private readonly NCR_SIService ncrSiService;
        private readonly CustomizeWorkflowDetailService customizeWorkflowDetailService;
        private readonly PVGASDocumentAttachFileService documentAttachFileService ;
        private new Guid ObjId
        {
            get
            {
                return new Guid(Request.QueryString["objId"]);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectInfoEditForm"/> class.
        /// </summary>
        public AttachWorkflow()
        {
            this.userService = new UserService();
            this.wfService = new WorkflowService();
            this.objAssignedUserService = new ObjectAssignedUserService();
            this.objAssignedWfService = new ObjectAssignedWorkflowService();
            this.wfStepService = new WorkflowStepService();
            this.wfDetailService = new WorkflowDetailService();
            this.holidayConfigService = new HolidayConfigService();
            this.documentService = new DocumentPackageService();
            this.matrixService = new DistributionMatrixService();
            this.matrixDetailService = new DistributionMatrixDetailService();
            this.contractorTransmittalDocFileService = new ContractorTransmittalDocFileService();
            this.PVGASTransmittalService = new PVGASTransmittalService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.changeRequestService = new ChangeRequestService();
            this.ncrSiService = new NCR_SIService();
            this.customizeWorkflowDetailService = new CustomizeWorkflowDetailService();
            this.documentAttachFileService = new PVGASDocumentAttachFileService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var holidayList = this.holidayConfigService.GetAll();
                foreach (var holidayConfig in holidayList)
                {
                    for (DateTime i = holidayConfig.FromDate.GetValueOrDefault(); i < holidayConfig.ToDate.GetValueOrDefault(); i = i.AddDays(1))
                    {
                        this.holidays.Add(i);
                    }
                }

                this.AttachFrom.Value= Request.QueryString["attachFrom"];
                this.ObjectId.Value = Request.QueryString["objId"];
                this.ObjectType.Value = Request.QueryString["type"];
                this.CustomizeWfFrom.Value = Request.QueryString["customizeWfFrom"];
                LoadComboData();
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
          
           
             var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
             var objType = Convert.ToInt32(this.Request.QueryString["type"]);
             var attachFrom = Request.QueryString["attachFrom"];

             var customizeWfFrom = Request.QueryString["customizeWfFrom"];
             var isHaveCustomizeWf = false;

             var wfObj = this.wfService.GetById(Convert.ToInt32(ddlWorkflow.SelectedValue));
             if (wfObj != null)
             {
                 var clientclickManual = this.ClickManual.Value;
                 if (clientclickManual != "true")
                 {
                     var CustomOBj = new List<CustomizeWorkflowDetail>();
                     if (customizeWfFrom == "Trans")
                     {
                         CustomOBj = customizeWorkflowDetailService.GetAllByTransId(this.ObjId);
                     }
                     else
                     {
                         CustomOBj = customizeWorkflowDetailService.GetAllByObjId(this.ObjId);
                     }
                     if (CustomOBj != null && CustomOBj.Count > 0)
                     {

                     }
                     else
                     {
                         InserteCustomizework(wfObj.ID, customizeWfFrom);
                     }

                 }
                 switch (objType)
                 {
                     // Document
                     case 1:
                         switch (attachFrom)
                         {
                             case "ProjectDocList":
                                 var docObj = this.documentService.GetById(this.ObjId);
                                 if (docObj != null)
                                 {
                                     var wfFirstStepObj = this.wfStepService.GetFirstStep(wfObj.ID);
                                     var previousstep = new ObjectAssignedWorkflow();
                                     // Update docobj workflow status
                                     docObj.IsInWFProcess = true;
                                     docObj.IsAttachWorkflow = true;
                                     docObj.IsWFComplete = false;
                                     docObj.CurrentWorkflowName = wfObj.Name;
                                     docObj.CurrentWorkflowStepName = wfFirstStepObj.Name;

                                     //if (this.cbCustomizeWorkflow.Checked)
                                     //{
                                         //if (customizeWfFrom == "Trans")
                                         //{
                                         //    docObj.IsUseCustomWfFromTrans = true;
                                         //}
                                         //else
                                         //{
                                             docObj.IsUseIsUseCustomWfFromObj = true;
                                         //}

                                         this.ProcessCustomizeWorkflow(wfFirstStepObj, wfObj, docObj, previousstep, DateTime.Now, objType, customizeWfFrom);
                                     //}
                                     //else
                                     //{
                                     //    this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, docObj, previousstep, DateTime.Now, objType);
                                     //}

                                     this.documentService.Update(docObj);
                                    this.GenerateCRSfile(docObj);
                                }
                                 break;
                             case "AttachByDocTrans":
                                 //var contractorTransDocFileObj = contractorTransmittalDocFileService.GetById(this.ObjId);
                                 ////var TranObjId= new Guid(Request.QueryString["TransPECC2"]);
                                 //if (contractorTransDocFileObj.PECC2ProjectDocId != null)
                                 //{
                                 //    var docObj2 = this.documentService.GetById(contractorTransDocFileObj.PECC2ProjectDocId.GetValueOrDefault());
                                 //    if (docObj2 != null)
                                 //    {
                                 //        var wfFirstStepObj = this.wfStepService.GetFirstStep(wfObj.ID);
                                 //        var previousstep = new ObjectAssignedWorkflow();
                                 //        //this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, docObj2, previousstep, DateTime.Now, objType);

                                 //        // Update docobj workflow status
                                 //        docObj2.IsInWFProcess = true;
                                 //        docObj2.IsWFComplete = false;
                                 //        docObj2.CurrentWorkflowName = wfObj.Name;
                                 //        docObj2.CurrentWorkflowStepName = wfFirstStepObj.Name;

                                 //        if (this.cbCustomizeWorkflow.Checked)
                                 //        {
                                 //            if (customizeWfFrom == "Trans")
                                 //            {
                                 //                docObj2.IsUseCustomWfFromTrans = true;
                                 //            }
                                 //            else
                                 //            {
                                 //                docObj2.IsUseIsUseCustomWfFromObj = true;
                                 //            }

                                 //            this.ProcessCustomizeWorkflow(wfFirstStepObj, wfObj, docObj2, previousstep, DateTime.Now, objType, customizeWfFrom);
                                 //        }
                                 //        else
                                 //        {
                                 //            this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, docObj2, previousstep, DateTime.Now, objType);
                                 //        }
                                 //        this.documentService.Update(docObj2);
                                 //    }
                                 //}
                                 break;
                             case "AttachByTrans":
                                 //var pecc2IncomingTrans = this.PVGASTransmittalService.GetById(this.ObjId);
                                 //var contractorTransDocFileList = this.contractorTransmittalDocFileService.GetAllByTrans(pecc2IncomingTrans.ContractorTransId.GetValueOrDefault());
                                 //var doclistInTrans = this.attachDocToTransmittalService.GetAllByTransId(this.ObjId);
                                 //var docIds = doclistInTrans.Select(t => t.DocumentId).Distinct().ToList(); //contractorTransDocFileList.Select(t => t.PECC2ProjectDocId).Distinct().ToList();
                                 //foreach (var docId in docIds)
                                 //{
                                 //    var docObj1 = this.documentService.GetById(docId.GetValueOrDefault());
                                 //    if (docObj1 != null)
                                 //    {
                                 //        var wfFirstStepObj = this.wfStepService.GetFirstStep(wfObj.ID);
                                 //        var previousstep = new ObjectAssignedWorkflow();
                                 //        //this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, docObj1, previousstep, DateTime.Now, objType);

                                 //        // Update docobj workflow status
                                 //        docObj1.IsInWFProcess = true;
                                 //        docObj1.IsWFComplete = false;
                                 //        docObj1.CurrentWorkflowName = wfObj.Name;
                                 //        docObj1.CurrentWorkflowStepName = wfFirstStepObj.Name;

                                 //        if (this.cbCustomizeWorkflow.Checked)
                                 //        {
                                 //            if (customizeWfFrom == "Trans")
                                 //            {
                                 //                docObj1.IsUseCustomWfFromTrans = true;
                                 //            }
                                 //            else
                                 //            {
                                 //                docObj1.IsUseIsUseCustomWfFromObj = true;
                                 //            }

                                 //            this.ProcessCustomizeWorkflow(wfFirstStepObj, wfObj, docObj1, previousstep, DateTime.Now, objType, customizeWfFrom);
                                 //        }
                                 //        else
                                 //        {
                                 //            this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, docObj1, previousstep, DateTime.Now, objType);
                                 //        }

                                 //        this.documentService.Update(docObj1);
                                 //    }
                                 //}

                                 //// Update attach workflow status for contractor doc file, and PECC2 incoming trans
                                 //foreach (var contractorTransDocFile in contractorTransDocFileList)
                                 //{
                                 //    contractorTransDocFile.IsAttachWorkflow = true;
                                 //    this.contractorTransmittalDocFileService.Update(contractorTransDocFile);
                                 //}

                                 //pecc2IncomingTrans.IsAttachWorkflow = true;
                                 //this.PVGASTransmittalService.Update(pecc2IncomingTrans);
                                 //// -------------------------------------------------------------------------------------------------
                                 break;
                         }
                         break;
                     // Change request
                     case 2:
                         switch (attachFrom)
                         {
                             case "AttachByTrans":
                                 var pecc2IncomingTrans = this.PVGASTransmittalService.GetById(this.ObjId);
                                 var contractorTransDocFileList = this.contractorTransmittalDocFileService.GetAllByTrans(pecc2IncomingTrans.ContractorTransId.GetValueOrDefault());
                                 var changeRequestlistInTrans = this.attachDocToTransmittalService.GetAllByTransId(this.ObjId);
                                 var changeRequestIds = changeRequestlistInTrans.Select(t => t.DocumentId.GetValueOrDefault()).Distinct().ToList();
                                 foreach (var changeRequestId in changeRequestIds)
                                 {
                                     var changeRequestObj1 = this.changeRequestService.GetById(changeRequestId);
                                     if (changeRequestObj1 != null)
                                     {
                                         var wfFirstStepObj = this.wfStepService.GetFirstStep(wfObj.ID);
                                         var previousstep = new ObjectAssignedWorkflow();
                                         //this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, changeRequestObj, previousstep, DateTime.Now, objType);

                                         // Update docobj workflow status
                                         changeRequestObj1.IsInWFProcess = true;
                                         changeRequestObj1.IsWFComplete = false;
                                         changeRequestObj1.CurrentWorkflowName = wfObj.Name;
                                         changeRequestObj1.CurrentWorkflowStepName = wfFirstStepObj.Name;

                                         if (this.cbCustomizeWorkflow.Checked)
                                         {
                                             if (customizeWfFrom == "Trans")
                                             {
                                                 changeRequestObj1.IsUseCustomWfFromTrans = true;
                                             }
                                             else
                                             {
                                                 changeRequestObj1.IsUseIsUseCustomWfFromObj = true;
                                             }

                                             this.ProcessCustomizeWorkflow(wfFirstStepObj, wfObj, changeRequestObj1, previousstep, DateTime.Now, objType, customizeWfFrom);
                                         }
                                         else
                                         {
                                             this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, changeRequestObj1, previousstep, DateTime.Now, objType);
                                         }

                                         this.changeRequestService.Update(changeRequestObj1);
                                     }
                                 }

                                 // Update attach workflow status for contractor doc file, and PECC2 incoming trans
                                 foreach (var contractorTransDocFile in contractorTransDocFileList)
                                 {
                                     contractorTransDocFile.IsAttachWorkflow = true;
                                     this.contractorTransmittalDocFileService.Update(contractorTransDocFile);
                                 }

                                 pecc2IncomingTrans.IsAttachWorkflow = true;
                                 this.PVGASTransmittalService.Update(pecc2IncomingTrans);
                                 // -------------------------------------------------------------------------------------------------
                                 break;
                             default:
                                 var changeRequestObj = this.changeRequestService.GetById(this.ObjId);
                                 if (changeRequestObj != null)
                                 {
                                     var wfFirstStepObj = this.wfStepService.GetFirstStep(wfObj.ID);
                                     var previousstep = new ObjectAssignedWorkflow();
                                     //this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, changeRequestObj, previousstep, DateTime.Now, objType);

                                     // Update docobj workflow status
                                     changeRequestObj.IsInWFProcess = true;
                                     changeRequestObj.IsWFComplete = false;
                                     changeRequestObj.CurrentWorkflowName = wfObj.Name;
                                     changeRequestObj.CurrentWorkflowStepName = wfFirstStepObj.Name;

                                     if (this.cbCustomizeWorkflow.Checked)
                                     {
                                         if (customizeWfFrom == "Trans")
                                         {
                                             changeRequestObj.IsUseCustomWfFromTrans = true;
                                         }
                                         else
                                         {
                                             changeRequestObj.IsUseIsUseCustomWfFromObj = true;
                                         }

                                         this.ProcessCustomizeWorkflow(wfFirstStepObj, wfObj, changeRequestObj, previousstep, DateTime.Now, objType, customizeWfFrom);
                                     }
                                     else
                                     {
                                         this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, changeRequestObj, previousstep, DateTime.Now, objType);
                                     }

                                     this.changeRequestService.Update(changeRequestObj);
                                 }
                                 break;
                         }

                         break;
                     // NCR/SI
                     case 3:
                         var ncrsiObj = this.ncrSiService.GetById(this.ObjId);
                         if (ncrsiObj != null)
                         {
                             var wfFirstStepObj = this.wfStepService.GetFirstStep(wfObj.ID);
                             var previousstep = new ObjectAssignedWorkflow();
                             //this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, ncrsiObj, previousstep, DateTime.Now, objType);

                             // Update docobj workflow status
                             ncrsiObj.IsInWFProcess = true;
                             ncrsiObj.IsWFComplete = false;
                             ncrsiObj.CurrentWorkflowName = wfObj.Name;
                             ncrsiObj.CurrentWorkflowStepName = wfFirstStepObj.Name;

                             // Use customize workflow
                             if (this.cbCustomizeWorkflow.Checked)
                             {
                                 if (customizeWfFrom == "Trans")
                                 {
                                     ncrsiObj.IsUseCustomWfFromTrans = true;
                                 }
                                 else
                                 {
                                     ncrsiObj.IsUseIsUseCustomWfFromObj = true;
                                 }

                                 this.ProcessCustomizeWorkflow(wfFirstStepObj, wfObj, ncrsiObj, previousstep, DateTime.Now, objType, customizeWfFrom);
                             }
                             else
                             {
                                 this.ProcessOriginalWorkflow(wfFirstStepObj, wfObj, ncrsiObj, previousstep, DateTime.Now, objType);
                             }

                             this.ncrSiService.Update(ncrsiObj);
                         }
                         break;
                 }
             }

             ClientScript.RegisterStartupScript(Page.GetType(), "mykey", "CloseAndRebind();", true);
        }
        private void InserteCustomizework(int WorkflowId, string customizeWfFrom)
        {
            this.cbCustomizeWorkflow.Checked = true;
            this.ClickManual.Value = "true";
            var originalWorkflowDetails = this.wfDetailService.GetAllByWorkflow(WorkflowId);
            foreach (var wfDetail in originalWorkflowDetails)
            {
                var customizeWfDetail = new CustomizeWorkflowDetail()
                {
                    WorkflowID = wfDetail.WorkflowID,
                    WorkflowName = wfDetail.WorkflowName,
                    CurrentWorkflowStepID = wfDetail.CurrentWorkflowStepID,
                    CurrentWorkflowStepName = wfDetail.CurrentWorkflowStepName,
                    StepDefinitionID = wfDetail.StepDefinitionID,
                    StepDefinitionName = wfDetail.StepDefinitionName,
                    Duration = wfDetail.Duration,
                    AssignTitleIds = wfDetail.AssignTitleIds,
                    AssignUserIDs = wfDetail.AssignUserIDs,
                    ReviewUserIds = wfDetail.ReviewUserIds,
                    ConsolidateUserIds = wfDetail.ConsolidateUserIds,
                    CommentUserIds = wfDetail.CommentUserIds,
                    ApproveUserIds = wfDetail.ApproveUserIds,
                    InformationOnlyUserIDs = wfDetail.InformationOnlyUserIDs,
                    ManagementUserIds = wfDetail.ManagementUserIds,
                    AssignRoleIDs = wfDetail.AssignRoleIDs,
                    InformationOnlyRoleIDs = wfDetail.InformationOnlyRoleIDs,
                    DistributionMatrixIDs = wfDetail.DistributionMatrixIDs,
                    Recipients = wfDetail.Recipients,
                    NextWorkflowStepID = wfDetail.NextWorkflowStepID,
                    NextWorkflowStepName = wfDetail.NextWorkflowStepName,
                    RejectWorkflowStepID = wfDetail.RejectWorkflowStepID,
                    RejectWorkflowStepName = wfDetail.RejectWorkflowStepName,
                    CreatedBy = wfDetail.CreatedBy,
                    CreatedDate = wfDetail.CreatedDate,
                    UpdatedBy = wfDetail.UpdatedBy,
                    UpdatedDate = wfDetail.UpdatedDate,
                    ProjectID = wfDetail.ProjectID,
                    ProjectName = wfDetail.ProjectName,
                    IsFirst = wfDetail.IsFirst,
                    CanReject = wfDetail.CanReject,
                    IsOnlyWorkingDay = wfDetail.IsOnlyWorkingDay,
                    IsCanCreateOutgoingTrans = wfDetail.IsCanCreateOutgoingTrans,
                    ActionApplyCode = wfDetail.ActionApplyCode,
                    ActionApplyName = wfDetail.ActionApplyName,

                };
                if (customizeWfFrom == "Trans")
                {
                    customizeWfDetail.IncomingTransId = this.ObjId;
                }
                else
                {
                    customizeWfDetail.ObjectId = this.ObjId;
                }

                this.customizeWorkflowDetailService.Insert(customizeWfDetail);
            }
        }

        private void ProcessOriginalWorkflow(WorkflowStep wfStepObj, Data.Entities.Workflow wfObj, object obj,
            ObjectAssignedWorkflow ObjAssignWF, DateTime CurrenDate, int objType)
        {
            var groupId = 0;
            var wfDetailObj = this.wfDetailService.GetByCurrentStep(wfStepObj.ID);
            //  Guid? nullvalue = null;
            if (wfDetailObj != null)
            {
                var assignWorkFlow = new ObjectAssignedWorkflow
                {
                    ID = Guid.NewGuid(),
                    WorkflowID = wfObj.ID,
                    WorkflowName = wfObj.Name,
                    ObjectWFDwtailId = wfDetailObj.ID,

                    CurrentWorkflowStepID = wfDetailObj.CurrentWorkflowStepID,
                    CurrentWorkflowStepName = wfDetailObj.CurrentWorkflowStepName,
                    NextWorkflowStepID = wfDetailObj.NextWorkflowStepID,
                    NextWorkflowStepName = wfDetailObj.NextWorkflowStepName,
                    RejectWorkflowStepID = wfDetailObj.RejectWorkflowStepID,
                    RejectWorkflowStepName = wfDetailObj.RejectWorkflowStepName,
                    IsComplete = false,
                    IsReject = false,
                    IsLeaf = wfStepObj.IsFirst.GetValueOrDefault(),
                    AssignedBy = UserSession.Current.User.Id,
                    CanReject = wfStepObj.CanReject,


                };

                switch (objType)
                {
                    case 1:
                        var docObj = (DocumentPackage) obj;
                        assignWorkFlow.ObjectID = docObj.ID;
                        assignWorkFlow.ObjectNumber = docObj.DocNo;
                        assignWorkFlow.ObjectTitle = docObj.DocTitle;
                        assignWorkFlow.ObjectProject = docObj.ProjectName;
                        assignWorkFlow.ObjectType = "Project's Document";
                        groupId = docObj.DisciplineId.GetValueOrDefault();
                        break;
                    //case 2:
                    //    var changeRequestObj = (ChangeRequest) obj;
                    //    assignWorkFlow.ObjectID = changeRequestObj.ID;
                    //    assignWorkFlow.ObjectNumber = changeRequestObj.Number;
                    //    assignWorkFlow.ObjectTitle = changeRequestObj.Description;
                    //    assignWorkFlow.ObjectProject = changeRequestObj.ProjectCode;
                    //    assignWorkFlow.ObjectType = "Change Request";
                    //    groupId = changeRequestObj.GroupId.GetValueOrDefault();

                    //    break;
                    //case 3:
                    //    var ncrsiObj = (NCR_SI) obj;
                    //    assignWorkFlow.ObjectID = ncrsiObj.ID;
                    //    assignWorkFlow.ObjectNumber = ncrsiObj.Number;
                    //    assignWorkFlow.ObjectTitle = ncrsiObj.Subject;
                    //    assignWorkFlow.ObjectProject = ncrsiObj.ProjectName;
                    //    assignWorkFlow.ObjectType = "NCR/SI";
                    //    groupId = ncrsiObj.GroupId.GetValueOrDefault();

                    //    break;
                }


                if (ObjAssignWF != null)
                {
                    assignWorkFlow.PreviousStepId = ObjAssignWF.ID;
                }

                var assignWorkflowId = this.objAssignedWfService.Insert(assignWorkFlow);
                if (assignWorkflowId != null)
                {
                    // Get actual deadline if workflow step detail use only working day
                    var actualDeadline = CurrenDate;
                    if (wfDetailObj.IsOnlyWorkingDay.GetValueOrDefault())
                    {
                        for (int i = 1; i <= wfDetailObj.Duration.GetValueOrDefault(); i++)
                        {
                            actualDeadline = this.GetNextWorkingDay(actualDeadline);
                        }
                    }
                    // -------------------------------------------------------------------------

                    // Get assign User List
                    var wfStepWorkingAssignUser = new List<User>();
                    var infoUserIds = wfDetailObj.InformationOnlyUserIDs != null
                        ? wfDetailObj.InformationOnlyUserIDs.Split(';').ToList()
                        : new List<string>();
                    var reviewUserIds = wfDetailObj.ReviewUserIds != null
                        ? wfDetailObj.ReviewUserIds.Split(';').ToList()
                        : new List<string>();
                    var consolidateUserIds = wfDetailObj.ConsolidateUserIds != null
                        ? wfDetailObj.ConsolidateUserIds.Split(';').ToList()
                        : new List<string>();
                    var approveUserIds = wfDetailObj.ApproveUserIds != null
                        ? wfDetailObj.ApproveUserIds.Split(';').ToList()
                        : new List<string>();

                    var matrixList =
                        wfDetailObj.DistributionMatrixIDs.Split(';')
                            .Where(t => !string.IsNullOrEmpty(t))
                            .Select(t => this.matrixService.GetById(Convert.ToInt32(t)));
                    foreach (var matrix in matrixList)
                    {
                        var matrixDetailList = this.matrixDetailService.GetAllByDM(matrix.ID).Where(t => t.GroupCodeId == groupId);
                        var acceptAction = wfStepObj.ActionApplyCode.Split(';').Where(t => !string.IsNullOrEmpty(t)).ToList();
                        var matrixDetailValid = matrixDetailList.Where(t => acceptAction.Contains(t.ActionTypeName)).ToList();

                        reviewUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 2).Select(t => t.UserId.ToString()));
                        consolidateUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 3).Select(t => t.UserId.ToString()));
                        approveUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 4).Select(t => t.UserId.ToString()));
                        infoUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 1).Select(t => t.UserId.ToString()));
                    }

                    foreach (
                        var userId in
                            consolidateUserIds.Distinct()
                                .Where(t => !string.IsNullOrEmpty(t))
                                .Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null)
                        {
                            userObj.ActionTypeId = 3;
                            userObj.ActionTypeName = "C - Consolicate";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }

                    foreach (
                        var userId in
                            reviewUserIds.Distinct()
                                .Where(t => !string.IsNullOrEmpty(t))
                                .Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null)
                        {
                            userObj.ActionTypeId = 2;
                            userObj.ActionTypeName = "R - Review";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }

                    foreach (
                        var userId in
                            approveUserIds.Distinct()
                                .Where(t => !string.IsNullOrEmpty(t))
                                .Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null)
                        {
                            userObj.ActionTypeId = 4;
                            userObj.ActionTypeName = "A - Approve";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }

                    foreach (
                        var userId in
                            infoUserIds.Distinct().Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null)
                        {
                            userObj.ActionTypeId = 1;
                            userObj.ActionTypeName = "I - For Information";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }
                    // ---------------------------------------------------------------------------
                    var assignWorkingUserInfor = new ObjectAssignedUser();
                    // Create assign user info
                    foreach (var user in wfStepWorkingAssignUser)
                    {
                        var assignWorkingUser = new ObjectAssignedUser
                        {
                            ID = Guid.NewGuid(),
                            ObjectAssignedWorkflowID = assignWorkflowId,
                            UserID = user.Id,
                            UserFullName = user.FullName,
                            ReceivedDate = CurrenDate,
                            PlanCompleteDate =
                                wfDetailObj.IsOnlyWorkingDay.GetValueOrDefault()
                                    ? actualDeadline
                                    : DateTime.Now.AddDays(wfDetailObj.Duration.GetValueOrDefault()),
                            IsOverDue = false,
                            IsComplete = user.ActionTypeId == 1,// && wfDetailObj.IsFirst.GetValueOrDefault(),
                            IsReject = false,
                            AssignedBy = UserSession.Current.User.Id,
                            WorkflowId = wfObj.ID,
                            WorkflowName = wfObj.Name,
                            CurrentWorkflowStepName = wfStepObj.Name,
                            CurrentWorkflowStepId = wfStepObj.ID,
                            CanReject = wfStepObj.CanReject,
                            IsCanCreateOutgoingTrans = wfDetailObj.IsCanCreateOutgoingTrans,
                            IsFinal = wfDetailObj.NextWorkflowStepID == 0,
                            ActionTypeId = user.ActionTypeId,
                            ActionTypeName = user.ActionTypeName,
                            WorkingStatus = string.Empty,
                            //Status = (user.ActionTypeId == 1 && wfDetailObj.IsFirst.GetValueOrDefault()) ? "SO" : wfDetailObj.IsFirst.GetValueOrDefault() ? "RS" : "NR",
                            
                            IsMainWorkflow = true,
                            IsReassign = false,
                            IsLeaf = wfStepObj.IsFirst.GetValueOrDefault()
                        };

                        switch (objType)
                        {
                            case 1:
                                var docObj = (DocumentPackage) obj;
                                assignWorkingUser.ObjectID = docObj.ID;
                                assignWorkingUser.ObjectNumber = docObj.DocNo;
                                assignWorkingUser.ObjectTitle = docObj.DocTitle;
                                assignWorkingUser.ObjectProject = docObj.ProjectName;
                                assignWorkingUser.ObjectProjectId = docObj.ProjectId;
                                assignWorkingUser.Revision = docObj.RevisionName;
                                //assignWorkingUser.Categoryid = docObj.CategoryId;
                                assignWorkingUser.ObjectType = "Project's Document";
                                assignWorkingUser.ObjectTypeId = 1;
                                break;
                            //case 2:
                            //    var changeRequestObj = (ChangeRequest) obj;
                            //    assignWorkingUser.ObjectID = changeRequestObj.ID;
                            //    assignWorkingUser.ObjectNumber = changeRequestObj.Number;
                            //    assignWorkingUser.ObjectTitle = changeRequestObj.Description;
                            //    assignWorkingUser.ObjectProject = changeRequestObj.ProjectCode;
                            //    assignWorkingUser.ObjectProjectId = changeRequestObj.ProjectId;
                            //    assignWorkingUser.Categoryid = 0;
                            //    assignWorkingUser.ObjectType = "Change Request";
                            //    assignWorkingUser.ObjectTypeId = 2;
                            //    break;
                            //case 3:
                            //    var ncrsiObj = (NCR_SI) obj;
                            //    assignWorkingUser.ObjectID = ncrsiObj.ID;
                            //    assignWorkingUser.ObjectNumber = ncrsiObj.Number;
                            //    assignWorkingUser.ObjectTitle = ncrsiObj.Subject;
                            //    assignWorkingUser.ObjectProject = ncrsiObj.ProjectName;
                            //    assignWorkingUser.ObjectProjectId = ncrsiObj.ProjectId;
                            //    assignWorkingUser.Categoryid = 0;
                            //    assignWorkingUser.ObjectType = "NCR/SI";
                            //    assignWorkingUser.ObjectTypeId = 3;
                            //    break;
                        }

                        objAssignedUserService.Insert(assignWorkingUser);
                        assignWorkingUserInfor = assignWorkingUser;
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]) &&
                            wfStepObj.IsFirst.GetValueOrDefault())
                        {
                           // this.SendNotification(assignWorkingUser, user, infoUserIds);
                        }
                    }

                    // Assign to re-assign user of workflow when can't find working user
                    if (wfStepWorkingAssignUser.Count == 0 && wfObj.Re_assignUserId != null)
                    {
                        var assignWorkingUser = new ObjectAssignedUser
                        {
                            ID = Guid.NewGuid(),
                            ObjectAssignedWorkflowID = assignWorkflowId,
                            UserID = wfObj.Re_assignUserId,
                            UserFullName = wfObj.Re_assignUserName,
                            ReceivedDate = CurrenDate,
                            PlanCompleteDate =
                                wfDetailObj.IsOnlyWorkingDay.GetValueOrDefault()
                                    ? actualDeadline
                                    : DateTime.Now.AddDays(wfDetailObj.Duration.GetValueOrDefault()),
                            IsOverDue = false,
                            IsComplete = false,
                            IsReject = false,
                            AssignedBy = UserSession.Current.User.Id,
                            WorkflowId = wfObj.ID,
                            WorkflowName = wfObj.Name,
                            CurrentWorkflowStepName = wfStepObj.Name,
                            CurrentWorkflowStepId = wfStepObj.ID,
                            CanReject = wfStepObj.CanReject,
                            IsCanCreateOutgoingTrans = wfDetailObj.IsCanCreateOutgoingTrans,
                            IsFinal = wfDetailObj.NextWorkflowStepID == 0,
                            ActionTypeId = 5,
                            ActionTypeName = "Can't find working user. Re-assign to another user.",
                            WorkingStatus = string.Empty,
                            //Status = "RS",
                            IsMainWorkflow = true,
                            IsReassign = true
                        };

                        switch (objType)
                        {
                            case 1:
                                var docObj = (DocumentPackage) obj;
                                assignWorkingUser.ObjectID = docObj.ID;
                                assignWorkingUser.ObjectNumber = docObj.DocNo;
                                assignWorkingUser.ObjectTitle = docObj.DocTitle;
                                assignWorkingUser.ObjectProject = docObj.ProjectName;
                                assignWorkingUser.Revision = docObj.RevisionName;
                                break;
                            //case 2:
                            //    var changeRequestObj = (ChangeRequest) obj;
                            //    assignWorkingUser.ObjectID = changeRequestObj.ID;
                            //    assignWorkingUser.ObjectNumber = changeRequestObj.Number;
                            //    assignWorkingUser.ObjectTitle = changeRequestObj.Description;
                            //    assignWorkingUser.ObjectProject = changeRequestObj.ProjectCode;
                            //    break;
                            //case 3:
                            //    var ncrsiObj = (NCR_SI) obj;
                            //    assignWorkingUser.ObjectID = ncrsiObj.ID;
                            //    assignWorkingUser.ObjectNumber = ncrsiObj.Number;
                            //    assignWorkingUser.ObjectTitle = ncrsiObj.Description;
                            //    assignWorkingUser.ObjectProject = ncrsiObj.ProjectName;
                            //    break;
                        }

                        objAssignedUserService.Insert(assignWorkingUser);
                        //if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]))
                        //    this.SendNotification(assignWorkingUser,
                        //        this.userService.GetByID(wfObj.Re_assignUserId.GetValueOrDefault()), infoUserIds);
                    }
                    // ----------------------------------------------------------------------------------

                    // Send notification for Info & Management user

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]) && infoUserIds.Count > 0 &&
                        wfDetailObj.IsFirst.GetValueOrDefault())
                    {
                      //  this.SendNotificationInfor(assignWorkingUserInfor, this.userService.GetByID(wfObj.Re_assignUserId.GetValueOrDefault()), infoUserIds);
                    }

                    //var wfNextStepObj = this.wfStepService.GetById(wfDetailObj.NextWorkflowStepID.GetValueOrDefault());
                    //if (wfNextStepObj != null)
                    //{
                    //    this.ProcessWorkflow(wfNextStepObj, wfObj, obj, assignWorkFlow, actualDeadline, objType);
                    //}
                }
            }
        }

        private void ProcessCustomizeWorkflow(WorkflowStep wfStepObj, Data.Entities.Workflow wfObj, object obj,
            ObjectAssignedWorkflow ObjAssignWF, DateTime CurrenDate, int objType, string customizeWfFrom)
        {
            Guid groupId=new Guid() ;
            var listemailto = new List<string>();
            CustomizeWorkflowDetail wfDetailObj = null;
            if (customizeWfFrom == "Trans")
            {
                switch (objType)
                {
                    case 1:
                        var docObj = (DocumentPackage) obj;
                        if (docObj.IncomingTransId != null)
                        {
                            wfDetailObj =
                                this.customizeWorkflowDetailService.GetByCurrentStepCustomizeFromTrans(wfStepObj.ID,
                                    docObj.IncomingTransId.GetValueOrDefault());
                        }
                        break;
                    case 2:
                        var changeRequestObj = (ChangeRequest) obj;
                        wfDetailObj =
                            this.customizeWorkflowDetailService.GetByCurrentStepCustomizeFromTrans(wfStepObj.ID,
                                changeRequestObj.IncomingTransId.GetValueOrDefault());
                        break;
                }
            }
            else
            {
                switch (objType)
                {
                    case 1:
                        var docObj = (DocumentPackage)obj;
                      
                            wfDetailObj =
                                this.customizeWorkflowDetailService.GetByCurrentStepCustomizeFromObj(wfStepObj.ID,
                                    docObj.ID);
                        
                        break;
                    case 2:
                        var changeRequestObj = (ChangeRequest)obj;
                        wfDetailObj =
                            this.customizeWorkflowDetailService.GetByCurrentStepCustomizeFromObj(wfStepObj.ID,
                                changeRequestObj.ID);
                        break;
                    case 3:
                        var ncrsiObj = (NCR_SI)obj;
                        wfDetailObj =
                            this.customizeWorkflowDetailService.GetByCurrentStepCustomizeFromObj(wfStepObj.ID,
                                ncrsiObj.ID);
                        break;
                }
            }

            if (wfDetailObj != null)
            {
                var assignWorkFlow = new ObjectAssignedWorkflow
                {
                    ID = Guid.NewGuid(),
                    WorkflowID = wfObj.ID,
                    WorkflowName = wfObj.Name,
                    ObjectWFDwtailId = wfDetailObj.ID,

                    CurrentWorkflowStepID = wfDetailObj.CurrentWorkflowStepID,
                    CurrentWorkflowStepName = wfDetailObj.CurrentWorkflowStepName,
                    NextWorkflowStepID = wfDetailObj.NextWorkflowStepID,
                    NextWorkflowStepName = wfDetailObj.NextWorkflowStepName,
                    RejectWorkflowStepID = wfDetailObj.RejectWorkflowStepID,
                    RejectWorkflowStepName = wfDetailObj.RejectWorkflowStepName,
                    IsComplete = false,
                    IsReject = false,
                    IsLeaf = wfStepObj.IsFirst.GetValueOrDefault(),
                    AssignedBy = UserSession.Current.User.Id,
                    CanReject = wfStepObj.CanReject,


                };

                switch (objType)
                {
                    case 1:
                        var docObj = (DocumentPackage)obj;
                        assignWorkFlow.ObjectID = docObj.ID;
                        assignWorkFlow.ObjectNumber = docObj.DocNo;
                        assignWorkFlow.ObjectTitle = docObj.DocTitle;
                        assignWorkFlow.ObjectProject = docObj.ProjectName;
                        assignWorkFlow.ObjectType = "Project's Document";
                        groupId = docObj.ID;
                        break;
                    //case 2:
                    //    var changeRequestObj = (ChangeRequest)obj;
                    //    assignWorkFlow.ObjectID = changeRequestObj.ID;
                    //    assignWorkFlow.ObjectNumber = changeRequestObj.Number;
                    //    assignWorkFlow.ObjectTitle = changeRequestObj.Description;
                    //    assignWorkFlow.ObjectProject = changeRequestObj.ProjectCode;
                    //    assignWorkFlow.ObjectType = "Change Request";
                    //    groupId = changeRequestObj.GroupId.GetValueOrDefault();

                    //    break;
                    //case 3:
                    //    var ncrsiObj = (NCR_SI)obj;
                    //    assignWorkFlow.ObjectID = ncrsiObj.ID;
                    //    assignWorkFlow.ObjectNumber = ncrsiObj.Number;
                    //    assignWorkFlow.ObjectTitle = ncrsiObj.Subject;
                    //    assignWorkFlow.ObjectProject = ncrsiObj.ProjectName;
                    //    assignWorkFlow.ObjectType = "NCR/SI";
                    //    groupId = ncrsiObj.GroupId.GetValueOrDefault();

                    //    break;
                }


                if (ObjAssignWF != null)
                {
                    assignWorkFlow.PreviousStepId = ObjAssignWF.ID;
                }

                var assignWorkflowId = this.objAssignedWfService.Insert(assignWorkFlow);
                if (assignWorkflowId != null)
                {
                    // Get actual deadline if workflow step detail use only working day
                    var actualDeadline = CurrenDate;
                    if (wfDetailObj.IsOnlyWorkingDay.GetValueOrDefault())
                    {
                        for (int i = 1; i <= wfDetailObj.Duration.GetValueOrDefault(); i++)
                        {
                            actualDeadline = this.GetNextWorkingDay(actualDeadline);
                        }
                    }
                    // -------------------------------------------------------------------------

                    // Get assign User List
                    var wfStepWorkingAssignUser = new List<User>();
                    var infoUserIds = wfDetailObj.InformationOnlyUserIDs != null
                        ? wfDetailObj.InformationOnlyUserIDs.Split(';').ToList()
                        : new List<string>();
                    var reviewUserIds = wfDetailObj.ReviewUserIds != null
                        ? wfDetailObj.ReviewUserIds.Split(';').ToList()
                        : new List<string>();
                    var consolidateUserIds = wfDetailObj.ConsolidateUserIds != null
                        ? wfDetailObj.ConsolidateUserIds.Split(';').ToList()
                        : new List<string>();
                    var approveUserIds = wfDetailObj.ApproveUserIds != null
                        ? wfDetailObj.ApproveUserIds.Split(';').ToList()
                        : new List<string>();

                    var matrixList =
                        //wfDetailObj.DistributionMatrixIDs.Split(';')
                        //    .Where(t => !string.IsNullOrEmpty(t))
                        //    .Select(t => this.matrixService.GetById(Convert.ToInt32(t)));
                        this.matrixService.GetAllByList(wfDetailObj.DistributionMatrixIDs.Split(';')
                            .Where(t => !string.IsNullOrEmpty(t))
                            .Select(t => Convert.ToInt32(t)).ToList());
                    foreach (var matrix in matrixList)
                    {
                        var matrixDetail = this.matrixDetailService.GetAllByDM(matrix.ID);
                        var matrixDetailList = matrixDetail.Where(t => t.DocId == groupId).ToList();
                        var acceptAction = wfStepObj.ActionApplyCode.Split(';').Where(t => !string.IsNullOrEmpty(t)).ToList();
                        var matrixDetailValid = matrixDetailList.Where(t => acceptAction.Contains(t.ActionTypeName)).ToList();

                        reviewUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 2).Select(t => t.UserId.ToString()));
                        consolidateUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 3).Select(t => t.UserId.ToString()));
                        approveUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 4).Select(t => t.UserId.ToString()));
                        infoUserIds.AddRange(matrixDetailValid.Where(t => t.ActionTypeId == 1).Select(t => t.UserId.ToString()));
                    }

                    foreach (
                        var userId in
                            consolidateUserIds.Distinct()
                                .Where(t => !string.IsNullOrEmpty(t))
                                .Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null && !wfStepWorkingAssignUser.Where(t => t.Id == userObj.Id && t.ActionTypeId == 3).Any())
                        {
                            userObj.ActionTypeId = 3;
                            userObj.ActionTypeName = "C - Consolicate";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }

                    foreach (
                        var userId in
                            reviewUserIds.Distinct()
                                .Where(t => !string.IsNullOrEmpty(t))
                                .Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null && !wfStepWorkingAssignUser.Where(t => t.Id == userObj.Id && t.ActionTypeId == 2).Any())
                        {
                            userObj.ActionTypeId = 2;
                            userObj.ActionTypeName = "R - Review";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }

                    foreach (
                        var userId in
                            approveUserIds.Distinct()
                                .Where(t => !string.IsNullOrEmpty(t))
                                .Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null && !wfStepWorkingAssignUser.Where(t => t.Id == userObj.Id && t.ActionTypeId == 4).Any())
                        {
                            userObj.ActionTypeId = 4;
                            userObj.ActionTypeName = "A - Approve";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }

                    foreach (
                        var userId in
                            infoUserIds.Distinct().Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)))
                    {
                        var userObj = this.userService.GetByID(userId);
                        if (userObj != null && !wfStepWorkingAssignUser.Where(t => t.Id == userObj.Id && t.ActionTypeId == 1).Any())
                        {
                            userObj.ActionTypeId = 1;
                            userObj.ActionTypeName = "I - For Information";
                            wfStepWorkingAssignUser.Add(userObj);
                        }
                    }
                    // ---------------------------------------------------------------------------
                    var assignWorkingUserInfor = new ObjectAssignedUser();
                    // Create assign user info
                    foreach (var user in wfStepWorkingAssignUser)
                    {
                        var assignWorkingUser = new ObjectAssignedUser
                        {
                            ID = Guid.NewGuid(),
                            ObjectAssignedWorkflowID = assignWorkflowId,
                            UserID = user.Id,
                            UserFullName = user.FullName,
                            ReceivedDate = CurrenDate,
                            PlanCompleteDate =
                                wfDetailObj.IsOnlyWorkingDay.GetValueOrDefault()
                                    ? actualDeadline
                                    : DateTime.Now.AddDays(wfDetailObj.Duration.GetValueOrDefault()),
                            IsOverDue = false,
                            IsComplete = user.ActionTypeId == 1,// && wfDetailObj.IsFirst.GetValueOrDefault(),
                            IsReject = false,
                            AssignedBy = UserSession.Current.User.Id,
                            WorkflowId = wfObj.ID,
                            WorkflowName = wfObj.Name,
                            CurrentWorkflowStepName = wfStepObj.Name,
                            CurrentWorkflowStepId = wfStepObj.ID,
                            CanReject = wfStepObj.CanReject,
                            IsCanCreateOutgoingTrans = wfDetailObj.IsCanCreateOutgoingTrans,
                            IsFinal = wfDetailObj.NextWorkflowStepID == 0,
                            ActionTypeId = user.ActionTypeId,
                            ActionTypeName = user.ActionTypeName,
                            WorkingStatus = string.Empty,
                            //Status = (user.ActionTypeId == 1 && wfDetailObj.IsFirst.GetValueOrDefault()) ? "SO" : wfDetailObj.IsFirst.GetValueOrDefault() ? "RS" : "NR",

                            IsMainWorkflow = true,
                            IsReassign = false,
                            IsLeaf = wfStepObj.IsFirst.GetValueOrDefault()
                        };

                        switch (objType)
                        {
                            case 1:
                                var docObj = (DocumentPackage)obj;
                                assignWorkingUser.ObjectID = docObj.ID;
                                assignWorkingUser.ObjectNumber = docObj.DocNo;
                                assignWorkingUser.ObjectTitle = docObj.DocTitle;
                                assignWorkingUser.ObjectProject = docObj.ProjectName;
                                assignWorkingUser.ObjectProjectId = docObj.ProjectId;
                                assignWorkingUser.Revision = docObj.RevisionName;
                               // assignWorkingUser.Categoryid = docObj.CategoryId;
                                assignWorkingUser.ObjectType = "Project's Document";
                                assignWorkingUser.ObjectTypeId = 1;
                                break;
                            //case 2:
                            //    var changeRequestObj = (ChangeRequest)obj;
                            //    assignWorkingUser.ObjectID = changeRequestObj.ID;
                            //    assignWorkingUser.ObjectNumber = changeRequestObj.Number;
                            //    assignWorkingUser.ObjectTitle = changeRequestObj.Description;
                            //    assignWorkingUser.ObjectProject = changeRequestObj.ProjectCode;
                            //    assignWorkingUser.ObjectProjectId = changeRequestObj.ProjectId;
                            //    assignWorkingUser.Categoryid = 0;
                            //    assignWorkingUser.ObjectType = "Change Request";
                            //    assignWorkingUser.ObjectTypeId = 2;
                            //    break;
                            //case 3:
                            //    var ncrsiObj = (NCR_SI)obj;
                            //    assignWorkingUser.ObjectID = ncrsiObj.ID;
                            //    assignWorkingUser.ObjectNumber = ncrsiObj.Number;
                            //    assignWorkingUser.ObjectTitle = ncrsiObj.Subject;
                            //    assignWorkingUser.ObjectProject = ncrsiObj.ProjectName;
                            //    assignWorkingUser.ObjectProjectId = ncrsiObj.ProjectId;
                            //    assignWorkingUser.Categoryid = 0;
                            //    assignWorkingUser.ObjectType = "NCR/SI";
                            //    assignWorkingUser.ObjectTypeId = 3;
                            //    break;
                        }

                        objAssignedUserService.Insert(assignWorkingUser);
                        assignWorkingUserInfor = assignWorkingUser;
                        listemailto.Add(user.Email);
                    }
                   
                    // Assign to re-assign user of workflow when can't find working user
                    if (wfStepWorkingAssignUser.Count == 0 && wfObj.Re_assignUserId != null)
                    {
                        var assignWorkingUser = new ObjectAssignedUser
                        {
                            ID = Guid.NewGuid(),
                            ObjectAssignedWorkflowID = assignWorkflowId,
                            UserID = wfObj.Re_assignUserId,
                            UserFullName = wfObj.Re_assignUserName,
                            ReceivedDate = CurrenDate,
                            PlanCompleteDate =
                                wfDetailObj.IsOnlyWorkingDay.GetValueOrDefault()
                                    ? actualDeadline
                                    : DateTime.Now.AddDays(wfDetailObj.Duration.GetValueOrDefault()),
                            IsOverDue = false,
                            IsComplete = false,
                            IsReject = false,
                            AssignedBy = UserSession.Current.User.Id,
                            WorkflowId = wfObj.ID,
                            WorkflowName = wfObj.Name,
                            CurrentWorkflowStepName = wfStepObj.Name,
                            CurrentWorkflowStepId = wfStepObj.ID,
                            CanReject = wfStepObj.CanReject,
                            IsCanCreateOutgoingTrans = wfDetailObj.IsCanCreateOutgoingTrans,
                            IsFinal = wfDetailObj.NextWorkflowStepID == 0,
                            ActionTypeId = 5,
                            ActionTypeName = "Can't find working user. Re-assign to another user.",
                            WorkingStatus = string.Empty,
                            //Status = "RS",
                            IsMainWorkflow = true,
                            IsReassign = true
                        };

                        switch (objType)
                        {
                            case 1:
                                var docObj = (DocumentPackage)obj;
                                assignWorkingUser.ObjectID = docObj.ID;
                                assignWorkingUser.ObjectNumber = docObj.DocNo;
                                assignWorkingUser.ObjectTitle = docObj.DocTitle;
                                assignWorkingUser.ObjectProject = docObj.ProjectName;
                                assignWorkingUser.Revision = docObj.RevisionName;
                                break;
                            //case 2:
                            //    var changeRequestObj = (ChangeRequest)obj;
                            //    assignWorkingUser.ObjectID = changeRequestObj.ID;
                            //    assignWorkingUser.ObjectNumber = changeRequestObj.Number;
                            //    assignWorkingUser.ObjectTitle = changeRequestObj.Description;
                            //    assignWorkingUser.ObjectProject = changeRequestObj.ProjectCode;
                            //    break;
                            //case 3:
                            //    var ncrsiObj = (NCR_SI)obj;
                            //    assignWorkingUser.ObjectID = ncrsiObj.ID;
                            //    assignWorkingUser.ObjectNumber = ncrsiObj.Number;
                            //    assignWorkingUser.ObjectTitle = ncrsiObj.Description;
                            //    assignWorkingUser.ObjectProject = ncrsiObj.ProjectName;
                            //    break;
                        }

                        objAssignedUserService.Insert(assignWorkingUser);
                        listemailto.Add(this.userService.GetByID(wfObj.Re_assignUserId.GetValueOrDefault()).Email);
                        //if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]))
                        //    this.SendNotification(assignWorkingUser,
                        //        this.userService.GetByID(wfObj.Re_assignUserId.GetValueOrDefault()), infoUserIds);
                    }
                    // ----------------------------------------------------------------------------------

                    // Send notification for Info & Management user
                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]) &&
                          wfStepObj.IsFirst.GetValueOrDefault())
                    {
                        this.SendNotification(assignWorkingUserInfor, listemailto, infoUserIds);
                    }
                    //if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]) && infoUserIds.Count > 0 &&
                    //    wfDetailObj.IsFirst.GetValueOrDefault())
                    //{
                    //    this.SendNotificationInfor(assignWorkingUserInfor,
                    //        this.userService.GetByID(wfObj.Re_assignUserId.GetValueOrDefault()), infoUserIds);
                    //}

                    //var wfNextStepObj = this.wfStepService.GetById(wfDetailObj.NextWorkflowStepID.GetValueOrDefault());
                    //if (wfNextStepObj != null)
                    //{
                    //    this.ProcessWorkflow(wfNextStepObj, wfObj, obj, assignWorkFlow, actualDeadline, objType);
                    //}
                }
            }
        }
        private void GenerateCRSfile(DocumentPackage docObj)
        {
            var targetFolder = "../../DocumentLibrary/ProjectDocs";
            var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                    + "/DocumentLibrary/ProjectDocs";

            var doclistFile = this.documentAttachFileService.GetAllDocId(docObj.ID).Where(t => t.TypeId == 5 && t.IsDefualtResponseContractor.GetValueOrDefault());
            var fileobj = doclistFile.FirstOrDefault();
            if (doclistFile.Any())
            { var rootPath = Server.MapPath(fileobj.FilePath);
                if (File.Exists(rootPath))
                {
                    var file = new FileInfo(rootPath);
                    var workbook = new Workbook();
                    workbook.Open(rootPath);

                    var dataSheet = workbook.Worksheets[0];
                    dataSheet.Cells["L5"].PutValue(String.IsNullOrEmpty(docObj.IncomingTransNo) ? string.Empty : docObj.IncomingTransNo);
                    var filenamesave = docObj.ID.ToString() + DateTime.Now.ToBinary() + ".xlsm";
               

                    //// Path file to download from server
                    //var serverFilePath = serverFolder + "/" + filenamesave;

                    var serverDocFileName = "CRS_" + docObj.DocNo + "_" + docObj.DocTitle + "_" + docObj.RevisionName + ".xlsm";

                   
                    var saveFilePath = Path.Combine(Server.MapPath(targetFolder), filenamesave);

                    // Path file to download from server
                    var serverFilePath = serverFolder + "/" + filenamesave;
                    workbook.Save(saveFilePath);
                    // File.Delete(rootPath);
                    //fileobj.FilePath = serverFilePath;
                    //this.documentAttachFileService.Update(fileobj);
                    var attachFile = new PVGASDocumentAttachFile()
                    {
                        ID = Guid.NewGuid(),
                        ProjectDocumentId = docObj.ID,
                        FileName = serverDocFileName,
                        Extension = ".xlsm",
                        FilePath = serverFilePath,
                        ExtensionIcon = "~/images/excelfile.png",
                        FileSize =(double)file.Length/1024,
                        TypeId = 4,
                        //IsDefualtResponseContractor=true,
                        TypeName = "Comment Sheet",
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedByName = UserSession.Current.User.UserNameWithFullName,
                        CreatedDate = DateTime.Now
                    };

                    this.documentAttachFileService.Insert(attachFile);
                }

            }
            else
            {
                var rootPath = Server.MapPath("../../Exports") + @"\Template\";
                var workbook = new Workbook();
                workbook.Open(rootPath + @"CRS_New.xlsm");

                var dataSheet = workbook.Worksheets[0];
                dataSheet.Cells["D5"].PutValue(docObj.DocTitle);
                dataSheet.Cells["G5"].PutValue(docObj.DocNo);
                dataSheet.Cells["J5"].PutValue(String.IsNullOrEmpty(docObj.IncomingTransNo) ? string.Empty : docObj.IncomingTransNo);
                dataSheet.Cells["L5"].PutValue(docObj.RevisionName);

                var serverDocFileName = "CRS_" + docObj.DocNo + "_" + docObj.DocTitle + "_" + docObj.RevisionName + ".xlsm";

                var filenamesave = docObj.ID.ToString() + DateTime.Now.ToBinary() + ".xlsm";
                // Path file to save on server disc
                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), filenamesave);

                // Path file to download from server
                var serverFilePath = serverFolder + "/" + filenamesave;

                workbook.Save(saveFilePath);

                var attachFile = new PVGASDocumentAttachFile()
                {
                    ID = Guid.NewGuid(),
                    ProjectDocumentId = docObj.ID,
                    FileName = serverDocFileName,
                    Extension = ".xlsm",
                    FilePath = serverFilePath,
                    ExtensionIcon = "~/images/excelfile.png",
                     FileSize = 22.0,
                    //IsDefualtResponseContractor = true,
                    TypeId = 4,
                    TypeName = "Comment Sheet",
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedByName = UserSession.Current.User.UserNameWithFullName,
                    CreatedDate = DateTime.Now
                };

                this.documentAttachFileService.Insert(attachFile);
            }

            }
            private void SendNotification(ObjectAssignedUser assignWorkingUser, List<string> assignUserObj, List<string> infoUserIds)
        {try
            {
                // Implement send mail function
                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                   // UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                   // EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                   // TargetName = "STARTTLS/smtp.gmail.com",
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    // Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    Credentials = new NetworkCredential(UserSession.Current.User.Email, "")
                };


                var message = new MailMessage();
                message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                message.BodyEncoding = new UTF8Encoding();
                message.IsBodyHtml = true;

                message.Subject = "ASSIGNMENT: " + assignWorkingUser.ObjectNumber + ", " + assignWorkingUser.ObjectTitle + ", " + assignWorkingUser.CurrentWorkflowStepName + ", " + assignWorkingUser.PlanCompleteDate.GetValueOrDefault().ToString("dd/MM/yyyy");

                // Generate email body
                var bodyContent = @"<<<< FOR ACTION >>>>
                             Please action by due date for " + assignWorkingUser.ObjectType + " \"" + assignWorkingUser.ObjectNumber + @""":<br/>
                                <table border='1' cellspacing='0'>
	                                <tr>
		                                <td style=""width: 200px;"">Current Workflow</td>
                                        <td style=""width: 500px;"">" + assignWorkingUser.WorkflowName + @"</td>
	                                </tr>
                                    <tr>
		                                <td>Current Workflow Step</td>
                                        <td>" + assignWorkingUser.CurrentWorkflowStepName + @"</td>
	                                </tr>
                                    <tr>
		                                <td>Title</td>
                                        <td>" + assignWorkingUser.ObjectTitle + @"</td>
	                                </tr>

                                      <tr>
		                                <td>Assign From User</td>
                                        <td>" + this.userService.GetByID(assignWorkingUser.AssignedBy.GetValueOrDefault()).FullNameWithDeptPosition + @"</td>
	                                </tr>
                                    <tr>
		                                <td>Received Date</td>
                                        <td>" + assignWorkingUser.ReceivedDate.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
	                                </tr>
                                    <tr>
		                                <td>Due Date</td>
                                        <td>" + assignWorkingUser.PlanCompleteDate.GetValueOrDefault().ToString("dd/MM/yyyy HH:mm:ss") + @"</td>
	                                </tr>
                                    
                                </table></br>
                                   <br/>
                                   <br/>
                                    &nbsp;Click on the this link to access the PEDMS system&nbsp;:&nbsp; <a href='" + ConfigurationSettings.AppSettings.Get("WebAddress")
                                           + "/ToDoListPage.aspx?DocNo=" + assignWorkingUser.ObjectNumber + "'>" + ConfigurationSettings.AppSettings.Get("WebAddress") + @"</a>
                                    </br>
                         &nbsp;&nbsp;&nbsp; EDMS WORKFLOW NOTIFICATION </br>
                        [THIS IS SYSTEM GENERATED NOTIFICATION PLEASE DO NOT REPLY]

                                ";
                message.Body = bodyContent;
                foreach (var email in assignUserObj.Distinct().Where(t => !string.IsNullOrEmpty(t)))
                {

                    message.To.Add(email);

                }
                foreach (var email in infoUserIds.Distinct().Where(t => !string.IsNullOrEmpty(t)))
                {

                    message.CC.Add(email);

                }
                smtpClient.Send(message);
            }
            catch(Exception e) { }
        }
       private void SendNotificationInfor(ObjectAssignedUser assignWorkingUser, User assignUserObj, List<string> infoUserIds)
        {
       try
            {
                var smtpClient = new SmtpClient
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    // UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    // EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    Host = ConfigurationManager.AppSettings["Host"],
                    // TargetName = "STARTTLS/smtp.gmail.com",
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    // Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    Credentials = new NetworkCredential(UserSession.Current.User.Email, "")
                };


                var message = new MailMessage();
                message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                message.BodyEncoding = new UTF8Encoding();
        message.IsBodyHtml = true;

                message.Subject = "INFORMATION: " + assignWorkingUser.ObjectNumber + ", " + assignWorkingUser.ObjectTitle + ", " + assignWorkingUser.CurrentWorkflowStepName + ", " + assignWorkingUser.PlanCompleteDate.GetValueOrDefault().ToString("dd/MM/yyyy");

        // Generate email body
        var bodyContent = @"<<<< FOR INFORMATION >>>>
                             Please check information by due date for "  + assignWorkingUser.ObjectNumber + @""":<br/>
                                <table border='1' cellspacing='0'>
	                                <tr>
		                                <td style=""width: 200px;"">Current Workflow</td>
                                        <td style=""width: 500px;"">" + assignWorkingUser.WorkflowName + @"</td>
	                                </tr>
                                    <tr>
		                                <td>Current Workflow Step</td>
                                        <td>" + assignWorkingUser.CurrentWorkflowStepName + @"</td>
	                                </tr>
                                     <tr>
		                                <td>Document No</td>
                                        <td>" + assignWorkingUser.ObjectNumber + @"</td>
	                                </tr>
                                    <tr>
		                                <td>Title</td>
                                        <td>" + assignWorkingUser.ObjectTitle + @"</td>
	                                </tr>
                                     <tr>
		                                <td>Revision</td>
                                        <td>" + assignWorkingUser.Revision + @"</td>
	                                </tr>
                                      <tr>
		                                <td>Assign From User</td>
                                        <td>" + this.userService.GetByID(assignWorkingUser.AssignedBy.GetValueOrDefault()).FullNameWithDeptPosition + @"</td>
	                                </tr>
                                </table></br>
                                   <br/>

                                    &nbsp;Click on the this link to access the PEDMS system&nbsp;:&nbsp; <a href='" + ConfigurationSettings.AppSettings.Get("WebAddress")
                                           + "/ProjectDocumentsList.aspx'>" + ConfigurationSettings.AppSettings.Get("WebAddress") + @"</a>
                                    </br>
                         &nbsp;&nbsp;&nbsp; EDMS WORKFLOW NOTIFICATION </br>
                        [THIS IS SYSTEM GENERATED NOTIFICATION PLEASE DO NOT REPLY]

                                ";
                message.Body = bodyContent;
                foreach (var userId in infoUserIds.Distinct().Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)))
                {
                    var userObj = this.userService.GetByID(userId);
                    if (userObj != null)
                    {
                        if (!string.IsNullOrEmpty(userObj.Email)) message.To.Add(userObj.Email);
                    }
                }
                smtpClient.Send(message);
            }
            catch { }
        }

        private bool IsHoliday(DateTime date)
        {
            return holidays.Contains(date);
        }
        private bool IsWeekEnd(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        private DateTime GetNextWorkingDay(DateTime date)
        {
            do
            {
                date = date.AddDays(1);
            }
            while (IsHoliday(date) || IsWeekEnd(date));

            return date;
        }

        private void LoadComboData()
        {
            var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
            var objType = Convert.ToInt32(this.Request.QueryString["type"]);
            var wfList = wfService.GetAllByProject(projectId, objType);
            ddlWorkflow.DataSource = wfList;
            ddlWorkflow.DataTextField = "Name";
            ddlWorkflow.DataValueField = "ID";
            ddlWorkflow.DataBind();
            if (this.ddlWorkflow.SelectedItem != null)
            {
                var wfId = Convert.ToInt32(this.ddlWorkflow.SelectedValue);
                var firstStep = this.wfStepService.GetAllByWorkflow(wfId).FirstOrDefault(t => t.IsFirst.GetValueOrDefault());
                if (firstStep != null)
                {
                    this.txtFirstStep.Text = firstStep.Name;
                }
            }
        }

        protected void ddlWorkflow_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddlWorkflow.SelectedItem != null)
            {
                var wfId = Convert.ToInt32(this.ddlWorkflow.SelectedValue);
                var firstStep = this.wfStepService.GetAllByWorkflow(wfId).FirstOrDefault(t => t.IsFirst.GetValueOrDefault());
                if (firstStep != null)
                {
                    this.txtFirstStep.Text = firstStep.Name;
                }
            }
        }

        protected void cbCustomizeWorkflow_OnCheckedChanged(object sender, EventArgs e)
        {
            this.btnManual.Visible = this.cbCustomizeWorkflow.Checked;
        }
    }
}