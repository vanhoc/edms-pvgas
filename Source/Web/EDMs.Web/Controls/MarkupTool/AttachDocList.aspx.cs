﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Data.Entities;
using EDMs.Web.Controls.Document;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using iTextSharp.awt.geom;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.MarkupTool
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class AttachDocList : Page
    {
        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentPackageService pecc2DocumentService;

        private readonly PVGASDocumentAttachFileService attachFileService;

        private readonly DocumentCodeServices documentCodeServices;

        private readonly ChangeRequestAttachFileService _ChangeRepuestAttachFileService;
        private readonly NCR_SIAttachFileService _NCR_SIAttachFileService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public AttachDocList()
        {
            this.pecc2DocumentService = new   DocumentPackageService();
            this.attachFileService = new   PVGASDocumentAttachFileService();
            this.documentCodeServices = new DocumentCodeServices();
            this._ChangeRepuestAttachFileService = new ChangeRequestAttachFileService();
            this._NCR_SIAttachFileService = new NCR_SIAttachFileService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.lblUserId.Value = UserSession.Current.User.Id.ToString();
                var docId = new Guid(this.Request.QueryString["docId"]);
                if (!string.IsNullOrEmpty(this.Request.QueryString["actionType"]))
                {
                    this.btnConsolidate.Visible = this.Request.QueryString["actionType"] == "3";
                    this.btnExportCRS.Visible = this.Request.QueryString["actionType"] == "3"&& !this.attachFileService.GetAllDocId(docId).Any(t => t.TypeId == 4 && t.IsDefualtResponseContractor.GetValueOrDefault()); ;
                    }
                //if (!string.IsNullOrEmpty(Request.QueryString["ObjectType"]))
                //{
                //    var ObjectTypeID = Request.QueryString["ObjectType"];
                //    if (ObjectTypeID != "1")
                //    {
                //        this.btnConsolidate.Visible = false;
                //       
                //        this.cbConsolidateFile.Enabled = false;
                //        this.cbCRS.Enabled = false;
                //        this.btnSave.Visible = false;
                //    }
                //    }
            }
        }


        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = new Guid(this.Request.QueryString["docId"]);

               
                //if (!string.IsNullOrEmpty(Request.QueryString["ObjectType"]))
                //{ var ObjectTypeID = Request.QueryString["ObjectType"];
                //    if (ObjectTypeID == "3")
                //    {
                //      var  attachList =  this._NCR_SIAttachFileService.GetByNCRSI(docId).Where(t => t.TypeId == 1 && t.Extension.ToLower().Contains("pdf"));
                //        this.grdDocument.DataSource = attachList;
                //    }
                //    else if (ObjectTypeID == "2")
                //    {
                //        var attachList = this._ChangeRepuestAttachFileService.GetByChangeRequest(docId).Where(t => (t.TypeId == null || t.TypeId == 1) && t.Extension.ToLower().Contains("pdf"));
                //        this.grdDocument.DataSource = attachList;
                //    }
                //    else
                //    {
                        var attachList = this.attachFileService.GetAllDocId(docId).Where(t => t.TypeId == 1 && t.Extension.ToLower().Contains("pdf"));
                        this.grdDocument.DataSource = attachList;
                  //  }

               // }
                    
            }
            else
            {
                this.grdDocument.DataSource = new List<PVGASTransmittalAttachFileService>();
            }
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("Checkout"))
            {
                var crsFileId = new Guid(e.Argument.Split('_')[1]);
                var crsFile = this.attachFileService.GetById(crsFileId);
                if (crsFile != null && !crsFile.IsCheckOut.GetValueOrDefault())
                {
                    crsFile.IsCheckOut = true;
                    crsFile.CheckoutBy = UserSession.Current.User.Id;
                    crsFile.CheckoutByName = UserSession.Current.User.FullName;
                    crsFile.CheckinDate = DateTime.Now;
                    this.attachFileService.Update(crsFile);
                }

                this.grdCRSFile.Rebind();
            }
        }

        protected void grdAttachMarkupCommentFile_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = new Guid(this.Request.QueryString["docId"]);

             
                //if (!string.IsNullOrEmpty(Request.QueryString["ObjectType"]))
                //{
                //    var ObjectTypeID = Request.QueryString["ObjectType"];
                //    if (ObjectTypeID == "3")
                //    {
                //        var attachList = this._NCR_SIAttachFileService.GetByNCRSI(docId).Where(t => t.TypeId != 1 && t.Extension.ToLower().Contains("pdf"));
                //        this.grdDocument.DataSource = attachList;
                //    }
                //    else if (ObjectTypeID == "2")
                //    {
                //        var attachList = this._ChangeRepuestAttachFileService.GetByChangeRequest(docId).Where(t => (t.TypeId != null && t.TypeId != 1) && t.Extension.ToLower().Contains("pdf"));
                //        this.grdDocument.DataSource = attachList;
                //    }
                //    else
                //    {
                        var attachList = this.attachFileService.GetAllDocId(docId).Where(t => !t.IsOnlyMarkupPage.GetValueOrDefault() && t.TypeId != 4 && t.TypeId != 1 && t.TypeId != 5).OrderBy(t => t.CreatedDate);
                      //  this.btnExportCRS.Visible = attachList.Any(t => t.TypeId == 3);
                        this.grdAttachMarkupCommentFile.DataSource = attachList;
                   // }

               // }
            }
            else
            {
                this.grdAttachMarkupCommentFile.DataSource = new List<PVGASTransmittalAttachFileService>();
            }

            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                Guid docId = new Guid(this.Request.QueryString["docId"]);
                var docObj = this.pecc2DocumentService.GetById(docId);

                var flag = false;
                var targetFolder = "../../DocumentLibrary/MarkupFile";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                    + "/DocumentLibrary/MarkupFile";


                var listUpload = docuploader.UploadedFiles;

                

                if (listUpload.Count > 0)
                {
                    foreach (UploadedFile docFile in listUpload)
                    {
                        var IsUpdateFileConsolidate = false;
                        PVGASDocumentAttachFile fileconsolidate= new PVGASDocumentAttachFile();
                        var docFileName = docFile.FileName;
                        var fileExt = docFile.GetExtension();
                       // var docId = new Guid(this.Request.QueryString["docId"]);
                        var docObjAttach= this.attachFileService.GetAllDocId(docId).Where(t => t.TypeId == 1 && t.Extension.ToLower().Contains("pdf")).FirstOrDefault();
                        var serverDocFileName = docObjAttach.FileName; // docFileName;

                        if (this.cbMarkupFile.Checked)
                        {
                            //serverDocFileName = docFile.GetNameWithoutExtension() + "_Markup_" + UserSession.Current.User.Username + fileExt;
                            serverDocFileName = docFile.GetNameWithoutExtension() + "_Markup_" + UserSession.Current.User.Username + fileExt;
                            var listmarkup = this.attachFileService.GetByUserUploadMarkupFile(UserSession.Current.User.Id, docId);
                            if (listmarkup.Count > 0)
                            {
                                serverDocFileName = docFile.GetNameWithoutExtension() + "_Markup_" + listmarkup.Count + "_" + UserSession.Current.User.Username + fileExt;
                            }
                        }
                        else
                        {
                            serverDocFileName = docFile.GetNameWithoutExtension() + "_ConsolidateMarkup_" + UserSession.Current.User.Username + fileExt;
                            fileconsolidate = this.attachFileService.GetByUserUploadConsolidateFile(UserSession.Current.User.Id);
                            if (fileconsolidate != null)
                            {
                                serverDocFileName = fileconsolidate.FileName;
                                IsUpdateFileConsolidate = true;
                                if (File.Exists(fileconsolidate.FilePath)) File.Delete(fileconsolidate.FilePath);
                            }
                        }

                        // Path file to save on server disc
                        var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);

                        // Path file to download from server
                        var serverFilePath = serverFolder + "/" + serverDocFileName;

                        docFile.SaveAs(saveFilePath, true);

                        var attachFile = new PVGASDocumentAttachFile()
                        {
                            ID = Guid.NewGuid(),
                            ProjectDocumentId = docId,
                            FileName = serverDocFileName,
                            Extension = fileExt,
                            FilePath = serverFilePath,
                            ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                            FileSize = (double)docFile.ContentLength / 1024,
                            TypeId = this.cbMarkupFile.Checked 
                                            ? 2 
                                            : this.cbConsolidateFile.Checked
                                                ? 3
                                                : 4,
                            TypeName = this.cbMarkupFile.Checked 
                                            ? "Document Markup/Comment Files" 
                                            : this.cbConsolidateFile.Checked
                                                ? "Document Markup/Comment Consolidate Files"
                                                : "Comment Response Sheet (CRS)",
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedByName = UserSession.Current.User.UserNameWithFullName,
                            CreatedDate = DateTime.Now
                        };
                        if (IsUpdateFileConsolidate)
                        {
                            fileconsolidate.FilePath = serverFilePath;
                            fileconsolidate.FileSize = (double)docFile.ContentLength / 1024;
                            fileconsolidate.CreatedDate = DateTime.Now;

                        }
                        else
                        {
                             this.attachFileService.Insert(attachFile);
                        }
                       

                        //if (this.cbCRS.Checked)
                        //{
                        //    if (File.Exists(saveFilePath))
                        //    {
                        //        var workbook = new Workbook(saveFilePath);
                        //        var dataSheet = workbook.Worksheets[0];
                        //        var reviewStatusCode = dataSheet.Cells["E7"].Value.ToString()
                        //                .Replace("Document Review Status: ", string.Empty)
                        //                .Replace(Environment.NewLine, string.Empty)
                        //                .Trim();
                        //        var reviewStatusObj = this.documentCodeServices.GetByCode(reviewStatusCode);
                        //        if (reviewStatusObj != null)
                        //        {
                        //            docObj.DocReviewStatusId = reviewStatusObj.ID;
                        //            docObj.DocReviewStatusCode = reviewStatusObj.Code;
                        //        }
                        //    }
                        //}
                        
                        /*
                        // Extract only comment page
                        if (this.cbConsolidateFile.Checked && fileExt.ToLower() == ".pdf")
                        {
                            var outputOnlyMarkupPageFileName = docFile.GetNameWithoutExtension() + "_ConsolidateOnlyMarkupPages_" + UserSession.Current.User.Username + fileExt;
                            var outputOnlyMarkupPageFilePath = Server.MapPath("../../DocumentLibrary/MarkupFile/" + outputOnlyMarkupPageFileName);
                            PdfImportedPage importedPage = null;
                            var pdfReader = new PdfReader(saveFilePath);

                            var sourceDocument = new iTextSharp.text.Document(pdfReader.GetPageSizeWithRotation(1));
                            var pdfCopyProvider = new PdfCopy(sourceDocument, new FileStream(outputOnlyMarkupPageFilePath, FileMode.Create));
                            sourceDocument.Open();
                            for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                            {
                                PdfArray array = pdfReader.GetPageN(i).GetAsArray(PdfName.ANNOTS);
                                if (array != null)
                                {
                                    importedPage = pdfCopyProvider.GetImportedPage(pdfReader, i);
                                    pdfCopyProvider.AddPage(importedPage);
                                }
                            }

                            sourceDocument.Close();
                            pdfReader.Close();

                            // Add to attach file
                            var fileInfo = new FileInfo(outputOnlyMarkupPageFilePath);
                            if (fileInfo.Exists)
                            {
                                var onlyMarkupPageAttachFile = new PVGASDocumentAttachFile()
                                {
                                    ID = Guid.NewGuid(),
                                    ProjectDocumentId = docId,
                                    FileName = outputOnlyMarkupPageFileName,
                                    Extension = fileExt,
                                    FilePath = "/DocumentLibrary/MarkupFile/" + outputOnlyMarkupPageFileName,
                                    ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                                    FileSize = (double)fileInfo.Length / 1024,
                                    TypeId = this.cbMarkupFile.Checked ? 2 : 3,
                                    TypeName = this.cbMarkupFile.Checked ? "Document Markup/Comment Files" : "Document Markup/Comment Consolidate Files",
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedByName = UserSession.Current.User.UserNameWithFullName,
                                    CreatedDate = DateTime.Now,
                                    IsOnlyMarkupPage = true
                                };

                                this.attachFileService.Insert(onlyMarkupPageAttachFile);
                            }
                            // -------------------------------------------------------------------------------------------------------------------
                        }
                        */
                        // -------------------------------------------------------------------------------------------------


                    }

                    //docObj.IsHasAttachFile = this.attachFileService.GetAllDocId(docId).Any();
                    this.pecc2DocumentService.Update(docObj);
                }
            }

            this.docuploader.UploadedFiles.Clear();
            this.grdAttachMarkupCommentFile.Rebind();
        }

        protected void grdAttachMarkupCommentFile_OnDeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            Guid docId = new Guid(item.GetDataKeyValue("ID").ToString());
            this.attachFileService.Delete(docId);
            this.grdAttachMarkupCommentFile.Rebind();
        }

        protected void btnConsolidate_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                Guid docId = new Guid(this.Request.QueryString["docId"]);
                var docObj = this.pecc2DocumentService.GetById(docId);
                var flag = false;
                var targetFolder = "../../DocumentLibrary/MarkupFile";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                    + "/DocumentLibrary/MarkupFile";

                var originalAttachDoc = this.attachFileService.GetAllDocId(docId).FirstOrDefault(t => t.TypeId == 1 && (t.Extension.ToLower() == "pdf" || t.Extension.ToLower() == ".pdf"));

                var markupfileList = this.attachFileService.GetAllDocId(docId).Where(t => t.TypeId == 2 && (t.Extension.ToLower() == "pdf" || t.Extension.ToLower() == ".pdf")).ToList();
                if (originalAttachDoc != null && markupfileList.Count > 0)
                {
                    var consolidateFile = new List<string>();

                    // Consolicate markup files
                    var originalFilePath = Server.MapPath("../.." + originalAttachDoc.FilePath);
                    // Split original file to files have same page size
                    var splitFiles = this.SplitSamePageSize(originalFilePath, targetFolder + "/Temp");
                    // -------------------------------------------------------------------------------------
                    var tempPageCount = 0;
                    var tempFileCount = 0;

                    // Process to consolidate on split file
                    foreach (var splitFile in splitFiles)
                    {

                        tempFileCount += 1;
                        var output = new MemoryStream();
                        var splitReader = new PdfReader(splitFile);
                        var document = new iTextSharp.text.Document(splitReader.GetPageSize(1));
                        var writer = PdfWriter.GetInstance(document, output);
                        writer.CloseStream = false;
                        document.Open();
                        var pageCount = splitReader.NumberOfPages;
                        for (var i = 0; i < pageCount; i++)
                        {
                            tempPageCount += 1;
                            var imp = writer.GetImportedPage(splitReader, (i + 1));
                            writer.DirectContent.AddTemplate(imp, new AffineTransform());

                            foreach (var markupFile in markupfileList)
                            {
                                var markupFilePath = Server.MapPath("../.." + markupFile.FilePath);
                                var markupReader = new PdfReader(markupFilePath);
                                var annots = markupReader.GetPageN(tempPageCount).GetAsArray(PdfName.ANNOTS);
                                var ussercomment = markupFile.CreatedByName.Substring(0, markupFile.CreatedByName.IndexOf('/'));
                                if (annots != null && annots.Size != 0)
                                {
                                    foreach (var a in annots)
                                    {
                                        var newannot = new PdfAnnotation(writer, new Rectangle(0, 0));
                                        var annotObj = (PdfDictionary)PdfReader.GetPdfObject(a);
                                        annotObj.Put(PdfName.T, new PdfString(ussercomment));
                                        newannot.PutAll(annotObj);
                                        writer.AddAnnotation(newannot);
                                    }
                                }
                            }

                            document.NewPage();
                        }

                        document.Close();
                        splitReader.Close();
                        output.Position = 0;
                        // ---------------------------------------------------------------------------------------------------------------

                        //Path file to save on server disc
                        var consolidateSplitSaveFilePath = Path.Combine(Server.MapPath(targetFolder), Guid.NewGuid().ToString()) + "_Split" + tempFileCount + "Consolidate.pdf"; ;
                        consolidateFile.Add(consolidateSplitSaveFilePath);
                        File.WriteAllBytes(consolidateSplitSaveFilePath, output.ToArray());
                    }

                    // Save Consolidate markup file
                    var consolidateFileName = docObj.DocNo + "_ConsolidateMarkup_" + UserSession.Current.User.Username + ".pdf";
                    // Path file to save on server disc
                    var consolidateSaveFilePath = Path.Combine(Server.MapPath(targetFolder), consolidateFileName);
                    // Merge consolidate on split files
                    this.MergePDF(consolidateFile, consolidateSaveFilePath);
                    // ------------------------------------------------------------------------------------

                    var fileInfo = new FileInfo(consolidateSaveFilePath);
                    if (fileInfo.Exists)
                    {
                        var attachFile = new PVGASDocumentAttachFile()
                        {
                            ID = Guid.NewGuid(),
                            ProjectDocumentId = docId,
                            FileName = consolidateFileName,
                            Extension = "pdf",
                            FilePath = "/DocumentLibrary/MarkupFile/" + consolidateFileName,
                            ExtensionIcon = "~/images/pdffile.png",
                            FileSize = (double)fileInfo.Length / 1024,
                            TypeId = 3,
                            TypeName = "Document Markup/Comment Consolidate Files",
                            CreatedBy = UserSession.Current.User.Id,
                            CreatedByName = UserSession.Current.User.UserNameWithFullName,
                            CreatedDate = DateTime.Now
                        };

                        this.attachFileService.Insert(attachFile);
                        this.grdAttachMarkupCommentFile.Rebind();
                    }

                    // -----------------------------------------------------------------------------------------------------------------
                    this.btnExportCRS.Visible = true;
                }

            }
        }
        private List<string> SplitSamePageSize(string filePath, string targetFolder)
        {
            var splitFile = new List<string>();

            //var originalFilePath = @"D:\01. Desktop\011. EDMS BSR - PECC2\Doc Test\15001-000-CN-0008-001-B.pdf";
            ////var originalFilePath = @"D:\01. Desktop\011. EDMS BSR - PECC2\Doc Test\15001-000-PP-602-B5.PDF";
            var originalReader = new PdfReader(filePath);

            // Consolicate markup files
            var output = new MemoryStream();
            var document = new iTextSharp.text.Document();
            var writer = new PdfCopy(document, output);
            writer.CloseStream = false;
            document.Open();
            var count = 0;
            var originalPageCount = originalReader.NumberOfPages;
            if (originalPageCount == 1)
            {
                splitFile.Add(filePath);
            }
            else
            {
                for (var i = 2; i <= originalPageCount; i++)
            {
                var currentPageSize = originalReader.GetPageSize(i);
                var prevPageSize = originalReader.GetPageSize(i - 1);
                if (i == 2)
                {
                    var imp = writer.GetImportedPage(originalReader, (i - 1));
                    writer.AddPage(imp);
                }

                if (!Equals(currentPageSize, prevPageSize))
                {
                    count += 1;
                    document.Close();
                    var filepath = Path.Combine(Server.MapPath(targetFolder), Guid.NewGuid().ToString()) + "_Split" + count + ".pdf";
                    splitFile.Add(filepath);
                    File.WriteAllBytes(filepath, output.ToArray());

                    output = new MemoryStream();
                    document = new iTextSharp.text.Document();
                    writer = new PdfCopy(document, output);
                    writer.CloseStream = false;
                    document.Open();

                    var imp = writer.GetImportedPage(originalReader, (i));
                    writer.AddPage(imp);
                }
                else
                {
                    var imp = writer.GetImportedPage(originalReader, (i));
                    writer.AddPage(imp);

                    if (i == originalReader.NumberOfPages)
                    {
                        count += 1;
                        document.Close();
                        originalReader.Close();

                        var filepath = Path.Combine(Server.MapPath(targetFolder), Guid.NewGuid().ToString()) + "_Split" + count + ".pdf";
                        splitFile.Add(filepath);
                        File.WriteAllBytes(filepath, output.ToArray());
                    }
                }
            }
        }
            return splitFile;
        }
        private void MergePDF(List<string> fileList, string outputFile)
        {
            var output = new MemoryStream();
            var document = new iTextSharp.text.Document();
            var writer = new PdfCopy(document, output);
            writer.CloseStream = false;
            document.Open();

            foreach (var fileName in fileList)
            {
                var pdfFile = new PdfReader(fileName);
                for (int i = 1; i <= pdfFile.NumberOfPages; i++)
                {
                    var imp = writer.GetImportedPage(pdfFile, (i));
                    writer.AddPage(imp);
                }

                pdfFile.Close();
            }

            document.Close();

            var filepath = outputFile;
            File.WriteAllBytes(filepath, output.ToArray());
        }

        protected void btnExportCRS_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var consolidateDocFile = this.attachFileService.GetAllDocId(new Guid(Request.QueryString["docId"])).FirstOrDefault(t => t.TypeId == 3);
                if (consolidateDocFile != null)
                {
                    var crsFile = this.attachFileService.GetAllDocId(new Guid(Request.QueryString["docId"])).FirstOrDefault(t => t.TypeId == 4);
                    if (crsFile != null)
                    {
                        var rootPath = Server.MapPath(crsFile.FilePath);
                        var filePath = Server.MapPath("../../Exports") + @"\";
                        var workbook = new Workbook(rootPath);
                        var dataSheet = workbook.Worksheets[0];
                        int filerow = Convert.ToInt32(dataSheet.Cells["C1"].Value);
                        var docObj = this.pecc2DocumentService.GetById(consolidateDocFile.ProjectDocumentId.GetValueOrDefault());
                        if (docObj != null)
                        {
                         //   dataSheet.Cells["B4"].PutValue("Doc. No.: " + docObj.DocNo);
                       //     dataSheet.Cells["B5"].PutValue("Rev. No.: " + docObj.RevisionName);
                         //   dataSheet.Cells["B6"].PutValue("Transmittal Contractor: " + Environment.NewLine + docObj.IncomingTransNo);
                       //     dataSheet.Cells["E4"].PutValue(docObj.DocTitle);
                         //   dataSheet.Cells["E6"].PutValue("Transmittal No.: " + Environment.NewLine + docObj.OutgoingTransNo);

                            // dataSheet.Cells["B7"].PutValue("Document Action Code: " + Environment.NewLine + docObj.DocActionCode);
                            dataSheet.Cells["O5"].PutValue( docObj.DocReviewStatusCode);
                         //   dataSheet.Cells["E7"].PutValue("Consolidate Date: " + Environment.NewLine + consolidateDocFile.CreatedDate.GetValueOrDefault().ToString("dd-MM-yyyy"));

                            var dtFull = new DataTable();
                            dtFull.Columns.AddRange(new[]
                            {
                            new DataColumn("NoIndex", typeof (int)),
                           // new DataColumn("FileName", typeof (String)),
                            new DataColumn("Page", typeof (String)),
                            new DataColumn("Comment", typeof (String)),
                            new DataColumn("Empty1", typeof (String)),
                             new DataColumn("Empty2", typeof (String)),
                            new DataColumn("Review", typeof (String)),
                             new DataColumn("Empty3", typeof (String)),
                             new DataColumn("Empty4", typeof (String)),
                              new DataColumn("Empty5", typeof (String)),
                             new DataColumn("Empty6", typeof (String)),
                              new DataColumn("Empty7", typeof (String)),
                           // new DataColumn("Confirmation", typeof (String)),
                        });

                            var pdfReader = new PdfReader(Server.MapPath("../.." + consolidateDocFile.FilePath));
                            var count = 0;
                            for (int i = 1; i <= pdfReader.NumberOfPages; i++)
                            {
                                PdfArray array = pdfReader.GetPageN(i).GetAsArray(PdfName.ANNOTS);
                                if (array == null) continue;
                                for (int j = 0; j < array.Size; j++)
                                {
                                    var annot = array.GetAsDict(j);
                                    var text = annot.GetAsString(PdfName.CONTENTS);
                                    var username = annot.GetAsString(PdfName.T)?.ToString();
                                    if (text != null && !string.IsNullOrEmpty(text.ToString()) && !text.ToString().Contains("None set by")  && (username != null && username != "AutoCAD SHX Text"))
                                    {
                                         //username = annot.GetAsString(PdfName.T);
                                        count += 1;
                                        var dataRow = dtFull.NewRow();
                                        dataRow["NoIndex"] = this.IsNumber(text.ToString().Split('.')[0]) ? Convert.ToInt32(text.ToString().Split('.')[0]): count;
                                        //dataRow["FileName"] = consolidateDocFile.FileName.Split('_')[0] + ".pdf";
                                        dataRow["Page"] = i;
                                        dataRow["Comment"] = text.ToString().Replace("þÿ", string.Empty).Split('.')[1];
                                        dataRow["Review"] = username;
                                        // dataRow["User"] = consolidateDocFile.CreatedByName.Split('/')[0];
                                       // dataRow["Confirmation"] = string.Empty;

                                        dtFull.Rows.Add(dataRow);
                                    }
                                }
                            }
                            if (dtFull.Rows.Count > 0)
                            {
                                dtFull = dtFull.AsEnumerable().OrderBy(t => t["NoIndex"]).CopyToDataTable();
                            }
                           
                            dataSheet.Cells.ImportDataTable(dtFull, false, filerow, 1, dtFull.Rows.Count, dtFull.Columns.Count, true);
                            //   dataSheet.Cells.DeleteRow(9 + dtFull.Rows.Count);
                            for (int i = 0; i < dtFull.Rows.Count; i++)
                            {
                                dataSheet.Cells.Merge(filerow + i, 3, 1, 3);
                                dataSheet.Cells.Merge(filerow + i, 7, 1, 6);
                               // dataSheet.Cells.Merge(filerow + i, 10, 1, 2);
                            }
                            //if (this.attachFileService.GetAllDocId(docObj.ID).All(t => t.TypeId == 4))
                            //{
                            //    var CRSFile = this.attachFileService.GetAllDocId(docObj.ID).Find(t => t.TypeId == 4);
                            //    if (CRSFile != null)
                            //    {
                                    try
                                    {
                                        File.Delete(rootPath);
                                        this.attachFileService.Delete(crsFile);
                                    }
                                    catch { }
                            //    }
                            //}
                            var filename = "CRS_" + Utility.RemoveSpecialCharacterFileName(docObj.DocNo) + ".xlsm";
                            var saveFilePath = Server.MapPath("../../DocumentLibrary/MarkupFile/" + filename);
                            workbook.Save(saveFilePath);

                            // Attach CRS to document Obj
                            var fileInfo = new FileInfo(saveFilePath);
                            if (fileInfo.Exists)
                            {


                                var attachFile = new PVGASDocumentAttachFile()
                                {
                                    ID = Guid.NewGuid(),
                                    ProjectDocumentId = docObj.ID,
                                    FileName = filename,
                                    Extension = ".xlsm",
                                    FilePath = "/DocumentLibrary/MarkupFile/" + filename,
                                    ExtensionIcon = "~/images/excelfile.png",
                                    FileSize = (double)fileInfo.Length / 1024,
                                    TypeId = 4,
                                    IsDefualtResponseContractor=true,
                                    TypeName = "Comment Sheet",
                                    CreatedBy = UserSession.Current.User.Id,
                                    CreatedByName = UserSession.Current.User.UserNameWithFullName,
                                    CreatedDate = DateTime.Now
                                };

                                this.attachFileService.Insert(attachFile);
                            }
                            // -------------------------------------------------------------------------------------------------

                            this.grdAttachMarkupCommentFile.Rebind();
                        }
                    }
                }
            }
        }
        public bool IsNumber(string pText)
        {
            Regex regex = new Regex(@"^[-+]?[0-9]*\.?[0-9]+$");
            return regex.IsMatch(pText);
        }
        protected void grdCRSFile_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            {
                var docId = new Guid(this.Request.QueryString["docId"]);
                var attachList = this.attachFileService.GetAllDocId(docId).Where(t => t.TypeId == 4);
                foreach (var DocAttachFile in attachList)
                {
                    DocAttachFile.IsCanCheckin = UserSession.Current.User.Id == DocAttachFile.CheckoutBy;
                }

                this.grdCRSFile.DataSource = attachList;
            }
            else
            {
                this.grdCRSFile.DataSource = new List<PVGASDocumentAttachFile>();
            }
        }
    }
}