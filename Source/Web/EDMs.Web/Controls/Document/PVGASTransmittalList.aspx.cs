﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Security;
using EDMs.Business.Services.Workflow;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Text.RegularExpressions;
namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class PVGASTransmittalList : Page
    {
        private readonly ProjectCodeService projectCodeService = new ProjectCodeService();

        private readonly ContractorTransmittalService contractorTransmittalService = new ContractorTransmittalService();

        private readonly ContractorTransmittalDocFileService contractorTransmittalDocFileService = new ContractorTransmittalDocFileService();

        private readonly PVGASTransmittalService transmittalService = new PVGASTransmittalService();

        private readonly PVGASTransmittalAttachFileService transmittalAttachFileService = new PVGASTransmittalAttachFileService();

        private readonly OrganizationCodeService organizationCodeService = new OrganizationCodeService();

        private readonly AttachDocToTransmittalService attachDocToTransmittalService = new AttachDocToTransmittalService();

        private readonly DocumentPackageService documentProjectService = new DocumentPackageService();

        private readonly DocumentPackageService documentPackageService = new DocumentPackageService();

        private readonly PVGASDocumentAttachFileService documentAttachFileService = new PVGASDocumentAttachFileService();

        private readonly RevisionStatuService revisionStatusService = new RevisionStatuService();

        private readonly PurposeCodeService purposeCodeService = new PurposeCodeService();

        private readonly UserService userService = new UserService();

        private readonly RoleService roleService = new RoleService();

        private readonly AreaService areaService = new AreaService();

        private readonly ObjectAssignedUserService objectAssignedUser = new  ObjectAssignedUserService();

        private readonly DocumentTypeService documentTypeService = new DocumentTypeService();

        private readonly ChangeRequestService changeRequestService = new ChangeRequestService();

        private readonly ChangeRequestAttachFileService changeRequestAttachFileService = new ChangeRequestAttachFileService();
        private readonly RevisionService revisionService= new RevisionService();
        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Add("SelectedMainMenu", "Transmittals Management");

            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            var temp = (RadPane)this.Master.FindControl("leftPane");
            temp.Collapsed = true;
            if (!Page.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["TransNoContractor"]))
                {
                    var txtSearchIncoming = (TextBox)this.radMenuIncoming.Items[2].FindControl("txtSearchIncoming");
                    txtSearchIncoming.Text = Request.QueryString["TransNoContractor"];

                }
                if (!string.IsNullOrEmpty(this.Request.QueryString["TransNoPVGAS"]))
                {
                    var txtSearchOutgoing = (TextBox)this.radMenuOutgoing.Items[2].FindControl("txtSearchOutgoing");
                    txtSearchOutgoing.Text = Request.QueryString["TransNoPVGAS"];
                    var ddlStatusOutgoing = (DropDownList)this.radMenuOutgoing.Items[2].FindControl("ddlStatusOutgoing");
                    if (ddlStatusOutgoing != null)
                    {
                        ddlStatusOutgoing.SelectedIndex = 0;
                    }
                    this.OutgoingTransView.Selected = true;
                    RadTab tab1 = RadTabStrip1.Tabs.FindTabByText("Outgoing Transmittals");
                    tab1.Selected = true;
                }
            }
        }

        private void LoadComboData()
        {
            var ddlProjectOutgoing = (RadComboBox)this.radMenuOutgoing.Items[2].FindControl("ddlProjectOutgoing");
            var ddlProjectIncoming = (RadComboBox)this.radMenuIncoming.Items[2].FindControl("ddlProjectIncoming");
            var projectList = this.projectCodeService.GetAll().OrderBy(t => t.Code);

            if (ddlProjectOutgoing != null)
            {
                ddlProjectOutgoing.DataSource = projectList;
                ddlProjectOutgoing.DataTextField = "FullName";
                ddlProjectOutgoing.DataValueField = "ID";
                ddlProjectOutgoing.DataBind();

                int projectId = Convert.ToInt32(ddlProjectOutgoing.SelectedValue);
                this.lblProjectOutgoingId.Value = projectId.ToString();
                Session.Add("SelectedProject", projectId);
            }

            if (ddlProjectIncoming != null)
            {
                ddlProjectIncoming.DataSource = projectList;
                ddlProjectIncoming.DataTextField = "FullName";
                ddlProjectIncoming.DataValueField = "ID";
                ddlProjectIncoming.DataBind();

                int projectId = Convert.ToInt32(ddlProjectIncoming.SelectedValue);
                this.lblProjectIncomingId.Value = projectId.ToString();
            }

            var ddlStatusOutgoing = (DropDownList)this.radMenuOutgoing.Items[2].FindControl("ddlStatusOutgoing");
            if(ddlStatusOutgoing != null)
            {
                ddlStatusOutgoing.SelectedIndex = 0;
            }

            var ddlStatusIncoming = (DropDownList)this.radMenuIncoming.Items[2].FindControl("ddlStatusIncoming");
            if (ddlStatusIncoming != null)
            {
                ddlStatusIncoming.SelectedIndex = 0;
            }

            var organizationList = this.organizationCodeService.GetAll().Where(t=> !t.Code.Contains("PV GAS SEG")).OrderBy(t => t.Code).ToList();
            organizationList.Insert(0, new OrganizationCode() { ID = 0, Code = "All" });
            //RadTreeNode node = new RadTreeNode();
            //node.Text = "All";
            //node.Value = "0";
            ////node.ImageUrl = "~/Images/package.png";
            //node.Target = "Package";
            //  this.rtvorinationIn.Nodes.Add(node);
            this.rtvorinationIn.DataSource = organizationList;
            this.rtvorinationIn.DataTextField = "Code";
            this.rtvorinationIn.DataValueField = "ID";
            this.rtvorinationIn.DataBind();
            this.rtvorinationIn.Nodes[0].Selected = true;

            this.rtvOrganizationOut.DataSource = organizationList;
            this.rtvOrganizationOut.DataTextField = "Code";
            this.rtvOrganizationOut.DataValueField = "ID";
            this.rtvOrganizationOut.DataBind();
            this.rtvOrganizationOut.Nodes[0].Selected = true;

        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdIncomingTrans.Rebind();
            }
            else if (e.Argument.Contains("DeleteTrans_"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.transmittalService.GetById(objId);
                if (!string.IsNullOrEmpty(transObj?.StoreFolderPath))
                {
                    var folderPath = Server.MapPath("../.." + transObj.StoreFolderPath);
                    if (Directory.Exists(folderPath))
                    {
                        Directory.Delete(folderPath,true);
                    }
                }

                this.transmittalService.Delete(objId);
                this.grdOutgoingTrans.Rebind();
            }
            else if (e.Argument.Contains("ExportContractorETRM"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.contractorTransmittalService.GetById(objId);
                if (transObj != null)
                {
                    this.ExportContractorETRM(transObj);
                }
            }
            else if (e.Argument.Contains("ExportPVGASETRM"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.transmittalService.GetById(objId);
                if (transObj != null)
                {
                    this.ExportETRM(transObj, e.Argument.Split('_')[2]);
                }
            }
            
            else if (e.Argument.Contains("ImportDocument"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var objPECC2Id = new Guid(e.Argument.Split('_')[2]);
                var contractorTransObj = this.contractorTransmittalService.GetById(objId);
                var PECC2TransIn = this.transmittalService.GetById(objPECC2Id);
                if (contractorTransObj != null)
                {
                    this.ImportTransDocument(contractorTransObj, objPECC2Id);
                    this.grdIncomingTrans.Rebind();
                }
            }
            else if (e.Argument.Contains("SendTrans"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var forSendId = Convert.ToInt32(e.Argument.Split('_')[2]);
                var transObj = this.transmittalService.GetById(objId);
                if (transObj != null)
                {
                    var contractorTrans = new ContractorTransmittal();
                    contractorTrans.ID = Guid.NewGuid();
                    contractorTrans.PECC2TransId = transObj.ID;
                    contractorTrans.Status = string.Empty;
                    contractorTrans.IsValid = true;
                    contractorTrans.IsSend = false;
                    contractorTrans.IsOpen = false;
                    contractorTrans.ErrorMessage = string.Empty;
                    contractorTrans.ForSentId = forSendId;
                    contractorTrans.ForSentName = forSendId == 1 ? "Project's Document" : "Change Request";

                    contractorTrans.Attn = transObj.Attn;
                    contractorTrans.Contract = transObj.Contract;
                    contractorTrans.FileTo = transObj.FileTo;

                    contractorTrans.ProjectId = transObj.ProjectCodeId;
                    contractorTrans.ProjectName = transObj.ProjectCodeName;
                    contractorTrans.TransNo = transObj.TransmittalNo;
                    contractorTrans.TransDate = transObj.IssuedDate;
                    contractorTrans.Description = transObj.Description;
                    contractorTrans.OriginatingOrganizationId = transObj.OriginatingOrganizationId;
                    contractorTrans.OriginatingOrganizationName = transObj.OriginatingOrganizationName;
                    contractorTrans.ReceivingOrganizationId = transObj.ReceivingOrganizationId;
                    contractorTrans.ReceivingOrganizationName = transObj.ReceivingOrganizationName;
                    contractorTrans.FromValue = transObj.FromValue;
                    contractorTrans.ToValue = transObj.ToValue;
                    contractorTrans.CCValue = transObj.CCValue;
                    contractorTrans.GroupId = transObj.GroupId;
                    contractorTrans.GroupCode = transObj.GroupCode;
                    contractorTrans.TransmittedByName = transObj.TransmittedByName;
                    contractorTrans.TransmittedByDesignation = transObj.TransmittedByDesignation;
                    contractorTrans.AcknowledgedByName = transObj.AcknowledgedByName;
                    contractorTrans.AcknowledgedByDesignation = transObj.AcknowledgedByDesignation;
                    contractorTrans.Remark = transObj.Remark;
                    contractorTrans.Year = transObj.Year;
                    contractorTrans.DueDate = transObj.DueDate;
                    contractorTrans.ReceivedDate = DateTime.Now;
                    contractorTrans.TypeId = 1;

                    var contractorTransId = this.contractorTransmittalService.Insert(contractorTrans);
                    if (contractorTransId != null)
                    {
                        transObj.ContractorTransId = contractorTransId;
                        transObj.IsSend = true;
                        transObj.ReceivedDate = DateTime.Now;

                        this.transmittalService.Update(transObj);
                        //update transout on documentproject
                        this.UpdateTransOutToDocument(transObj, forSendId);

                        //

                        //send email to dc contractors
                        if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]))
                        {
                            this.NotifiNewTransmittal(transObj);
                        }
                        //send email to all user 
                        //if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]))
                        //{
                        //    this.NotifiNewTransmittalOut(transObj);
                        //}

                    //Update Date actual Codecomment
                    var listDoc= this.attachDocToTransmittalService.GetAllByTransId(transObj.ID).Select(t => this.documentPackageService.GetById(t.DocumentId.GetValueOrDefault())).OrderBy(t => t.DocNo);
                        foreach(var itemdoc in listDoc)
                        {
                            if(itemdoc.DocReviewStatusCode.ToLower().Trim()== "code 2" )
                            {
                               if( itemdoc.ReIFRActual == null) itemdoc.ReIFRActual= transObj.IssuedDate;
                                itemdoc.DeadlineClientResponse = this.GetDate(3, transObj.IssuedDate.GetValueOrDefault());

                            }
                            else if (itemdoc.DocReviewStatusCode.ToLower().Trim() == "code 1" && itemdoc.IFAActual== null)
                            {
                                itemdoc.IFAActual = transObj.IssuedDate;
                               
                            }
                            else if (itemdoc.DocReviewStatusCode.ToLower().Trim() == "code 3")
                            {
                                itemdoc.DeadlineClientResponse = this.GetDate(5, transObj.IssuedDate.GetValueOrDefault());
                            }
                            this.documentPackageService.Update(itemdoc);
                        }
                    }

                    this.grdOutgoingTrans.Rebind();
                }
            }
            else if (e.Argument.Contains("Undo"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.transmittalService.GetById(objId);
                if (transObj != null)
                {
                    var contractorTrans = this.contractorTransmittalService.GetById(transObj.ContractorTransId.GetValueOrDefault());
                    if (contractorTrans != null) {
                        this.contractorTransmittalService.Delete(contractorTrans);
                        transObj.ContractorTransId = null;
                        transObj.IsSend = false;
                        transObj.ReceivedDate = null;
                        this.transmittalService.Update(transObj);
                        this.grdOutgoingTrans.Rebind();
                    }
                   
                }
            }
            else if (e.Argument.Contains("DownLoadFolder"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.transmittalService.GetById(objId);
                if (transObj != null)
                {
                    var contractorTrans = this.contractorTransmittalService.GetById(transObj.ContractorTransId.GetValueOrDefault());
                    var filename = "Contrractor_" + transObj.TransmittalNo + "_" + DateTime.Now.ToBinary() + ".zip";
                    var serverTotalDocPackPath = Server.MapPath("../../DocumentLibrary/ContractorTransmittal/" + filename);
              

                    ////////xu ly nen
                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                    {
                        zip.AlternateEncoding = System.Text.Encoding.Unicode;
                        zip.AddDirectory(Server.MapPath("~"+ contractorTrans.StoreFolderPath));
                        zip.Save(serverTotalDocPackPath);
                    };
                    ////down load va xoa nguon
                    Response.ClearContent();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
                    Response.TransmitFile(serverTotalDocPackPath);

                    HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
                    cookie.Value = "Flag";
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.AppendCookie(cookie);

                    Response.Flush();
                    System.IO.File.Delete(serverTotalDocPackPath);
                    Response.End();
                    //  Response.Redirect("~/DownloadFile.ashx?path=" + filename);

                }
            }
        }
        private DateTime GetDate(int day, DateTime transdate)
        {
            var actualDeadline = transdate;
            for (int i = 1; i <= day; i++)
            {
                actualDeadline = this.GetNextWorkingDay(actualDeadline);
            }
            return actualDeadline;
        }
        private bool IsWeekEnd(DateTime date)
        {
            return ConfigurationManager.AppSettings["WeekendWork"] == "false" ? date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday : false;
        }
        private DateTime GetNextWorkingDay(DateTime date)
        {
            do
            {
                date = date.AddDays(1);
            }
            while (IsWeekEnd(date));

            return date;
        }
        private void ImportTransDocument(ContractorTransmittal contractorTransObj, Guid objPECC2Id)
        {
            var pecc2IncomingTrans = this.transmittalService.GetById(contractorTransObj.PECC2TransId.GetValueOrDefault());
            // var transPurpose = this.purposeCodeService.GetById(contractorTransObj.PurposeId.GetValueOrDefault());
            var revisionStatus = new RevisionStatu();// this.revisionStatusService.GetByCode(transPurpose.Code);

            var fullDocList = this.contractorTransmittalDocFileService.GetAllByTrans(contractorTransObj.ID);
            var filterDocList = new List<ContractorTransmittalDocFile>();

            // Remove duplicate Document
            foreach (var document in fullDocList)
            {
                //if (filterDocList.All(t => t.DocumentNo != document.DocumentNo))
                //{
                    filterDocList.Add(document);
               // }
            }
            // --------------------------------------------------------------------------------------------

            // Process import
            if (pecc2IncomingTrans.ForSentId == 1)
            {
                foreach (var contractorDoc in filterDocList.Where(t=> t.ChangeRequestTypeId==1).ToList())
                {
                    this.ProcessImportProjectDocument(fullDocList, contractorDoc, pecc2IncomingTrans, revisionStatus, objPECC2Id);
                }
            }
            //else
            //{
            //    foreach (var contractorDoc in filterDocList)
            //    {
            //        this.ProcessImportChangeRequest(fullDocList, contractorDoc, pecc2IncomingTrans, objPECC2Id);
            //    }
            //}
        }
        private void ProcessImportProjectDocument(List<ContractorTransmittalDocFile> fullDocList, ContractorTransmittalDocFile contractorDoc, PVGASTransmittal pecc2IncomingTrans, RevisionStatu revisionStatus, Guid objPECC2Id)
        {
            // Get attach doc list
           // var contractorDocAttach = fullDocList.Where(t => t.ID == contractorDoc.ID).ToList();
            // ----------------------------------------------------------------------------------------

            var currentProjectDocList = this.documentProjectService.GetAllByProjectDocNo(contractorDoc.DocumentNo);

            Guid PECC2DocId = contractorDoc.ID;
            var projectDoc = new DocumentPackage();
            // Case: Already have previous document
            if (currentProjectDocList.Count > 0)
            {
                var currentLeafProjectDoc = currentProjectDocList.FirstOrDefault(t => t.IsLeaf.GetValueOrDefault());
                if (currentLeafProjectDoc != null)
                {
                    projectDoc = currentLeafProjectDoc;

                    if (projectDoc.RevisionName.ToLower().Trim() == contractorDoc.Revision.ToLower().Trim())
                    {
                        // Fill incoming trans info
                        projectDoc.StatusID = pecc2IncomingTrans.PurposeId;
                        projectDoc.StatusName = pecc2IncomingTrans.PurposeName.Split('-')[0];

                        projectDoc.DocTitle = contractorDoc.DocumentTitle;
                        if (contractorDoc.ChangeRequestTypeId == 1)
                        {
                            projectDoc.IsHasAttachFile = true;
                        }
                        projectDoc.IncomingTransId = pecc2IncomingTrans.ID;
                        projectDoc.IncomingTransNo = pecc2IncomingTrans.TransmittalNo;
                        projectDoc.IncomingTransDate = pecc2IncomingTrans.IssuedDate;
                        projectDoc.DeadlineTransOut = pecc2IncomingTrans.DueDate;
                        projectDoc.RevisionActualDate = pecc2IncomingTrans.IssuedDate;
                        projectDoc.RevisionReceiveTransNo = pecc2IncomingTrans.TransmittalNo;

                        if (projectDoc.RevisionName == "1")
                        {
                            projectDoc.IFRActual = DateTime.Now;
                            projectDoc.FirstIssueActualDate = pecc2IncomingTrans.IssuedDate;
                            projectDoc.FirstIssueTransNo = pecc2IncomingTrans.TransmittalNo;
                        }
                        else if (projectDoc.RevisionName == "AC")
                        {
                            projectDoc.AFCActual = DateTime.Now;
                            projectDoc.FinalIssuePlanDate = pecc2IncomingTrans.IssuedDate;
                            projectDoc.FinalIssueTransNo = pecc2IncomingTrans.TransmittalNo;
                        }
                        this.documentProjectService.Update(projectDoc);
                        //-------------------------------------------------------------------------------
                        // -------------------------------------------------------------------------------
                        if (objPECC2Id != null && !string.IsNullOrEmpty(objPECC2Id.ToString()))
                        {
                            var attachDoc = new AttachDocToTransmittal()
                            {
                                TransmittalId = objPECC2Id,
                                DocumentId = projectDoc.ID
                            };
                            if (!this.attachDocToTransmittalService.IsExist(objPECC2Id, projectDoc.ID))
                            {
                                this.attachDocToTransmittalService.Insert(attachDoc);
                            }
                        }
                       
                    }
                    else
                    {
                        // Collect new project doc info
                        projectDoc = new DocumentPackage();
                        this.CollectProjectDocData(currentLeafProjectDoc, projectDoc);

                        projectDoc.StatusID = pecc2IncomingTrans.PurposeId;
                        projectDoc.StatusName = pecc2IncomingTrans.PurposeName.Split('-')[0];
                        if (projectDoc.RevisionName == "1")
                        {
                            projectDoc.IFRActual = DateTime.Now;
                            projectDoc.FirstIssueActualDate = pecc2IncomingTrans.IssuedDate;
                            projectDoc.FirstIssueTransNo = pecc2IncomingTrans.TransmittalNo;
                        }
                        else if (projectDoc.RevisionName == "AC")
                        {
                            projectDoc.AFCActual = DateTime.Now;
                            projectDoc.FinalIssuePlanDate = pecc2IncomingTrans.IssuedDate;
                            projectDoc.FinalIssueTransNo = pecc2IncomingTrans.TransmittalNo;
                        }
                        projectDoc.DocTitle = contractorDoc.DocumentTitle;
                        projectDoc.CreatedBy = UserSession.Current.User.Id;
                        projectDoc.CreatedDate = DateTime.Now;
                        projectDoc.IsLeaf = true;
                        projectDoc.IsDelete = false;
                        projectDoc.IsHasAttachFile = true;
                        projectDoc.ParentId = currentLeafProjectDoc.ParentId ?? currentLeafProjectDoc.ID;
                        projectDoc.CodeId = pecc2IncomingTrans.PurposeId;
                        projectDoc.CodeFullName = pecc2IncomingTrans.PurposeName;
                        projectDoc.CodeName = pecc2IncomingTrans.PurposeName.Split('-')[0];
                        var revObj = this.revisionService.GetByName(contractorDoc.Revision, currentLeafProjectDoc.ProjectId.GetValueOrDefault());
                        if (revObj != null)
                        {
                            projectDoc.RevisionId = revObj.ID;
                            projectDoc.RevisionName = revObj.Name;
                            projectDoc.RevDate = DateTime.Now;
                        }
                        // Fill incoming trans info
                        projectDoc.IncomingTransId = pecc2IncomingTrans.ID;
                        projectDoc.IncomingTransNo = pecc2IncomingTrans.TransmittalNo;
                        projectDoc.IncomingTransDate = pecc2IncomingTrans.IssuedDate;
                        //-------------------------------------------------------------------------------

                        this.documentProjectService.Insert(projectDoc);
                        PECC2DocId = projectDoc.ID;
                        // -------------------------------------------------------------------------------
                        if (objPECC2Id != null && !string.IsNullOrEmpty(objPECC2Id.ToString()))
                        {
                            var attachDoc = new AttachDocToTransmittal()
                            {
                                TransmittalId = objPECC2Id,
                                DocumentId = projectDoc.ID
                            };
                            if (!this.attachDocToTransmittalService.IsExist(objPECC2Id, projectDoc.ID))
                            {
                                this.attachDocToTransmittalService.Insert(attachDoc);
                            }
                        }


                        // Update leaf project doc
                        currentLeafProjectDoc.IsLeaf = false;
                        this.documentProjectService.Update(currentLeafProjectDoc);


                        // -------------------------------------------------------------------------------------------------------------
                    }

                    //Attach doc file to project doc
                    this.AttachDocFileToProjectDoc(fullDocList, projectDoc);
                    // --------------------------------------------------------------------------------------------------------------

                    // Update PECC2 Incoming trans info
                    pecc2IncomingTrans.Status = string.Empty;
                    pecc2IncomingTrans.ErrorMessage = string.Empty;
                    pecc2IncomingTrans.IsImport = true;

                    this.transmittalService.Update(pecc2IncomingTrans);
                    // --------------------------------------------------------------------------------------------------------------
                }
            }
            // Case: Document sent by contractor is new doc
            else
            {
                // Collect new project doc info
                projectDoc = new DocumentPackage();
                //  this.CollectProjectDocData(contractorDoc, projectDoc);
                projectDoc.ID = Guid.NewGuid();
                projectDoc.DocNo = contractorDoc.DocumentNo;
                projectDoc.DocTitle = contractorDoc.DocumentTitle;
                projectDoc.ProjectId = contractorDoc.ProjectId;
                projectDoc.ProjectName = contractorDoc.ProjectName;
               // projectDoc.ProjectFullName = contractorDoc.pro;


                projectDoc.DocumentTypeId = contractorDoc.DocumentTypeId;
              //  projectDoc.DocTypeFullName = contractorDoc.DocumentTypeName;
                projectDoc.DocumentTypeName = contractorDoc.DocumentTypeName;

                projectDoc.DisciplineId = contractorDoc.DisciplineCodeId;
               // projectDoc.DisciplineFullName = contractorDoc.DisciplineFullName;
                projectDoc.DisciplineName = contractorDoc.DisciplineCodeName;
              //  projectDoc.Weight = contractorDoc.Weight;

               // projectDoc.OriginatorId = contractorDoc.OriginatingOrganizationId;
             //   projectDoc.OriginatorFullName = contractorDoc.OriginatorFullName;
             ///   projectDoc.OriginatorName = contractorDoc.OriginatingOrganizationName;

                projectDoc.SequencetialNumber = contractorDoc.Sequence;
              //  projectDoc.DrawingSheetNumber = contractorDoc.DrawingSheetNumber;


                projectDoc.ContractorId = contractorDoc.OriginatingOrganizationId;
              //  projectDoc.ContractorFullName = contractorDoc.ContractorFullName;
                projectDoc.ContractorName = contractorDoc.OriginatingOrganizationName;

               projectDoc.PhaseId = contractorDoc.PhaseId;
              // projectDoc.PhaseFullName = contractorDoc.PhaseFullName;
                projectDoc.PhaseName = contractorDoc.PhaseName;

                projectDoc.AreaId = contractorDoc.AreaId;
              //  projectDoc.AreaFullName = contractorDoc.AreaFullName;
                projectDoc.AreaName = contractorDoc.AreaName;

                projectDoc.SubAreaId = contractorDoc.SubAreaId;
              //  projectDoc.SubAreaFullName = contractorDoc.SubAreaFullName;
                projectDoc.SubAreaName = contractorDoc.SubAreaName;



                projectDoc.DrawingCodeId = contractorDoc.DrawingCodeId;
              //  projectDoc.DrawingCodeFullName = contractorDoc.DrawingCodeFullName;
                projectDoc.DrawingCodeName = contractorDoc.DrawingCodeName;

               projectDoc.DrawingDetailCodeId = contractorDoc.DrawinDetailCodeId;
               //projectDoc.DrawingDetailCodeFullName = contractorDoc.DrawingDetailCodeFullName;
                projectDoc.DrawingDetailCodeName = contractorDoc.DrawinDetailCodeName;

                projectDoc.SystemCodeId = contractorDoc.SystemId;
       //         projectDoc.SystemCodeFullName = contractorDoc.SystemCodeFullName;
                projectDoc.SystemCodeName = contractorDoc.SystemName;


                ////
                projectDoc.ProjectId = contractorDoc.ProjectId;
                projectDoc.ProjectName = contractorDoc.ProjectName;
                projectDoc.StatusID = pecc2IncomingTrans.PurposeId;
                projectDoc.StatusName = pecc2IncomingTrans.PurposeName.Split('-')[0];
                if (projectDoc.RevisionName == "1")
                {
                    projectDoc.IFRActual = DateTime.Now;
                    projectDoc.FirstIssueActualDate = pecc2IncomingTrans.IssuedDate;
                    projectDoc.FirstIssueTransNo = pecc2IncomingTrans.TransmittalNo;
                }
                else if (projectDoc.RevisionName == "AC")
                {
                    projectDoc.AFCActual = DateTime.Now;
                    projectDoc.FinalIssuePlanDate = pecc2IncomingTrans.IssuedDate;
                    projectDoc.FinalIssueTransNo = pecc2IncomingTrans.TransmittalNo;
                }
                projectDoc.DocTitle = contractorDoc.DocumentTitle;
                projectDoc.CreatedBy = UserSession.Current.User.Id;
                projectDoc.CreatedDate = DateTime.Now;
                projectDoc.IsLeaf = true;
                projectDoc.IsDelete = false;
                projectDoc.IsHasAttachFile = true;
               // projectDoc.ParentId = currentLeafProjectDoc.ParentId ?? currentLeafProjectDoc.ID;
                projectDoc.CodeId = pecc2IncomingTrans.PurposeId;
                projectDoc.CodeFullName = pecc2IncomingTrans.PurposeName;
                projectDoc.CodeName = pecc2IncomingTrans.PurposeName.Split('-')[0];
                var revObj = this.revisionService.GetByName(contractorDoc.Revision, pecc2IncomingTrans.ProjectCodeId.GetValueOrDefault());
                if (revObj != null)
                {
                    projectDoc.RevisionId = revObj.ID;
                    projectDoc.RevisionName = revObj.Name;
                    projectDoc.RevDate = DateTime.Now;
                }
                // Fill incoming trans info
                projectDoc.IncomingTransId = pecc2IncomingTrans.ID;
                projectDoc.IncomingTransNo = pecc2IncomingTrans.TransmittalNo;
                projectDoc.IncomingTransDate = pecc2IncomingTrans.IssuedDate;
                //-------------------------------------------------------------------------------

                this.documentProjectService.Insert(projectDoc);
                PECC2DocId = projectDoc.ID;
                // -------------------------------------------------------------------------------
                if (objPECC2Id != null && !string.IsNullOrEmpty(objPECC2Id.ToString()))
                {
                    var attachDoc = new AttachDocToTransmittal()
                    {
                        TransmittalId = objPECC2Id,
                        DocumentId = projectDoc.ID
                    };
                    if (!this.attachDocToTransmittalService.IsExist(objPECC2Id, projectDoc.ID))
                    {
                        this.attachDocToTransmittalService.Insert(attachDoc);
                    }
                }

            }
            contractorDoc.PECC2ProjectDocId = PECC2DocId != contractorDoc.ID ? PECC2DocId : contractorDoc.PECC2ProjectDocId;
            contractorDoc.IsReject = false;
            this.contractorTransmittalDocFileService.Update(contractorDoc);
        }

        private void AttachDocFileToProjectDoc(List<ContractorTransmittalDocFile> attachList, DocumentPackage projectDoc)
        {
            var targetFolder = "../../DocumentLibrary/ProjectDocs";
            var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                + "/DocumentLibrary/ProjectDocs";
            foreach (var contractorAttachFile in attachList)
            {
                var docFileName = contractorAttachFile.FileName;

                // Path file to save on server disc
                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), docFileName);
                // Path file to download from server
                var serverFilePath = serverFolder + "/" + docFileName;
                var StingPath = "../.." + contractorAttachFile.FilePath;
                
                    File.Copy(Server.MapPath(StingPath), saveFilePath, true);
                

                var attachFile = new PVGASDocumentAttachFile()
                {
                    ID = Guid.NewGuid(),
                    ProjectDocumentId = projectDoc.ID,
                    FileName = docFileName,
                    Extension = contractorAttachFile.Extension,
                    FilePath = serverFilePath,
                    ExtensionIcon = contractorAttachFile.ExtensionIcon,
                    FileSize = contractorAttachFile.FileSize,
                    TypeId = contractorAttachFile.ChangeRequestTypeId== 5?  contractorAttachFile.ChangeRequestTypeId:1,
                    TypeName = contractorAttachFile.ChangeRequestTypeId == 5 ? "Comment Response Sheet File": "Document file"   ,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedByName = UserSession.Current.User.UserNameWithFullName,
                    CreatedDate = DateTime.Now
                };

                this.documentAttachFileService.Insert(attachFile);
            }
        }

        private void AttachDocFileToChangeRequest(List<ContractorTransmittalDocFile> attachList, ChangeRequest changeRequestObj)
        {
            var targetFolder = "../../DocumentLibrary/ChangeRequest";
            var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                + "/DocumentLibrary/ChangeRequest";
            foreach (var contractorAttachFile in attachList)
            {
                var docFileName = contractorAttachFile.FileName;

                // Path file to save on server disc
                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), docFileName);
                // Path file to download from server
                var serverFilePath = serverFolder + "/" + docFileName;

                var StingPath = "../.." + contractorAttachFile.FilePath;
               
                    File.Copy(Server.MapPath(StingPath), saveFilePath, true);
                

                var attachFile = new ChangeRequestAttachFile()
                {
                    ID = Guid.NewGuid(),
                    ChangeRequestId = changeRequestObj.ID,
                    FileName = docFileName,
                    Extension = contractorAttachFile.Extension,
                    FilePath = serverFilePath,
                    ExtensionIcon = contractorAttachFile.ExtensionIcon,
                    FileSize = contractorAttachFile.FileSize,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedByName = UserSession.Current.User.UserNameWithFullName,
                    CreatedDate = DateTime.Now
                };

                this.changeRequestAttachFileService.Insert(attachFile);
            }
        }

        private void UpdateTransOutToDocument(PVGASTransmittal transObj, int forSendId)
        {
            var attachDocToTrans = this.attachDocToTransmittalService.GetAllByTransId(transObj.ID);
            foreach (var docobj in attachDocToTrans)
            {
                if (forSendId == 1)
                {
                    var projectDoc = this.documentProjectService.GetById(docobj.DocumentId.GetValueOrDefault());
                    projectDoc.OutgoingTransId = transObj.ID;
                    projectDoc.OutgoingTransNo = transObj.TransmittalNo;
                    projectDoc.IsCreateOutgoingTrans = true;
                    this.documentProjectService.Update(projectDoc);
                }
                else
                {
                    var changeRequest = this.changeRequestService.GetById(docobj.DocumentId.GetValueOrDefault());
                    changeRequest.OutgoingTransId = transObj.ID;
                    changeRequest.OutgoingTransNo = transObj.TransmittalNo;
                    changeRequest.IsCreateOutgoingTrans = true;
                    this.changeRequestService.Update(changeRequest);
                }
                
            }
        }

        private void CollectProjectDocData(DocumentPackage contractorDoc, DocumentPackage docObj)
        {
            docObj.ID = Guid.NewGuid();
            docObj.DocNo = contractorDoc.DocNo;
            docObj.DocTitle = contractorDoc.DocTitle;
            docObj.ProjectId = contractorDoc.ProjectId;
            docObj.ProjectName = contractorDoc.ProjectName;
            docObj.ProjectFullName = contractorDoc.ProjectFullName;

          
            docObj.DocumentTypeId = contractorDoc.DocumentTypeId;
            docObj.DocTypeFullName = contractorDoc.DocTypeFullName;
            docObj.DocumentTypeName = contractorDoc.DocumentTypeName;

            //docObj.DocReviewStatusCodeID = contractorDoc;
            //docObj.DocReviewStatusCode = this.ddlDocReviewStatus.SelectedItem != null
            //                              ? this.ddlDocReviewStatus.SelectedItem.Text
            //                              : string.Empty;

            //docObj.ResponseCAId = this.ddlResponseCodeCA.SelectedItem != null
            //                           ? Convert.ToInt32(this.ddlResponseCodeCA.SelectedValue)
            //                           : 0;
            //docObj.ResponseCACode = this.ddlResponseCodeCA.SelectedItem != null
            //                              ? this.ddlResponseCodeCA.SelectedItem.Text
            //                              : string.Empty;

            docObj.DisciplineId = contractorDoc.DisciplineId;
            docObj.DisciplineFullName = contractorDoc.DisciplineFullName;
            docObj.DisciplineName = contractorDoc.DisciplineName;

            docObj.Weight = contractorDoc.Weight;

        
            //docObj.RevisionPlanedDate = this.txtRevisionPlaned.SelectedDate;
            //docObj.RevisionActualDate = this.txtRevisionReceivedDate.SelectedDate;
            //docObj.RevisionReceiveTransNo = this.txtRevisionReceivedTrans.Text.Trim();
            // docObj.CompleteForProject = Math.Round(this.txtComplete.Value.GetValueOrDefault() * docObj.Weight.GetValueOrDefault() / 100, 2);

            docObj.OriginatorId = contractorDoc.OriginatorId;
            docObj.OriginatorFullName = contractorDoc.OriginatorFullName;
            docObj.OriginatorName = contractorDoc.OriginatorName;

            docObj.SequencetialNumber = contractorDoc.SequencetialNumber;
            docObj.DrawingSheetNumber = contractorDoc.DrawingSheetNumber;
          

            docObj.ContractorId = contractorDoc.ContractorId;
            docObj.ContractorFullName = contractorDoc.ContractorFullName;
            docObj.ContractorName = contractorDoc.ContractorName;

            docObj.PhaseId = contractorDoc.PhaseId;
            docObj.PhaseFullName = contractorDoc.PhaseFullName;
            docObj.PhaseName = contractorDoc.PhaseName;

            docObj.AreaId = contractorDoc.AreaId;
            docObj.AreaFullName = contractorDoc.AreaFullName;
            docObj.AreaName = contractorDoc.AreaName;

            docObj.SubAreaId = contractorDoc.SubAreaId;
            docObj.SubAreaFullName = contractorDoc.SubAreaFullName;
            docObj.SubAreaName = contractorDoc.SubAreaName;

          

            docObj.DrawingCodeId = contractorDoc.DrawingCodeId;
            docObj.DrawingCodeFullName = contractorDoc.DrawingCodeFullName;
            docObj.DrawingCodeName = contractorDoc.DrawingCodeName;

            docObj.DrawingDetailCodeId = contractorDoc.DrawingDetailCodeId;
            docObj.DrawingDetailCodeFullName = contractorDoc.DrawingDetailCodeFullName;
            docObj.DrawingDetailCodeName = contractorDoc.DrawingDetailCodeName;

            docObj.SystemCodeId = contractorDoc.SystemCodeId;
            docObj.SystemCodeFullName = contractorDoc.SystemCodeFullName;
            docObj.SystemCodeName = contractorDoc.SystemCodeName;


            docObj.StartPlan = contractorDoc.StartPlan;
            docObj.StartRevised = contractorDoc.StartRevised;
            docObj.StartActual = contractorDoc.StartActual;
            docObj.IDCPlan = contractorDoc.IDCPlan;
            docObj.IDCRevised = contractorDoc.IDCRevised;
            docObj.IDCACtual = contractorDoc.IDCACtual;
            docObj.IFRPlan = contractorDoc.IFRPlan;
            docObj.IFRRevised = contractorDoc.IFRRevised;
            docObj.IFRActual = contractorDoc.IFRActual;
            docObj.ReIFRPlan = contractorDoc.ReIFRPlan;
            docObj.ReIFRRevised = contractorDoc.ReIFRRevised;
            docObj.ReIFRActual = contractorDoc.ReIFRActual;
            docObj.IFAPlan = contractorDoc.IFAPlan;
            docObj.IFARevised = contractorDoc.IFARevised;
            docObj.IFAActual = contractorDoc.IFAActual;
            docObj.Manhours = contractorDoc.Manhours;

            docObj.AFCPlan = contractorDoc.AFCPlan;
            docObj.AFCRevised = contractorDoc.AFCRevised;
            docObj.AFCActual = contractorDoc.AFCActual;

        }

        private void CollectChangeRequestData(ContractorTransmittalDocFile contractorDoc, ChangeRequest obj)
        {
            obj.ID = Guid.NewGuid();
            obj.Number = contractorDoc.DocumentNo;
            obj.Description = contractorDoc.DocumentTitle;
            obj.ConfidentialityId = 0;
            obj.ConfidentialityName = string.Empty;
            obj.GroupId = contractorDoc.GroupCodeId;
            obj.GroupName = contractorDoc.GroupCodeName;
            obj.AreaId = contractorDoc.AreaId;
            obj.AreaCode = contractorDoc.AreaName;
            obj.UnitId = contractorDoc.UnitCodeId;
            obj.UnitCode = contractorDoc.UnitCodeName;
            obj.Sequence = Convert.ToInt32(contractorDoc.Sequence);
            obj.SequentialNumber = contractorDoc.Sequence;
            obj.Year = Convert.ToInt32(contractorDoc.Year);
            obj.TypeId = contractorDoc.ChangeRequestTypeId;
            obj.TypeName = contractorDoc.ChangeRequestTypeName;
        }


        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }

        private void ExportContractorETRM(ContractorTransmittal transObj)
        {
            var attachDocFullList = this.contractorTransmittalDocFileService.GetAllByTrans(transObj.ID);
            var attachDocFileFilter = new List<ContractorTransmittalDocFile>();

            // Remove duplicate Document
            foreach (var document in attachDocFullList)
            {
                if (attachDocFileFilter.All(t => t.DocumentNo != document.DocumentNo))
                {
                    attachDocFileFilter.Add(document);
                }
            }
            // --------------------------------------------------------------------------------------------

            var filePath = Server.MapPath("../../Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PECC2_ContractorTransTemplate.xlsm");
            var workSheets = workbook.Worksheets;
            var transSheet = workSheets[0];
            //var fileListSheet = workSheets[9];
            // Export trans Info
            var dtFull = new DataTable();
            dtFull.Columns.AddRange(new[]
            {
                new DataColumn("DocNo", typeof(String)),
                new DataColumn("1Empty", typeof(String)),
                new DataColumn("Revision", typeof(string)),
                new DataColumn("ActionCode", typeof(String)),
                new DataColumn("DocTitle", typeof(String)),
                new DataColumn("2Empty", typeof(String)),
                new DataColumn("3Empty", typeof(String)),
                new DataColumn("4Empty", typeof(String)),
                new DataColumn("5Empty", typeof(String)),
                new DataColumn("6Empty", typeof(String)),
                new DataColumn("RevRemark", typeof(String)),
                new DataColumn("7Empty", typeof(String)),
            });

            var count = 1;
            foreach (var doc in attachDocFileFilter)
            {
                var dataRow = dtFull.NewRow();
                dataRow["DocNo"] = doc.DocumentNo;
                dataRow["DocTitle"] = doc.DocumentTitle;
                dataRow["Revision"] = doc.Revision;
                dataRow["ActionCode"] = doc.PurposeName;
                dataRow["RevRemark"] = doc.RevRemark;
                dtFull.Rows.Add(dataRow);
                count += 1;
            }

            transSheet.Cells.ImportDataTable(dtFull, false, 8, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);

            for (int i = 0; i < attachDocFileFilter.Count; i++)
            {
                transSheet.Cells.Merge(8 + i, 0, 1, 2);
                transSheet.Cells.Merge(8 + i, 4, 1, 6);
                transSheet.Cells.Merge(8 + i, 10, 1, 2);
            }

            //transSheet.Cells.DeleteRow(19 + attachDocFileFilter.Count);
            var organisationObj =
                this.organizationCodeService.GetById(transObj.OriginatingOrganizationId.GetValueOrDefault());
            var projectObj = this.projectCodeService.GetById(transObj.ProjectId.GetValueOrDefault());
            transSheet.Cells["A2"].PutValue(projectObj.FullName);
            transSheet.Cells["J1"].PutValue(transObj.TransNo);
            transSheet.Cells["H5"].PutValue(projectObj.Code);
            transSheet.Cells["H2"].PutValue(transObj.TransDate.GetValueOrDefault().ToString("yyyy-MM-dd"));
            transSheet.Cells["C3"].PutValue(transObj.OriginatingOrganizationName);
            if (organisationObj != null)
            {
                transSheet.Cells["C4"].PutValue(organisationObj.HeadOffice);
                transSheet.Cells["C5"].PutValue(organisationObj.Phone);
                transSheet.Cells["C6"].PutValue(organisationObj.Fax);
            }

            // ---------------------------------------------------------------------

            var savePath = Server.MapPath("../.." + transObj.StoreFolderPath) + "\\eTRM File\\";
            var fileName = Utilities.Utility.RemoveSpecialCharacter(transObj.TransNo) + "_eTRM_" +
                           transObj.TransDate.GetValueOrDefault().ToString("dd-MM-yyyy") + ".xlsm";
            workbook.Save(savePath + fileName);

            this.Download_File(savePath + fileName);
        }

        private void ExportETRM(PVGASTransmittal transObj, string forSend)
        {
            var attachDocToTrans = this.attachDocToTransmittalService.GetAllByTransId(transObj.ID);
            if (attachDocToTrans != null)
            {
                var targetFolder = "../.." + transObj.StoreFolderPath + "/eTRM File";
                var templatePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Exports") + @"\";

                //var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(templatePath + @"Template\PVGASTransmittalTemplate.xlsm");

                var dataSheet = workbook.Worksheets[0];

                var dtFull = new DataTable();

                dtFull.Columns.AddRange(new[]
                {
                    new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("IncomingTransNo", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("CodeName", typeof(String)),
                    new DataColumn("Remark", typeof(String)),
                });

                var count = 1;
                foreach (var docobj in attachDocToTrans)
                {
                    var dataRow = dtFull.NewRow();
                  
                            var documentObj = this.documentProjectService.GetById(docobj.DocumentId.GetValueOrDefault());
                            dataRow["Index"] = count;
                            dataRow["DocTitle"] = documentObj.DocTitle;
                            dataRow["DocNo"] = documentObj.DocNo;
                            dataRow["IncomingTransNo"] = documentObj.IncomingTransNo;
                            dataRow["RevisionName"] = documentObj.RevisionName;
                            dataRow["CodeName"] = documentObj.CodeName;
                            dataRow["Remark"] = documentObj.Remarks;

                         

                    dtFull.Rows.Add(dataRow);
                    count += 1;
                }

                var projectObj = this.projectCodeService.GetById(transObj.ProjectCodeId.GetValueOrDefault());

                var filename = Utilities.Utility.RemoveSpecialCharacter(transObj.TransmittalNo) + "_Trans_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsm";
                dataSheet.Cells["C5"].PutValue(transObj.IssuedDate.GetValueOrDefault().ToString("dd/MM/yyyy"));
                dataSheet.Cells["F5"].PutValue(transObj.TransmittalNo);
                ////dataSheet.Cells["C4"].PutValue(transObj.OriginatingOrganizationName);
                ////dataSheet.Cells["C5"].PutValue(transObj.ReceivingOrganizationName);
              //  dataSheet.Cells["B10"].PutValue(transObj.Attn);
               // dataSheet.Cells["B12"].PutValue(transObj.Contract);
               


                dataSheet.Cells.ImportDataTable(dtFull, false, 18, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);

                for (int i = 0; i < dtFull.Rows.Count; i++)
                {
                    dataSheet.Cells.Merge(18 + i, 1, 1, 2);
                    dataSheet.Cells.Merge(18 + i, 3, 1, 2);
                  //  dataSheet.Cells.Merge(12 + i, 5, 1, 2);
                }

               // dataSheet.Cells.DeleteRow(12 + dtFull.Rows.Count);
                dataSheet.Cells[19 + dtFull.Rows.Count, 1].PutValue(transObj.CCValue);
                dataSheet.Cells[20 + dtFull.Rows.Count, 1].PutValue(transObj.FileTo);

                // Fill Signed
                ////if (!string.IsNullOrEmpty(UserSession.Current.User.SignImageUrl))
                ////{
                ////    dataSheet.Pictures.Add(23 + dtFull.Rows.Count, 2, Server.MapPath("../.." + UserSession.Current.User.SignImageUrl));
                ////}
                // ---------------------------------------------------------------------
                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), filename);
                workbook.Save(saveFilePath);

                // Insert trans cover to TransAttachFiles
                var transCover = this.transmittalAttachFileService.GetByTrans(transObj.ID).FirstOrDefault(t => t.TypeId == 1);
                if (transCover != null)
                {
                    File.Delete(Server.MapPath("~" + transCover.FilePath));
                    this.transmittalAttachFileService.Delete(transCover);
                }
                    var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                    + transObj.StoreFolderPath + "/eTRM File";
                    var serverFilePath = serverFolder + "/" + filename;
                    var fileInfo = new FileInfo(saveFilePath);
                    var attachFile = new PVGASTransmittalAttachFile()
                    {
                        ID = Guid.NewGuid(),
                        TransId = transObj.ID,
                        Filename = filename,
                        Extension = "xlsm",
                        FilePath = serverFilePath,
                        ExtensionIcon = "~/images/excelfile.png",
                        FileSize = (double)fileInfo.Length / 1024,
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedByName = UserSession.Current.User.FullName,
                        CreatedDate = DateTime.Now,
                        TypeId = 1,
                        TypeName = "Transmittal Cover"
                    };

                    this.transmittalAttachFileService.Insert(attachFile);
                
                // ------------------------------------------------------------------------------------------------

                this.Download_File(saveFilePath);
            }
        }
        protected void ddlProjectOutgoing_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var ddlProjectOutgoing = (RadComboBox)this.radMenuOutgoing.Items[2].FindControl("ddlProjectOutgoing");
            int projectId = Convert.ToInt32(ddlProjectOutgoing.SelectedValue);
            this.lblProjectOutgoingId.Value = projectId.ToString();
            this.grdOutgoingTrans.Rebind();
        }

        protected void ddlProjectIncoming_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var ddlProjectIncoming = (RadComboBox)this.radMenuIncoming.Items[2].FindControl("ddlProjectIncoming");
            int projectId = Convert.ToInt32(ddlProjectIncoming.SelectedValue);
            this.lblProjectIncomingId.Value = projectId.ToString();
            this.grdIncomingTrans.Rebind();
        }

        protected void ddlProjectOutgoing_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"~/Images/project.png";
        }

        protected void ddlProjectIncoming_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"~/Images/project.png";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void grdOutgoingTrans_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var ddlProjectOutgoing = (RadComboBox)this.radMenuOutgoing.Items[2].FindControl("ddlProjectOutgoing");
            var ddlStatusOutgoing = (DropDownList)this.radMenuOutgoing.Items[2].FindControl("ddlStatusOutgoing");
            var txtSearchOutgoing = (TextBox)this.radMenuOutgoing.Items[2].FindControl("txtSearchOutgoing");
            var outgoingTransList = new List<PVGASTransmittal>();

            if (ddlProjectOutgoing != null && ddlProjectOutgoing.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(ddlProjectOutgoing.SelectedValue);

                outgoingTransList = this.transmittalService.GetAllByProject(projectId, 2, txtSearchOutgoing.Text).OrderByDescending(t => t.TransmittalNo).ToList();

                if (ddlStatusOutgoing != null)
                {
                    switch (ddlStatusOutgoing.SelectedValue)
                    {
                        case "Invalid":
                            outgoingTransList = outgoingTransList.Where(t => !t.IsValid.GetValueOrDefault()).ToList();
                            break;
                        case "Waiting":
                            outgoingTransList = outgoingTransList.Where(t => t.IsValid.GetValueOrDefault() && !t.IsSend.GetValueOrDefault()).ToList();
                            break;
                        case "Sent":
                            outgoingTransList = outgoingTransList.Where(t => t.IsSend.GetValueOrDefault()).ToList();
                            break;
                    }
                }
                if (this.rtvOrganizationOut.SelectedNode != null && this.rtvOrganizationOut.SelectedNode.Value != "0")
                {
                    outgoingTransList = outgoingTransList.Where(t => t.ReceivingOrganizationId.GetValueOrDefault() == Convert.ToInt32(this.rtvOrganizationOut.SelectedNode.Value)).ToList();
                }

            }

            this.grdOutgoingTrans.DataSource = outgoingTransList.OrderByDescending(t=>t.IssuedDate.GetValueOrDefault());
        }

        protected void grdIncomingTrans_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var ddlProjectIncoming = (RadComboBox)this.radMenuIncoming.Items[2].FindControl("ddlProjectIncoming");
            var ddlStatusIncoming = (DropDownList)this.radMenuIncoming.Items[2].FindControl("ddlStatusIncoming");
            var txtSearchIncoming = (TextBox)this.radMenuIncoming.Items[2].FindControl("txtSearchIncoming");
            var incomingTransList = new List<PVGASTransmittal>();
            if (ddlProjectIncoming != null && ddlProjectIncoming.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(ddlProjectIncoming.SelectedValue);

                incomingTransList = this.transmittalService.GetAllByProject(projectId, 1, txtSearchIncoming.Text).OrderByDescending(t => t.TransmittalNo).ToList();

                if (ddlStatusIncoming != null)
                {
                    switch (ddlStatusIncoming.SelectedValue)
                    {
                        case "WaitingImport":
                            incomingTransList = incomingTransList.Where(t => !t.IsImport.GetValueOrDefault()).ToList();
                            break;
                        case "Imported":
                            incomingTransList = incomingTransList.Where(t => t.IsImport.GetValueOrDefault()).ToList();
                            break;
                    }
                }
                if(this.rtvorinationIn.SelectedNode != null && this.rtvorinationIn.SelectedNode.Value != "0")
                {
                    incomingTransList = incomingTransList.Where(t => t.OriginatingOrganizationId.GetValueOrDefault()== Convert.ToInt32(this.rtvorinationIn.SelectedNode.Value)).ToList();
                }
            }

            this.grdIncomingTrans.DataSource = incomingTransList.OrderByDescending(t=> t.IssuedDate.GetValueOrDefault());
        }

        protected void ddlStatusOutgoing_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.grdOutgoingTrans.Rebind();
        }

        protected void btnSearchOutgoing_Click(object sender, EventArgs e)
        {
            
            this.grdOutgoingTrans.Rebind();
        }

        protected void ddlStatusIncoming_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.grdIncomingTrans.Rebind();
        }

        protected void btnSearchIncoming_Click(object sender, EventArgs e)
        {
            this.grdIncomingTrans.Rebind();
        }

        protected void grdOutgoingTrans_OnDeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var objId = new Guid(item.GetDataKeyValue("ID").ToString());
            var transObj = this.transmittalService.GetById(objId);
            if (!string.IsNullOrEmpty(transObj?.StoreFolderPath))
            {
                var folderPath = Server.MapPath("../.." + transObj.StoreFolderPath);
                if (Directory.Exists(folderPath))
                {
                    Directory.Delete(folderPath);
                }
            }

            this.transmittalService.Delete(objId);
            this.grdOutgoingTrans.Rebind();

        }
        private void NotifiNewTransmittal(PVGASTransmittal transmittal)
        {
            try
            {

                if (transmittal != null)
                {

                    var userList = this.userService.GetAllByDC().Where(t => !string.IsNullOrEmpty(t.Email)).ToList();
                    var projctobj = this.projectCodeService.GetById(transmittal.ProjectCodeId.GetValueOrDefault());
                    var roleObj = this.roleService.GetByContractor(transmittal.ReceivingOrganizationId.GetValueOrDefault()).Select(t => t.Id).ToList();
                    //var smtpClient = new SmtpClient
                    //{
                    //    DeliveryMethod = SmtpDeliveryMethod.Network,
                    //    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    //    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    //    Host = ConfigurationManager.AppSettings["Host"],
                    //    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    //    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    //};
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        //UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        //EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        //TargetName = "STARTTLS/smtp.gmail.com",
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        // Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        Credentials = new NetworkCredential(UserSession.Current.User.Email, "")
                    };
                    int count = 0;
                    var containtable = string.Empty;

                    var subject = "FYI: New transmittal submitted, " + transmittal.TransmittalNo + ", " + transmittal.IssuedDate.GetValueOrDefault().ToString("dd/MM/yyyy") + ", " + transmittal.Description;

                    var message = new MailMessage();
                    message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    var emailto = string.Empty;
                    var to = userList.Where(t => roleObj.Contains(t.RoleId.GetValueOrDefault())).ToList();
                    foreach (var user in to)
                    {

                        try
                        {
                            if (user.Email.Contains(";"))
                            {
                                foreach (string stemail in user.Email.Split(';').Where(t => !string.IsNullOrEmpty(t)).ToList())
                                {
                                    message.To.Add(new MailAddress(stemail));
                                    emailto += stemail + "; ";
                                }
                            }
                            else
                            {
                                message.To.Add(new MailAddress(user.Email));
                                emailto += user.Email + "; ";
                            }

                        }
                        catch { }
                    }
                    //var infoUserIds = customObj.CCUserIDs != null
                    //   ? customObj.CCUserIDs.Split(';').ToList()
                    //   : new List<string>();


                    var emailCC = string.Empty;
                    //var UsserCC = this.userService.GetListUser(infoUserIds.Distinct().Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList());
                    var listCC = userList.Where(t => t.Id == transmittal.CreatedBy).ToList();
                    foreach (var user in listCC)
                    {

                        try
                        {
                            if (user.Email.Contains(";"))
                            {
                                foreach (string stemail in user.Email.Split(';').Where(t => !string.IsNullOrEmpty(t)).ToList())
                                {
                                    message.CC.Add(new MailAddress(stemail));
                                    emailCC += stemail + "; ";
                                }
                            }
                            else
                            {
                                message.CC.Add(new MailAddress(user.Email));
                                emailCC += user.Email + "; ";
                            }

                        }
                        catch { }

                    }



                    var bodyContent = @"<head><title></title><style>
                  body {font-family:Calibri;font-size:10px;}
                hr {color:#2C4E9C;background-color:#2C4E9C; height:3px;}
                .msg {font-size:16px;}                        
                table {width:98.0%;border-collapse:collapse;margin-left:20px;color:black;background-color:white;border:1px solid #ACCEF5;padding:3px;font-size:16px;}
                td {border:1px solid #ACCEF5;}
                .span1 {font-size:16px;}
                .ch1 {background-color:#F7FAFF;padding:10px;font-weight:bold;color:#2C4E9C;}
                .ch2 {background-color:#F7FAFF;padding:5px;}
                a {color:mediumblue;}
                .system {font-weight:bolder; font-family:'Bookman Old Style'; color:#2C4E9C;}
                .company {font-weight:bolder; font-family:'Bookman Old Style'; color:#2C4E9C;}
                .link {font-size:16px;margin-left:30px;}
                .footer {color:darkgray; font-size:12px;}
                /*TYPE OF NOTIFICATION PURPOSE*/
                .action {background-color:#fffda5;}
                .info {background-color:#d1fcbd;}
                .overdue {background-color:#f00;color:white;font-weight:bold;}
                  .header_ {width:50.0%;border:none;border-bottom:solid #98C6EA 1.0pt;mso-border-bottom-alt:solid #98C6EA .75pt;background:#D4EFFC;padding:3.75pt 3.75pt 3.75pt 3.75pt}
                  .footer_ {border:none;border-top:solid #98C6EA 1.0pt;mso-border-top-alt:solid #98C6EA .75pt;background:#D4EFFC;padding:6.0pt 6.0pt 6.0pt 6.0pt}
                  .font_l {font-size:13.5pt;font-family:'Verdana',sans-serif}
                .font_m {font-size:10.0pt;font-family:'Verdana',sans-serif}
                .font_s {font-size:9.0pt;font-family:'Verdana',sans-serif}
                .font_xs {font-size:7.5pt;font-family:'Verdana',sans-serif}
                </style></head>
                <body>
                 <table border='1'>
                  <tr>
                                <td width='50%' class='header_'>
								<b><span class='font_m'>" + transmittal.Description + @"</span></b><br>				
								<b><span class='font_xs' style='color:red'>" + projctobj.Description + ", " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + @"</span></b>
							</td>
							<td width='50%' class='header_'>
								<p class='MsoNormal' align='right' style='text-align:right'>
									<b><span class='font_l' style='color:#000066'>PVGAS</span></b>
									<em><b><span class='font_l' style='color:red'>SEG</span></b></em>
								</p>
							</td>
                    </tr>
                  
                  <tr><td colspan='2' > 
                    <p align='center' style='margin-bottom:12.0pt;text-align:center'>
							<span class='font_m'>
								<br><b>New Transmittal :</b> Transmittal <b>" + transmittal.TransmittalNo + @"</b> has been sent to you for your review
							</span>
						</p>
						<div align='center' style='border:none;padding:3.75pt 3.75pt 3.75pt 3.75pt'>
                    <table border='1'>
                <tr>
                    <td class='ch2' style='width:167px' ><span class='font_s' style='color:#003399'>From</span> </td><td colspan='3'><span class='font_s' style='color:#003399'>" + UserSession.Current.User.FullNameWithPosition + @"</span></td>
                   
                </tr>
                <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>To</span></td><td colspan='3'><span class='font_s' style='color:#003399'>
								<a href='mailto:" + emailto + "'>" + emailto + @"</a>
							</span></td>
                   
                </tr>
   <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>CC</span></td><td colspan='3'><span class='font_s' style='color:#003399'>
								<a href='mailto:" + emailCC + "'>" + emailCC + @"</a>
							</span></td>
                   
                </tr>
                <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>Transmittal No.</span></td><td colspan='3' class='font_s' style='color:red'>" + transmittal.TransmittalNo + @"</td>
                   
               
                <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>Issued Date</span></td><td class='font_s'>" + transmittal.IssuedDate.GetValueOrDefault().ToString("dd/MM/yyyy") + @"</td>
                    
                  
                  </table>
                  </div>";

                    var st = ConfigurationManager.AppSettings["WebAddress"] + @"/Controls/Document/PVGASTransmittalList.aspx?TransNoPVGAS=" + transmittal.TransmittalNo;
                    var st1 = ConfigurationManager.AppSettings["WebAddress"] + @"/Controls/Document/PVGASTransmittalList.aspx";
                    bodyContent += @"  <p style='margin-bottom:12.0pt'>
			            <span class='font_m'>
				            <u><b>Useful Links:</b></u>
				            <ul class='font_m'>
					            <li>
								Click <a href='" + st + @"'>here</a> to show <u>this transmittal</u> in EDMS System
							</li>
							<li>
								Click <a href= '" + st1 + @"' > here</a> to show <u>all transmittals</u> in EDMS System
							</li>
						</ul>
					   </span>
						</p>			
						<p  align='center' style='margin-bottom:12.0pt'>
						<span class='font_m'>[THIS IS SYSTEM AUTO-GENERATED NOTIFICATION. PLEASE DO NOT REPLY.]
						</span>
						</p>
						</td>
						</tr>
						<tr>
							<td class='footer_'>
								<b><span class='font_xs'>PETROVIETNAM GAS JOINT STOCK CORPORATION (PV GAS)</span></b>
							</td>
							<td class='footer_'>
								<p  align=right style='text-align:right'>
									<b><span class='font_xs'>05th Floor, 673 Nguyen Huu Tho, Nha Be District, Ho Chi Minh, Vietnam <br>
                                                        Tel: +84 028 37840930  Fax: +84 028 37816606
									</span></b>
								</p>
							</td>
						</tr>
					</table></body>";
                    message.Body = bodyContent;

                    smtpClient.Send(message);
                }
            }
            catch (Exception ex) { }
        }
        private void NotifiNewTransmittal_Back(PVGASTransmittal transmittal)
        {
            try
            {
                if (transmittal != null)
                {

                    var userListid = this.userService.GetAllByRoleId(this.roleService.GetByContractor(transmittal.ReceivingOrganizationId.GetValueOrDefault()).FirstOrDefault().Id);

                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };
                    int count = 0;
                    var containtable = string.Empty;

                    var subject = "New Transmittal (#Trans#) has been sended from (" + transmittal.OriginatingOrganizationName + ")";

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject.Replace("#Trans#", transmittal.TransmittalNo);
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;

                    var bodyContent = @"<div style=‘text-align: center;’> 
                                    <span class=‘Apple-tab-span’>Dear All,&nbsp;</span><br />
                                   
                                    <p style=‘text-align: center;’><strong><span style=‘font-size: 18px;’>Please be informed that the following document of transmittal (#Trans#)</span></strong></p><br/><br/>

                                       <table border='1' cellspacing='0'>
                                       <tr>
                                       <th style='text-align:center; width:40px'>No.</th>
                                       <th style='text-align:center; width:330px'>Document Number</th>
                                       <th style='text-align:center; width:60px'>Revision</th>
                                       <th style='text-align:center; width:330px'>Document Title</th>
                                       <th style='text-align:center; width:330px'>Project</th>
                                       <th style='text-align:center; width:330px'>Issue Date</th>
                                       <th style='text-align:center; width:330px'>Code</th>
                                       <th style='text-align:center; width:330px'>Trans In</th>
                                       
                                       </tr>";
                    var listDocument = new List<DocumentPackage>();

                    var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(transmittal.ID);
                    foreach (var item in attachDocList)
                    {
                        var docObj = this.documentProjectService.GetById(item.DocumentId.GetValueOrDefault());
                        if (docObj != null)
                        {
                            listDocument.Add(docObj);
                        }
                    }
                    
                    var deadline = string.Empty;
                    deadline = transmittal.IssuedDate != null ? transmittal.IssuedDate.Value.ToString("dd/MM/yyyy") : "";

                    foreach (var document in listDocument)
                    {

                        count += 1;

                        bodyContent += @"<tr>
                               <td>" + count + @"</td>
                               <td>" + document.DocNo + @"</td>
                               <td>"
                                       + document.RevisionName + @"</td>
                               <td>"
                                       + document.DocTitle + @"</td>
                               <td>"
                                       + document.ProjectName + @"</td>
                               <td>"
                                       + deadline + @"</td>
                               <td>"
                                       + string.Empty + @"</td>
                               <td>"
                                       + document.IncomingTransNo + @"</td>";

                    }
                    var st = ConfigurationManager.AppSettings["WebAddress"] + @"/Controls/Document/ContractorTransmittalList.aspx";
                    bodyContent += @" </table>
                                       <br/>
                                       <span><br />
                                    &nbsp;This link to access&nbsp;:&nbsp; <a href='" + st + "'>" + st + "</a>" +
                                 @" <br/> &nbsp;&nbsp;&nbsp; EDMS TRANSMITTAL NOTIFICATION </br>
                        [THIS IS SYSTEM GENERATED NOTIFICATION PLEASE DO NOT REPLY]</span>";
                    message.Body = bodyContent.Replace("#Trans#", transmittal.TransmittalNo); ;

                    var Userlist = userListid.Where(t => !string.IsNullOrEmpty(t.Email)).Distinct().ToList();
                    foreach (var user in Userlist)
                    {
                        try
                        {
                            message.To.Add(new MailAddress(user.Email));
                        }
                        catch { }

                    }
                    smtpClient.Send(message);
                }
            }
            catch { }
        }

        private void NotifiNewTransmittalOut(PVGASTransmittal transmittal)
        {
            try
            {
                if (transmittal != null)
                {
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };
                    int count = 0;
                    var containtable = string.Empty;

                    var subject = "New Transmittal (#Trans#) has been sent to contractor (" + transmittal.ReceivingOrganizationName+ ")";

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject.Replace("#Trans#", transmittal.TransmittalNo);
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;

                    var bodyContent = @"<div style=‘text-align: center;’> 
                                    <span class=‘Apple-tab-span’>Dear All,&nbsp;</span><br />
                                   
                                    <p style=‘text-align: center;’><strong><span style=‘font-size: 18px;’>Please be informed the document of transmittal (#Trans#)</span></strong></p><br/><br/>

                                       <table border='1' cellspacing='0'>
                                       <tr>
                                       <th style='text-align:center; width:40px'>No.</th>
                                       <th style='text-align:center; width:330px'>Document Number</th>
                                       <th style='text-align:center; width:60px'>Revision</th>
                                       <th style='text-align:center; width:330px'>Document Title</th>
                                        <th style='text-align:center; width:330px'>Project</th>
                                        <th style='text-align:center; width:330px'>Code</th>
                                       </tr>";
                    var listDocument = new List<DocumentPackage>();

                    var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(transmittal.ID);
                    foreach (var item in attachDocList)
                    {
                        var docObj = this.documentProjectService.GetById(item.DocumentId.GetValueOrDefault());
                        if (docObj != null)
                        {
                            listDocument.Add(docObj);
                        }
                    }

                    var deadline = string.Empty;
                    deadline = transmittal.DueDate != null ? transmittal.DueDate.Value.ToString("dd/MM/yyyy") : "";

                    foreach (var document in listDocument)
                    {

                        count += 1;

                        bodyContent += @"<tr>
                               <td>" + count + @"</td>
                               <td>" + document.DocNo + @"</td>
                               <td>"
                                       + document.RevisionName + @"</td>
                               <td>"
                                       + document.DocTitle + @"</td>
                               <td>"
                                       + document.ProjectName + @"</td>
                               <td>"
                                       + string.Empty + @"</td>";

                    }
                    var st = ConfigurationManager.AppSettings["WebAddress"] + @"/AdvanceSearch.aspx?TransOut="+transmittal.TransmittalNo;
                    bodyContent += @" </table>
                                       <br/>
                                       <span><br />
                                    &nbsp;This link to access&nbsp;:&nbsp; <a href='" + st + "'>" + st + "</a>" +
                                 @" <br/> &nbsp; EDMS TRANSMITTAL NOTIFICATION </br>
                        [THIS IS SYSTEM GENERATED NOTIFICATION PLEASE DO NOT REPLY]</span>";
                    message.Body = bodyContent.Replace("#Trans#", transmittal.TransmittalNo); ;


                   List<Guid> ListattachDocList = this.attachDocToTransmittalService.GetAllByTransId(transmittal.ID).Select(t=> (Guid)t.DocumentId).ToList();
                    var ListAllUserInWf = this.objectAssignedUser.GetAllListByDoc(ListattachDocList).Select(t=>(int) t.UserID).Distinct().ToList();
                    var Userlist = this.userService.GetListUser(ListAllUserInWf);
                    foreach (var user in Userlist)
                    {
                        try
                        {
                            message.To.Add(new MailAddress(user.Email));
                        }
                        catch { }

                    }
                    smtpClient.Send(message);
                }
            }
            catch { }
        }

        protected void rtvorinationIn_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.grdIncomingTrans.CurrentPageIndex = 0;
            this.grdIncomingTrans.Rebind();
        }

        protected void rtvorinationIn_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
           e.Node.ImageUrl = "~/Images/originator.png"; 
        }

        protected void rtvOrganizationOut_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            this.grdOutgoingTrans.CurrentPageIndex = 0;
            this.grdOutgoingTrans.Rebind();
        }

        protected void rtvOrganizationOut_NodeDataBound(object sender, RadTreeNodeEventArgs e)
        {
            e.Node.ImageUrl = "~/Images/originator.png";
        }
    }
}