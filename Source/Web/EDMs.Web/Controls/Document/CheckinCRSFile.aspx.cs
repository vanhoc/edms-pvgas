﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.UI;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using EDMs.Web.Utilities.Sessions;
using iTextSharp.awt.geom;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class CheckinCRSFile : Page
    {


        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly DocumentPackageService documentpackageservice;

        private readonly PVGASDocumentAttachFileService attachfileservice;

      
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public CheckinCRSFile()
        {
            this.attachfileservice = new PVGASDocumentAttachFileService();
            this.documentpackageservice = new DocumentPackageService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
         
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.lblmess.Visible = false;
                this.lblUserId.Value = UserSession.Current.User.Id.ToString();
                if (!string.IsNullOrEmpty(Request.QueryString["objId"]))
                {
                    var transId = new Guid(this.Request.QueryString["objId"]);
                //    var crsList =
                //this.pecc2TransmittalAttachFileService.GetByTrans(transId)
                //    .FirstOrDefault(t => t.TypeId == 2);

                //    if (crsList != null)
                //    {
                //        this.IsHasCRSFile.Value = "True";
                //    }
                //    else
                //    {
                //        this.IsHasCRSFile.Value = "False";
                //    }
                }
            }
        
        }

       
        private void ReuploadFileCRS()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["objId"]))
            {
                var objId = new Guid(this.Request.QueryString["objId"]);
              
                var crsFile = this.attachfileservice.GetById(objId);
                var docObj = this.documentpackageservice.GetById(crsFile.ProjectDocumentId.GetValueOrDefault());
                var targetFolder =  "../../DocumentLibrary/ProjectDocs";
                var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                    + "/DocumentLibrary/ProjectDocs";
                foreach (UploadedFile docFile in docuploader.UploadedFiles)
                {
                    var fileExt = docFile.GetExtension();
                    try
                    {
                        if (File.Exists(Server.MapPath("../.." + crsFile.FilePath)))
                        {
                            File.Delete(Server.MapPath("../.." + crsFile.FilePath));
                        }
                    }
                    catch { }
                    var serverDocFileName = docObj.ID.ToString()+ DateTime.Now.ToBinary()+ fileExt;
                    var filename="CRS_" + Utility.RemoveSpecialCharacterFileName(docObj.DocNo) +"_"+ docObj.RevisionName + fileExt;

                    // Path file to save on server disc
                    var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);
                    // Path file to download from server
                    var serverFilePath = serverFolder + "/" + serverDocFileName;
                    // Path file to download from server
                    docFile.SaveAs(saveFilePath, true);
                    crsFile.ExtensionIcon = Utility.FileIcon.ContainsKey(fileExt.ToLower()) ? Utility.FileIcon[fileExt.ToLower()] : "~/images/otherfile.png";
                    crsFile.FilePath = serverFilePath;
                    crsFile.FileName = filename;
                    crsFile.Extension = fileExt;
                    crsFile.IsCheckOut = false;
                    crsFile.CheckinDate = DateTime.Now;
                    crsFile.CreatedDate = DateTime.Now;
                    crsFile.CreatedBy = UserSession.Current.User.Id;
                    crsFile.CreatedByName = UserSession.Current.User.UserNameWithFullName;
                    crsFile.FileSize = (double)docFile.ContentLength / 1024;
                    this.attachfileservice.Update(crsFile);
                }

                this.docuploader.UploadedFiles.Clear();
                this.lblmess.Visible = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.ReuploadFileCRS();
            this.docuploader.UploadedFiles.Clear();
            //  this.lblmess.Visible = true;
            ScriptManager.RegisterStartupScript(this, GetType(), "close", "CancelEdit();", true);
        }
        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "UploadCRSFile")
            {
                this.ReuploadFileCRS();
            }
        }
    }
}