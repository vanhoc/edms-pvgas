﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Linq;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Security;
    using EDMs.Business.Services.Scope;
    using EDMs.Business.Services.Library;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class CategoryEditForm : Page
    {
        private readonly PackageService _packageService;
        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly CategoryService CategoryService;
        
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly ProjectCodeService projectService;
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public CategoryEditForm()
        {
            this.userService = new UserService();
            this.CategoryService = new CategoryService();
            this._packageService = new PackageService();
            this.projectService = new ProjectCodeService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                var projectList = this.projectService.GetAll().OrderBy(t => t.Code).ToList();
               // projectList.Insert(0, new ProjectCode() { ID = 0 });
                this.ddlProject.DataSource = projectList;
                this.ddlProject.DataTextField = "FullName";
                this.ddlProject.DataValueField = "ID";
                this.ddlProject.DataBind();
                this.ddlProject.SelectedIndex = 0;
                this.LoadList();
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var objDis = this.CategoryService.GetById(Convert.ToInt32(this.Request.QueryString["disId"]));
                    if (objDis != null)
                    {
                        this.txtName.Text = objDis.Name;
                        this.txtDescription.Text = objDis.Description;
                        this.ddlProject.SelectedValue = objDis.ProjectId.GetValueOrDefault().ToString();
                        this.ddlPackage.SelectedValue = objDis.PackageId.GetValueOrDefault().ToString();
                        var rtvparent = (RadTreeView)this.ddlParentName.Items[0].FindControl("rtvParentName");
                        if (rtvparent != null && objDis.ParentId != 0 && objDis.ParentId != null)
                        {
                            var nodeObj = rtvparent.FindNodeByValue(objDis.ParentId.GetValueOrDefault().ToString());
                            if (nodeObj != null)
                            {
                                nodeObj.Selected = true;
                                this.ddlParentName.Items[0].Text = nodeObj.Text;
                                this.ddlParentName.Items[0].Value = nodeObj.Value;
                            }
                        }
                        var createdUser = this.userService.GetByID(objDis.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + objDis.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (objDis.LastUpdatedBy != null && objDis.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(objDis.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + objDis.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        private void LoadList()
        {
            var packagelist = this._packageService.GetAllPackageOfProject(Convert.ToInt32(this.ddlProject.SelectedValue));
            this.ddlPackage.DataSource = packagelist.OrderBy(t=> t.ID);
            this.ddlPackage.DataTextField = "Name";
            this.ddlPackage.DataValueField = "ID";
            this.ddlPackage.DataBind();

            var categorylist = this.CategoryService.GetAllOfProject(Convert.ToInt32(this.ddlProject.SelectedValue));
            var rtvobj = (RadTreeView)ddlParentName.Items[0].FindControl("rtvParentName");
            if (rtvobj != null)
            {
                categorylist.Insert(0, new Category() { Name = string.Empty });

                rtvobj.DataSource = categorylist;
                rtvobj.DataFieldParentID = "ParentId";
                rtvobj.DataTextField = "Name";
                rtvobj.DataValueField = "ID";
                rtvobj.DataFieldID = "ID";
                rtvobj.DataBind();

                if (rtvobj.Nodes.Count > 0)
                {
                    rtvobj.Nodes[0].Expanded = true;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["disId"]))
                {
                    var disId = Convert.ToInt32(this.Request.QueryString["disId"]);
                    var obj = this.CategoryService.GetById(disId);
                    if (obj != null)
                    {
                        obj.Name = this.txtName.Text.Trim();
                        obj.Description = this.txtDescription.Text.Trim();
                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedDate = DateTime.Now;
                        obj.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                        obj.ProjectName = this.ddlProject.SelectedItem.Text;
                        obj.PackageId = Convert.ToInt32(this.ddlPackage.SelectedValue);
                        obj.PackageName = this.ddlPackage.SelectedItem.Text;
                        var rtvparent = (RadTreeView)this.ddlParentName.Items[0].FindControl("rtvParentName");
                        if (rtvparent != null && rtvparent.SelectedNode != null)
                        {
                            obj.ParentId =Convert.ToInt32(rtvparent.SelectedNode.Value);
                            obj.ParentName = rtvparent.SelectedNode.Text;
                        }
                        else
                        {
                            obj.ParentId = (int?)null;
                            obj.ParentName = string.Empty;
                        }
                        this.CategoryService.Update(obj);
                    }
                }
                else
                {
                    var obj = new Category()
                    {
                        Name = this.txtName.Text.Trim(),
                        Description = this.txtDescription.Text.Trim(),
                        
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now
                    };
                    obj.ProjectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                    obj.ProjectName = this.ddlProject.SelectedItem.Text;
                    obj.PackageId = Convert.ToInt32(this.ddlPackage.SelectedValue);
                    obj.PackageName = this.ddlPackage.SelectedItem.Text;
                    var rtvparent = (RadTreeView)this.ddlParentName.Items[0].FindControl("rtvParentName");
                    if (rtvparent != null && rtvparent.SelectedNode != null)
                    {
                        obj.ParentId = Convert.ToInt32(rtvparent.SelectedNode.Value);
                        obj.ParentName = rtvparent.SelectedNode.Text;
                    }
                    else
                    {
                        obj.ParentId = (int?)null;
                        obj.ParentName = string.Empty;
                    }
                    this.CategoryService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtName.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter Discipline name.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LoadList();
        }

        protected void ddlPackage_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categorylist = this.CategoryService.GetAllOfProjectAndPAckage(Convert.ToInt32(this.ddlProject.SelectedValue),Convert.ToInt32(this.ddlPackage.SelectedValue));
            var rtvobj = (RadTreeView)ddlParentName.Items[0].FindControl("rtvParentName");
            if (rtvobj != null)
            {
                categorylist.Insert(0, new Category() { Name = string.Empty });

                rtvobj.DataSource = categorylist;
                rtvobj.DataFieldParentID = "ParentId";
                rtvobj.DataTextField = "Name";
                rtvobj.DataValueField = "ID";
                rtvobj.DataFieldID = "ID";
                rtvobj.DataBind();

                if (rtvobj.Nodes.Count > 0)
                {
                    rtvobj.Nodes[0].Expanded = true;
                }
            }
        }
    }
}