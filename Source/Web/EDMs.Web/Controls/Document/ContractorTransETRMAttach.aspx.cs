﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Hosting;
using System.Web.UI;
using System.Linq;
using EDMs.Business.Services.Document;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ContractorTransETRMAttach : Page
    {
        private readonly ContractorTransmittalAttachFileService transAttachFileService;

        private readonly ContractorTransmittalService transService;
        private readonly ContractorTransmittalDocFileService contractorTransmittalDocFileService;
        /// <summary>
        /// Initializes a new instance of the <see cref="ContractorTransETRMAttach"/> class.
        /// </summary>
        public ContractorTransETRMAttach()
        {
            this.transAttachFileService = new ContractorTransmittalAttachFileService();
            this.transService = new ContractorTransmittalService();
            this.contractorTransmittalDocFileService = new ContractorTransmittalDocFileService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var objId = new Guid(this.Request.QueryString["objId"]);
                var transObj = this.transService.GetById(objId);
                this.divUploadControl.Visible = transObj != null && !transObj.IsSend.GetValueOrDefault();
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        protected void btnSaveAttachFile_Click(object sender, EventArgs e)
        {
            var objId = new Guid(this.Request.QueryString["objId"]);
            var transObj = this.transService.GetById(objId);
            var fileIcon = new Dictionary<string, string>()
                    {
                        { "doc", "~/images/wordfile.png" },
                        { "docx", "~/images/wordfile.png" },
                        { "dotx", "~/images/wordfile.png" },
                        { "xls", "~/images/excelfile.png" },
                        { "xlsx", "~/images/excelfile.png" },
                        { "pdf", "~/images/pdffile.png" },
                        { "7z", "~/images/7z.png" },
                        { "dwg", "~/images/dwg.png" },
                        { "dxf", "~/images/dxf.png" },
                        { "rar", "~/images/rar.png" },
                        { "zip", "~/images/zip.png" },
                        { "txt", "~/images/txt.png" },
                        { "xml", "~/images/xml.png" },
                        { "xlsm", "~/images/excelfile.png" },
                        { "bmp", "~/images/bmp.png" },
                    };

            var targetFolder = "../.." + transObj.StoreFolderPath + "/eTRM File";
            var serverFolder = (HostingEnvironment.ApplicationVirtualPath == "/" ? string.Empty : HostingEnvironment.ApplicationVirtualPath)
                    + transObj.StoreFolderPath + "/eTRM File";

            var docFileList = this.contractorTransmittalDocFileService.GetAllByTrans(objId).Select(t=> t.DocumentNo).ToList();
            foreach (UploadedFile docFile in docuploader.UploadedFiles)
            {
               // var docno = docFile.FileName.Replace(docFile.GetExtension(), "").Split('_')[1];

                var docFileName = docFile.FileName;

                var serverDocFileName = docFileName;

                // Path file to save on server disc
                var saveFilePath = Path.Combine(Server.MapPath(targetFolder), serverDocFileName);

                // Path file to download from server
                var serverFilePath = serverFolder + "/" + serverDocFileName;
                var fileExt = docFileName.Substring(docFileName.LastIndexOf(".") + 1, docFileName.Length - docFileName.LastIndexOf(".") - 1);

                docFile.SaveAs(saveFilePath, true);
                var typeId = cbETRM.Checked
                                  ? 1
                                  : cbCRS.Checked
                                      ? 2
                                      : 3;
                var typeName = cbETRM.Checked
                                ? "Transmittal Cover Sheet"
                                : cbCRS.Checked
                                    ? "Comment Response Sheet (CRS)"
                                    : "Other File";
                var attachFile = new ContractorTransmittalAttachFile()
                {
                    ID = Guid.NewGuid(),
                    ContractorTransId = objId,
                    Filename = docFileName,
                    Extension = fileExt,
                    FilePath = serverFilePath,
                    ExtensionIcon = fileIcon.ContainsKey(fileExt.ToLower()) ? fileIcon[fileExt.ToLower()] : "~/images/otherfile.png",
                    FileSize = (double)docFile.ContentLength / 1024,
                    CreatedBy = UserSession.Current.User.Id,
                    CreatedByName = UserSession.Current.User.FullName,
                    CreatedDate = DateTime.Now,
                    TypeId = typeId,
                    TypeName = typeName
                };

                this.transAttachFileService.Insert(attachFile);
            }

            this.docuploader.UploadedFiles.Clear();
             
            this.grdAttachFile.Rebind();
        }

        protected void grdAttachFile_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var objId = new Guid(item.GetDataKeyValue("ID").ToString());
            var tranObj = this.transAttachFileService.GetById(objId);
            if(tranObj!= null)
            {
                if (File.Exists(Server.MapPath("~"+tranObj.FilePath))) File.Delete(Server.MapPath("~" + tranObj.FilePath));
            this.transAttachFileService.Delete(objId);
            }
           
            this.grdAttachFile.Rebind();
        }

        protected void grdAttachFile_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var objId = new Guid(this.Request.QueryString["objId"]);
            var attachList = this.transAttachFileService.GetByTrans(objId);

            this.grdAttachFile.DataSource = attachList;
        }
        
    }
}