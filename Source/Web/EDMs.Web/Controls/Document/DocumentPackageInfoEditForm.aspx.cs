﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Business.Services.Scope;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using System.Collections.Generic;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageInfoEditForm : Page
    {



        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        /// <summary>
        /// The scope project service.
        /// </summary>
        private readonly ProjectCodeService scopeProjectService;

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly DocumentPackageService documentPackageService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        private readonly StatusService statusService;

        private readonly ProcessActualService processActualService;

        private readonly OriginatorService originatorService;

        private readonly DocumentSequenceManagementService documentSequenceManagementService;

        private readonly DocumentNumberingService documentNumberingService;

        private readonly PhaseService phaseService;

        private readonly AreaService areaService;

        private readonly SubAreaService subAreaService;

        private readonly DrawingDetailCodeService drawingDetailCodeService;

        private readonly DrawingCodeService drawingCodeService;

        private readonly SystemCodeService systemCodeService;

        private readonly OrganizationCodeService contractorService;

        private readonly DocumentStatuService DocumentStatusService;
        private readonly CodeService codeSerVice;
        private readonly PackageService PackageService;
        private readonly CategoryService categoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentPackageInfoEditForm()
        {
            this.revisionService = new RevisionService();
            this.documentService = new DocumentService();
            this.scopeProjectService = new ProjectCodeService();
            this.documentPackageService = new DocumentPackageService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.userService = new UserService();
            this.statusService = new StatusService();
            this.processActualService = new ProcessActualService();
            this.originatorService = new OriginatorService();
            this.documentSequenceManagementService = new DocumentSequenceManagementService();
            this.documentNumberingService = new DocumentNumberingService();
            this.codeSerVice = new CodeService();
            this.phaseService = new PhaseService();
            this.areaService = new AreaService();
            this.subAreaService = new SubAreaService();
            this.drawingDetailCodeService = new DrawingDetailCodeService();
            this.drawingCodeService = new DrawingCodeService();
            this.systemCodeService = new SystemCodeService();
            this.contractorService = new OrganizationCodeService();
            this.DocumentStatusService = new DocumentStatuService();
            this.PackageService = new PackageService();
            this.categoryService = new CategoryService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                ////this.LoadComboData();
                if (!UserSession.Current.User.IsAdmin.GetValueOrDefault() && !UserSession.Current.User.IsDC.GetValueOrDefault())
                {
                    this.btnSave.Visible = false;
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["projId"]))
                {
                    var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                    var projObj = this.scopeProjectService.GetById(projectId);
                    this.txtProjectName.Text = projObj.FullName;
                    this.LoadComboData(projectId);
                }

                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    //this.DocNumberingControl.Visible = false;
                    this.CreatedInfo.Visible = true;
                    var docId = new Guid(this.Request.QueryString["docId"]);
                    var docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        this.LoadComboData(docObj.ProjectId.GetValueOrDefault());
                        this.txtProjectName.Text = docObj.ProjectFullName;

                        this.LoadDocInfo(docObj);

                        var createdUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + docObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (docObj.LastUpdatedBy != null && docObj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(docObj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + docObj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    // this.DocNumberingControl.Visible = true;
                    this.CreatedInfo.Visible = false;
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid && !string.IsNullOrEmpty(this.Request.QueryString["projId"]))
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
                var projObj = this.scopeProjectService.GetById(projectId);
                DocumentPackage docObj;
                if (!string.IsNullOrEmpty(this.Request.QueryString["docId"]))
                {
                    var docId = new Guid(this.Request.QueryString["docId"]);
                    docObj = this.documentPackageService.GetById(docId);
                    if (docObj != null)
                    {
                        var oldRevision = docObj.RevisionId;
                        var newRevision = Convert.ToInt32(this.ddlRevision.SelectedValue);
                        if (newRevision > oldRevision && !string.IsNullOrEmpty(this.Request.QueryString["ReviseRev"]))
                        {
                            var docObjNew = new DocumentPackage();
                            this.CollectDataRevise(ref docObjNew, projObj);

                            // Insert new doc
                            docObjNew.ID = Guid.NewGuid();
                            docObjNew.CreatedBy = UserSession.Current.User.Id;
                            docObjNew.CreatedDate = DateTime.Now;
                            docObjNew.RevDate = DateTime.Now;
                            docObjNew.IsLeaf = true;
                            docObjNew.IsDelete = false;
                            docObjNew.IsEMDR = true;
                            docObjNew.RevisionActualDate = null;
                            docObjNew.RevisionReceiveTransNo = string.Empty;

                            docObjNew.ParentId = docObj.ParentId ?? docObj.ID;
                            docObjNew.DNV_FinalCode = docObj.DNV_FinalCode;
                            docObjNew.DNB_FinalCode = docObj.DNB_FinalCode;
                            docObjNew.KDN_Operator_FinalCode = docObj.KDN_Operator_FinalCode;
                            docObjNew.NCS_Operator_FinalCode = docObj.NCS_Operator_FinalCode;
                            docObjNew.KDN_Owner_FinalCode = docObj.KDN_Owner_FinalCode;
                            docObjNew.NCS_Owner_FinalCode = docObj.NCS_Owner_FinalCode;
                            if(this.documentPackageService.GetOneByDocNo(docObjNew.DocNo, docObjNew.RevisionName, docObjNew.ProjectId.GetValueOrDefault()) == null)
                            {  this.documentPackageService.Insert(docObjNew);
                            // Upate old doc
                            docObj.IsLeaf = false;

                            }
                          
                        }
                        else
                        {


                            this.CollectData(ref docObj, projObj);
                        }

                        docObj.LastUpdatedBy = UserSession.Current.User.Id;
                        docObj.LastUpdatedDate = DateTime.Now;
                        this.documentPackageService.Update(docObj);

                        //if (projObj.IsAutoCalculate.GetValueOrDefault())
                        //{
                        //    this.UpdateActualProgress(projObj);
                        //}
                    }
                }
                else
                {
                    docObj = new DocumentPackage()
                    {
                        ID = Guid.NewGuid(),
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedDate = DateTime.Now,
                        IsLeaf = true,
                        IsDelete = false
                    };

                    this.CollectData(ref docObj, projObj);
                    this.documentPackageService.Insert(docObj);

                    // Update Sequence of document
                    ////var sequenceObj = this.documentSequenceManagementService.GetByDisciplineDocType(docObj.DisciplineId, docObj.DocumentTypeId);
                    ////if (sequenceObj == null)
                    ////{
                    ////    sequenceObj = new DocumentSequenceManagement()
                    ////    {
                    ////        DisciplineId = docObj.DisciplineId,
                    ////        DocumentTypeId = docObj.DocumentTypeId,
                    ////        CurrentSequence = 1
                    ////    };

                    ////    this.documentSequenceManagementService.Insert(sequenceObj);
                    ////}
                    ////else
                    ////{
                    ////    sequenceObj.CurrentSequence += 1;
                    ////    this.documentSequenceManagementService.Update(sequenceObj);
                    ////}


                    //if (projObj.IsAutoCalculate.GetValueOrDefault())
                    //{
                    //    this.UpdateActualProgress(projObj);
                    //}
                    // ------------------------------------------------------
                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseWindow", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            ////if(this.txtName.Text.Trim().Length == 0)
            ////{
            ////    this.fileNameValidator.ErrorMessage = "Please enter file name.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = false;
            ////}
            ////else if (!string.IsNullOrEmpty(Request.QueryString["docId"]))
            ////{
            ////    var docId = Convert.ToInt32(Request.QueryString["docId"]);
            ////    this.fileNameValidator.ErrorMessage = "The specified name is already in use.";
            ////    this.divFileName.Style["margin-bottom"] = "-26px;";
            ////    args.IsValid = true; ////!this.documentService.IsDocumentExistUpdate(folderId, this.txtName.Text.Trim(), docId);
            ////}
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                var fileName = e.Argument.Split('$')[1];
                var folderId = Convert.ToInt32(Request.QueryString["folId"]);

                if (this.documentService.IsDocumentExist(folderId, fileName))
                {
                }
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData(int projectId)
        {
            var listDisciplineInPermission = this.disciplineService.GetAllDisciplineOfProject(projectId).OrderBy(t => t.Name).ToList();
            this.ddlDiscipline.DataSource = listDisciplineInPermission;
            listDisciplineInPermission.Insert(0, new Discipline() { ID = 0 });
            this.ddlDiscipline.DataTextField = "FullName";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();

            var listRevision = this.revisionService.GetAllByProject(projectId);
            this.ddlRevision.DataSource = listRevision;
            this.ddlRevision.DataTextField = "Name";
            this.ddlRevision.DataValueField = "ID";
            this.ddlRevision.DataBind();

            var docTypeList = this.documentTypeService.GetAll().OrderBy(t => t.Name).ToList();
            docTypeList.Insert(0, new DocumentType() { ID = 0 });
            this.ddlDocType.DataSource = docTypeList;
            this.ddlDocType.DataTextField = "FullName";
            this.ddlDocType.DataValueField = "ID";
            this.ddlDocType.DataBind();

            var originatorList = this.originatorService.GetAll().OrderBy(t => t.Name).ToList();
            originatorList.Insert(0, new Originator() { ID = 0 });
            this.ddlOriginator.DataSource = originatorList;
            this.ddlOriginator.DataTextField = "FullName";
            this.ddlOriginator.DataValueField = "ID";
            this.ddlOriginator.DataBind();

            var CodeReview = this.codeSerVice.GetAll().Where(t => t.TypeId == 2).ToList();
            CodeReview.Insert(0, new Code { ID = 0 });
            this.ddlDocReviewStatus.DataSource = CodeReview;
            this.ddlDocReviewStatus.DataTextField = "Name";
            this.ddlDocReviewStatus.DataValueField = "ID";
            this.ddlDocReviewStatus.DataBind();

            var CodeCA = this.codeSerVice.GetAll().Where(t => t.TypeId == 1).ToList();
            CodeCA.Insert(0, new Code { ID = 0 });
            this.ddlResponseCodeCA.DataSource = CodeCA;
            this.ddlResponseCodeCA.DataTextField = "Name";
            this.ddlResponseCodeCA.DataValueField = "ID";
            this.ddlResponseCodeCA.DataBind();
            //DNV
            this.ddl_KDN_OPERATOR_ResponeCode.DataSource = CodeCA;
            this.ddl_KDN_OPERATOR_ResponeCode.DataTextField = "Name";
            this.ddl_KDN_OPERATOR_ResponeCode.DataValueField = "ID";
            this.ddl_KDN_OPERATOR_ResponeCode.DataBind();
            //KDN
            this.ddl_KDN_OWNER_ResponeCode.DataSource = CodeCA;
            this.ddl_KDN_OWNER_ResponeCode.DataTextField = "Name";
            this.ddl_KDN_OWNER_ResponeCode.DataValueField = "ID";
            this.ddl_KDN_OWNER_ResponeCode.DataBind();
            //NCS
            this.ddl_NCS_OPERATOR_ResponeCode.DataSource = CodeCA;
            this.ddl_NCS_OPERATOR_ResponeCode.DataTextField = "Name";
            this.ddl_NCS_OPERATOR_ResponeCode.DataValueField = "ID";
            this.ddl_NCS_OPERATOR_ResponeCode.DataBind();
            //NCS
            this.ddl_NCS_OWNER_ResponeCode.DataSource = CodeCA;
            this.ddl_NCS_OWNER_ResponeCode.DataTextField = "Name";
            this.ddl_NCS_OWNER_ResponeCode.DataValueField = "ID";
            this.ddl_NCS_OWNER_ResponeCode.DataBind();

            var documentNumberingList = this.documentNumberingService.GetAll().OrderBy(t => t.Name).ToList();
            this.ddlDocumentNumbering.DataSource = documentNumberingList;
            this.ddlDocumentNumbering.DataTextField = "FullNameLenght";
            this.ddlDocumentNumbering.DataValueField = "ID";
            this.ddlDocumentNumbering.DataBind();

            var contractorList = this.contractorService.GetAll().OrderBy(t => t.Name).ToList();
            contractorList.Insert(0, new OrganizationCode { ID = 0 });
            this.ddlContractor.DataSource = contractorList;
            this.ddlContractor.DataTextField = "FullName";
            this.ddlContractor.DataValueField = "ID";
            this.ddlContractor.DataBind();

            var phaseList = this.phaseService.GetAllPhaseOfProject(projectId).OrderBy(t => t.Name).ToList();
            phaseList.Insert(0, new Phase() { ID = 0 });
            this.ddlPhase.DataSource = phaseList;
            this.ddlPhase.DataTextField = "FullName";
            this.ddlPhase.DataValueField = "ID";
            this.ddlPhase.DataBind();

            var areaList = this.areaService.GetAllAreaOfProject(projectId).OrderBy(t => t.Name).ToList();
            areaList.Insert(0, new Area() { ID = 0 });
            this.ddlArea.DataSource = areaList;
            this.ddlArea.DataTextField = "FullName";
            this.ddlArea.DataValueField = "ID";
            this.ddlArea.DataBind();

            var packagelist = this.PackageService.GetAllPackageOfProject(projectId).OrderBy(t => t.ID).ToList();
            packagelist.Insert(0, new Package() {ID=0, Name = string.Empty });
            this.ddlPackage.DataSource = packagelist;
            this.ddlPackage.DataTextField = "Name";
            this.ddlPackage.DataValueField = "ID";
            this.ddlPackage.DataBind();

            var subAreaList = this.subAreaService.GetAllSubAreaOfProject(projectId).OrderBy(t => t.Name).ToList();
            subAreaList.Insert(0, new SubArea() { ID = 0 });
            this.ddlSubArea.DataSource = subAreaList;
            this.ddlSubArea.DataTextField = "FullName";
            this.ddlSubArea.DataValueField = "ID";
            this.ddlSubArea.DataBind();

            var codeList = this.DocumentStatusService.GetAll().OrderBy(t => t.Name).ToList();
            codeList.Insert(0, new DocumentStatu() { ID = 0 });
            this.ddlRevStatus.DataSource = codeList;
            this.ddlRevStatus.DataTextField = "FullName";
            this.ddlRevStatus.DataValueField = "ID";
            this.ddlRevStatus.DataBind();

            var drawingCodeList = this.drawingCodeService.GetAllDrawingCodeOfProject(projectId).OrderBy(t => t.Name).ToList();
            drawingCodeList.Insert(0, new DrawingCode() { ID = 0 });
            this.ddlDrawingCode.DataSource = drawingCodeList;
            this.ddlDrawingCode.DataTextField = "FullName";
            this.ddlDrawingCode.DataValueField = "ID";
            this.ddlDrawingCode.DataBind();

            var drawingDetailCodeList = this.drawingDetailCodeService.GetAllDrawingDetailCodeOfProject(projectId).OrderBy(t => t.Name).ToList();
            drawingDetailCodeList.Insert(0, new DrawingDetailCode() { ID = 0 });
            this.ddlDrawingDetailCode.DataSource = drawingDetailCodeList;
            this.ddlDrawingDetailCode.DataTextField = "FullName";
            this.ddlDrawingDetailCode.DataValueField = "ID";
            this.ddlDrawingDetailCode.DataBind();

            var systemCodeList = this.systemCodeService.GetAllSystemCodeOfProject(projectId).OrderBy(t => t.Name).ToList();
            systemCodeList.Insert(0, new SystemCode() { ID = 0 });
            this.ddlSystemCode.DataSource = systemCodeList;
            this.ddlSystemCode.DataTextField = "FullName";
            this.ddlSystemCode.DataValueField = "ID";
            this.ddlSystemCode.DataBind();

            var categorylist = this.categoryService.GetAllOfProject(projectId);
            var rtvobj = (RadTreeView)ddlParentName.Items[0].FindControl("rtvParentName");
            if (rtvobj != null)
            {
                categorylist.Insert(0, new Category() { Name = string.Empty });

                rtvobj.DataSource = categorylist;
                rtvobj.DataFieldParentID = "ParentId";
                rtvobj.DataTextField = "Name";
                rtvobj.DataValueField = "ID";
                rtvobj.DataFieldID = "ID";
                rtvobj.DataBind();

                if (rtvobj.Nodes.Count > 0)
                {
                    rtvobj.Nodes[0].Expanded = true;
                }
            }
        }
        private void CollectDataRevise(ref DocumentPackage docObj, ProjectCode projObj)
        {
            docObj.ProjectId = projObj.ID;
            docObj.ProjectName = projObj.Code;
            docObj.ProjectFullName = projObj.FullName;

            docObj.DocNo = this.txtDocNumber.Text.Trim();
            docObj.DocTitle = this.txtDocumentTitle.Text.Trim();
            docObj.DocumentTypeId = this.ddlDocType.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDocType.SelectedValue)
                                        : 0;
            docObj.DocTypeFullName = this.ddlDocType.SelectedItem != null
                                          ? this.ddlDocType.SelectedItem.Text
                                          : string.Empty;
            docObj.DocumentTypeName = docObj.DocTypeFullName.Split(',')[0].Trim();



            // docObj.StatusID=this
            docObj.StatusName = !string.IsNullOrEmpty(docObj.StatusFullName)
                ? docObj.StatusFullName.Split('(')[0].Trim()
                : string.Empty;

            docObj.DisciplineId = this.ddlDiscipline.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlDiscipline.SelectedValue)
                                      : 0;
            docObj.DisciplineFullName = this.ddlDiscipline.SelectedItem != null
                                        ? this.ddlDiscipline.SelectedItem.Text
                                        : string.Empty;
            docObj.DisciplineName = docObj.DisciplineFullName.Split('-')[0].Trim();
            // docObj.Complete = this.txtComplete.Value.GetValueOrDefault();
            docObj.PackageId = this.ddlPackage.SelectedItem != null
                                    ? Convert.ToInt32(this.ddlPackage.SelectedValue)
                                    : 0;
            docObj.PackageName = this.ddlPackage.SelectedItem != null
                                        ? this.ddlPackage.SelectedItem.Text
                                        : string.Empty;
            var rtvparent = (RadTreeView)this.ddlParentName.Items[0].FindControl("rtvParentName");
            if (rtvparent != null && rtvparent.SelectedNode != null)
            {
                docObj.CategoryID = Convert.ToInt32(rtvparent.SelectedNode.Value);
                docObj.CategoryName = rtvparent.SelectedNode.Text;
            }
            else
            {
                docObj.CategoryID = (int?)null;
                docObj.CategoryName = string.Empty;
            }
            docObj.Weight = this.txtWeight.Value.GetValueOrDefault();
            docObj.Notes = this.txtNotes.Text.Trim();

            docObj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            docObj.RevisionName = this.ddlRevision.SelectedItem.Text;
 
            docObj.OriginatorId = this.ddlOriginator.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlOriginator.SelectedValue)
                                      : 0;
            docObj.OriginatorFullName = this.ddlOriginator.SelectedItem != null
                                        ? this.ddlOriginator.SelectedItem.Text
                                        : string.Empty;
            docObj.OriginatorName = docObj.OriginatorFullName.Split('-')[0].Trim();

            docObj.SequencetialNumber = this.txtSequentialNumber.Text.Trim();
            docObj.DrawingSheetNumber = this.txtDrawingSheetNumber.Text.Trim();


            docObj.ContractorId = this.ddlContractor.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlContractor.SelectedValue)
                                        : 0;
            docObj.ContractorFullName = this.ddlContractor.SelectedItem != null
                                          ? this.ddlContractor.SelectedItem.Text
                                          : string.Empty;
            docObj.ContractorName = docObj.ContractorFullName.Split('-')[0].Trim();

            docObj.PhaseId = this.ddlPhase.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlPhase.SelectedValue)
                                        : 0;
            docObj.PhaseFullName = this.ddlPhase.SelectedItem != null
                                          ? this.ddlPhase.SelectedItem.Text
                                          : string.Empty;
            docObj.PhaseName = docObj.PhaseFullName.Split('-')[0].Trim();

            docObj.AreaId = this.ddlArea.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlArea.SelectedValue)
                                        : 0;
            docObj.AreaFullName = this.ddlArea.SelectedItem != null
                                          ? this.ddlArea.SelectedItem.Text
                                          : string.Empty;
            docObj.AreaName = docObj.AreaFullName.Split('-')[0].Trim();

            docObj.SubAreaId = this.ddlSubArea.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlSubArea.SelectedValue)
                                        : 0;
            docObj.SubAreaFullName = this.ddlSubArea.SelectedItem != null
                                          ? this.ddlSubArea.SelectedItem.Text
                                          : string.Empty;
            docObj.SubAreaName = docObj.SubAreaFullName.Split('-')[0].Trim();

            docObj.CodeId = this.ddlRevStatus.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlRevStatus.SelectedValue)
                                       : 0;
            docObj.CodeFullName = this.ddlRevStatus.SelectedItem != null
                                          ? this.ddlRevStatus.SelectedItem.Text
                                         : string.Empty;
            docObj.CodeName = docObj.CodeFullName.Split('-')[0].Trim();

            docObj.DrawingCodeId = this.ddlDrawingCode.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDrawingCode.SelectedValue)
                                        : 0;
            docObj.DrawingCodeFullName = this.ddlDrawingCode.SelectedItem != null
                                          ? this.ddlDrawingCode.SelectedItem.Text
                                          : string.Empty;
            docObj.DrawingCodeName = docObj.DrawingCodeFullName.Split('-')[0].Trim();

            docObj.DrawingDetailCodeId = this.ddlDrawingDetailCode.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDrawingDetailCode.SelectedValue)
                                        : 0;
            docObj.DrawingDetailCodeFullName = this.ddlDrawingDetailCode.SelectedItem != null
                                          ? this.ddlDrawingDetailCode.SelectedItem.Text
                                          : string.Empty;
            docObj.DrawingDetailCodeName = docObj.DrawingDetailCodeFullName.Split('-')[0].Trim();

            docObj.SystemCodeId = this.ddlSystemCode.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlSystemCode.SelectedValue)
                                        : 0;
            docObj.SystemCodeFullName = this.ddlSystemCode.SelectedItem != null
                                          ? this.ddlSystemCode.SelectedItem.Text
                                          : string.Empty;
            docObj.SystemCodeName = docObj.SystemCodeFullName.Split('-')[0].Trim();

            docObj.Remarks = this.txtNotes.Text;

            docObj.StartPlan = this.txtPlannedDate.SelectedDate;
            docObj.StartRevised = this.txtforecastDate.SelectedDate;
            docObj.StartActual = this.txtActualDate.SelectedDate;
            docObj.IDCPlan = this.txtplandateIDC.SelectedDate;
            docObj.IDCRevised = this.txtForecastDateIDC.SelectedDate;
            docObj.IDCACtual = this.txtActualDateIDC.SelectedDate;
            docObj.IFRPlan = this.txtPlanedDateIFR.SelectedDate;
            docObj.IFRRevised = this.txtForecastDateIFR.SelectedDate;
            docObj.IFRActual = this.txtActualDateIFR.SelectedDate;
            docObj.ReIFRPlan = this.txtPlannedDateReIFR.SelectedDate;
            docObj.ReIFRRevised = this.txtForecastDateReIFR.SelectedDate;
            docObj.ReIFRActual = this.txtActualDateReIFR.SelectedDate;
            docObj.IFAPlan = this.txtPlannedDateIFA.SelectedDate;
            docObj.IFARevised = this.txtForecastDateIFA.SelectedDate;
            docObj.IFAActual = this.txtActualDateIFA.SelectedDate;
            docObj.RevDate = this.txtRevDate.SelectedDate;
            docObj.Manhours = this.txtManHour.Value;

            docObj.AFCPlan = this.txtPlannedDateAFC.SelectedDate;
            docObj.AFCRevised = this.txtForecastDateAFC.SelectedDate;
            docObj.AFCActual = this.txtActualDateAFC.SelectedDate;
            //Mark up
            docObj.DNV_markup = this.ddlDNVMarkup.SelectedValue;
            docObj.KDN_markup = this.ddlKDNMarkup.SelectedValue;
            docObj.NCS_markup = this.ddlNCSMarkup.SelectedValue;
        }
        private void CollectData(ref DocumentPackage docObj, ProjectCode projObj)
        {
            docObj.ProjectId = projObj.ID;
            docObj.ProjectName = projObj.Code;
            docObj.ProjectFullName = projObj.FullName;

            docObj.DocNo = this.txtDocNumber.Text.Trim();
            docObj.DocTitle = this.txtDocumentTitle.Text.Trim();
            docObj.DocumentTypeId = this.ddlDocType.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDocType.SelectedValue)
                                        : 0;
            docObj.DocTypeFullName = this.ddlDocType.SelectedItem != null
                                          ? this.ddlDocType.SelectedItem.Text
                                          : string.Empty;
            docObj.DocumentTypeName = docObj.DocTypeFullName.Split(',')[0].Trim();

            docObj.DocReviewStatusCodeID = this.ddlDocReviewStatus.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDocReviewStatus.SelectedValue)
                                        : 0;
            docObj.DocReviewStatusCode = this.ddlDocReviewStatus.SelectedItem != null
                                          ? this.ddlDocReviewStatus.SelectedItem.Text
                                          : string.Empty;

            // docObj.StatusID=this
            docObj.IsEMDR = this.cbIsEMDR.Checked;
            docObj.StatusName = !string.IsNullOrEmpty(docObj.StatusFullName)
                ? docObj.StatusFullName.Split('(')[0].Trim()
                : string.Empty;

            docObj.DisciplineId = this.ddlDiscipline.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlDiscipline.SelectedValue)
                                      : 0;
            docObj.DisciplineFullName = this.ddlDiscipline.SelectedItem != null
                                        ? this.ddlDiscipline.SelectedItem.Text
                                        : string.Empty;
            docObj.DisciplineName = docObj.DisciplineFullName.Split('-')[0].Trim();
            // docObj.Complete = this.txtComplete.Value.GetValueOrDefault();
            docObj.PackageId = this.ddlPackage.SelectedItem != null
                                  ? Convert.ToInt32(this.ddlPackage.SelectedValue)
                                  : 0;
            docObj.PackageName = this.ddlPackage.SelectedItem != null
                                        ? this.ddlPackage.SelectedItem.Text
                                        : string.Empty;
            docObj.Weight = this.txtWeight.Value.GetValueOrDefault();
            docObj.Notes = this.txtNotes.Text.Trim();

            docObj.RevisionId = Convert.ToInt32(this.ddlRevision.SelectedValue);
            docObj.RevisionName = this.ddlRevision.SelectedItem.Text;
            //docObj.RevisionPlanedDate = this.txtRevisionPlaned.SelectedDate;
            //docObj.RevisionActualDate = this.txtRevisionReceivedDate.SelectedDate;
            //docObj.RevisionReceiveTransNo = this.txtRevisionReceivedTrans.Text.Trim();
           // docObj.CompleteForProject = Math.Round(this.txtComplete.Value.GetValueOrDefault() * docObj.Weight.GetValueOrDefault() / 100, 2);

            docObj.OriginatorId = this.ddlOriginator.SelectedItem != null
                                      ? Convert.ToInt32(this.ddlOriginator.SelectedValue)
                                      : 0;
            docObj.OriginatorFullName = this.ddlOriginator.SelectedItem != null
                                        ? this.ddlOriginator.SelectedItem.Text
                                        : string.Empty;
            docObj.OriginatorName = docObj.OriginatorFullName.Split('-')[0].Trim();

            docObj.SequencetialNumber = this.txtSequentialNumber.Text.Trim();
            docObj.DrawingSheetNumber = this.txtDrawingSheetNumber.Text.Trim();
            //docObj.RevisionPlanReceiveCommentDate = this.txtCommentPlaned.SelectedDate;
            //docObj.RevisionActualReceiveCommentDate = this.txtActualCommentDate.SelectedDate;

            //docObj.FirstIssuePlanDate = this.txtFirstIssuePlanDate.SelectedDate;
            //docObj.FirstIssueActualDate = this.txtFirstIssueActualDate.SelectedDate;
            //docObj.FirstIssueTransNo = this.txtFirstIssueTransNo.Text.Trim();

            //docObj.FinalIssuePlanDate = this.txtFinalIssuePlanDate.SelectedDate;
            //docObj.FinalIssueActualDate = this.txtFinalIssueActualDate.SelectedDate;
            //docObj.FinalIssueTransNo = this.txtFinalIssueTransNo.Text.Trim();

            docObj.ContractorId = this.ddlContractor.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlContractor.SelectedValue)
                                        : 0;
            docObj.ContractorFullName = this.ddlContractor.SelectedItem != null
                                          ? this.ddlContractor.SelectedItem.Text
                                          : string.Empty;
            docObj.ContractorName = docObj.ContractorFullName.Split('-')[0].Trim();

            docObj.PhaseId = this.ddlPhase.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlPhase.SelectedValue)
                                        : 0;
            docObj.PhaseFullName = this.ddlPhase.SelectedItem != null
                                          ? this.ddlPhase.SelectedItem.Text
                                          : string.Empty;
            docObj.PhaseName = docObj.PhaseFullName.Split('-')[0].Trim();

            docObj.AreaId = this.ddlArea.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlArea.SelectedValue)
                                        : 0;
            docObj.AreaFullName = this.ddlArea.SelectedItem != null
                                          ? this.ddlArea.SelectedItem.Text
                                          : string.Empty;
            docObj.AreaName = docObj.AreaFullName.Split('-')[0].Trim();

            docObj.SubAreaId = this.ddlSubArea.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlSubArea.SelectedValue)
                                        : 0;
            docObj.SubAreaFullName = this.ddlSubArea.SelectedItem != null
                                          ? this.ddlSubArea.SelectedItem.Text
                                          : string.Empty;
            docObj.SubAreaName = docObj.SubAreaFullName.Split('-')[0].Trim();

            docObj.CodeId = this.ddlRevStatus.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlRevStatus.SelectedValue)
                                       : 0;
            docObj.CodeFullName = this.ddlRevStatus.SelectedItem != null
                                          ? this.ddlRevStatus.SelectedItem.Text
                                         : string.Empty;
            docObj.CodeName = docObj.CodeFullName.Split('-')[0].Trim();

            docObj.DrawingCodeId = this.ddlDrawingCode.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDrawingCode.SelectedValue)
                                        : 0;
            docObj.DrawingCodeFullName = this.ddlDrawingCode.SelectedItem != null
                                          ? this.ddlDrawingCode.SelectedItem.Text
                                          : string.Empty;
            docObj.DrawingCodeName = docObj.DrawingCodeFullName.Split('-')[0].Trim();

            docObj.DrawingDetailCodeId = this.ddlDrawingDetailCode.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlDrawingDetailCode.SelectedValue)
                                        : 0;
            docObj.DrawingDetailCodeFullName = this.ddlDrawingDetailCode.SelectedItem != null
                                          ? this.ddlDrawingDetailCode.SelectedItem.Text
                                          : string.Empty;
            docObj.DrawingDetailCodeName = docObj.DrawingDetailCodeFullName.Split('-')[0].Trim();

            docObj.SystemCodeId = this.ddlSystemCode.SelectedItem != null
                                        ? Convert.ToInt32(this.ddlSystemCode.SelectedValue)
                                        : 0;
            docObj.SystemCodeFullName = this.ddlSystemCode.SelectedItem != null
                                          ? this.ddlSystemCode.SelectedItem.Text
                                          : string.Empty;
            docObj.SystemCodeName = docObj.SystemCodeFullName.Split('-')[0].Trim();

            docObj.Remarks = this.txtNotes.Text;

            docObj.StartPlan = this.txtPlannedDate.SelectedDate;
            docObj.StartRevised = this.txtforecastDate.SelectedDate;
            docObj.StartActual = this.txtActualDate.SelectedDate;
            docObj.IDCPlan = this.txtplandateIDC.SelectedDate;
            docObj.IDCRevised = this.txtForecastDateIDC.SelectedDate;
            docObj.IDCACtual = this.txtActualDateIDC.SelectedDate;
            docObj.IFRPlan = this.txtPlanedDateIFR.SelectedDate;
            docObj.IFRRevised = this.txtForecastDateIFR.SelectedDate;
            docObj.IFRActual = this.txtActualDateIFR.SelectedDate;
            docObj.ReIFRPlan = this.txtPlannedDateReIFR.SelectedDate;
            docObj.ReIFRRevised = this.txtForecastDateReIFR.SelectedDate;
            docObj.ReIFRActual = this.txtActualDateReIFR.SelectedDate;
            docObj.IFAPlan = this.txtPlannedDateIFA.SelectedDate;
            docObj.IFARevised = this.txtForecastDateIFA.SelectedDate;
            docObj.IFAActual = this.txtActualDateIFA.SelectedDate;
            docObj.RevDate = this.txtRevDate.SelectedDate;
            docObj.Manhours = this.txtManHour.Value;

            docObj.AFCPlan =    this.txtPlannedDateAFC.SelectedDate;
            docObj.AFCRevised = this.txtForecastDateAFC.SelectedDate;
            docObj.AFCActual =  this.txtActualDateAFC.SelectedDate;



            //DNV
            if (!string.IsNullOrEmpty(this.txt_DNV_InNo.Text) )
            {
                docObj.IncomingTransDate_CA = this.txt_DNV_InDate.SelectedDate;
                docObj.IncomingTransNo_CA = this.txt_DNV_InNo.Text;

                docObj.ResponseCAId = this.ddlResponseCodeCA.SelectedItem != null
                                           ? Convert.ToInt32(this.ddlResponseCodeCA.SelectedValue)
                                           : 0;
                docObj.ResponseCACode = this.ddlResponseCodeCA.SelectedItem != null
                                              ? this.ddlResponseCodeCA.SelectedItem.Text
                                              : string.Empty;
                docObj.DNV_FinalCode = docObj.ResponseCACode;
            }
                //docObj.DNV_DueDate = this.txt_DNV_OutDate.SelectedDate;
                if (!string.IsNullOrEmpty(docObj.DNV_markup) && (docObj.DNV_markup.ToUpper() == "R" || docObj.DNV_markup.ToUpper() == "A") && this.txt_DNV_OutDate.SelectedDate != null)
                {
                    //var Tempdate = this.txt_DNV_OutDate.SelectedDate.GetValueOrDefault();
                    //for (int i = 1; i <= 5; i++)
                    //{
                    //    Tempdate = this.GetNextWorkingDay(Tempdate);
                    //}
                    docObj.DNV_DueDate = GetDate(5, this.txt_DNV_OutDate.SelectedDate.GetValueOrDefault());
                }
            
            docObj.OutgoingTransNo_CA= this.txt_DNV_OutNo.Text;
            docObj.OutgoingTransDate_CA= this.txt_DNV_OutDate.SelectedDate;

            //KDN Operator
            if (!string.IsNullOrEmpty(this.txt_KDN_OPERATOR_InNo.Text) )
            {
                docObj.KDN_Operator_IncomingTransDate = this.txt_KDN_OPERATOR_InDate.SelectedDate;
                docObj.KDN_Operator_IncomingTransNo = this.txt_KDN_OPERATOR_InNo.Text;

                docObj.KDN_Operator_ResponseId = this.ddl_KDN_OPERATOR_ResponeCode.SelectedItem != null
                                           ? Convert.ToInt32(this.ddl_KDN_OPERATOR_ResponeCode.SelectedValue)
                                           : 0;
                docObj.KDN_Operator_ResponseCode = this.ddl_KDN_OPERATOR_ResponeCode.SelectedItem != null
                                              ? this.ddl_KDN_OPERATOR_ResponeCode.SelectedItem.Text
                                              : string.Empty;
                docObj.KDN_Operator_FinalCode = docObj.KDN_Operator_ResponseCode;
            }
            // this.txt_KDN_OPERATOR_OutDate.SelectedDate = docObj.KDN_Operator_DueDate;
            if (!string.IsNullOrEmpty(docObj.KDN_markup) && (docObj.KDN_markup.ToUpper() == "R" || docObj.KDN_markup.ToUpper() == "A") && this.txt_NCS_OWNER_OutDate.SelectedDate != null)
            {
                //var Tempdate = this.txt_KDN_OPERATOR_OutDate.SelectedDate.GetValueOrDefault();
                //for (int i = 1; i <= 5; i++)
                //{
                //    Tempdate = this.GetNextWorkingDay(Tempdate);
                //}
                docObj.KDN_Operator_DueDate = GetDate(5, this.txt_KDN_OPERATOR_OutDate.SelectedDate.GetValueOrDefault());
            }

            docObj.KDN_Operator_OutgoingTransNo = this.txt_KDN_OPERATOR_OutNo.Text;
             docObj.KDN_Operator_OutgoingTransDate= this.txt_KDN_OPERATOR_OutDate.SelectedDate;

            //KDN Owner
            if (!string.IsNullOrEmpty(this.txt_KDN_OWNER_InNo.Text) )
            {
                docObj.KDN_Owner_IncomingTransDate = this.txt_KDN_OWNER_InDate.SelectedDate;
                docObj.KDN_Owner_IncomingTransNo = this.txt_KDN_OWNER_InNo.Text;
                docObj.KDN_Owner_ResponseId = this.ddl_KDN_OWNER_ResponeCode.SelectedItem != null
                                         ? Convert.ToInt32(this.ddl_KDN_OWNER_ResponeCode.SelectedValue)
                                         : 0;
                docObj.KDN_Owner_ResponseCode = this.ddl_KDN_OWNER_ResponeCode.SelectedItem != null
                                              ? this.ddl_KDN_OWNER_ResponeCode.SelectedItem.Text
                                              : string.Empty;
                docObj.KDN_Owner_FinalCode = docObj.KDN_Owner_ResponseCode;
            }
            //this.txt_KDN_OWNER_OutDate.SelectedDate = docObj.KDN_Owner_DueDate;
            if (!string.IsNullOrEmpty(docObj.KDN_markup) && (docObj.KDN_markup.ToUpper() == "R" || docObj.KDN_markup.ToUpper() == "A") && this.txt_NCS_OWNER_OutDate.SelectedDate != null)
            {
                //var Tempdate = this.txt_KDN_OWNER_OutDate.SelectedDate.GetValueOrDefault();
                //for (int i = 1; i <= 5; i++)
                //{
                //    Tempdate = this.GetNextWorkingDay(Tempdate);
                //}
                docObj.KDN_Owner_DueDate = GetDate(5, this.txt_KDN_OWNER_OutDate.SelectedDate.GetValueOrDefault());
            }
             docObj.KDN_Owner_OutgoingTransNo= this.txt_KDN_OWNER_OutNo.Text;
             docObj.KDN_Owner_OutgoingTransDate= this.txt_KDN_OWNER_OutDate.SelectedDate;

            //NCS Operator
            if (!string.IsNullOrEmpty(this.txt_NCS_OPERATOR_InNo.Text))
            {
                docObj.NCS_Operator_IncomingTransDate = this.txt_NCS_OPERATOR_InDate.SelectedDate;
                docObj.NCS_Operator_IncomingTransNo = this.txt_NCS_OPERATOR_InNo.Text;
                docObj.NCS_Operator_ResponseId = this.ddl_NCS_OPERATOR_ResponeCode.SelectedItem != null
                                          ? Convert.ToInt32(this.ddl_NCS_OPERATOR_ResponeCode.SelectedValue)
                                          : 0;
                docObj.NCS_Operator_ResponseCode = this.ddl_NCS_OPERATOR_ResponeCode.SelectedItem != null
                                              ? this.ddl_NCS_OPERATOR_ResponeCode.SelectedItem.Text
                                              : string.Empty;
                docObj.NCS_Operator_FinalCode = docObj.NCS_Operator_ResponseCode;
            }
            //this.txt_NCS_OPERATOR_OutDate.SelectedDate = docObj.NCS_Operator_DueDate;
            if (!string.IsNullOrEmpty(docObj.NCS_markup) && (docObj.NCS_markup.ToUpper() == "R" || docObj.NCS_markup.ToUpper() == "A") && this.txt_NCS_OWNER_OutDate.SelectedDate != null)
            {
                //var Tempdate = this.txt_NCS_OPERATOR_OutDate.SelectedDate.GetValueOrDefault();
                //for (int i = 1; i <= 5; i++)
                //{
                //    Tempdate = this.GetNextWorkingDay(Tempdate);
                //}
                docObj.NCS_Operator_DueDate = GetDate(5, this.txt_NCS_OPERATOR_OutDate.SelectedDate.GetValueOrDefault());
            }
             docObj.NCS_Operator_OutgoingTransNo= this.txt_NCS_OPERATOR_OutNo.Text;
             docObj.NCS_Operator_OutgoingTransDate= this.txt_NCS_OPERATOR_OutDate.SelectedDate;

            //NCS Owner
            if (!string.IsNullOrEmpty(this.txt_NCS_OWNER_InNo.Text))
            {
                docObj.NCS_Owner_IncomingTransDate = this.txt_NCS_OWNER_InDate.SelectedDate;
                docObj.NCS_Owner_IncomingTransNo = this.txt_NCS_OWNER_InNo.Text;
                docObj.NCS_Owner_ResponseId = this.ddl_NCS_OWNER_ResponeCode.SelectedItem != null
                                        ? Convert.ToInt32(this.ddl_NCS_OWNER_ResponeCode.SelectedValue)
                                        : 0;
                docObj.NCS_Owner_ResponseCode = this.ddl_NCS_OWNER_ResponeCode.SelectedItem != null
                                              ? this.ddl_NCS_OWNER_ResponeCode.SelectedItem.Text
                                              : string.Empty;
                docObj.NCS_Owner_FinalCode = docObj.NCS_Owner_ResponseCode;
            }
            // this.txt_NCS_OWNER_OutDate.SelectedDate = docObj.NCS_Owner_DueDate;
            if (!string.IsNullOrEmpty(docObj.NCS_markup) && (docObj.NCS_markup.ToUpper() == "R" || docObj.NCS_markup.ToUpper() == "A") && this.txt_NCS_OWNER_OutDate.SelectedDate!= null)
            {
                //var Tempdate = this.txt_NCS_OWNER_OutDate.SelectedDate.GetValueOrDefault();
               //// for (int i = 1; i <= 5; i++)
               // {
                //    Tempdate = GetDate(5, this.txt_NCS_OWNER_OutDate.SelectedDate.GetValueOrDefault());
               // }
                docObj.NCS_Owner_DueDate = GetDate(5, this.txt_NCS_OWNER_OutDate.SelectedDate.GetValueOrDefault()); 
            }
             docObj.NCS_Owner_OutgoingTransNo= this.txt_NCS_OWNER_OutNo.Text;
             docObj.NCS_Owner_OutgoingTransDate= this.txt_NCS_OWNER_OutDate.SelectedDate;

            //Mark up
             docObj.DNV_markup= this.ddlDNVMarkup.SelectedValue;
            docObj.KDN_markup = this.ddlKDNMarkup.SelectedValue;
            docObj.NCS_markup = this.ddlNCSMarkup.SelectedValue;

            var rtvparent = (RadTreeView)this.ddlParentName.Items[0].FindControl("rtvParentName");
            if (rtvparent != null && rtvparent.SelectedNode != null)
            {
                docObj.CategoryID = Convert.ToInt32(rtvparent.SelectedNode.Value);
                docObj.CategoryName = rtvparent.SelectedNode.Text;
            }
            else
            {
                docObj.CategoryID = (int?)null;
                docObj.CategoryName = string.Empty;
            }
        }

        private void LoadDocInfo(DocumentPackage docObj)
        {
            this.txtDocNumber.Text = docObj.DocNo;
            this.txtDocumentTitle.Text = docObj.DocTitle;
            this.ddlDocType.SelectedValue = docObj.DocumentTypeId.ToString();
            this.ddlOriginator.SelectedValue = docObj.OriginatorId.ToString();
            this.ddlDiscipline.SelectedValue = docObj.DisciplineId.ToString();
            this.ddlDocReviewStatus.SelectedValue = docObj.DocReviewStatusCodeID.ToString();
           // this.txtComplete.Value = docObj.Complete;
            this.txtWeight.Value = docObj.Weight;
            this.txtNotes.Text = docObj.Notes;

            this.ddlRevision.SelectedValue = docObj.RevisionId.ToString();
            //this.txtRevisionPlaned.SelectedDate = docObj.RevisionPlanedDate;
            this.ddlResponseCodeCA.SelectedValue = docObj.ResponseCAId.ToString();
           // this.txtRevisionReceivedTrans.Text = docObj.RevisionReceiveTransNo;

            this.txtSequentialNumber.Text = docObj.SequencetialNumber;
            this.txtDrawingSheetNumber.Text = docObj.DrawingSheetNumber;
            //this.txtCommentPlaned.SelectedDate = docObj.RevisionPlanReceiveCommentDate;
            //this.txtActualCommentDate.SelectedDate = docObj.RevisionActualReceiveCommentDate;
            // this.txtFinalCode.Text = docObj.FinalCodeName;
            this.ddlPackage.SelectedValue = docObj.PackageId.ToString();
            this.txtTransOutDate.SelectedDate = docObj.OutgoingTransDate;
            this.txtTansOutNo.Text = docObj.OutgoingTransNo;
            this.txtIcomingDate.SelectedDate = docObj.IncomingTransDate;
            this.txtIncomingNo.Text = docObj.IncomingTransNo;

           // this.txtFinalIssuePlanDate.SelectedDate = docObj.FinalIssuePlanDate;
           // this.txtFinalIssueActualDate.SelectedDate = docObj.FinalIssueActualDate;
           // this.txtFinalIssueTransNo.Text = docObj.FinalIssueTransNo;

            this.ddlContractor.SelectedValue = docObj.ContractorId.ToString();
            this.ddlPhase.SelectedValue = docObj.PhaseId.ToString();
            this.ddlArea.SelectedValue = docObj.AreaId.ToString();
            this.ddlSubArea.SelectedValue = docObj.SubAreaId.ToString();
             this.ddlRevStatus.SelectedValue = docObj.CodeId.ToString();
            this.ddlDrawingCode.SelectedValue = docObj.DrawingCodeId.ToString();
            this.ddlDrawingDetailCode.SelectedValue = docObj.DrawingDetailCodeId.ToString();
            this.ddlSystemCode.SelectedValue = docObj.SystemCodeId.ToString();
            this.txtRevDate.SelectedDate = docObj.RevDate;
            this.txtNotes.Text = docObj.Remarks;
            this.txtManHour.Value = docObj.Manhours;
            this.txtPlannedDate.SelectedDate = docObj.StartPlan;
            this.txtforecastDate.SelectedDate = docObj.StartRevised;
            this.txtActualDate.SelectedDate = docObj.StartActual;
            this.txtplandateIDC.SelectedDate = docObj.IDCPlan;
            this.txtForecastDateIDC.SelectedDate = docObj.IDCRevised;
            this.txtActualDateIDC.SelectedDate = docObj.IDCACtual;
            this.txtPlanedDateIFR.SelectedDate = docObj.IFRPlan;
            this.txtForecastDateIFR.SelectedDate = docObj.IFRRevised;
            this.txtActualDateIFR.SelectedDate = docObj.IFRActual;
            this.txtPlannedDateReIFR.SelectedDate = docObj.ReIFRPlan;
            this.txtForecastDateReIFR.SelectedDate = docObj.ReIFRRevised;
            this.txtActualDateReIFR.SelectedDate = docObj.ReIFRActual;
            this.txtPlannedDateIFA.SelectedDate = docObj.IFAPlan;
            this.txtForecastDateIFA.SelectedDate = docObj.IFARevised;
            this.txtActualDateIFA.SelectedDate = docObj.IFAActual;
            this.txtPlannedDateAFC.SelectedDate=docObj.AFCPlan;
            this.txtForecastDateAFC.SelectedDate=docObj.AFCRevised;
            this.txtActualDateAFC.SelectedDate=docObj.AFCActual;

            var rtvparent = (RadTreeView)this.ddlParentName.Items[0].FindControl("rtvParentName");
            if (rtvparent != null && docObj.CategoryID != 0 && docObj.CategoryID != null)
            {
                var nodeObj = rtvparent.FindNodeByValue(docObj.CategoryID.GetValueOrDefault().ToString());
                if (nodeObj != null)
                {
                    nodeObj.Selected = true;
                    this.ddlParentName.Items[0].Text = nodeObj.Text;
                    this.ddlParentName.Items[0].Value = nodeObj.Value;
                }
            }
            //DNV
            this.txt_DNV_InDate.SelectedDate = docObj.IncomingTransDate_CA;
            this.txt_DNV_InNo.Text = docObj.IncomingTransNo_CA;
            this.txt_DNV_OutDueDate.SelectedDate = docObj.DNV_DueDate;
            this.txt_DNV_OutNo.Text = docObj.OutgoingTransNo_CA;
            this.txt_DNV_OutDate.SelectedDate = docObj.OutgoingTransDate_CA;

            //KDN Operator
            this.txt_KDN_OPERATOR_InDate.SelectedDate = docObj.KDN_Operator_IncomingTransDate;
            this.txt_KDN_OPERATOR_InNo.Text = docObj.KDN_Operator_IncomingTransNo;
            this.txt_KDN_OPERATOR_OutDueDate.SelectedDate = docObj.KDN_Operator_DueDate;
            this.txt_KDN_OPERATOR_OutNo.Text = docObj.KDN_Operator_OutgoingTransNo;
            this.txt_KDN_OPERATOR_OutDate.SelectedDate = docObj.KDN_Operator_OutgoingTransDate;
            this.ddl_KDN_OPERATOR_ResponeCode.SelectedValue = docObj.KDN_Operator_ResponseId.ToString();

            //KDN Owner
            this.txt_KDN_OWNER_InDate.SelectedDate = docObj.KDN_Owner_IncomingTransDate;
            this.txt_KDN_OWNER_InNo.Text = docObj.KDN_Owner_IncomingTransNo;
            this.txt_KDN_OWNER_OutDueDate.SelectedDate = docObj.KDN_Owner_DueDate;
            this.txt_KDN_OWNER_OutNo.Text = docObj.KDN_Owner_OutgoingTransNo;
            this.txt_KDN_OWNER_OutDate.SelectedDate = docObj.KDN_Owner_OutgoingTransDate;
            this.ddl_KDN_OWNER_ResponeCode.SelectedValue = docObj.KDN_Owner_ResponseId.ToString();

            //NCS Operator
            this.txt_NCS_OPERATOR_InDate.SelectedDate = docObj.NCS_Operator_IncomingTransDate;
            this.txt_NCS_OPERATOR_InNo.Text = docObj.NCS_Operator_IncomingTransNo;
            this.txt_NCS_OPERATOR_OutDueDate.SelectedDate = docObj.NCS_Operator_DueDate;
            this.txt_NCS_OPERATOR_OutNo.Text = docObj.NCS_Operator_OutgoingTransNo;
            this.txt_NCS_OPERATOR_OutDate.SelectedDate = docObj.NCS_Operator_OutgoingTransDate;
            this.ddl_NCS_OPERATOR_ResponeCode.SelectedValue = docObj.NCS_Operator_ResponseId.ToString();
            //NCS Owner
            this.txt_NCS_OWNER_InDate.SelectedDate = docObj.NCS_Owner_IncomingTransDate;
            this.txt_NCS_OWNER_InNo.Text = docObj.NCS_Owner_IncomingTransNo;
            this.txt_NCS_OWNER_OutDueDate.SelectedDate = docObj.NCS_Owner_DueDate;
            this.txt_NCS_OWNER_OutNo.Text = docObj.NCS_Owner_OutgoingTransNo;
            this.txt_NCS_OWNER_OutDate.SelectedDate = docObj.NCS_Owner_OutgoingTransDate;
            this.ddl_NCS_OWNER_ResponeCode.SelectedValue = docObj.NCS_Owner_ResponseId.ToString();
            //Mark up
            this.ddlDNVMarkup.SelectedValue = docObj.DNV_markup;
            this.ddlKDNMarkup.SelectedValue = docObj.KDN_markup;
            this.ddlNCSMarkup.SelectedValue = docObj.NCS_markup;


        }

        //protected void ddlStatus_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var statusObj = this.statusService.GetById(Convert.ToInt32(this.ddlStatus.SelectedValue));
        //    if (statusObj != null)
        //    {
        //        //this.txtComplete.Value = Math.Round(statusObj.PercentCompleteDefault.GetValueOrDefault(), 2);
        //    }
        //}

        //protected void ddlRevision_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    this.txtFinalCode.Text = string.Empty;
        //}

        //private void UpdateActualProgress(ScopeProject objProject)
        //{
        //    var count = 0;
        //    for (var j = objProject.StartDate.GetValueOrDefault();
        //        j < objProject.EndDate.GetValueOrDefault();
        //        j = j.AddDays(objProject.FrequencyForProgressChart != null && objProject.FrequencyForProgressChart != 0 ? objProject.FrequencyForProgressChart.Value : 7))
        //    {
        //        if (DateTime.Now > j)
        //        {
        //            count += 1;
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }

        //    var listDiscipline = this.disciplineService.GetAllDisciplineOfProject(objProject.ID).OrderBy(t => t.ID).ToList();
        //    foreach (var discipline in listDiscipline)
        //    {
        //        var docList = this.documentPackageService.GetAllByDiscipline(discipline.ID).OrderBy(t => t.DocNo).ToList();
        //        double? complete = 0;
        //        complete = docList.Aggregate(complete, (current, t) => current + t.CompleteForProject.GetValueOrDefault());
        //        discipline.Complete = complete;
        //        var existProgressActual = this.processActualService.GetByProjectAndWorkgroup(objProject.ID, discipline.ID);
        //        if (existProgressActual != null)
        //        {
        //            var arrActual = existProgressActual.Actual.Split('$');
        //            if (arrActual.Count() >= count)
        //            {
        //                arrActual[count] = Math.Round(complete.GetValueOrDefault(), 2).ToString();

        //                var newAtualProgress = string.Empty;
        //                newAtualProgress = arrActual.Aggregate(newAtualProgress, (current, t) => current + t + "$");
        //                newAtualProgress = newAtualProgress.Substring(0, newAtualProgress.Length - 1);

        //                existProgressActual.Actual = newAtualProgress;

        //                this.processActualService.Update(existProgressActual);
        //            }


        //        }
        //        else
        //        {

        //            var progressActual = new ProcessActual();
        //            progressActual.ProjectId = discipline.ProjectId;
        //            progressActual.WorkgroupId = discipline.ID;
        //            progressActual.Actual = Math.Round(complete.GetValueOrDefault(), 2).ToString();
        //            this.processActualService.Insert(progressActual);
        //        }
        //        this.disciplineService.Update(discipline);
        //    }
        //}
        private DateTime GetDate(int day, DateTime transdate)
        {
            var actualDeadline = transdate;
            for (int i = 1; i <= day; i++)
            {
                actualDeadline = this.GetNextWorkingDay(actualDeadline);
            }
            return actualDeadline;
        }
        private bool IsWeekEnd(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        private DateTime GetNextWorkingDay(DateTime date)
        {

            do
            {
                date = date.AddDays(1);
            }
            while (IsWeekEnd(date));

            return date;
        }

        protected void ddlOriginator_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            //this.txtDocNumber.Text = this.GenerateDocumentNumber();
        }

        protected void ddlDiscipline_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ////var sequenceObj = this.documentSequenceManagementService.GetByDisciplineDocType(Convert.ToInt32(this.ddlDiscipline.SelectedValue), Convert.ToInt32(this.ddlDocType.SelectedValue));
            ////if (sequenceObj == null)
            ////{
            ////    this.txtSequentialNumber.Text = Utility.ReturnSequenceString(1, 4);
            ////}
            ////else
            ////{
            ////    this.txtSequentialNumber.Text = Utility.ReturnSequenceString(sequenceObj.CurrentSequence.Value + 1, 4);
            ////}

            //  this.txtDocNumber.Text = this.GenerateDocumentNumber();
        }

        protected void ddlDocType_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ////var sequenceObj = this.documentSequenceManagementService.GetByDisciplineDocType(Convert.ToInt32(this.ddlDiscipline.SelectedValue), Convert.ToInt32(this.ddlDocType.SelectedValue));
            ////if (sequenceObj == null)
            ////{
            ////    this.txtSequentialNumber.Text = Utility.ReturnSequenceString(1, 4);
            ////}
            ////else
            ////{
            ////    this.txtSequentialNumber.Text = Utility.ReturnSequenceString(sequenceObj.CurrentSequence.Value + 1, 4);
            ////}

            // this.txtDocNumber.Text = this.GenerateDocumentNumber();
        }

        protected void txtDocNumber_TextChanged(object sender, EventArgs e)
        {
            var projectId = Convert.ToInt32(this.Request.QueryString["projId"]);
            var documentNumberingList = this.documentNumberingService.GetAllByProject(projectId);
            //--------------------------------------

            if (documentNumberingList != null)
            {
                // Get document numbering format for Doc type
                var documentNumbering = new DocumentNumbering();
                //---------------------------------------------
                var partOfFileName = txtDocNumber.Text.Replace(" ", string.Empty).Split('-');
                if (partOfFileName.Length > 3)
                {
                    //var lenghtSegment = partOfFileName.Length;

                    //var segmentEnd = partOfFileName[lenghtSegment - 1];
                    //if (segmentEnd.Length == 3)
                    //{
                    //    documentNumbering = documentNumberingList.Find(t => t.Lenght == lenghtSegment && t.TypeID == 2);
                    //}
                    //else if (segmentEnd.Length == 4)
                    //{
                    //    documentNumbering = documentNumberingList.Find(t => t.Lenght == lenghtSegment && t.TypeID == 1);
                    //}
                    var lenghtSegment = partOfFileName.Length;

                    var segmentEnd = partOfFileName[lenghtSegment - 1].Length;
                    foreach (var fromat in documentNumberingList)
                    {
                        if (documentNumbering.Name == null)
                        {
                            var fromat_lenghtSegment = fromat.Format.Split('-').Length;

                            var fromat_segmentEnd = fromat.Format.Split('-')[fromat_lenghtSegment - 1].Length;
                            if (fromat_lenghtSegment == lenghtSegment && segmentEnd == fromat_segmentEnd) { documentNumbering = fromat; }
                        }
                    }

                }
                if (documentNumbering.Format != null)
                {
                    var partOfFullDocumentNumbering = documentNumbering.Format.Split('-');

                    if (documentNumbering.Lenght <= partOfFileName.Length)
                    {
                        var fullDocNo = string.Empty;
                        for (int i = 0; i < documentNumbering.Lenght; i++)
                        {
                            fullDocNo += i == documentNumbering.Lenght - 1
                                ? partOfFileName[i]
                                : partOfFileName[i] + "-";

                            switch (partOfFullDocumentNumbering[i])
                            {
                                //case "AAAA":
                                //    var projectCode = partOfFileName[i];
                                //    var projectCodeObj = this.projectService.GetByCode(projectCode);


                                //    if (projectCodeObj != null)
                                //    {
                                //        docObj.ProjectName = projectCode;
                                //        docObj.ProjectId = projectCodeObj.ID;
                                //    }
                                    
                                //    break;
                                case "HH":
                                    var doctypeObj = documentTypeService.GetByCode(partOfFileName[i]);
                                    if (doctypeObj != null)
                                    {
                                        this.ddlDocType.SelectedValue = doctypeObj.ID.ToString();
                                      
                                    }
                                    
                                    break;
                                case "BBB":
                                    var originatingOrganizationCode = partOfFileName[i];
                                    var originatingOrganizationCodeObj =
                                        this.contractorService.GetByCode(originatingOrganizationCode);
                                    if (originatingOrganizationCodeObj != null)
                                    {
                                        this.ddlContractor.SelectedValue= originatingOrganizationCodeObj.ID.ToString();
                                       
                                    }
                                   
                                    break;

                                case "CC":
                                    var Phasecode = partOfFileName[i];
                                    var PhaseCodeObj = this.phaseService.GetByName(Phasecode, projectId);
                                    if (PhaseCodeObj != null)
                                    {
                                        this.ddlPhase.SelectedValue = PhaseCodeObj.ID.ToString();
                                       
                                    }
                                   
                                    break;
                                case "OOO":
                                case "OO":
                                    this.txtSequentialNumber.Text = partOfFileName[i];
                                    break;
                                case "NNOO":
                                    this.txtSequentialNumber.Text  = partOfFileName[i].Substring(2, 2);
                                    var systemcode = partOfFileName[i].Substring(0, 2);
                                    var systemcodeObj = this.systemCodeService.GetByName(systemcode,projectId);
                                    if (systemcodeObj != null)
                                    {
                                       this.ddlSystemCode.SelectedValue= systemcodeObj.ID.ToString();
                                        
                                    }
                                    
                                    break;
                                case "NN":
                                    var systemcodes = partOfFileName[i];
                                    var systemcodesObj = this.systemCodeService.GetByName(systemcodes, projectId);
                                    if (systemcodesObj != null)
                                    {
                                        this.ddlSystemCode.SelectedValue= systemcodesObj.ID.ToString();
                                       
                                    }
                                   
                                    break;
                                case "D":
                                    var AreaCode = partOfFileName[i];
                                    var AreaCodeObj = this.areaService.GetByName(AreaCode, projectId);
                                    if (AreaCodeObj != null)
                                    {
                                        this.ddlArea.SelectedValue = AreaCodeObj.ID.ToString();
                                       
                                    }
                                   
                                    break;
                                case "EE":
                                    var SubAreaCode = partOfFileName[i];
                                    var SubAreaCodeObj = this.subAreaService.GetByName(SubAreaCode, projectId, Convert.ToInt32(this.ddlArea.SelectedValue));
                                    if (SubAreaCodeObj != null)
                                    {
                                        this.ddlSubArea.SelectedValue = SubAreaCodeObj.ID.ToString();
                                    }
                                   
                                    break;
                                case "GG":
                                    var disciplineCode = partOfFileName[i];
                                    var disciplineObj = this.disciplineService.GetByName(disciplineCode);
                                    if (disciplineObj != null)
                                    {
                                        this.ddlDiscipline.SelectedValue = disciplineObj.ID.ToString();
                                       
                                    }
                                    
                                    break;
                                //case "L":
                                //    var drawincode = partOfFileName[i];
                                //    var drawincodeObj = this.drawingCodeService.GetByName(drawincode, projectId);
                                //    if (drawincodeObj != null)
                                //    {
                                //        this.ddlDrawingCode.SelectedValue = drawincodeObj.ID.ToString();

                                //    } break;
                                case "LM":
                                case "L":
                                    if (partOfFileName[i].Length == 1)
                                    {
                                        var drawingCodeObj = this.drawingCodeService.GetByName(partOfFileName[i], projectId);
                                        if (drawingCodeObj != null)
                                        {
                                            this.ddlDrawingCode.SelectedValue = drawingCodeObj.ID.ToString();

                                        }
                                    }
                                    else
                                    {
                                        var stL = partOfFileName[i].Substring(0, 1);
                                        var drawingCodeObj_ = this.drawingCodeService.GetByName(stL, projectId);
                                        if (drawingCodeObj_ != null)
                                        {
                                            this.ddlDrawingCode.SelectedValue = drawingCodeObj_.ID.ToString();

                                        }
                                        var stM = partOfFileName[i].Substring(1, 1);
                                        var stMDetailCodeObj = this.drawingDetailCodeService.GetByName(stM, projectId);
                                        if (stMDetailCodeObj != null)
                                        {
                                            this.ddlDrawingDetailCode.SelectedValue = stMDetailCodeObj.ID.ToString();

                                        }
                                    }

                                    break;
                                case "M":
                                    var drawingDetailcode = partOfFileName[i];
                                    var drawingDetailcodeObj = this.drawingDetailCodeService.GetByName(drawingDetailcode,projectId);
                                    if (drawingDetailcodeObj != null)
                                    {
                                       this.ddlDrawingDetailCode.SelectedValue = drawingDetailcodeObj.ID.ToString();
                                       
                                    }
                                 break;
                            }
                        }

                    }
                 
                }
                
            }

        }

        protected void ddlArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            var areaid = Convert.ToInt32(this.ddlArea.SelectedValue);
            var listsubarea = this.subAreaService.GetAllSubAreaOfProject(Convert.ToInt32(this.Request.QueryString["projId"])).Where(t => t.AreaId == areaid).OrderBy(t => t.Name).ToList();
            listsubarea.Insert(0, new SubArea() { ID = 0 });
            this.ddlSubArea.DataSource = listsubarea;
            this.ddlSubArea.DataTextField = "FullName";
            this.ddlSubArea.DataValueField = "ID";
            this.ddlSubArea.DataBind();
           
        }

        protected void ddlPackage_SelectedIndexChanged(object sender, EventArgs e)
        {
            var categorylist = this.categoryService.GetAllOfProjectAndPAckage(Convert.ToInt32(this.Request.QueryString["projId"]), Convert.ToInt32(this.ddlPackage.SelectedValue));
            var rtvobj = (RadTreeView)ddlParentName.Items[0].FindControl("rtvParentName");
            if (rtvobj != null)
            {
                categorylist.Insert(0, new Category() { Name = string.Empty });

                rtvobj.DataSource = categorylist;
                rtvobj.DataFieldParentID = "ParentId";
                rtvobj.DataTextField = "Name";
                rtvobj.DataValueField = "ID";
                rtvobj.DataFieldID = "ID";
                rtvobj.DataBind();

                if (rtvobj.Nodes.Count > 0)
                {
                    rtvobj.Nodes[0].Expanded = true;
                }
            }
        }
    }
}