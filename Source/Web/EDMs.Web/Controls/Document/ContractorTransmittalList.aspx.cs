﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   Class customer
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Aspose.Cells;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities;
using System.Web;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;
using SaveFormat = Aspose.Words.SaveFormat;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;



namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// Class customer
    /// </summary>
    public partial class ContractorTransmittalList : Page
    {
        private readonly ProjectCodeService projectCodeService = new ProjectCodeService();

        private readonly ContractorTransmittalService contractorTransmittalService = new ContractorTransmittalService();

        private readonly ContractorTransmittalDocFileService contractorTransmittalDocFileService = new ContractorTransmittalDocFileService();

        private readonly PVGASTransmittalService PVGASTransmittalService = new PVGASTransmittalService();

        private readonly DocumentPackageService documentService = new  DocumentPackageService();

        private readonly OrganizationCodeService organizationCodeService = new OrganizationCodeService();

        private readonly AttachDocToTransmittalService attachDocToTransmittalService = new AttachDocToTransmittalService();

        private readonly UserService userService = new UserService();
        private readonly RoleService rolseService = new RoleService();


        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string UnreadPattern = @"\(\d+\)";


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Add("SelectedMainMenu", "Contractor Transmittals Management");

            this.Title = ConfigurationManager.AppSettings.Get("AppName");
            var temp = (RadPane)this.Master.FindControl("leftPane");
            temp.Collapsed = true;
            if (!Page.IsPostBack)
            {
                this.LoadComboData();
                if (!string.IsNullOrEmpty(this.Request.QueryString["TransNoContractor"]))
                {
                    var txtSearchOutgoing = (TextBox)this.radMenuOutgoing.Items[2].FindControl("txtSearchOutgoing");
                    txtSearchOutgoing.Text = Request.QueryString["TransNoContractor"];
                    var ddlStatusOutgoing = (DropDownList)this.radMenuOutgoing.Items[2].FindControl("ddlStatusOutgoing");
                    if (ddlStatusOutgoing != null)
                    {
                        ddlStatusOutgoing.SelectedIndex = 0;
                    }
                    this.OutgoingTransView.Selected = true;
                    this.OutgoingTransView.Selected = true;
                    RadTab tab1 = RadTabStrip1.Tabs.FindTabByText("Outgoing Transmittals");
                    tab1.Selected = true;
                }
                if (!string.IsNullOrEmpty(this.Request.QueryString["TransNoPVGAS"]))
                {
                    var txtSearchIncoming = (TextBox)this.radMenuIncoming.Items[1].FindControl("txtSearchIncoming");
                    txtSearchIncoming.Text = Request.QueryString["TransNoPVGAS"];
                }
            }
        }

        private void LoadComboData()
        {
            var ddlProjectOutgoing = (RadComboBox)this.radMenuOutgoing.Items[2].FindControl("ddlProjectOutgoing");
            var ddlProjectIncoming = (RadComboBox)this.radMenuIncoming.Items[1].FindControl("ddlProjectIncoming");
            var projectList = this.projectCodeService.GetAll().OrderBy(t => t.Code);

            if (ddlProjectOutgoing != null)
            {
                ddlProjectOutgoing.DataSource = projectList;
                ddlProjectOutgoing.DataTextField = "FullName";
                ddlProjectOutgoing.DataValueField = "ID";
                ddlProjectOutgoing.DataBind();

                int projectId = Convert.ToInt32(ddlProjectOutgoing.SelectedValue);
                this.lblProjectOutgoingId.Value = projectId.ToString();
                Session.Add("SelectedProject", projectId);
            }

            if (ddlProjectIncoming != null)
            {
                ddlProjectIncoming.DataSource = projectList;
                ddlProjectIncoming.DataTextField = "FullName";
                ddlProjectIncoming.DataValueField = "ID";
                ddlProjectIncoming.DataBind();

                int projectId = Convert.ToInt32(ddlProjectIncoming.SelectedValue);
                this.lblProjectIncomingId.Value = projectId.ToString();
            }
            var ddlStatusOutgoing = (DropDownList)this.radMenuOutgoing.Items[2].FindControl("ddlStatusOutgoing");
            if(ddlStatusOutgoing != null){
                ddlStatusOutgoing.SelectedIndex = 1;
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("ExportContractorETRM"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.contractorTransmittalService.GetById(objId);
                if (transObj != null)
                {
                    this.ExportContractorETRM(transObj);
                }
            }
            else if (e.Argument.Contains("ExportPECC2ETRM"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.PVGASTransmittalService.GetById(objId);
                if (transObj != null)
                {
                    this.ExportPECC2ETRM(transObj);
                }
            }
            else if (e.Argument.Contains("DeleteTrans"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.contractorTransmittalService.GetById(objId);
                if (transObj != null)
                {
                    if (!string.IsNullOrEmpty(transObj?.StoreFolderPath))
                    {
                        var folderPath = Server.MapPath("../.." + transObj.StoreFolderPath);
                        if (Directory.Exists(folderPath))
                        {
                            Directory.Delete(folderPath, true);
                        }
                    }

                    this.contractorTransmittalService.Delete(objId);
                    this.grdOutgoingTrans.Rebind();
                }
            }
            else if (e.Argument.Contains("SendTrans"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var forSend = Convert.ToInt32(e.Argument.Split('_')[2]);

                var transObj = this.contractorTransmittalService.GetById(objId);
                if (transObj != null && transObj.IsSend ==false )
                {
                    if (transObj.IsReject.GetValueOrDefault())
                    {
                        var PVGASTrans = this.PVGASTransmittalService.GetById(transObj.PECC2TransId.GetValueOrDefault());
                        if (PVGASTrans != null)
                        {
                            // Update pecc2 transmittal
                           PVGASTrans.IsOpen = false;
                           PVGASTrans.IsImport = false;
                            PVGASTrans.IsValid = false;
                           PVGASTrans.Status = "Waiting for DC review";
                           PVGASTrans.ErrorMessage = "Waiting for DC review";
                           PVGASTrans.IsReject = false;
                           PVGASTrans.CurrentRejectReason = string.Empty;
                           PVGASTrans.TransmittalNo = transObj.TransNo;
                           PVGASTrans.ProjectCodeId = transObj.ProjectId;
                           PVGASTrans.ProjectCodeName = transObj.ProjectName;
                           PVGASTrans.IssuedDate = transObj.TransDate;
                           PVGASTrans.Description = transObj.Description;
                           PVGASTrans.OriginatingOrganizationId = transObj.OriginatingOrganizationId;
                           PVGASTrans.OriginatingOrganizationName = transObj.OriginatingOrganizationName;
                           PVGASTrans.ReceivingOrganizationId = transObj.ReceivingOrganizationId;
                           PVGASTrans.ReceivingOrganizationName = transObj.ReceivingOrganizationName;
                           PVGASTrans.FromValue = transObj.FromValue;
                           PVGASTrans.ToValue = transObj.ToValue;
                           PVGASTrans.CCValue = transObj.CCValue;
                           PVGASTrans.GroupId = transObj.GroupId;
                           PVGASTrans.GroupCode = transObj.GroupCode;
                           PVGASTrans.TransmittedByName = transObj.TransmittedByName;
                           PVGASTrans.TransmittedByDesignation = transObj.TransmittedByDesignation;
                           PVGASTrans.AcknowledgedByName = transObj.AcknowledgedByName;
                           PVGASTrans.AcknowledgedByDesignation = transObj.AcknowledgedByDesignation;
                           PVGASTrans.Remark = transObj.Remark;
                           PVGASTrans.Year = transObj.Year;
                           PVGASTrans.DueDate = transObj.DueDate;
                           PVGASTrans.ReceivedDate = DateTime.Now;
                           PVGASTrans.IsAllDocCompleteWorkflow = false;
                           PVGASTrans.CCOrganizationId = transObj.CCOrganizationId;
                           PVGASTrans.CCOrganizationName = transObj.CCOrganizationName;
                           PVGASTrans.PurposeId = transObj.PurposeId;
                           PVGASTrans.PurposeName = transObj.PurposeName;
                           
                           PVGASTrans.CreatedDate = DateTime.Now;
                            this.PVGASTransmittalService.Update(PVGASTrans);
                            // -------------------------------------------------

                            // Update Contractor Trans
                            transObj.IsSend = true;
                            transObj.IsReject = false;
                            transObj.Status = string.Empty;
                            transObj.ErrorMessage = string.Empty;
                            this.contractorTransmittalService.Update(transObj);
                            // -------------------------------------------------

                        }
                    }
                    else
                    {
                        var PVGASTrans = new PVGASTransmittal();
                        PVGASTrans.ID = Guid.NewGuid();
                        PVGASTrans.ContractorTransId = transObj.ID;
                        PVGASTrans.IsOpen = false;
                        PVGASTrans.IsImport = false;
                        PVGASTrans.Status = "Waiting for DC review";
                        PVGASTrans.ErrorMessage = "Waiting for DC review";
                        PVGASTrans.TypeId = 1;
                        PVGASTrans.ForSentId = 1;
                        PVGASTrans.ForSentName = "Project's Document";
                        PVGASTrans.TransmittalNo = transObj.TransNo;
                        PVGASTrans.ProjectCodeId = transObj.ProjectId;
                        PVGASTrans.ProjectCodeName = transObj.ProjectName;
                        PVGASTrans.IssuedDate = transObj.TransDate;
                        PVGASTrans.Description = transObj.Description;
                        PVGASTrans.OriginatingOrganizationId = transObj.OriginatingOrganizationId;
                        PVGASTrans.OriginatingOrganizationName = transObj.OriginatingOrganizationName;
                        PVGASTrans.ReceivingOrganizationId = transObj.ReceivingOrganizationId;
                        PVGASTrans.ReceivingOrganizationName = transObj.ReceivingOrganizationName;
                        PVGASTrans.FromValue = transObj.FromValue;
                        PVGASTrans.ToValue = transObj.ToValue;
                        PVGASTrans.CCValue = transObj.CCValue;
                        PVGASTrans.GroupId = transObj.GroupId;
                        PVGASTrans.GroupCode = transObj.GroupCode;
                        PVGASTrans.TransmittedByName = transObj.TransmittedByName;
                        PVGASTrans.TransmittedByDesignation = transObj.TransmittedByDesignation;
                        PVGASTrans.AcknowledgedByName = transObj.AcknowledgedByName;
                        PVGASTrans.AcknowledgedByDesignation = transObj.AcknowledgedByDesignation;
                        PVGASTrans.Remark = transObj.Remark;
                        PVGASTrans.Year = transObj.Year;
                        PVGASTrans.DueDate = transObj.DueDate;
                        PVGASTrans.ReceivedDate = DateTime.Now;
                        PVGASTrans.IsAllDocCompleteWorkflow = false;
                        PVGASTrans.CCOrganizationId = transObj.CCOrganizationId;
                        PVGASTrans.CCOrganizationName = transObj.CCOrganizationName;
                        PVGASTrans.PurposeId = transObj.PurposeId;
                        PVGASTrans.PurposeName = transObj.PurposeName;
                        PVGASTrans.IsReject = false;
                        PVGASTrans.CurrentRejectReason = string.Empty;
                        var dqreTransId = this.PVGASTransmittalService.Insert(PVGASTrans);
                        if (dqreTransId != null)
                        {
                            transObj.PECC2TransId = dqreTransId;
                            transObj.IsSend = true;

                            this.contractorTransmittalService.Update(transObj);
                        }
                    }

                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["SendEmail"]))
                    {
                        //this.NotifiNewTransmittal(transObj);
                        this.ContrctorSendemail(transObj);
                    }
                    this.grdOutgoingTrans.Rebind();
                }
            }
            else if (e.Argument.Contains("DownLoadFolder"))
            {
                var objId = new Guid(e.Argument.Split('_')[1]);
                var transObj = this.contractorTransmittalService.GetById(objId);
                if (transObj != null)
                {
                    var contractorTrans = this.PVGASTransmittalService.GetById(transObj.PECC2TransId.GetValueOrDefault());
                    var filename = "PVGAS_" + Utilities.Utility.RemoveSpecialCharacter( transObj.TransNo) + "_" + DateTime.Now.ToBinary() + ".zip";
                    var serverTotalDocPackPath = Server.MapPath("../../DocumentLibrary/PVGASTransmittal/" + filename);


                    ////////xu ly nen
                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                    {
                        zip.AlternateEncoding = System.Text.Encoding.Unicode;
                        zip.AddDirectory(Server.MapPath("~" + contractorTrans.StoreFolderPath));
                        zip.Save(serverTotalDocPackPath);
                    };
                    ////down load va xoa nguon
                    Response.ClearContent();
                    Response.Clear();
                    Response.ContentType = "application/zip";
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
                    Response.TransmitFile(serverTotalDocPackPath);

                    HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
                    cookie.Value = "Flag";
                    cookie.Expires = DateTime.Now.AddDays(1);
                    Response.AppendCookie(cookie);

                    Response.Flush();
                    System.IO.File.Delete(serverTotalDocPackPath);
                    Response.End();
                    //  Response.Redirect("~/DownloadFile.ashx?path=" + filename);

                }
            }
        }

        private void ExportPECC2ETRM(PVGASTransmittal transObj)
        {
            var attachDocToTrans = this.attachDocToTransmittalService.GetAllByTransId(transObj.ID);
            if (attachDocToTrans != null)
            {
                var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Exports") + @"\";
                //var filePath = Server.MapPath("Exports") + @"\";
                var workbook = new Workbook();
                workbook.Open(filePath + @"Template\PVGASTransmittalTemplate.xlsm");

                var dataSheet = workbook.Worksheets[0];

                var dtFull = new DataTable();

                dtFull.Columns.AddRange(new[]
                {
                    new DataColumn("DocumentNo", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("Revision", typeof(String)),
                    new DataColumn("DocumentTitle", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String)),
                    new DataColumn("Empty5", typeof(String)),
                    new DataColumn("Empty6", typeof(String)),
                    new DataColumn("Empty7", typeof(String)),
                    new DataColumn("Empty8", typeof(String)),
                });

                foreach (var docobj in attachDocToTrans)
                {
                    var dataRow = dtFull.NewRow();
                    var documentObj = this.documentService.GetById(docobj.DocumentId.GetValueOrDefault());
                    dataRow["DocumentNo"] = documentObj.DocNo;
                    dataRow["Revision"] = documentObj.RevisionName;
                    dataRow["DocumentTitle"] = documentObj.DocTitle;
                    dtFull.Rows.Add(dataRow);
                }

                var projectObj = this.projectCodeService.GetById(transObj.ProjectCodeId.GetValueOrDefault());

                var filename = transObj.TransmittalNo + "_Trans_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xlsx";
                dataSheet.Cells["J5"].PutValue(transObj.TransmittalNo);
                dataSheet.Cells["J6"].PutValue(DateTime.Now.ToString("dd-MMM-yy"));
                dataSheet.Cells["B5"].PutValue(projectObj.FullName);
                dataSheet.Cells["B7"].PutValue(transObj.Description);
                dataSheet.Cells["B8"].PutValue(transObj.FromValue);
                dataSheet.Cells["B10"].PutValue(transObj.ToValue);
                dataSheet.Cells["B12"].PutValue(transObj.CCValue);

                dataSheet.Cells.ImportDataTable(dtFull, false, 20, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);

                for (int i = 0; i < dtFull.Rows.Count; i++)
                {
                    dataSheet.Cells.Merge(20 + i, 0, 1, 3);
                    dataSheet.Cells.Merge(20 + i, 4, 1, 7);
                }

                workbook.Save(filePath + filename);
                this.Download_File(filePath + filename);
            }
        }

        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }

        private void ExportContractorETRM(ContractorTransmittal transObj)
        {
            var attachDocFullList = this.contractorTransmittalDocFileService.GetAllByTrans(transObj.ID).OrderBy(t=> t.DocumentNo);
            var attachDocFileFilter = new List<ContractorTransmittalDocFile>();

            // Remove duplicate Document
            foreach (var document in attachDocFullList)
            {
                if (document.ChangeRequestTypeId == 5)
                {
                    document.PurposeName = "CRS";
                }
                    attachDocFileFilter.Add(document);
                //}
                //else if (attachDocFileFilter.All(t => t.DocumentNo != document.DocumentNo))
                //{
                //    attachDocFileFilter.Add(document);
                //}
            }
            // --------------------------------------------------------------------------------------------

            var filePath = Server.MapPath("../../Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\_ContractorTransTemplate.xlsm");
            var workSheets = workbook.Worksheets;
            var transSheet = workSheets[0];
            //var fileListSheet = workSheets[9];
            // Export trans Info
            var dtFull = new DataTable();
            dtFull.Columns.AddRange(new[]
            {
                new DataColumn("Index", typeof(String)),
                new DataColumn("DocNo", typeof(String)),
                 new DataColumn("1Empty", typeof(String)),
                new DataColumn("DocTitle", typeof(String)),
                 new DataColumn("2Empty", typeof(String)),
                  new DataColumn("3Empty", typeof(String)),
                   new DataColumn("4Empty", typeof(String)),
                new DataColumn("Revision", typeof(string)),
                new DataColumn("ActionCode", typeof(String)),
                new DataColumn("6Empty", typeof(String)),
                new DataColumn("RevRemark", typeof(String)),
            });

            var count = 1;
            foreach (var doc in attachDocFileFilter)
            {
                var dataRow = dtFull.NewRow();
                dataRow["Index"] = count;
                dataRow["DocNo"] = doc.DocumentNo;
                dataRow["DocTitle"] = doc.DocumentTitle;
                dataRow["Revision"] = doc.Revision;
                dataRow["ActionCode"] = doc.PurposeName.Split('-')[0];
                dataRow["RevRemark"] = doc.RevRemark;
                dtFull.Rows.Add(dataRow);
                count += 1;
            }

            //transSheet.Cells.DeleteRow(19 + attachDocFileFilter.Count);
            //  var organisationObj = this.organizationCodeService.GetById(transObj.OriginatingOrganizationId.GetValueOrDefault());
            var projectObj = this.projectCodeService.GetById(transObj.ProjectId.GetValueOrDefault());
            var st= @"Vui lòng ký xác nhận và chuyển về cho #From# để lưu hồ sơ (qua email/ bưu điện/ fax)./ 
Please acknowledge receipt by signing, dating and returning a copy of this Transmittal(sent by email / post / fax).";
             transSheet.Cells["A20"].PutValue(st.Replace("#From#", transObj.OriginatingOrganizationName.Split(',')[0]));
            transSheet.Cells["I2"].PutValue(transObj.TransNo);
            transSheet.Cells["C5"].PutValue(transObj.FromValue);
            transSheet.Cells["C7"].PutValue(transObj.ToValue);
            transSheet.Cells["C17"].PutValue(DateTime.Now.ToString("dd/MM/yyyy"));
            transSheet.Cells["I3"].PutValue(transObj.TransDate.GetValueOrDefault());


            transSheet.Cells.ImportDataTable(dtFull, false, 9, 0, dtFull.Rows.Count, dtFull.Columns.Count, true);

            for (int i = 0; i < attachDocFileFilter.Count; i++)
            {
                transSheet.Cells.Merge(9 + i, 1, 1, 2);
                transSheet.Cells.Merge(9 + i, 3, 1, 4);
               // transSheet.Cells.Merge(9 + i, 10, 1, 2);
            }

           
          
            //if (organisationObj != null)
            //{
            //    transSheet.Cells["C4"].PutValue(organisationObj.HeadOffice);
            //    transSheet.Cells["C5"].PutValue(organisationObj.Phone);
            //    transSheet.Cells["C6"].PutValue(organisationObj.Fax);
            //}
            
            // ---------------------------------------------------------------------
            
            // Fill Signed
            if (!string.IsNullOrEmpty(UserSession.Current.User.SignImageUrl))
            {
                transSheet.Pictures.Add(13 + dtFull.Rows.Count, 7, Server.MapPath("../.." + UserSession.Current.User.SignImageUrl));
            }
            // ---------------------------------------------------------------------

            var savePath = Server.MapPath("../.." + transObj.StoreFolderPath) + "\\eTRM File\\";
            var fileName = transObj.TransNo + "_eTRM_" +
                           transObj.TransDate.GetValueOrDefault().ToString("dd-MM-yyyy") + ".xlsm";
            workbook.Save(savePath + fileName);

            this.Download_File(savePath + fileName);
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
        }

        /// <summary>
        /// The grd document_ item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "RebindGrid")
            {

            }
            
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
            {

            }
        }

        
        protected void ddlProjectOutgoing_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var ddlProjectOutgoing = (RadComboBox)this.radMenuOutgoing.Items[2].FindControl("ddlProjectOutgoing");
            int projectId = Convert.ToInt32(ddlProjectOutgoing.SelectedValue);
            this.lblProjectOutgoingId.Value = projectId.ToString();
            this.grdOutgoingTrans.Rebind();

            Session.Add("SelectedProject", projectId);
        }

        

        protected void ddlProjectOutgoing_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"~/Images/project.png";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void grdOutgoingTrans_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var ddlProjectOutgoing = (RadComboBox)this.radMenuOutgoing.Items[2].FindControl("ddlProjectOutgoing");
            var ddlStatusOutgoing = (DropDownList)this.radMenuOutgoing.Items[2].FindControl("ddlStatusOutgoing");
            var txtSearchOutgoing = (TextBox)this.radMenuOutgoing.Items[2].FindControl("txtSearchOutgoing");
            var outgoingTransList = new List<ContractorTransmittal>();

            if (ddlProjectOutgoing != null && ddlProjectOutgoing.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(ddlProjectOutgoing.SelectedValue);

                if (UserSession.Current.User.RoleId == 1
                    || UserSession.Current.User.IsAdmin.GetValueOrDefault())
                {
                    outgoingTransList = this.contractorTransmittalService.GetAllByProject(projectId, 2, txtSearchOutgoing.Text.Trim()).OrderByDescending(t => t.TransNo).ToList();
                }
                else
                {
                    if (UserSession.Current.User.Role.ContractorId != null)
                    {
                        outgoingTransList = this.contractorTransmittalService.GetAllByProject(projectId, 2, txtSearchOutgoing.Text.Trim())
                            .Where(t => t.OriginatingOrganizationId.GetValueOrDefault() == UserSession.Current.User.Role.ContractorId)
                            .OrderByDescending(t => t.TransNo).ToList();
                    }
                }

                if (ddlStatusOutgoing != null)
                {
                    switch (ddlStatusOutgoing.SelectedValue)
                    {
                        case "Invalid":
                            outgoingTransList = outgoingTransList.Where(t => !t.IsValid.GetValueOrDefault()).ToList();
                            break;
                        case "Waiting":
                            outgoingTransList = outgoingTransList.Where(t => t.IsValid.GetValueOrDefault() && !t.IsSend.GetValueOrDefault()).ToList();
                            break;
                        case "Sent":
                            outgoingTransList = outgoingTransList.Where(t => t.IsSend.GetValueOrDefault()).ToList();
                            break;
                    }
                }
            }

            this.grdOutgoingTrans.DataSource = outgoingTransList.OrderByDescending(t=> t.TransDate.GetValueOrDefault());
        }

        protected void ddlStatusOutgoing_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.grdOutgoingTrans.Rebind();
        }

        protected void btnSearchOutgoing_Click(object sender, EventArgs e)
        {
            this.grdOutgoingTrans.Rebind();
        }

        protected void grdOutgoingTrans_OnDeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var objId = new Guid(item.GetDataKeyValue("ID").ToString());
            var transObj = this.contractorTransmittalService.GetById(objId);
            if (!string.IsNullOrEmpty(transObj?.StoreFolderPath))
            {
                var folderPath = Server.MapPath("../.." + transObj.StoreFolderPath);
                if (Directory.Exists(folderPath))
                {
                    Directory.Delete(folderPath);
                }
            }

            this.contractorTransmittalService.Delete(objId);
            this.grdOutgoingTrans.Rebind();
        }

        protected void btnSearchIncoming_Click(object sender, EventArgs e)
        {
            this.grdIncomingTrans.Rebind();
        }

        protected void grdIncomingTrans_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var ddlProjectIncoming = (RadComboBox)this.radMenuIncoming.Items[1].FindControl("ddlProjectIncoming");
            var txtSearchIncoming = (TextBox)this.radMenuIncoming.Items[1].FindControl("txtSearchIncoming");
            var incomingTransList = new List<ContractorTransmittal>();
            if (ddlProjectIncoming != null && ddlProjectIncoming.SelectedItem != null)
            {
                var projectId = Convert.ToInt32(ddlProjectIncoming.SelectedValue);
                if (UserSession.Current.User.RoleId == 1
                    || UserSession.Current.User.IsAdmin.GetValueOrDefault())
                {
                    incomingTransList =
                        this.contractorTransmittalService.GetAllByProject(projectId, 1, txtSearchIncoming.Text)
                            .OrderByDescending(t => t.TransNo)
                            .ToList();
                }
                else
                {
                    if (UserSession.Current.User.Role.ContractorId != null)
                    {
                        incomingTransList =
                            this.contractorTransmittalService.GetAllByProject(projectId, 1, txtSearchIncoming.Text)
                                .Where(
                                    t =>
                                        t.ReceivingOrganizationId.GetValueOrDefault() ==
                                        UserSession.Current.User.Role.ContractorId)
                                .OrderByDescending(t => t.TransNo)
                                .ToList();
                    }
                }
            }
            this.grdIncomingTrans.DataSource = incomingTransList.OrderByDescending(t=> t.TransDate.GetValueOrDefault());
        }

        protected void ddlProjectIncoming_ItemDataBound(object sender, RadComboBoxItemEventArgs e)
        {
            e.Item.ImageUrl = @"~/Images/project.png";
        }

        protected void ddlProjectIncoming_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            var ddlProjectIncoming = (RadComboBox)this.radMenuIncoming.Items[1].FindControl("ddlProjectIncoming");
            int projectId = Convert.ToInt32(ddlProjectIncoming.SelectedValue);
            this.lblProjectIncomingId.Value = projectId.ToString();
            this.grdIncomingTrans.Rebind();
        }

        private void NotifiNewTransmittal( ContractorTransmittal transmittal)
        {
            try
            {
                if (transmittal != null)
                {

                    var userListid = this.userService.GetAllByDC();

                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                        EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                        TargetName = "STARTTLS/smtp.gmail.com",
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    };
                    int count = 0;
                    var containtable = string.Empty;

                    var subject = "New Transmittal (#Trans#) has been sended from contractor (" + transmittal.FromName + ")";

                    var message = new MailMessage();
                    message.From = new MailAddress(ConfigurationManager.AppSettings["EmailAccount"], "EDMS System");
                    message.Subject = subject.Replace("#Trans#", transmittal.TransNo);
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;

                    var bodyContent = @"<div style=‘text-align: center;’> 
                                    <span class=‘Apple-tab-span’>Dear All,&nbsp;</span><br />
                                   
                                    <p style=‘text-align: center;’><strong><span style=‘font-size: 18px;’>Please be informed that the following document of transmittal (#Trans#)</span></strong></p><br/>

                                       <table border='1' cellspacing='0'>
                                       <tr>
                                       <th style='text-align:center; width:40px'>No.</th>
                                       <th style='text-align:center; width:330px'>Document Number</th>
                                       <th style='text-align:center; width:60px'>Revision</th>
                                       <th style='text-align:center; width:330px'>Document Title</th>
                                        <th style='text-align:center; width:330px'>Project</th>
                                       <th style='text-align:center; width:330px'>Issue Date</th>
                                       <th style='text-align:center; width:330px'>Deadline Comment</th>
                                       <th style='text-align:center; width:330px'>Purpose</th>
                                       </tr>";

                    var Listdoc = this.contractorTransmittalDocFileService.GetAllByTrans(transmittal.ID);
                    var deadline = string.Empty;
                    deadline = transmittal.DueDate != null ? transmittal.DueDate.Value.ToString("dd/MM/yyyy") : "";
                    var Issuedate = string.Empty;
                    Issuedate = transmittal.TransDate != null ? transmittal.TransDate.Value.ToString("dd/MM/yyyy") : "";
                    List<string> ListDoc = new List<string>();
                    foreach (var document in Listdoc)
                    {if (!ListDoc.Where(t => t == document.DocumentNo).Any())
                        {

                            count += 1;
                            ListDoc.Add(document.DocumentNo);
                            bodyContent += @"<tr>
                               <td>" + count + @"</td>
                               <td>" + document.DocumentNo + @"</td>
                               <td>"
                                           + document.Revision + @"</td>
                               <td>"
                                           + document.DocumentTitle + @"</td>
                                <td>"
                                           + document.ProjectName + @"</td>
                               <td>"
                                           + Issuedate + @"</td>
                                 <td>"
                                           + deadline + @"</td>
                                <td>"
                                           + transmittal.PurposeName + @"</td>";
                        }
                    }
                    var st = ConfigurationManager.AppSettings["WebAddress"]+ @"/Controls/Document/PVGASTransmittalList.aspx"; 
                     bodyContent += @" </table>
                                       <br/>
                                       <span><br />
                                    &nbsp;This link to access&nbsp;:&nbsp; <a href='" + st + "'>" + st + "</a>" +
                                  @" <br/>  &nbsp;&nbsp;&nbsp; EDMS TRANSMITTAL NOTIFICATION </br>
                        [THIS IS SYSTEM GENERATED NOTIFICATION PLEASE DO NOT REPLY]";
                    message.Body = bodyContent.Replace("#Trans#", transmittal.TransNo); ;

                    var Userlist = userListid.Where(t => !string.IsNullOrEmpty(t.Email)).Distinct().ToList();
                    foreach (var user in Userlist)
                    {
                        try
                        {
                            message.To.Add(new MailAddress(user.Email));
                        }
                        catch { }

                    }
                    var listCC = ConfigurationManager.AppSettings["CCEmail"].Split(';').Where(t => !string.IsNullOrEmpty(t)).Distinct().ToList();
                    foreach (var user in listCC)
                    {
                        try
                        {
                            message.CC.Add(new MailAddress(user));
                        }
                        catch { }

                    }
                    smtpClient.Send(message);
                }
            }
            catch(Exception ex) { }
        }

        ///---- new template email
        ///
        private void ContrctorSendemail(ContractorTransmittal transmittal)
        {
            try
            {
                
                if (transmittal != null )
                {

                    var userList= this.userService.GetAllByDC().Where(t => !string.IsNullOrEmpty(t.Email)).ToList();
                    var projctobj = this.projectCodeService.GetById(transmittal.ProjectId.GetValueOrDefault());
                    var roleObj = this.rolseService.GetByContractor(transmittal.ReceivingOrganizationId.GetValueOrDefault()).Select(t=> t.Id).ToList();
                    //var smtpClient = new SmtpClient
                    //{
                    //    DeliveryMethod = SmtpDeliveryMethod.Network,
                    //    UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                    //    EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                    //    Host = ConfigurationManager.AppSettings["Host"],
                    //    Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                    //    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                    //};
                    var smtpClient = new SmtpClient
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                       // UseDefaultCredentials = Convert.ToBoolean(ConfigurationManager.AppSettings["UseDefaultCredentials"]),
                       // EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]),
                        Host = ConfigurationManager.AppSettings["Host"],
                       // TargetName = "STARTTLS/smtp.gmail.com",
                        Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]),
                        // Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAccount"], ConfigurationManager.AppSettings["EmailPass"])
                        Credentials = new NetworkCredential(UserSession.Current.User.Email, "")
                    };
                    int count = 0;
                    var containtable = string.Empty;

                    var subject = "FYI: New transmittal submitted, " + transmittal.TransNo + ", " + transmittal.TransDate.GetValueOrDefault().ToString("dd/MM/yyyy") + ", " + transmittal.PurposeName.Split('-')[1] + ", " + transmittal.Description;

                    var message = new MailMessage();
                    message.From = new MailAddress(UserSession.Current.User.Email, UserSession.Current.User.FullName);
                    message.Subject = subject;
                    message.BodyEncoding = new UTF8Encoding();
                    message.IsBodyHtml = true;
                    var emailto = string.Empty;
                    var to = userList.Where(t =>roleObj.Contains(t.RoleId.GetValueOrDefault())).ToList();
                    foreach (var user in to)
                    {
                       
                        try
                        {
                            if (user.Email.Contains(";"))
                            {
                                foreach (string stemail in user.Email.Split(';').Where(t => !string.IsNullOrEmpty(t)).ToList())
                                {
                                    message.To.Add(new MailAddress(stemail));
                                    emailto += stemail + "; ";
                                }
                            }
                            else
                            {
                                message.To.Add(new MailAddress(user.Email));
                                emailto += user.Email + "; ";
                            }

                        }
                        catch { }
                    }
                    //var infoUserIds = customObj.CCUserIDs != null
                    //   ? customObj.CCUserIDs.Split(';').ToList()
                    //   : new List<string>();


                    var emailCC = string.Empty;
                    //var UsserCC = this.userService.GetListUser(infoUserIds.Distinct().Where(t => !string.IsNullOrEmpty(t)).Select(t => Convert.ToInt32(t)).ToList());
                    var listCC = userList.Where(t => t.Id == transmittal.CreatedBy).ToList();
                    foreach (var user in listCC)
                    {
                      
                        try
                        {
                            if (user.Email.Contains(";"))
                            {
                                foreach (string stemail in user.Email.Split(';').Where(t => !string.IsNullOrEmpty(t)).ToList())
                                {
                                    message.CC.Add(new MailAddress(stemail));
                                    emailCC += stemail + "; ";
                                }
                            }
                            else
                            {
                                message.CC.Add(new MailAddress(user.Email));
                                emailCC += user.Email + "; ";
                            }

                        }
                        catch { }

                    }



                    var bodyContent = @"<head><title></title><style>
                  body {font-family:Calibri;font-size:10px;}
                hr {color:#2C4E9C;background-color:#2C4E9C; height:3px;}
                .msg {font-size:16px;}                        
                table {width:98.0%;border-collapse:collapse;margin-left:20px;color:black;background-color:white;border:1px solid #ACCEF5;padding:3px;font-size:16px;}
                td {border:1px solid #ACCEF5;}
                .span1 {font-size:16px;}
                .ch1 {background-color:#F7FAFF;padding:10px;font-weight:bold;color:#2C4E9C;}
                .ch2 {background-color:#F7FAFF;padding:5px;}
                a {color:mediumblue;}
                .system {font-weight:bolder; font-family:'Bookman Old Style'; color:#2C4E9C;}
                .company {font-weight:bolder; font-family:'Bookman Old Style'; color:#2C4E9C;}
                .link {font-size:16px;margin-left:30px;}
                .footer {color:darkgray; font-size:12px;}
                /*TYPE OF NOTIFICATION PURPOSE*/
                .action {background-color:#fffda5;}
                .info {background-color:#d1fcbd;}
                .overdue {background-color:#f00;color:white;font-weight:bold;}
                  .header_ {width:50.0%;border:none;border-bottom:solid #98C6EA 1.0pt;mso-border-bottom-alt:solid #98C6EA .75pt;background:#D4EFFC;padding:3.75pt 3.75pt 3.75pt 3.75pt}
                  .footer_ {border:none;border-top:solid #98C6EA 1.0pt;mso-border-top-alt:solid #98C6EA .75pt;background:#D4EFFC;padding:6.0pt 6.0pt 6.0pt 6.0pt}
                  .font_l {font-size:13.5pt;font-family:'Verdana',sans-serif}
                .font_m {font-size:10.0pt;font-family:'Verdana',sans-serif}
                .font_s {font-size:9.0pt;font-family:'Verdana',sans-serif}
                .font_xs {font-size:7.5pt;font-family:'Verdana',sans-serif}
                </style></head>
                <body>
                 <table border='1'>
                  <tr>
                                <td width='50%' class='header_'>
								<b><span class='font_m'>" + transmittal.Description + @"</span></b><br>				
								<b><span class='font_xs' style='color:red'>" + projctobj.Description + ", " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + @"</span></b>
							</td>
							<td width='50%' class='header_'>
								<p class='MsoNormal' align='right' style='text-align:right'>
									<b><span class='font_l' style='color:#000066'>PVGAS</span></b>
									<em><b><span class='font_l' style='color:red'>SEG</span></b></em>
								</p>
							</td>
                    </tr>
                  
                  <tr><td colspan='2' > 
                    <p align='center' style='margin-bottom:12.0pt;text-align:center'>
							<span class='font_m'>
								<br><b>New Transmittal :</b> Transmittal <b>" + transmittal.TransNo + @"</b> has been sent to you for your review
							</span>
						</p>
						<div align='center' style='border:none;padding:3.75pt 3.75pt 3.75pt 3.75pt'>
                    <table border='1'>
                <tr>
                    <td class='ch2' style='width:167px' ><span class='font_s' style='color:#003399'>From</span> </td><td colspan='3'><span class='font_s' style='color:#003399'>" + UserSession.Current.User.FullNameWithPosition + @"</span></td>
                   
                </tr>
                <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>To</span></td><td colspan='3'><span class='font_s' style='color:#003399'>
								<a href='mailto:" + emailto + "'>" + emailto + @"</a>
							</span></td>
                   
                </tr>
   <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>CC</span></td><td colspan='3'><span class='font_s' style='color:#003399'>
								<a href='mailto:" + emailCC + "'>" + emailCC + @"</a>
							</span></td>
                   
                </tr>
                <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>Transmittal No.</span></td><td colspan='3' class='font_s' style='color:red'>" + transmittal.TransNo + @"</td>
                   
                </tr>
                      <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>Transmittal Title</span></td><td class='font_s' colspan='3'>" + transmittal.Description + @"</td>
                   
                </tr>
                <tr>
                    <td class='ch2' style='width:167px'><span class='font_s' style='color:#003399'>Issued Date</span></td><td class='font_s'>" + transmittal.TransDate.GetValueOrDefault().ToString("dd/MM/yyyy") + @"</td>
                    <td class='ch2'><span class='font_s' style='color:#003399'>Action Code</span></td><td class='font_s' style='color:red'>" + transmittal.PurposeName + @"</td>
                  </tr>
                  </table>
                  </div>";

                    var st = ConfigurationManager.AppSettings["WebAddress"] + @"/Controls/Document/PVGASTransmittalList.aspx?TransNoContractor=" + transmittal.TransNo;
                    var st1 = ConfigurationManager.AppSettings["WebAddress"] + @"/Controls/Document/PVGASTransmittalList.aspx";
                    bodyContent += @"  <p style='margin-bottom:12.0pt'>
			            <span class='font_m'>
				            <u><b>Useful Links:</b></u>
				            <ul class='font_m'>
					            <li>
								Click <a href='" + st + @"'>here</a> to show <u>this transmittal</u> in EDMS System
							</li>
							<li>
								Click <a href= '" + st1 + @"' > here</a> to show <u>all transmittals</u> in EDMS System
							</li>
						</ul>
					   </span>
						</p>			
						<p  align='center' style='margin-bottom:12.0pt'>
						<span class='font_m'>[THIS IS SYSTEM AUTO-GENERATED NOTIFICATION. PLEASE DO NOT REPLY.]
						</span>
						</p>
						</td>
						</tr>
						<tr>
							<td class='footer_'>
								<b><span class='font_xs'>PETROVIETNAM GAS JOINT STOCK CORPORATION (PV GAS)</span></b>
							</td>
							<td class='footer_'>
								<p  align=right style='text-align:right'>
									<b><span class='font_xs'>05th Floor, 673 Nguyen Huu Tho, Nha Be District, Ho Chi Minh, Vietnam <br>
                                                        Tel: +84 028 37840930  Fax: +84 028 37816606
									</span></b>
								</p>
							</td>
						</tr>
					</table></body>";
                    message.Body = bodyContent;

                    smtpClient.Send(message);
                  //  Response.Write(message);
                }
            }
            catch(Exception ex) {

               // Response.Write( ex.ToString());
            }
        }
    }
}