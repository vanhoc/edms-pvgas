﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.UI;
    using Business.Services.Document;
    using Business.Services.Library;
    using Data.Entities;
    using Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ContractorTransmittalAttachDocFileEditForm : Page
    {
        private readonly ContractorTransmittalService transmittalService;

        private readonly ContractorTransmittalDocFileService contractorTransmittalDocFileService;

       // private readonly DQREDocumentService documentService;

        private readonly DocumentTypeService documentTypeService;

       /// private readonly DQREDocumentNumberingService documentNumberingService;

        private readonly ProjectCodeService projectService;

        private readonly PhaseService phaseservice;

        private readonly DrawingCodeService drawingCodeService;

        private readonly DrawingDetailCodeService drawindetailcodeservice;

        private readonly SystemCodeService systemcodeservice;

        private readonly DisciplineService disciplineService;

        private readonly DocumentStatuService documentCodeServices;

        private readonly DocumentClassService documentClassService;

        private readonly AreaService areaservice;

        private readonly OrganizationCodeService organizationCodeService;

        private readonly SubAreaService subareaservice; 

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ContractorTransmittalAttachDocFileEditForm()
        {
            this.transmittalService = new ContractorTransmittalService();
            this.projectService = new ProjectCodeService();
            this.contractorTransmittalDocFileService = new ContractorTransmittalDocFileService();
          //  this.documentService = new DQREDocumentService();
            this.documentTypeService = new DocumentTypeService();
         //   this.documentNumberingService = new DQREDocumentNumberingService();
            this.drawingCodeService = new  DrawingCodeService();
            this.phaseservice=new PhaseService();
            this.systemcodeservice= new SystemCodeService();
            this.disciplineService = new DisciplineService();
            this.documentCodeServices = new   DocumentStatuService();
            this.areaservice = new  AreaService();
            this.documentClassService = new DocumentClassService();
            this.subareaservice = new  SubAreaService();
            this.drawindetailcodeservice = new   DrawingDetailCodeService();
            this.organizationCodeService = new OrganizationCodeService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                
                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    var obj = this.contractorTransmittalDocFileService.GetById(new Guid(this.Request.QueryString["objId"]));
                    if (obj != null)
                    {
                        this.txtFileName.Text = obj.FileName;
                        this.txtDOcumentNo.Text = obj.DocumentNo;
                        this.ddlProjectCode.SelectedValue = obj.ProjectId.ToString();
                        this.txtRevision.Text = obj.Revision;
                        this.txtDocumentTitle.Text = obj.DocumentTitle;
                        this.ddlPhaseName.SelectedValue = obj.PhaseId.ToString();
                        this.ddlDocumentType.SelectedValue = obj.DocumentTypeId.ToString();
                        this.ddlDiscipline.SelectedValue = obj.DisciplineCodeId.ToString();
                        this.txtSequence.Text = obj.Sequence;
                       // this.txtContractorRefNo.Text = obj.ContractorRefNo;
                        this.ddlAreaName.SelectedValue = obj.AreaId.ToString();
                        this.ddlSubAreaName.SelectedValue = obj.SubAreaId.ToString();
                        this.txtTrainNo.Text = obj.TrainNo;
                        this.ddlOriginatingOrganization.SelectedValue = obj.OriginatingOrganizationId.ToString();
                        this.ddlDrawingCodeName.SelectedValue = obj.DrawingCodeId.ToString();
                        this.ddlSubDrawingCodeName.SelectedValue = obj.DrawinDetailCodeId.ToString();
                        this.ddlSystemCode.SelectedValue = obj.SystemId.ToString();
                        this.txtRevRemark.Text = obj.RevRemark;
                        this.ddlPurpose.SelectedValue = obj.PurposeId.ToString();

                        if (!string.IsNullOrEmpty(obj.ErrorPosition))
                        {
                            foreach (var position in obj.ErrorPosition.Split('$').Where(t => !string.IsNullOrEmpty(t)))
                            {
                                switch (position)
                                {
                                    case "0":
                                        this.txtDOcumentNo.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "1":
                                        this.ddlProjectCode.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "2":
                                        this.ddlOriginatingOrganization.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "3":
                                        this.ddlDocumentType.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "4":
                                        this.ddlPhaseName.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "5":
                                        this.ddlSystemCode.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "6":
                                        this.ddlAreaName.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "7":
                                        this.ddlSubAreaName.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "8":
                                        this.ddlDiscipline.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "9":
                                        this.ddlDrawingCodeName.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                    case "10":
                                        this.ddlSubDrawingCodeName.CssClass = "min25Percent qlcbFormRequired";
                                        break;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    var objId = new Guid(this.Request.QueryString["objId"]);
                    var obj = this.contractorTransmittalDocFileService.GetById(objId);
                    if (obj != null)
                    {
                        this.CollectData(obj);
                        this.contractorTransmittalDocFileService.Update(obj);

                        var listdocfile = contractorTransmittalDocFileService.GetAllByTrans(obj.TransId.GetValueOrDefault());
                        var checklist = listdocfile.Where(t => t.ChangeRequestTypeId == 1 && t.PurposeId == null);
                        var transobj = transmittalService.GetById(obj.TransId.GetValueOrDefault());
                        if(!checklist.Any() && listdocfile.Where(t=> string.IsNullOrEmpty(t.Status)).Any())
                        {
                            transobj.IsValid = true;
                            transmittalService.Update(transobj);
                        }
                    }
                   
                }
                else
                {
                    var obj = new ContractorTransmittalDocFile();
                    obj.ID = Guid.NewGuid();
                    this.CollectData(obj);
                    this.contractorTransmittalDocFileService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        private void CollectData(ContractorTransmittalDocFile obj)
        {
            var purposed = obj.PurposeId != null || obj.PurposeId != 0;
            obj.Revision = this.txtRevision.Text;
            obj.DocumentTitle = this.txtDocumentTitle.Text;
            //obj.ContractorRefNo = this.txtContractorRefNo.Text;
            obj.PurposeId = Convert.ToInt32(this.ddlPurpose.SelectedValue);
            obj.PurposeName = this.ddlPurpose.SelectedItem.Text.Split(',')[0];
            obj.RevRemark = this.txtRevRemark.Text;
            if(purposed && obj.PurposeId != null && obj.PurposeId != 0)
            {
               obj.Status = string.Empty;
                obj.ErrorMessage = string.Empty;
                if (!string.IsNullOrEmpty(obj.ErrorPosition)) obj.ErrorPosition.Replace("11$",string.Empty);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var projectList = this.projectService.GetAll().OrderBy(t => t.Code).ToList();
            projectList.Insert(0, new ProjectCode() {ID = 0});
            this.ddlProjectCode.DataSource = projectList;
            this.ddlProjectCode.DataTextField = "Code";
            this.ddlProjectCode.DataValueField = "ID";
            this.ddlProjectCode.DataBind();

            var pahseCodeList = this.phaseservice.GetAll().OrderBy(t => t.Name).ToList();
            pahseCodeList.Insert(0, new Phase() { ID = 0 });
            this.ddlPhaseName.DataSource = pahseCodeList;
            this.ddlPhaseName.DataTextField = "Name";
            this.ddlPhaseName.DataValueField = "ID";
            this.ddlPhaseName.DataBind();

            var doctypeList = this.documentTypeService.GetAll().OrderBy(t => t.Code).ToList();
            doctypeList.Insert(0, new DocumentType() { ID = 0 });
            this.ddlDocumentType.DataSource = doctypeList;
            this.ddlDocumentType.DataTextField = "Code";
            this.ddlDocumentType.DataValueField = "ID";
            this.ddlDocumentType.DataBind();

            var disciplineList = this.disciplineService.GetAll().OrderBy(t => t.Name).ToList();
            disciplineList.Insert(0, new Discipline() { ID = 0 });
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataTextField = "Name";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();

            var areacodelist = this.areaservice.GetAll();
            areacodelist.Insert(0, new Area() {ID = 0});
            this.ddlAreaName.DataSource = areacodelist.OrderBy(t => t.Name);
            this.ddlAreaName.DataTextField = "Name";
            this.ddlAreaName.DataValueField = "ID";
            this.ddlAreaName.DataBind();

            var subareacodelist = this.subareaservice.GetAll();
            subareacodelist.Insert(0, new SubArea() { ID = 0 });
            this.ddlSubAreaName.DataSource = subareacodelist.OrderBy(t => t.Name);
            this.ddlSubAreaName.DataTextField = "Name";
            this.ddlSubAreaName.DataValueField = "ID";
            this.ddlSubAreaName.DataBind();

            var drawingcodelist = this.drawingCodeService.GetAll();
            drawingcodelist.Insert(0, new DrawingCode() { ID = 0 });
            this.ddlDrawingCodeName.DataSource = drawingcodelist.OrderBy(t => t.Name);
            this.ddlDrawingCodeName.DataTextField = "Name";
            this.ddlDrawingCodeName.DataValueField = "ID";
            this.ddlDrawingCodeName.DataBind();

            var drawingdetaillist = this.drawindetailcodeservice.GetAll();
            drawingdetaillist.Insert(0, new DrawingDetailCode() { ID = 0 });
            this.ddlSubDrawingCodeName.DataSource = drawingdetaillist.OrderBy(t => t.Name);
            this.ddlSubDrawingCodeName.DataTextField = "Name";
            this.ddlSubDrawingCodeName.DataValueField = "ID";
            this.ddlSubDrawingCodeName.DataBind();

            var organizationList = this.organizationCodeService.GetAll();
            organizationList.Insert(0, new OrganizationCode() {ID = 0});
            this.ddlOriginatingOrganization.DataSource = organizationList.OrderBy(t => t.Code);
            this.ddlOriginatingOrganization.DataTextField = "Code";
            this.ddlOriginatingOrganization.DataValueField = "ID";
            this.ddlOriginatingOrganization.DataBind();

            var systemcodelist = this.systemcodeservice.GetAll();
            systemcodelist.Insert(0, new SystemCode() {ID = 0});
            this.ddlSystemCode.DataSource = systemcodelist.OrderBy(t => t.Name);
            this.ddlSystemCode.DataTextField = "Name";
            this.ddlSystemCode.DataValueField = "ID";
            this.ddlSystemCode.DataBind();

            //var purposeList = this.documentCodeServices.GetAllActionCode();
            //purposeList.Insert(0, new DocumentCode() { ID = 0 });
            //this.ddlPurpose.DataSource = purposeList.OrderBy(t => t.Code);
            //this.ddlPurpose.DataTextField = "FullName";
            //this.ddlPurpose.DataValueField = "ID";
            //this.ddlPurpose.DataBind();
            var purposeList = this.documentCodeServices.GetAll();
            purposeList.Insert(0, new DocumentStatu() { Name = string.Empty });
            this.ddlPurpose.DataSource = purposeList.OrderBy(t => t.Name);
            this.ddlPurpose.DataTextField = "FullName";
            this.ddlPurpose.DataValueField = "ID";
            this.ddlPurpose.DataBind();
        }
    }
}