﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System.IO;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using Aspose.Words;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class TransmittalAttachDocument : Page
    {
        private readonly PVGASTransmittalService transmittalService;

        private readonly ProjectCodeService projectCodeService;

        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        private readonly DisciplineService disciplineService;

        private readonly DocumentPackageService documentsService;

        private readonly PVGASDocumentAttachFileService PVGASDocumentAttachFileService;

        private readonly PVGASTransmittalAttachDocFileService PVGASTransmittalAttachDocFileService;

        private readonly PVGASTransmittalAttachFileService transAttachFileService;

        /// <summary>
        /// The unread pattern.
        /// </summary>
        protected const string unreadPattern = @"\(\d+\)";

        private readonly int TransmittalFolderId = Convert.ToInt32(ConfigurationManager.AppSettings.Get("TransFolderId"));

        /// <summary>
        /// Initializes a new instance of the <see cref="TransmittalAttachDocument"/> class.
        /// </summary>
        public TransmittalAttachDocument()
        {
            this.transmittalService = new PVGASTransmittalService();
            this.projectCodeService = new ProjectCodeService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.disciplineService = new DisciplineService();
            this.documentsService = new DocumentPackageService();
            this.PVGASDocumentAttachFileService = new PVGASDocumentAttachFileService();
            this.PVGASTransmittalAttachDocFileService = new PVGASTransmittalAttachDocFileService();
            this.transAttachFileService = new PVGASTransmittalAttachFileService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.Page.IsPostBack)
            {
                this.LoadComboData();
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.Rebind();
            }
            else if (e.Argument == "RebindAndNavigate")
            {
                this.grdDocument.MasterTableView.SortExpressions.Clear();
                this.grdDocument.MasterTableView.GroupByExpressions.Clear();
                this.grdDocument.MasterTableView.CurrentPageIndex = this.grdDocument.MasterTableView.PageCount - 1;
                this.grdDocument.Rebind();
            }
        }

        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (this.ddlDiscipline.Items.Count > 0)
            {
                var projectId = Convert.ToInt32(this.ddlProject.SelectedValue);
                var disciplineId = Convert.ToInt32(this.ddlDiscipline.SelectedValue);

                var incomingTransId = new Guid(this.ddlIncomingTransmittal.SelectedValue);
                var incomingTransObj = this.transmittalService.GetById(incomingTransId);
                var docNumber = this.txtDocNumber.Text.Trim();
                var docTitle = this.txtDocTitle.Text.Trim();

                var isGetAllRevision = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("GetAllRevisionInTrans"));

                var listDoc = this.documentsService.SearchDocument(
                    projectId,
                    disciplineId,
                    docNumber,
                    docTitle,
                    string.Empty,
                    isGetAllRevision);

                // Filter by Incoming Trans
                if (incomingTransObj != null)
                {
                    listDoc = listDoc.Where(t => t.IncomingTransId == incomingTransObj.ID).ToList();
                }
                // --------------------------------------------------------------------------------------------------------------

                // Filter exist doc of trans
                var transId = new Guid(this.Request.QueryString["objId"]);
                var existDocIds = this.attachDocToTransmittalService.GetAllByTransId(transId).Select(t => t. DocumentId.GetValueOrDefault()).ToList();
                listDoc = listDoc.Where(t => !existDocIds.Contains(t.ID) && t.IsHasAttachFile.GetValueOrDefault() && string.IsNullOrEmpty(t.OutgoingTransNo)).ToList();
                //-------------------------------------------------

                this.grdDocument.DataSource = listDoc.OrderBy(t => t.DocNo);
            }
        }

        /// <summary>
        /// The rad menu_ item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void radMenu_ItemClick(object sender, RadMenuEventArgs e)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The btn search_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.grdDocument.Rebind();
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var listProject = this.projectCodeService.GetAll().OrderBy(t => t.Code).ToList();

            this.ddlProject.DataSource = listProject;
            this.ddlProject.DataTextField = "Fullname";
            this.ddlProject.DataValueField = "ID";
            this.ddlProject.DataBind();

            var listDiscipline = this.disciplineService.GetAll().OrderBy(t => t.Name).ToList();

            listDiscipline.Insert(0, new Discipline() { ID = 0 });

            this.ddlDiscipline.DataSource = listDiscipline;
            this.ddlDiscipline.DataTextField = "Fullname";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataBind();

            if (this.ddlProject.SelectedItem != null)
            {
                var incomingTransList = this.transmittalService.GetAllByProject(Convert.ToInt32(this.ddlProject.SelectedValue), 1, string.Empty);
                incomingTransList.Insert(0, new PVGASTransmittal() { ID = Guid.NewGuid() });
                this.ddlIncomingTransmittal.DataSource = incomingTransList;
                this.ddlIncomingTransmittal.DataTextField = "TransmittalNo";
                this.ddlIncomingTransmittal.DataValueField = "ID";
                this.ddlIncomingTransmittal.DataBind();
            }
        }
        private DateTime GetDate(int day, DateTime transdate)
        {
            var actualDeadline = transdate;
            for (int i = 1; i <= day; i++)
            {
                actualDeadline = this.GetNextWorkingDay(actualDeadline);
            }
            return actualDeadline;
        }
        private bool IsWeekEnd(DateTime date)
        {
            return ConfigurationManager.AppSettings["WeekendWork"] == "false" ? date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday : false;
        }
        private DateTime GetNextWorkingDay(DateTime date)
        {
            do
            {
                date = date.AddDays(1);
            }
            while (IsWeekEnd(date));

            return date;
        }
        /// <summary>
        /// The btn save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
            {
                var objId = new Guid(this.Request.QueryString["objId"]);
                var objTrans = this.transmittalService.GetById(objId);
                var listSelectedDocId = new List<Guid>();
                var haveAttachDoc = false;
                if (objTrans != null)
                {
                    foreach (GridDataItem item in this.grdDocument.MasterTableView.Items)
                    {
                        var cboxSelected = (CheckBox)item["IsSelected"].FindControl("cboxSelectDocTransmittal");
                        if (cboxSelected.Checked)
                        {
                            haveAttachDoc = true;
                            var docId = new Guid(item.GetDataKeyValue("ID").ToString());
                            var docObj = this.documentsService.GetById(docId);
                            if(docObj != null)
                            {
                                docObj.OutgoingTransId = objTrans.ID;
                                docObj.OutgoingTransNo = objTrans.TransmittalNo;
                                docObj.OutgoingTransDate = objTrans.IssuedDate;
                                if (!string.IsNullOrEmpty(docObj.DocReviewStatusCode))
                                {
                                    docObj.DNB_FinalCode = docObj.DocReviewStatusCode;
                                    //Update Date actual Codecomment

                                    if (docObj.DocReviewStatusCode.ToLower().Trim() == "code 2")
                                    {
                                        if (docObj.ReIFRActual == null) docObj.ReIFRActual = objTrans.IssuedDate;
                                        docObj.DeadlineClientResponse = this.GetDate(3, objTrans.IssuedDate.GetValueOrDefault());

                                    }
                                    else if (docObj.DocReviewStatusCode.ToLower().Trim() == "code 1" && docObj.IFAActual == null)
                                    {
                                        docObj.IFAActual = objTrans.IssuedDate;

                                    }
                                    else if (docObj.DocReviewStatusCode.ToLower().Trim() == "code 3")
                                    {
                                        docObj.DeadlineClientResponse = this.GetDate(5, objTrans.IssuedDate.GetValueOrDefault());
                                    }
                                }
                                this.documentsService.Update(docObj);

                                listSelectedDocId.Add(docId);

                                var attachDoc = new AttachDocToTransmittal()
                                {
                                    TransmittalId = objId,
                                    DocumentId = docId
                                };
                                if (!this.attachDocToTransmittalService.IsExist(objId, docId))
                                {
                                    this.attachDocToTransmittalService.Insert(attachDoc);
                                }

                                cboxSelected.Checked = false;
                            }
                            else
                            {
                                lblNotification.Text = "Please Enter Code Review after add transmittal Out. <br/> "+ docObj.DocNo;
                                RadNotification1.Title = "WARING";
                                RadNotification1.ContentIcon = "~/Images/error.png";
                                RadNotification1.Show();
                            }


                            // Get document file for outgoing trans
                            #region Update file CRS
                            var markupCommentAttachDocList = this.PVGASDocumentAttachFileService.GetAllCmtResByDocId(docId);
                            foreach (var attachFile in markupCommentAttachDocList)
                            {
                                if (attachFile.TypeId == 4)
                                {
                                    var rootPath = Server.MapPath("../.." + attachFile.FilePath);
                                    if (File.Exists(rootPath))
                                    {
                                        //Aspose.Words.Document doc = new Aspose.Words.Document(rootPath);

                                        //DocumentBuilder b = new DocumentBuilder(doc);
                                        //b.MoveToCell(0, 0, 5, 0);

                                        //b.Writeln(String.IsNullOrEmpty(docObj.OutgoingTransNo) ? string.Empty : docObj.OutgoingTransNo);
                                        //b.MoveToCell(0, 0, 7, 0);

                                        //b.Writeln(docObj.RevisionName);
                                        //b.MoveToCell(0, 0, 9, 0);

                                        //b.Writeln(String.IsNullOrEmpty(docObj.DocReviewStatusCode) ? string.Empty : docObj.DocReviewStatusCode);
                                        //var filename = "CRS_" + docObj.DocNo + "_" + docObj.DocTitle + "_" + docObj.RevisionName + ".docx";

                                        //doc.Save(Path.Combine(Server.MapPath("../.." + objTrans.StoreFolderPath), filename));

                                    }
                                }
                                else
                                {
                                    File.Copy(Server.MapPath("../.." + attachFile.FilePath), Path.Combine(Server.MapPath("../.." + objTrans.StoreFolderPath), attachFile.FileName), true);
                                }


                                var transAttachDocFile = new PVGASTransmittalAttachDocFile
                                {
                                    ID = Guid.NewGuid(),
                                    DocumentId = docObj.ID,
                                    TransId = objTrans.ID,
                                    DocNo = docObj.DocNo,
                                    DocTitle = docObj.DocTitle,
                                    Revision = docObj.RevisionName,
                                    FileName = attachFile.FileName,
                                    ExtensionIcon = attachFile.ExtensionIcon,
                                    Extension = attachFile.Extension,
                                    FileSize = attachFile.FileSize,
                                    FilePath = objTrans.StoreFolderPath + "/" + attachFile.FileName,
                                };

                                this.PVGASTransmittalAttachDocFileService.Insert(transAttachDocFile);

                                // Add comment sheet of document
                                //var transAttachFile = new PVGASTransmittalAttachFile()
                                //{
                                //    ID = Guid.NewGuid(),
                                //    TransId = objTrans.ID,
                                //    Filename = attachFile.FileName,
                                //    Extension = attachFile.Extension,
                                //    FilePath = objTrans.StoreFolderPath + "/" + attachFile.FileName,
                                //    ExtensionIcon = attachFile.ExtensionIcon,
                                //    FileSize = attachFile.FileSize,
                                //    CreatedBy = UserSession.Current.User.Id,
                                //    CreatedByName = UserSession.Current.User.FullName,
                                //    CreatedDate = DateTime.Now,
                                //    TypeId = 2,
                                //    TypeName = "Comment Sheet",
                                //    DocumentId = docObj.ID,
                                //    DocumentNo = docObj.DocNo
                                //};

                                //this.transAttachFileService.Insert(transAttachFile);
                            }
                            #endregion
                            // -----------------------------------------------------------------------------------------------
                        }
                    }

                    // Update trans info
                    if (haveAttachDoc)
                    {
                        objTrans.IsValid = true;
                        objTrans.Status = string.Empty;
                        objTrans.ErrorMessage = string.Empty;

                        this.transmittalService.Update(objTrans);
                    }
                    // ---------------------------------------------------------

                    this.grdDocument.Rebind();
                    this.grdExistAttachDoc.Rebind();
                }
            }
        }

        protected void ddlProject_SelectedIndexChange(object sender, EventArgs e)
        {
            if (this.ddlProject.SelectedItem != null)
            {
                var incomingTransList = this.transmittalService.GetAllByProject(Convert.ToInt32(this.ddlProject.SelectedValue), 1, string.Empty);
                incomingTransList.Insert(0, new PVGASTransmittal() { ID = Guid.NewGuid() });
                this.ddlIncomingTransmittal.DataSource = incomingTransList;
                this.ddlIncomingTransmittal.DataTextField = "TransmittalNo";
                this.ddlIncomingTransmittal.DataValueField = "ID";
                this.ddlIncomingTransmittal.DataBind();
            }

            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            
        }

        protected void grdExistAttachDoc_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var transId = new Guid(this.Request.QueryString["objId"]);
            //  this.grdExistAttachDoc.DataSource = this.attachDocToTransmittalService.GetAllByTransId(transId).Select(t => this.documentsService.GetById(t.DocumentId.GetValueOrDefault())).OrderBy(t => t.DocNo);
            var listDocument = new List<DocumentPackage>();

            var attachDocList = this.attachDocToTransmittalService.GetAllByTransId(transId);
            foreach (var item in attachDocList)
            {
                var docObj = this.documentsService.GetById(item.DocumentId.GetValueOrDefault());
                if (docObj != null)
                {
                    listDocument.Add(docObj);
                }
            }

            var newtable = (from A in listDocument
                            join B in attachDocList on A.ID equals B.DocumentId
                            select new
                            {
                                ID = B.ID,
                                DocumentId = B.DocumentId,
                                TransId = B.TransmittalId,
                                DocNo = A.DocNo,
                                DocTitle = A.DocTitle,
                                RevisionName = A.RevisionName,
                                StartDate = A.StartDate, 
                            });
            this.grdExistAttachDoc.DataSource = newtable.OrderBy(t => t.DocNo);
        }

        protected void grdExistAttachDoc_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var objId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
            var attachDocToTransObj = this.attachDocToTransmittalService.GetById(objId);
            this.attachDocToTransmittalService.Delete(objId);

            // Remove comment sheet of document attached in transmittal
            var transAttachFileList = this.transAttachFileService.GetByDoc(attachDocToTransObj.DocumentId.GetValueOrDefault());
            foreach (var transAttachFile in transAttachFileList)
            {
                this.transAttachFileService.Delete(transAttachFile);
            }

            var docObj = this.documentsService.GetById(attachDocToTransObj.DocumentId.GetValueOrDefault());
            if(docObj != null)
            {
                var objTrans = new DocumentPackage();
                docObj.OutgoingTransId = objTrans.OutgoingTransId;
                docObj.OutgoingTransNo = objTrans.OutgoingTransNo;
                docObj.OutgoingTransDate = objTrans.OutgoingTransDate;
                this.documentsService.Update(docObj);
            }
            // ---------------------------------------------------------------------------------------------------------------

            this.grdExistAttachDoc.Rebind();
            this.grdDocument.Rebind();
        }
    }
}