﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentPackageSendTo_back.aspx.cs" Inherits="EDMs.Web.Controls.Document.DocumentPackageSendTo_back" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        function CloseAndRefreshGrid() {
            var oWin = GetRadWindow();
            var parentWindow = oWin.BrowserWindow;
            $(oWin).ready(function () {
                oWin.close();
            });
            parentWindow.refreshGrid();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including classic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }


    </script>
    <style type="text/css">
        .RadGrid .rgRow td, .RadGrid .rgAltRow td, .RadGrid .rgEditRow td, .RadGrid .rgFooter td, .RadGrid .rgFilterRow td, .RadGrid .rgHeader, .RadGrid .rgResizeCol, .RadGrid .rgGroupHeader td {
            padding-left: 1px !important;
            padding-right: 1px !important;
        }

        /*Hide change page size control*/
        div.RadGrid .rgPager .rgAdvPart     
        {     
        display:none;        
        } 
          .RadGrid .rgSelectedRow
        {
            background-image : none !important;
            background-color: darkseagreen !important;
        }
           
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="content" runat="server" ID="divContent">
            <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
          
            <telerik:RadGrid AllowCustomPaging="False" AllowPaging="True" AllowSorting="True" 
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None" 
                                Height="550" ID="grdDocument"  AllowFilteringByColumn="True" AllowMultiRowSelection="True" 
               Skin="Windows7"
                OnNeedDataSource="grdDocument_OnNeedDataSource" 
                 PageSize="100" runat="server" Style="outline: none" Width="100%">
                <SortingSettings SortedBackColor="#FFF6D6"></SortingSettings>
                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                <MasterTableView AllowMultiColumnSorting="false" 
                    ClientDataKeyNames="ID" DataKeyNames="ID" Font-Size="8pt">
                         <GroupByExpressions>
                                            <telerik:GridGroupByExpression>
                                                <SelectFields>
                                                    <telerik:GridGroupByField FieldAlias="-" FieldName="CategoryName" FormatString="{0:D}"
                                                        HeaderValueSeparator=""></telerik:GridGroupByField>
                                                </SelectFields>
                                                <GroupByFields>
                                                    <telerik:GridGroupByField FieldName="CategoryID" SortOrder="Ascending" ></telerik:GridGroupByField>
                                                </GroupByFields>
                                            </telerik:GridGroupByExpression>
                                        </GroupByExpressions>    
                        <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                        <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                        <ColumnGroups>
                            <telerik:GridColumnGroup HeaderText="Markup" Name="markup"
                                             HeaderStyle-HorizontalAlign="Center"/>
                          <%--  <telerik:GridColumnGroup HeaderText="First Issue Info" Name="FirstIssueInfo"
                                    HeaderStyle-HorizontalAlign="Center"/>
                            <telerik:GridColumnGroup HeaderText="Final Issue Info" Name="FinalIssueInfo"
                                    HeaderStyle-HorizontalAlign="Center"/>
                            <telerik:GridColumnGroup HeaderText="INCOMING TRANSMITTAL" Name="IncomingTrans"
                                    HeaderStyle-HorizontalAlign="Center"/>
                            <telerik:GridColumnGroup HeaderText="ICA REVIEW DETAILS" Name="ICAReviews"
                                    HeaderStyle-HorizontalAlign="Center"/>--%>
                             <telerik:GridColumnGroup HeaderText="Incoming Transmittal" Name="ReceivedInfo"
                                             HeaderStyle-HorizontalAlign="Center"/>

                                        <telerik:GridColumnGroup HeaderText="Outgoing Transmittal" Name="OutTrans" HeaderStyle-HorizontalAlign="Center"></telerik:GridColumnGroup>
                        </ColumnGroups>
                        <Columns>
                                <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                              <telerik:GridClientSelectColumn UniqueName="IsSelected">
                                                <HeaderStyle Width="25"  />
                                                <ItemStyle HorizontalAlign="Center" Width="25"/>
                                            </telerik:GridClientSelectColumn>
                                <telerik:GridTemplateColumn HeaderText="No." Groupable="False">
                                    <HeaderStyle HorizontalAlign="Center" Width="2%" VerticalAlign="Middle"></HeaderStyle>
                                    <ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSoTT" runat="server" Text='<%# grdDocument.CurrentPageIndex * grdDocument.PageSize + grdDocument.Items.Count+1 %>'>
                                        </asp:Label>
                                      
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--<telerik:GridEditCommandColumn ButtonType="ImageButton" EditImageUrl="~/Images/edit.png" 
                                    UpdateImageUrl="~/Images/ok.png" CancelImageUrl="~/Images/delete.png" UniqueName="EditColumn">
                                    <HeaderStyle HorizontalAlign="Center" Width="2%"  />
                                    <ItemStyle HorizontalAlign="Center" Width="2%"/>
                                </telerik:GridEditCommandColumn>--%>
                            <%--<telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" HeaderTooltip="Delete document"
                                    ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                    <HeaderStyle Width="1%" />
                                        <ItemStyle HorizontalAlign="Center" Width="1%"  />
                                </telerik:GridButtonColumn>
                                --%>
                            <telerik:GridTemplateColumn HeaderText="DOC. No." UniqueName="DocNo">
                                <HeaderStyle HorizontalAlign="Center" Width="160" />
                                <ItemStyle HorizontalAlign="Left"/>
                                <ItemTemplate>
                                    <%# Eval("DocNo") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField ID="DocNo" runat="server" Value='<%# Eval("DocNo") %>'/>
                                    <asp:Label runat="server" ID="lbldocNo"></asp:Label>
                                    <%--<asp:TextBox ID="txtDocNo" runat="server" Width="100%"></asp:TextBox>--%>
                                </EditItemTemplate>
                            </telerik:GridTemplateColumn>
                                            
                            <telerik:GridTemplateColumn HeaderText="DOC. Title" UniqueName="DocTitle"
                                        DataField="DocTitle" ShowFilterIcon="False" FilterControlWidth="97%" 
                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                        <HeaderStyle HorizontalAlign="Center" Width="220" />
                                        <ItemStyle HorizontalAlign="Left" Width="220" />
                                        <ItemTemplate>
                                            <%# Eval("DocTitle") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    
                                    <telerik:GridTemplateColumn HeaderText="Rev." UniqueName="Rev"
                                        AllowFiltering="false">
                                        <HeaderStyle HorizontalAlign="Center" Width="50" />
                                        <ItemStyle HorizontalAlign="Center" Width="50"/>
                                        <ItemTemplate>
                                            <%# Eval("RevisionName") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="KDN" UniqueName="KDN_markup"
                                        AllowFiltering="false" Display="true" ColumnGroupName="markup">
                                        <HeaderStyle HorizontalAlign="Center" Width="40" />
                                        <ItemStyle HorizontalAlign="Center"  />
                                        <ItemTemplate>
                                            <%# Eval("KDN_markup") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="DNV" UniqueName="DNV_markup"
                                        AllowFiltering="false" Display="true" ColumnGroupName="markup">
                                        <HeaderStyle HorizontalAlign="Center" Width="40" />
                                        <ItemStyle HorizontalAlign="Center"  />
                                        <ItemTemplate>
                                            <%# Eval("DNV_markup") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                        
                                         <telerik:GridTemplateColumn HeaderText="NCS" UniqueName="NCS_markup"
                                        AllowFiltering="false" Display="true" ColumnGroupName="markup">
                                        <HeaderStyle HorizontalAlign="Center" Width="40" />
                                        <ItemStyle HorizontalAlign="Center"  />
                                        <ItemTemplate>
                                            <%# Eval("NCS_markup") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                 
                                           <telerik:GridDateTimeColumn HeaderText="Received Date" UniqueName="IncomingTransDate" DataField="IncomingTransDate"
                                          DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate" AllowFiltering="false"  PickerType="DatePicker" ColumnGroupName="ReceivedInfo">
                                        <HeaderStyle HorizontalAlign="Center" Width="70" />
                                        <ItemStyle HorizontalAlign="Center" Width="70"/>
                                    </telerik:GridDateTimeColumn>
                                        
                                    <telerik:GridTemplateColumn HeaderText="Trans No." UniqueName="IncomingTransNo"
                                         AllowFiltering="false" ColumnGroupName="ReceivedInfo">
                                        <HeaderStyle HorizontalAlign="Center" Width="150" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Eval("IncomingTransNo") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>

                                <telerik:GridTemplateColumn HeaderText="Date" UniqueName="OutgoingTransDate" ColumnGroupName="OutTrans"  AllowFiltering="False">
                                    <HeaderStyle HorizontalAlign="Center" Width="70" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("OutgoingTransDate","{0:dd/MM/yyyy}") %>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                  <telerik:GridTemplateColumn HeaderText="Trans No." UniqueName="OutgoingTransNo"
                                         AllowFiltering="false" ColumnGroupName="OutTrans">
                                        <HeaderStyle HorizontalAlign="Center" Width="120" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <%# Eval("OutgoingTransNo") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                              <telerik:GridTemplateColumn HeaderText="Code" UniqueName="DocReviewStatusCode"  ColumnGroupName="OutTrans"
                                        AllowFiltering="false" Display="true">
                                        <HeaderStyle HorizontalAlign="Center" Width="50" />
                                        <ItemStyle HorizontalAlign="Center"  />
                                        <ItemTemplate>
                                            <%# Eval("DocReviewStatusCode") %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                  
                        </Columns>
                    </MasterTableView>
                    <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                    </ClientSettings>
            </telerik:RadGrid>
                 <div style="width:120px">
             <telerik:RadButton ID="btnDownLoad" runat="server" Text="Export Report"  Width="120px" OnClick="btnExport_Click"  style="text-align: center">
                                                <Icon PrimaryIconUrl="../../Images/download.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                                            </telerik:RadButton> </div> </div>
        <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
        <telerik:RadAjaxManager runat="Server" ID="ajaxCustomer" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart"></ClientEvents>
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxCustomer">
                    <UpdatedControls>
                         <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel2"/>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel2"></telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
            

            <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" >
            <Windows>
                
            <telerik:RadWindow ID="AttachDoc" runat="server" Title="Attach document files" 
                VisibleStatusbar="false" Height="500" Width="500" MinHeight="500" MinWidth="500" MaxHeight="500" MaxWidth="500" 
                Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
       
        <telerik:RadCodeBlock runat="server">
            <script type="text/javascript">
                
                //function RowDblClick(sender, eventArgs) {
                //    sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
                //}

                var ajaxManager;
                function pageLoad() {
                    ajaxManager = $find("<%=ajaxCustomer.ClientID %>");
                }

                function onRequestStart(sender, args) {
                    alert(args.get_eventTarget());
                    if (args.get_eventTarget().indexOf("btnDownloadPackage") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
               
            </script>
        </telerik:RadCodeBlock>
    </form>
</body>
</html>
