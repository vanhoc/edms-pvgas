﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.UI;
    using Business.Services.Document;
    using Business.Services.Library;
    using Data.Entities;
    using Utilities.Sessions;
    using System.Data;

    using Aspose.Cells;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ContractorTransmittalAttachDocFile : Page
    {
        private readonly ContractorTransmittalService transmittalService;
        private readonly ContractorTransmittalDocFileService contractorTransmittalDocFileService;
        private readonly DocumentPackageService documentService;
        private readonly DocumentTypeService documentTypeService;
        private readonly DocumentNumberingService documentNumberingService;
        private readonly ProjectCodeService projectService;
        private readonly SystemCodeService _SystemCodeService;
        private readonly DrawingCodeService drawingCodeService;
        private readonly DisciplineService disciplineService;
        private readonly DocumentCodeServices documentCodeServices;
        private readonly AreaService areaService;
        private readonly SubAreaService _SubAreaService;
        private readonly DrawingDetailCodeService _DrawingDetailCodeService;
        private readonly OrganizationCodeService organizationCodeService;
        private readonly GroupCodeService groupCodeService;
        private readonly PhaseService _PhaseService;
            

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ContractorTransmittalAttachDocFile()
        {
            this.transmittalService = new ContractorTransmittalService();
            this.projectService = new ProjectCodeService();
            this.contractorTransmittalDocFileService = new ContractorTransmittalDocFileService();
            this.documentService = new DocumentPackageService();
            this.documentTypeService = new DocumentTypeService();
            this.documentNumberingService = new  DocumentNumberingService();
            this.drawingCodeService = new  DrawingCodeService();
            this._SystemCodeService = new SystemCodeService();
            this.disciplineService = new DisciplineService();
            this.documentCodeServices = new DocumentCodeServices();
            this.areaService = new AreaService();
            this._SubAreaService = new SubAreaService();
            this.organizationCodeService = new OrganizationCodeService();
            this.groupCodeService = new GroupCodeService();
            this._DrawingDetailCodeService = new DrawingDetailCodeService();
            this._PhaseService = new PhaseService();

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    var objId = new Guid(this.Request.QueryString["objId"]);
                    var obj = this.transmittalService.GetById(objId);
                    if (obj != null)
                    {
                        this.CollectData(obj);

                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedByName = UserSession.Current.User.FullName;
                        obj.LastUpdatedDate = DateTime.Now;

                        this.transmittalService.Update(obj);
                    }
                }
                else
                {
                    var obj = new ContractorTransmittal();
                    obj.ID = Guid.NewGuid();
                    this.CollectData(obj);

                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedByName = UserSession.Current.User.FullName;
                    obj.CreatedDate = DateTime.Now;
                    this.transmittalService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        private void CollectData(ContractorTransmittal obj)
        {

        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        protected void grdDocumentFile_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
            {
                var transId = new Guid(this.Request.QueryString["objId"]);
                var docFileList = this.contractorTransmittalDocFileService.GetAllByTrans(transId).OrderBy(t => t.FileName);

                this.grdDocumentFile.DataSource = docFileList;
            }
        }

        protected void btnProcessDocNo_OnClick(object sender, EventArgs e)
        {
            var fileIcon = new Dictionary<string, string>()
                    {
                        { ".doc", "~/images/wordfile.png" },
                        { ".docx", "~/images/wordfile.png" },
                        { ".dotx", "~/images/wordfile.png" },
                        { ".xls", "~/images/excelfile.png" },
                        { ".xlsx", "~/images/excelfile.png" },
                        { ".xlsm", "~/images/excelfile.png" },
                        { ".pdf", "~/images/pdffile.png" },
                        { ".7z", "~/images/7z.png" },
                        { ".dwg", "~/images/dwg.png" },
                        { ".dxf", "~/images/dxf.png" },
                        { ".rar", "~/images/rar.png" },
                        { ".zip", "~/images/zip.png" },
                        { ".txt", "~/images/txt.png" },
                        { ".xml", "~/images/xml.png" },
                        { ".bmp", "~/images/bmp.png" },
                    };

            var transId = new Guid(this.Request.QueryString["objId"]);
            var transObj = this.transmittalService.GetById(transId);
            if (transObj != null)
            {
                               var physicalPath = Server.MapPath("../.." + transObj.StoreFolderPath);
                var transFlag = false;
                foreach (UploadedFile docFile in radUploadDoc.UploadedFiles)
                {
                    var errorFlag = false;
                    var shortFileName = docFile.FileName.Replace(docFile.GetExtension(), "");
                    var docObj = new ContractorTransmittalDocFile();
                    var shortDoc = shortFileName.Split('_')[0].ToLower()=="crs"? shortFileName.Split('_')[1]: shortFileName.Split('_')[0];

                   // if (projectDocMasterList != null)
                   // {
                        if (shortFileName.Split('_').Length == 3)
                        {
                            // Save doc file
                            var physicalFilePath = Path.Combine(physicalPath, Utilities.Utility.RemoveSpecialCharacterFileName(docFile.FileName));
                            docFile.SaveAs(physicalFilePath, true);
                            // -----------------------------------------------------------------

                            docObj.ID = Guid.NewGuid();
                            docObj.TransId = transObj.ID;
                            docObj.PurposeId = transObj.PurposeId;
                            docObj.PurposeName = transObj.PurposeName;
                            docObj.Extension = docFile.GetExtension();
                            docObj.ExtensionIcon = fileIcon.ContainsKey(docObj.Extension.ToLower())
                                ? fileIcon[docObj.Extension.ToLower()]
                                : "~/images/otherfile.png";
                            docObj.FilePath = transObj.StoreFolderPath + "/" + (Utilities.Utility.RemoveSpecialCharacterFileName(docFile.FileName));// docFile.FileName;
                            docObj.ChangeRequestTypeId = docObj.Extension.ToLower() == ".pdf" ? 1 : 2;

                            docObj.FileSize = (double)docFile.ContentLength / 1024;

                            // Detect DocType

                            var documentNumberingList = this.documentNumberingService.GetAllByProject(transObj.ProjectId.GetValueOrDefault());
                            //--------------------------------------

                            if (documentNumberingList != null)
                            {
                                // Get document numbering format for Doc type
                                var documentNumbering = new DocumentNumbering();
                                //---------------------------------------------
                                var partOfFileName = shortFileName.Split('_')[0].Replace(" ", string.Empty).Split('-');
                                if (partOfFileName.Length > 3)
                                {
                                //var lenghtSegment = partOfFileName.Length;

                                //var segmentEnd = partOfFileName[lenghtSegment - 1];
                                //if (segmentEnd.Length == 3)
                                //{
                                //    documentNumbering = documentNumberingList.Find(t => t.Lenght == lenghtSegment && t.TypeID == 2);
                                //}
                                //else if (segmentEnd.Length == 4)
                                //{
                                //    documentNumbering = documentNumberingList.Find(t => t.Lenght == lenghtSegment && t.TypeID == 1);
                                //}
                                var lenghtSegment = partOfFileName.Length;

                                var segmentEnd = partOfFileName[lenghtSegment - 1].Length;
                                foreach (var fromat in documentNumberingList)
                                {
                                    if (documentNumbering.Name == null)
                                    {
                                        var fromat_lenghtSegment = fromat.Format.Split('-').Length;

                                        var fromat_segmentEnd = fromat.Format.Split('-')[fromat_lenghtSegment - 1].Length;
                                        if (fromat_lenghtSegment == lenghtSegment && segmentEnd == fromat_segmentEnd) { documentNumbering = fromat; }
                                    }
                                }
                            }
                                if (documentNumbering != null)
                                {
                                    var partOfFullDocumentNumbering = documentNumbering.Format.Split('-');

                                    if (documentNumbering.Lenght <= partOfFileName.Length)
                                {
                                    var projectDocMasterList = this.documentService.GetByDocNo(shortDoc, transObj.ProjectId.GetValueOrDefault());
                                    if (projectDocMasterList != null)
                                    {
                                        docObj.ProjectName = projectDocMasterList.ProjectFullName;

                                        docObj.ProjectId = projectDocMasterList.ProjectId;
                                        docObj.DocumentTypeId = projectDocMasterList.DocumentTypeId;
                                        docObj.DocumentTypeName = projectDocMasterList.DocumentTypeName;
                                        docObj.OriginatingOrganizationId = projectDocMasterList.ContractorId;
                                        docObj.OriginatingOrganizationName = projectDocMasterList.ContractorName;
                                        docObj.PhaseId = projectDocMasterList.PhaseId;
                                        docObj.PhaseName = projectDocMasterList.PhaseName;
                                        docObj.SystemId = projectDocMasterList.SystemCodeId;
                                        docObj.SystemName = projectDocMasterList.SystemCodeName;
                                        docObj.AreaId = projectDocMasterList.AreaId;
                                        docObj.AreaName = projectDocMasterList.AreaName;
                                        docObj.SubAreaId = projectDocMasterList.SubAreaId;
                                        docObj.SubAreaName = projectDocMasterList.SubAreaName;
                                        docObj.DisciplineCodeId = projectDocMasterList.DisciplineId;
                                        docObj.DisciplineCodeName = projectDocMasterList.DisciplineName;
                                        docObj.DrawingCodeId = projectDocMasterList.DrawingCodeId;
                                        docObj.DrawingCodeName = projectDocMasterList.DrawingCodeName;
                                        docObj.DrawinDetailCodeId = projectDocMasterList.DrawingDetailCodeId;
                                        docObj.DrawinDetailCodeName = projectDocMasterList.DrawingDetailCodeName;
                                        docObj.Sequence = projectDocMasterList.SequencetialNumber;
                                        docObj.DocumentNo = projectDocMasterList.DocNo;
                                    }
                                    else
                                    {


                                        var fullDocNo = string.Empty;
                                        for (int i = 0; i < documentNumbering.Lenght; i++)
                                        {
                                            fullDocNo += i == documentNumbering.Lenght - 1
                                                ? partOfFileName[i]
                                                : partOfFileName[i] + "-";

                                            switch (partOfFullDocumentNumbering[i])
                                            {
                                                case "AAAA":
                                                    var projectCode = partOfFileName[i];
                                                    var projectCodeObj = this.projectService.GetByCode(projectCode);


                                                    if (projectCodeObj != null)
                                                    {
                                                        docObj.ProjectName = projectCode;
                                                        docObj.ProjectId = projectCodeObj.ID;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.ProjectName = projectCode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ Project code '" + projectCode +
                                                                               "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "1$";
                                                    }

                                                    break;
                                              
                                                case "BBB":
                                                    var originatingOrganizationCode = partOfFileName[i];
                                                    var originatingOrganizationCodeObj =
                                                        this.organizationCodeService.GetByCode(originatingOrganizationCode);
                                                    if (originatingOrganizationCodeObj != null)
                                                    {
                                                        docObj.OriginatingOrganizationId = originatingOrganizationCodeObj.ID;
                                                        docObj.OriginatingOrganizationName = originatingOrganizationCode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.OriginatingOrganizationName = originatingOrganizationCode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ Originating Organization code '" +
                                                                               originatingOrganizationCode + "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "2$";
                                                    }
                                                    break;

                                                case "CC":
                                                    var Phasecode = partOfFileName[i];
                                                    var PhaseCodeObj = this._PhaseService.GetByName(Phasecode, transObj.ProjectId.GetValueOrDefault());
                                                    if (PhaseCodeObj != null)
                                                    {
                                                        docObj.PhaseId = PhaseCodeObj.ID;
                                                        docObj.PhaseName = Phasecode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.PhaseName = Phasecode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ Phase code '" + Phasecode + "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "4$";
                                                    }
                                                    break;
                                             
                                                case "OOO":
                                                case "OO":
                                                    docObj.Sequence = partOfFileName[i];
                                                    break;
                                                case "NNOO":
                                                    docObj.Sequence = partOfFileName[i].Substring(2, 2);
                                                    var systemcode = partOfFileName[i].Substring(0, 2);
                                                    var systemcodeObj = _SystemCodeService.GetByName(systemcode, transObj.ProjectId.GetValueOrDefault());
                                                    if (systemcodeObj != null)
                                                    {
                                                        docObj.SystemId = systemcodeObj.ID;
                                                        docObj.SystemName = systemcode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.SystemName = systemcode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ System code '" + systemcode + "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "5$";
                                                    }
                                                    break;
                                                case "NN":
                                                    var systemcodes = partOfFileName[i];
                                                    var systemcodesObj = _SystemCodeService.GetByName(systemcodes, transObj.ProjectId.GetValueOrDefault());
                                                    if (systemcodesObj != null)
                                                    {
                                                        docObj.SystemId = systemcodesObj.ID;
                                                        docObj.SystemName = systemcodes;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.SystemName = systemcodes;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ System code '" + systemcodes + "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "5$";
                                                    }
                                                    break;
                                                case "D":
                                                    var AreaCode = partOfFileName[i];
                                                    var AreaCodeObj = this.areaService.GetByName(AreaCode, transObj.ProjectId.GetValueOrDefault());
                                                    if (AreaCodeObj != null)
                                                    {
                                                        docObj.AreaId = AreaCodeObj.ID;
                                                        docObj.AreaName = AreaCode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.AreaName = AreaCode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ Area code '" + AreaCode + "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "6$";
                                                    }
                                                    break;
                                                case "EE":
                                                    var SubAreaCode = partOfFileName[i];
                                                    var SubAreaCodeObj = this._SubAreaService.GetByName(SubAreaCode, transObj.ProjectId.GetValueOrDefault(), docObj.AreaId.GetValueOrDefault());
                                                    if (SubAreaCodeObj != null)
                                                    {
                                                        docObj.SubAreaId = SubAreaCodeObj.ID;
                                                        docObj.SubAreaName = SubAreaCode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.SubAreaName = SubAreaCode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ SubArea code '" + SubAreaCode + "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "7$";
                                                    }
                                                    break;
                                                case "GG":
                                                    var disciplineCode = partOfFileName[i];
                                                    var disciplineObj = this.disciplineService.GetByName(disciplineCode);
                                                    if (disciplineObj != null)
                                                    {
                                                        docObj.DisciplineCodeId = disciplineObj.ID;
                                                        docObj.DisciplineCodeName = disciplineCode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.DisciplineCodeName = disciplineCode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ Discipline code '" + disciplineCode +
                                                                               "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "8$";
                                                    }
                                                    break;
                                                case "HH":
                                                    var docTypeObj = this.documentTypeService.GetByCode(partOfFileName[i]);
                                                    if(docTypeObj != null)
                                                    {
                                                        docObj.DocumentTypeId = docTypeObj.ID;
                                                        docObj.DocumentTypeName = docTypeObj.Name;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.DisciplineCodeName = partOfFileName[i];
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ Document Type code '" + partOfFileName[i] +
                                                                               "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "11$";
                                                    }
                                                    break;
                                                case "LM":
                                                case "L":
                                                    if (partOfFileName[i].Length == 1) { 
                                                    var drawincode = partOfFileName[i];
                                                    var drawincodeObj = this.drawingCodeService.GetByName(drawincode, transObj.ProjectId.GetValueOrDefault());
                                                    if (drawincodeObj != null)
                                                    {
                                                        docObj.DrawingCodeId = drawincodeObj.ID;
                                                        docObj.DrawingCodeName = drawincode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.DrawingCodeName = drawincode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ Drawing code '" + drawincode +
                                                                               "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "9$";
                                                    }
                                                    }
                                                    else
                                                    {
                                                        var drawincode = partOfFileName[i].Substring(0, 1);
                                                        var drawincodeObj = this.drawingCodeService.GetByName(drawincode, transObj.ProjectId.GetValueOrDefault());
                                                        if (drawincodeObj != null)
                                                        {
                                                            docObj.DrawingCodeId = drawincodeObj.ID;
                                                            docObj.DrawingCodeName = drawincode;
                                                        }
                                                        else
                                                        {
                                                            errorFlag = true;
                                                            transFlag = true;
                                                            docObj.DrawingCodeName = drawincode;
                                                            docObj.Status = "Missing Doc Numbering Part";
                                                            docObj.ErrorMessage += "_ Drawing code '" + drawincode +
                                                                                   "' is invalid" +
                                                                                   Environment.NewLine;
                                                            docObj.ErrorPosition += "9$";
                                                        }

                                                        var drawingDetailcode_ = partOfFileName[i].Substring(1, 1);
                                                        var drawingDetailcodeObj_ = this._DrawingDetailCodeService.GetByName(drawingDetailcode_, transObj.ProjectId.GetValueOrDefault());
                                                        if (drawingDetailcodeObj_ != null)
                                                        {
                                                            docObj.DrawinDetailCodeId = drawingDetailcodeObj_.ID;
                                                            docObj.DrawinDetailCodeName = drawingDetailcode_;
                                                        }
                                                        else
                                                        {
                                                            errorFlag = true;
                                                            transFlag = true;
                                                            docObj.DrawinDetailCodeName = drawingDetailcode_;
                                                            docObj.Status = "Missing Doc Numbering Part";
                                                            docObj.ErrorMessage += "_ DrawingDetail code '" + drawingDetailcode_ +
                                                                                   "' is invalid" +
                                                                                   Environment.NewLine;
                                                            docObj.ErrorPosition += "10$";
                                                        }
                                                    }
                                                    break;
                                                case "M":
                                                    var drawingDetailcode = partOfFileName[i];
                                                    var drawingDetailcodeObj = this._DrawingDetailCodeService.GetByName(drawingDetailcode, transObj.ProjectId.GetValueOrDefault());
                                                    if (drawingDetailcodeObj != null)
                                                    {
                                                        docObj.DrawinDetailCodeId = drawingDetailcodeObj.ID;
                                                        docObj.DrawinDetailCodeName = drawingDetailcode;
                                                    }
                                                    else
                                                    {
                                                        errorFlag = true;
                                                        transFlag = true;
                                                        docObj.DrawinDetailCodeName = drawingDetailcode;
                                                        docObj.Status = "Missing Doc Numbering Part";
                                                        docObj.ErrorMessage += "_ DrawingDetail code '" + drawingDetailcode +
                                                                               "' is invalid" +
                                                                               Environment.NewLine;
                                                        docObj.ErrorPosition += "10$";
                                                    }
                                                    break;
                                            }
                                        }
                                    }

                                    docObj.DocumentNo = shortFileName.Split('_')[0];
                                    docObj.FileName = Utilities.Utility.RemoveSpecialCharacterFileName(docFile.FileName);// shortFileName.Split('_')[0] + "_" + shortFileName.Split('_')[1] +                                                      docObj.Extension.ToLower();//
                                        docObj.Revision = shortFileName.Split('_')[2];
                                        docObj.DocumentTitle = shortFileName.Split('_')[1];


                                        if (string.IsNullOrEmpty(docObj.GroupCodeName))
                                        {
                                            docObj.GroupCodeId = transObj.GroupId;
                                            docObj.GroupCodeName = transObj.GroupCode;
                                        }
                                    }
                                    else
                                    {
                                        errorFlag = true;
                                        transFlag = true;
                                        docObj.DocumentNo = shortFileName.Split('_')[0];
                                        docObj.FileName = docFile.FileName;
                                        docObj.Status = "Missing Doc Numbering";
                                        docObj.ErrorMessage += "Document is missing Numbering format" + Environment.NewLine;
                                        docObj.ErrorPosition += "0$";
                                    }
                                }
                                else
                                {
                                    errorFlag = true;
                                    transFlag = true;
                                    docObj.DocumentNo = shortFileName.Split('_')[0];
                                    docObj.FileName = docFile.FileName;
                                    docObj.Status = "Missing Doc Numbering";
                                    docObj.ErrorMessage += "Document is missing Numbering format" + Environment.NewLine;
                                    docObj.ErrorPosition += "0$";
                                }
                            }
                            else
                            {
                                errorFlag = true;
                                transFlag = true;
                                docObj.DocumentNo = shortFileName.Split('_')[0];
                                docObj.FileName = docFile.FileName;
                                docObj.Status = "Missing Doc Numbering";
                                docObj.ErrorMessage += "Document is missing Numbering format" + Environment.NewLine;
                                docObj.ErrorPosition += "0$";
                            }
                        }
                        else if (shortFileName.Split('_').Length >= 4 && shortFileName.Split('_')[0].ToLower() == "crs")
                        {
                            var physicalFilePath = Path.Combine(physicalPath, Utilities.Utility.RemoveSpecialCharacterFileName(docFile.FileName));
                            docFile.SaveAs(physicalFilePath, true);
                            // -----------------------------------------------------------------

                            docObj.ID = Guid.NewGuid();
                            docObj.TransId = transObj.ID;
                            docObj.PurposeId = transObj.PurposeId;
                            docObj.PurposeName = transObj.PurposeName;
                            docObj.Extension = docFile.GetExtension();
                            docObj.ExtensionIcon = fileIcon.ContainsKey(docObj.Extension.ToLower())
                                ? fileIcon[docObj.Extension.ToLower()]
                                : "~/images/otherfile.png";
                            docObj.FilePath = transObj.StoreFolderPath + "/" + (Utilities.Utility.RemoveSpecialCharacterFileName(docFile.FileName));// docFile.FileName;
                            docObj.Revision = shortFileName.Split('_')[3];
                            docObj.DocumentTitle = shortFileName.Split('_')[2];
                            docObj.FileSize = (double)docFile.ContentLength / 1024;
                            docObj.DocumentNo = shortFileName.Split('_')[1];
                            docObj.ChangeRequestTypeId = 5;
                            docObj.FileName = Utilities.Utility.RemoveSpecialCharacterFileName(docFile.FileName);
                        }
                        else
                        {
                            errorFlag = true;
                            transFlag = true;
                            docObj.ID = Guid.NewGuid();
                            docObj.TransId = transObj.ID;
                            docObj.ExtensionIcon = "~/images/error1.png";
                            docObj.DocumentNo = shortFileName.Split('_')[0];
                            docObj.FileName = docFile.FileName;
                            docObj.Status = "File name is invalid";
                            docObj.ErrorMessage += "File name don't follow format: DocNo_Title_Rev" + Environment.NewLine;
                            docObj.ErrorPosition += "0$";
                        }
                 //   }
                    //else
                    //{
                    //    errorFlag = true;
                    //    transFlag = true;
                    //    docObj.ID = Guid.NewGuid();
                    //    docObj.TransId = transObj.ID;
                    //    docObj.ExtensionIcon = "~/images/error1.png";
                    //    docObj.DocumentNo = shortFileName.Split('_')[0];
                    //    docObj.FileName = docFile.FileName;
                    //    docObj.Status = "File name is invalid";
                    //    docObj.ErrorMessage += "Document Number is not registered in the EMDR catalog!" + Environment.NewLine;
                    //    docObj.ErrorPosition += "0$";
                    //}
                    if (!errorFlag)
                    {
                        docObj.ErrorMessage = string.Empty;
                        docObj.Status = string.Empty;
                        //if (docObj.ChangeRequestTypeId == 1 && docObj.PurposeId==null) {
                        //    docObj.Status = "Missing Doc Numbering";
                        //    docObj.ErrorMessage += "Please select Purpose document." + Environment.NewLine;
                        //    docObj.ErrorPosition += "11$";
                        //    transFlag = true;
                        //}
                    }
                    else
                    {
                        docObj.ErrorMessage += Environment.NewLine + "** Please correct ducument file name and re-attach to transmittal.";
                    }
                   
                    docObj.IsReject = false;
                    docObj.RejectReason = string.Empty;
                    this.contractorTransmittalDocFileService.Insert(docObj);   
                }

                if (transFlag)
                {
                    transObj.Status = "Attach Doc File Invalid";
                    transObj.ErrorMessage = "Some attach document files are invalid format.";
                    transObj.IsValid = false;
                }
                else
                {
                    transObj.Status = string.Empty;
                    transObj.ErrorMessage = string.Empty;
                    transObj.IsValid = true;
                }

               this.transmittalService.Update(transObj);
                this.grdDocumentFile.Rebind();
            }
        }

        private DataTable GetMappingFile()
        {
            var filePath = Server.MapPath("~/DocumentLibrary") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Mapping/Document Title Mapping List.xlsx");
            var Sheet = workbook.Worksheets[0];
            var datatable = new DataTable();
            datatable = Sheet.Cells.ExportDataTable(1, 0,
                                   Sheet.Cells.MaxRow - 1, 3);
            return datatable;
        }
        private void MappingTitle(ref ContractorTransmittalDocFile objdoc,DataTable datatable)
        {
           
            foreach (DataRow dataRow in datatable.Rows)
            {
                var DocNo = dataRow["Column1"].ToString();
                var Title = dataRow["column2"].ToString();
                var ContractorNo = dataRow["column3"].ToString();
                if (DocNo.Trim() == objdoc.DocumentNoFull)
                {
                    objdoc.DocumentTitle = Title;
                    objdoc.ContractorRefNo = ContractorNo;
                }
            }
        }
        protected void grdDocumentFile_OnItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var item = e.Item as GridDataItem;
                var errorPosition = item["ErrorPosition"].Text;
                if (!string.IsNullOrEmpty(errorPosition))
                {
                    foreach (var position in errorPosition.Split('$').Where(t => !string.IsNullOrEmpty(t)))
                    {
                        switch (position)
                        {
                            case "0":
                                item["DocumentNo"].BackColor = Color.Red;
                                item["DocumentNo"].BorderColor = Color.Red;
                                break;

                            case "1":
                                item["ProjectName"].BackColor = Color.Red;
                                item["ProjectName"].BorderColor = Color.Red;
                                break;
                            case "2":
                                item["OriginatingOrganizationName"].BackColor = Color.Red;
                                item["OriginatingOrganizationName"].BorderColor = Color.Red;
                                break;
                            case "3":
                                item["DocumentTypeName"].BackColor = Color.Red;
                                item["DocumentTypeName"].BorderColor = Color.Red;
                                break;
                            case "4":
                                item["PhaseName"].BackColor = Color.Red;
                                item["PhaseName"].BorderColor = Color.Red;
                                break;
                            case "5":
                                item["SystemName"].BackColor = Color.Red;
                                item["SystemName"].BorderColor = Color.Red;
                                break;
                            case "6":
                                item["AreaName"].BackColor = Color.Red;
                                item["AreaName"].BorderColor = Color.Red;
                                break;
                            case "7":
                                item["SubAreaName"].BackColor = Color.Red;
                                item["SubAreaName"].BorderColor = Color.Red;
                                break;
                            case "8":
                                item["DisciplineCodeName"].BackColor = Color.Red;
                                item["DisciplineCodeName"].BorderColor = Color.Red;
                                break;
                            case "9":
                                item["DrawingCodeName"].BackColor = Color.Red;
                                item["DrawingCodeName"].BorderColor = Color.Red;
                                break;
                            case "10":
                                item["DrawinDetailCodeName"].BackColor = Color.Red;
                                item["DrawinDetailCodeName"].BorderColor = Color.Red;
                                break;

                        }
                    }
                }
            }
        }

        protected void grdDocumentFile_OnDeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var objId = new Guid(item.GetDataKeyValue("ID").ToString());
            var obj = this.contractorTransmittalDocFileService.GetById(objId);
            var transObj = this.transmittalService.GetById(obj.TransId.GetValueOrDefault());

            var physicalPath = Server.MapPath("../.." + obj.FilePath);
            if (File.Exists(physicalPath))
            {
                File.Delete(physicalPath);
            }

            this.contractorTransmittalDocFileService.Delete(objId);

            var currentTransAttachDocFile = this.contractorTransmittalDocFileService.GetAllByTrans(transObj.ID);
            if (!currentTransAttachDocFile.Any())
            {
                transObj.Status = "Missing Doc File";
                transObj.IsValid = false;
                transObj.ErrorMessage = "Missing Attach Document File.";
            }
            else if (currentTransAttachDocFile.Any(t => !string.IsNullOrEmpty(t.Status)))
            {
                transObj.Status = "Attach Doc File Invalid";
                transObj.ErrorMessage = "Some attach document files are invalid format.";
                transObj.IsValid = false;
            }
            else
            {
                transObj.Status = string.Empty;
                transObj.ErrorMessage = string.Empty;
                transObj.IsValid = true;
            }

            this.transmittalService.Update(transObj);
            this.grdDocumentFile.Rebind();
        }

        protected void ajaxDocument_OnAjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "DeleteAll")
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    var transId = new Guid(this.Request.QueryString["objId"]);
                    var transObj = this.transmittalService.GetById(transId);

                    var errorAttachDocFile = this.contractorTransmittalDocFileService.GetAllByTrans(transId).Where(t => !string.IsNullOrEmpty(t.ErrorMessage));
                    foreach (var errorItem in errorAttachDocFile)
                    {
                        var physicalPath = Server.MapPath("../.." + errorItem.FilePath);
                        if (File.Exists(physicalPath))
                        {
                            File.Delete(physicalPath);
                        }

                        this.contractorTransmittalDocFileService.Delete(errorItem);
                    }

                    // Update Trans status info
                    var currentTransAttachDocFile = this.contractorTransmittalDocFileService.GetAllByTrans(transObj.ID);
                    if (!currentTransAttachDocFile.Any())
                    {
                        transObj.Status = "Missing Doc File";
                        transObj.IsValid = false;
                        transObj.ErrorMessage = "Missing Attach Document File.";
                    }
                    else if (currentTransAttachDocFile.Any(t => !string.IsNullOrEmpty(t.Status)))
                    {
                        transObj.Status = "Attach Doc File Invalid";
                        transObj.ErrorMessage = "Some attach document files are invalid format.";
                        transObj.IsValid = false;
                    }
                    else
                    {
                        transObj.Status = string.Empty;
                        transObj.ErrorMessage = string.Empty;
                        transObj.IsValid = true;
                    }

                    this.transmittalService.Update(transObj);
                    // ----------------------------------------------------------------------------------------------------------

                    this.grdDocumentFile.Rebind();
                }
            }
        }
    }
}