﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DocumentPackageSendTo.aspx.cs" Inherits="EDMs.Web.Controls.Document.DocumentPackageSendTo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="~/Content/styles.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        html, body, form {
            overflow: hidden;
        }

        .RadComboBoxDropDown_Default .rcbHovered {
            background-color: #46A3D3;
            color: #fff;
        }

        .RadComboBoxDropDown .rcbItem, .RadComboBoxDropDown .rcbHovered, .RadComboBoxDropDown .rcbDisabled, .RadComboBoxDropDown .rcbLoading, .RadComboBoxDropDown .rcbCheckAllItems, .RadComboBoxDropDown .rcbCheckAllItemsHovered {
            margin: 0 0px;
        }
        /*.RadComboBox .rcbInputCell .rcbInput{
            border-left-color:#46A3D3 !important;
            border-color: #8E8E8E #B8B8B8 #B8B8B8 #46A3D3;
            border-style: solid;
            border-width: 1px 1px 1px 5px;
            color: #000000;
            float: left;
            font: 12px "segoe ui";
            margin: 0;
            padding: 2px 5px 3px;
            vertical-align: middle;
            width: 283px;
           }*/
        .RadComboBox table td.rcbInputCell, .RadComboBox .rcbInputCell .rcbInput {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }

        div.rgEditForm label {
            float: right;
            text-align: right;
            width: 72px;
        }

        .rgEditForm {
            text-align: right;
        }

        .RadComboBox {
            width: 115px !important;
            border-bottom: none !important;
        }

        .RadUpload .ruFileWrap {
            overflow: visible !important;
        }

        .demo-container.size-narrow {
            max-width: 500px;
            display: inline-block;
            text-align: left;
            background-color: #FFFFDB;
            padding-left: 5px;
        }

        .demo-container .RadUpload .ruUploadProgress {
            width: 300px;
            display: inline-block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            vertical-align: top;
        }

        html .demo-container .ruFakeInput {
            width: 300px;
        }

        .accordion dt a {
            letter-spacing: 0.1em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        .accordion dt span {
            color: #085B8F;
            border-bottom: 1px solid #46A3D3;
            font-size: 1.0em;
            font-weight: bold;
            letter-spacing: 0.1em;
            line-height: 1.2;
            margin: 0.5em auto 0.6em;
            padding: 0;
            text-align: left;
            text-decoration: none;
            display: block;
        }

        #grdDocument_GridData {
            height: 100%;
        }

        #grdDocumentPanel {
            height: 100%;
        }

        #grdAttachCRSFilePanel {
            height: 95% !important;
        }

        #Panel1 {
            display: initial !important;
        }

        .RadGrid .rgSelectedRow {
            background-image: none !important;
            background-color: darkseagreen !important;
        }

        .modal {
            /*position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 10000;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;*/
            position: fixed;
            left: 0;
            top: 0;
            z-index: 10000;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            opacity: 0;
            /*visibility: hidden;*/
            transform: scale(1.1);
            transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;
        }

        .loading {
            font-family: Arial;
            font-size: 10pt;
            border: 1px solid #67CFF5;
            width: 443px;
            height: 296px;
            display: none;
            position: fixed;
            background-color: White;
            z-index: 10001;
        }
    </style>

    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshOutgoingGrid(args);
            GetRadWindow().close();
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }


    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>

        <div style="width: 100%; height: 100vh;" runat="server" id="divContent">

            <telerik:RadSplitter ID="RadSplitter4" runat="server" Orientation="Vertical" Width="100%" Height="100%" Skin="Windows7">
                <telerik:RadPane ID="RadPane1" runat="server" Scrollable="false" Scrolling="None" Height="100%" Skin="Windows7">
                    <asp:Panel ID="Panel1" runat="server" Width="100%" Height="100%">

                        <div id="DivIncoming" runat="server" style="height: 90%;">

                            <telerik:RadGrid Skin="Windows7" AllowCustomPaging="False" AllowPaging="true" AllowSorting="True"
                                AutoGenerateColumns="False" CellPadding="0" CellSpacing="0" GridLines="None"
                                Height="99%" ID="grdDocument" AllowFilteringByColumn="False" AllowMultiRowSelection="true"
                                OnNeedDataSource="grdDocumentFile_OnNeedDataSource"
                                PageSize="100" runat="server" Style="outline: none" Width="100%">
                                <SortingSettings SortedBackColor="#FFF6D6"></SortingSettings>
                                <GroupingSettings CaseSensitive="False"></GroupingSettings>
                                <MasterTableView AllowMultiColumnSorting="false"
                                    ClientDataKeyNames="ID" DataKeyNames="ID" Font-Size="8pt">
                                    <GroupByExpressions>
                                        <telerik:GridGroupByExpression>
                                            <SelectFields>
                                                <telerik:GridGroupByField FieldAlias="-" FieldName="CategoryName" FormatString="{0:D}"
                                                    HeaderValueSeparator=""></telerik:GridGroupByField>
                                            </SelectFields>
                                            <GroupByFields>
                                                <telerik:GridGroupByField FieldName="CategoryID" SortOrder="Ascending"></telerik:GridGroupByField>
                                            </GroupByFields>
                                        </telerik:GridGroupByExpression>
                                    </GroupByExpressions>
                                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="First page" LastPageToolTip="Last page" NextPagesToolTip="Next page" NextPageToolTip="Next page" PagerTextFormat="Change page: {4} &amp;nbsp;Page &lt;strong&gt;{0}&lt;/strong&gt; / &lt;strong&gt;{1}&lt;/strong&gt;, Total:  &lt;strong&gt;{5}&lt;/strong&gt; Documents." PageSizeLabelText="Row/page: " PrevPagesToolTip="Previous page" PrevPageToolTip="Previous page" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ColumnGroups>
                                        <telerik:GridColumnGroup HeaderText="Markup" Name="markup"
                                            HeaderStyle-HorizontalAlign="Center" />
                                        <%--  <telerik:GridColumnGroup HeaderText="First Issue Info" Name="FirstIssueInfo"
                                    HeaderStyle-HorizontalAlign="Center"/>
                            <telerik:GridColumnGroup HeaderText="Final Issue Info" Name="FinalIssueInfo"
                                    HeaderStyle-HorizontalAlign="Center"/>
                            <telerik:GridColumnGroup HeaderText="INCOMING TRANSMITTAL" Name="IncomingTrans"
                                    HeaderStyle-HorizontalAlign="Center"/>
                            <telerik:GridColumnGroup HeaderText="ICA REVIEW DETAILS" Name="ICAReviews"
                                    HeaderStyle-HorizontalAlign="Center"/>--%>
                                        <telerik:GridColumnGroup HeaderText="Incoming Transmittal" Name="ReceivedInfo"
                                            HeaderStyle-HorizontalAlign="Center" />

                                        <telerik:GridColumnGroup HeaderText="Outgoing Transmittal" Name="OutTrans" HeaderStyle-HorizontalAlign="Center"></telerik:GridColumnGroup>
                                    </ColumnGroups>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="ID" UniqueName="ID" Visible="False" />
                                        <telerik:GridClientSelectColumn UniqueName="IsSelected">
                                            <HeaderStyle Width="25" />
                                            <ItemStyle HorizontalAlign="Center" Width="25" />
                                        </telerik:GridClientSelectColumn>
                                        <telerik:GridTemplateColumn HeaderText="No." Groupable="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="2%" VerticalAlign="Middle"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Width="2%"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label ID="lblSoTT" runat="server" Text='<%# grdDocument.CurrentPageIndex * grdDocument.PageSize + grdDocument.Items.Count+1 %>'>
                                                </asp:Label>

                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--<telerik:GridEditCommandColumn ButtonType="ImageButton" EditImageUrl="~/Images/edit.png" 
                                    UpdateImageUrl="~/Images/ok.png" CancelImageUrl="~/Images/delete.png" UniqueName="EditColumn">
                                    <HeaderStyle HorizontalAlign="Center" Width="2%"  />
                                    <ItemStyle HorizontalAlign="Center" Width="2%"/>
                                </telerik:GridEditCommandColumn>--%>
                                        <%--<telerik:GridButtonColumn UniqueName="DeleteColumn" CommandName="Delete" HeaderTooltip="Delete document"
                                    ConfirmText="Do you want to delete document?" ButtonType="ImageButton" ImageUrl="~/Images/delete.png">
                                    <HeaderStyle Width="1%" />
                                        <ItemStyle HorizontalAlign="Center" Width="1%"  />
                                </telerik:GridButtonColumn>
                                        --%>
                                        <telerik:GridTemplateColumn HeaderText="DOC. No." UniqueName="DocNo">
                                            <HeaderStyle HorizontalAlign="Center" Width="160" />
                                            <ItemStyle HorizontalAlign="Left" />
                                            <ItemTemplate>
                                                <%# Eval("DocNo") %>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:HiddenField ID="DocNo" runat="server" Value='<%# Eval("DocNo") %>' />
                                                <asp:Label runat="server" ID="lbldocNo"></asp:Label>
                                                <%--<asp:TextBox ID="txtDocNo" runat="server" Width="100%"></asp:TextBox>--%>
                                            </EditItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="DOC. Title" UniqueName="DocTitle"
                                            DataField="DocTitle" ShowFilterIcon="False" FilterControlWidth="97%"
                                            AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                            <HeaderStyle HorizontalAlign="Center" Width="220" />
                                            <ItemStyle HorizontalAlign="Left" Width="220" />
                                            <ItemTemplate>
                                                <%# Eval("DocTitle") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="Rev." UniqueName="Rev"
                                            AllowFiltering="false">
                                            <HeaderStyle HorizontalAlign="Center" Width="50" />
                                            <ItemStyle HorizontalAlign="Center" Width="50" />
                                            <ItemTemplate>
                                                <%# Eval("RevisionName") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="KDN" UniqueName="KDN_markup"
                                            AllowFiltering="false" Display="true" ColumnGroupName="markup">
                                            <HeaderStyle HorizontalAlign="Center" Width="40" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("KDN_markup") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="DNV" UniqueName="DNV_markup"
                                            AllowFiltering="false" Display="true" ColumnGroupName="markup">
                                            <HeaderStyle HorizontalAlign="Center" Width="40" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("DNV_markup") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="NCS" UniqueName="NCS_markup"
                                            AllowFiltering="false" Display="true" ColumnGroupName="markup">
                                            <HeaderStyle HorizontalAlign="Center" Width="40" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("NCS_markup") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridDateTimeColumn HeaderText="Received Date" UniqueName="IncomingTransDate" DataField="IncomingTransDate"
                                            DataFormatString="{0:dd/MM/yyyy}" SortExpression="StartDate" AllowFiltering="false" PickerType="DatePicker" ColumnGroupName="ReceivedInfo">
                                            <HeaderStyle HorizontalAlign="Center" Width="70" />
                                            <ItemStyle HorizontalAlign="Center" Width="70" />
                                        </telerik:GridDateTimeColumn>

                                        <telerik:GridTemplateColumn HeaderText="Trans No." UniqueName="IncomingTransNo"
                                            AllowFiltering="false" ColumnGroupName="ReceivedInfo">
                                            <HeaderStyle HorizontalAlign="Center" Width="150" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("IncomingTransNo") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                        <telerik:GridTemplateColumn HeaderText="Date" UniqueName="OutgoingTransDate" ColumnGroupName="OutTrans" AllowFiltering="False">
                                            <HeaderStyle HorizontalAlign="Center" Width="70" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("OutgoingTransDate","{0:dd/MM/yyyy}") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Trans No." UniqueName="OutgoingTransNo"
                                            AllowFiltering="false" ColumnGroupName="OutTrans">
                                            <HeaderStyle HorizontalAlign="Center" Width="120" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("OutgoingTransNo") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Code" UniqueName="DocReviewStatusCode" ColumnGroupName="OutTrans"
                                            AllowFiltering="false" Display="true">
                                            <HeaderStyle HorizontalAlign="Center" Width="50" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("DocReviewStatusCode") %>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>

                                    </Columns>
                                </MasterTableView>
                                <ClientSettings Selecting-AllowRowSelect="true" AllowColumnHide="True">
                                    <Scrolling AllowScroll="True" SaveScrollPosition="True" ScrollHeight="500" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <div style="width: 120px">
                                <telerik:RadButton ID="btnDownLoad" runat="server" Text="Export" Width="80px" OnClientClicked="ClickButton" Style="text-align: center">
                                    <Icon PrimaryIconUrl="../../Images/export.png" PrimaryIconLeft="4" PrimaryIconTop="4" PrimaryIconWidth="16" PrimaryIconHeight="16"></Icon>
                                </telerik:RadButton>

                            </div>

                        </div>
                    </asp:Panel>
                </telerik:RadPane>
            </telerik:RadSplitter>

        </div>
        <div class="loading" align="center" id="loader">
            Please wait.
                <br />
            <img id="abc" src="../../Images/Loading_icon.gif" alt="" />
        </div>

        <asp:HiddenField runat="server" ID="docUploadedIsExist" />
        <asp:HiddenField runat="server" ID="docIdUpdateUnIsLeaf" />
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Windows7" />

        <telerik:RadAjaxManager runat="Server" ID="ajaxDocument" OnAjaxRequest="ajaxDocument_OnAjaxRequest">
            <ClientEvents OnRequestStart="onRequestStart" OnResponseEnd="OnResponseEnd" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ajaxDocument">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="grdAttachCRSFile" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="btnSave">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="divContent" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="docuploader" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ddlToList">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtTransNo" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <%--<telerik:AjaxSetting AjaxControlID="btnDownLoad">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>--%>

                <telerik:AjaxSetting AjaxControlID="ActionMenu">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="ddlFromList">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="txtTransNo" />
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <telerik:AjaxSetting AjaxControlID="btnProcessDocNo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="grdDocument" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="radUploadDoc" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>

        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" EnableShadow="true" Skin="Windows7">
            <Windows>
                <telerik:RadWindow ID="CustomerDialog" runat="server" Title="Transmittal Document File Information"
                    VisibleStatusbar="false" Height="600" Width="650" IconUrl="~/Images/attach.png"
                    Left="150px" ReloadOnShow="true" ShowContentDuringLoad="false" Modal="true">
                </telerik:RadWindow>
            </Windows>
        </telerik:RadWindowManager>

        <telerik:RadScriptBlock runat="server">
            <script type="text/javascript">
                var ajaxManager;
                function pageLoad() {
                    ajaxManager = $find("<%=ajaxDocument.ClientID %>");
                }
                function onRequestStart(sender, args) {
                    //alert(args.get_eventTarget());
                    if (args.get_eventTarget().indexOf("btnDownloadPackage") >= 0 || args.get_eventTarget().indexOf("ajaxDocument") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                function DeleteFile(id) {
                    if (confirm("Do you want to delete item?") == false) return;
                    ajaxManager.ajaxRequest("DeleteFile_" + id);
                }
                function ShowEditForm(objId) {
                    var owd = $find("<%=CustomerDialog.ClientID %>");
                    owd.setSize(730, document.documentElement.offsetHeight);
                    owd.Show();
                    owd.setUrl("ContractorTransmittalAttachDocFileEditForm.aspx?objId=" + objId, "CustomerDialog");
                }
                function deleteCookie() {
                    var cook = getCookie('ExcelDownloadFlag');
                    if (cook != "") {
                        document.cookie = "ExcelDownloadFlag=; Path = /; expires=Thu, 01 Jan 1970 00:00:00 UTC";
                    }
                }
                function OnResponseEnd() {
                    var masterTable = $find("<%=grdDocument.ClientID%>").get_masterTableView();
                    masterTable.rebind();
                }
                function IsCookieValid() {
                    var cook = getCookie('ExcelDownloadFlag');
                    return cook != '';
                }

                function getCookie(cname) {
                    var name = cname + "=";
                    var ca = document.cookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) == 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                    return "";
                }
                function ClickButton() {
                    ShowHideProgress();
                    ajaxManager.ajaxRequest("DownloadFolder");
                }
                function ShowHideProgress() {
                    setTimeout(function () {

                        var modal = $('<div />');
                        modal.addClass("modal");
                        $('body').append(modal);
                        var loading = $(".loading");
                        loading.show();
                        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                        loading.css({ top: top, left: left });
                    }, 200);
                    deleteCookie();

                    var timeInterval = 500; // milliseconds (checks the cookie for every half second )

                    var loop = setInterval(function () {
                        if (IsCookieValid()) {

                            $("div.modal").hide();

                            var loading = $(".loading");
                            loading.hide();
                            clearInterval(loop)
                        }

                    }, timeInterval);
                }
            </script>
        </telerik:RadScriptBlock>
    </form>
</body>
</html>
