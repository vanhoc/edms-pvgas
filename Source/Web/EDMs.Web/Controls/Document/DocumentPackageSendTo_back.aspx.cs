﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.IO;
    using System.Data;
    using System.Linq;
    using System.ServiceProcess;
    using System.Web.Hosting;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;
    using Telerik.Web.Zip;
    using Aspose.Cells;
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageSendTo_back : Page
    {



        private readonly DocumentPackageService documentPackageService;

        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly PVGASDocumentAttachFileService attachFilePackageService;


        /// <summary>
        /// Initializes a new instance of the <see cref="RevisionHistory"/> class.
        /// </summary>
        public DocumentPackageSendTo_back()
        {

            this.documentPackageService = new DocumentPackageService();

            this.attachFilePackageService = new PVGASDocumentAttachFileService();

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!this.IsPostBack)
            {
            }
        }
        /// <summary>
        /// The rad grid 1_ on need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_OnNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (Request.QueryString["Type"] != null)
            {
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
                var lbPakageId = Convert.ToInt32(Request.QueryString["lbPakageId"]);
                switch (Request.QueryString["Type"])
                {
                    case "1":
                        this.grdDocument.DataSource = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                        && t.PackageId.GetValueOrDefault()== lbPakageId
            && !string.IsNullOrEmpty(t.KDN_markup)
            && (t.KDN_markup == "I" || t.KDN_markup == "R" || t.KDN_markup == "A")
            && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
            && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
            && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();
                        // this.grdDocument.DataSource = docList;
                        break;
                    case "2":
                        this.grdDocument.DataSource = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                         && t.PackageId.GetValueOrDefault() == lbPakageId
           && !string.IsNullOrEmpty(t.NCS_markup)
           && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")
           && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
           && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
            && string.IsNullOrEmpty(t.NCS_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();

                        break;

                }
                //var docId = new Guid(Request.QueryString["docId"]);
                //var objDoc = this.documentPackageService.GetById(docId);
                //if (objDoc != null)
                //{
                //    var revList = this.documentPackageService.GetAllRevDoc(objDoc.ParentId == null ? objDoc.ID : objDoc.ParentId.Value);
                //    this.grdDocument.DataSource = revList;
                //}
                //else
                //{
                //    this.grdDocument.DataSource = new List<DocumentPackage>(); 
                //}
            }
        }


        /// <summary>
        /// Grid KhacHang item created
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void grdDocument_ItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var editLink = (Image)e.Item.FindControl("uploadLink");
                editLink.Attributes["href"] = "#";
                editLink.Attributes["onclick"] = string.Format(
                    "return ShowUploadForm('{0}');",
                    e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ID"]);
            }
        }

        /// <summary>
        /// RadAjaxManager1  AjaxRequest
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument == "Rebind")
            {
                grdDocument.MasterTableView.SortExpressions.Clear();
                grdDocument.MasterTableView.GroupByExpressions.Clear();
                grdDocument.Rebind();
            }
        }





        private bool DownloadByWriteByte(string strFileName, string strDownloadName, bool DeleteOriginalFile)
        {
            try
            {
                //Kiem tra file co ton tai hay chua
                if (!File.Exists(strFileName))
                {
                    return false;
                }
                //Mo file de doc
                FileStream fs = new FileStream(strFileName, FileMode.Open);
                int streamLength = Convert.ToInt32(fs.Length);
                byte[] data = new byte[streamLength + 1];
                fs.Read(data, 0, data.Length);
                fs.Close();

                Response.Clear();
                Response.ClearHeaders();
                Response.AddHeader("Content-Type", "Application/octet-stream");
                Response.AddHeader("Content-Length", data.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + strDownloadName);
                Response.BinaryWrite(data);
                if (DeleteOriginalFile)
                {
                    File.SetAttributes(strFileName, FileAttributes.Normal);
                    File.Delete(strFileName);
                }

                Response.Flush();

                Response.End();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private void DocumentSendKDNOperator()
        {
            var filePath = Server.MapPath("Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGASTransmittalTemplate_KDN_Operator.xlsm");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            List<DocumentPackage> docList= new List<DocumentPackage>();
            if (this.grdDocument.SelectedItems.Count > 0)
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = new Guid(selectedItem.GetDataKeyValue("ID").ToString());
                    var docObj = this.documentPackageService.GetById(docId);
                    docList.Add(docObj);
                }
                }
            else
            {
                var lbPakageId = Convert.ToInt32(Request.QueryString["lbPakageId"]);
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
            docList = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
             && t.PackageId.GetValueOrDefault() == lbPakageId
             && !string.IsNullOrEmpty(t.KDN_markup)
             && (t.KDN_markup == "I" || t.KDN_markup == "R" || t.KDN_markup == "A")
             && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
             && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
             && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID.GetValueOrDefault()).ToList();
            }
           

            dataSheet.Cells["C5"].PutValue(DateTime.Now.Date);
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                     new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String))});
            var countDoc = 0;

            foreach (var docobj in docList)
            {
                countDoc += 1;

                var dataRow = dtEngDoc.NewRow();
                dataRow["Index"] = countDoc;
                dataRow["DocTitle"] = docobj.DocTitle;
                dataRow["DocNo"] = docobj.DocNo;
                dataRow["RevisionName"] = docobj.RevisionName;


                dtEngDoc.Rows.Add(dataRow);

            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 20, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
            for (int i = 0; i < dtEngDoc.Rows.Count; i++)
            {
                dataSheet.Cells.Merge(20 + i, 1, 1, 2);
                dataSheet.Cells.Merge(20 + i, 3, 1, 2);

            }
            var filename = "PVGAS_Documents be Send To KDN _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            this.Download_File(serverTotalDocPackPath);
        }

        private void Download_File(string FilePath)
        {
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();
        }
        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["Type"] != null)
            {
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
                switch (Request.QueryString["Type"])
                {
                    case "1":
                     
                        break;
                    case "2":
                        this.grdDocument.DataSource = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
           && !string.IsNullOrEmpty(t.NCS_markup)
           && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")
           && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
           && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
            && string.IsNullOrEmpty(t.NCS_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();

                        break;

                }
            }
        }
    }
}