﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Linq;
using Telerik.Web.UI;
using EDMs.Web.Utilities;
namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web;
    using System.Data;
    using System.Web.UI;
    using Business.Services.Document;
    using Business.Services.Library;
    using Business.Services.Security;
    using Data.Entities;
    using System.Text.RegularExpressions;
    using System.Collections.Generic;
    using EDMs.Web.Utilities.Sessions;
    using Aspose.Cells;
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DocumentPackageSendTo : Page
    {

        private readonly DocumentPackageService documentPackageService;


        private readonly PVGASDocumentAttachFileService attachFilePackageService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DocumentPackageSendTo()
        {
            this.documentPackageService = new DocumentPackageService();

            this.attachFilePackageService = new PVGASDocumentAttachFileService();

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
               
            }
        }

        protected void grdDocumentFile_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (Request.QueryString["Type"] != null)
            {
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
                var lbPakageId = Convert.ToInt32(Request.QueryString["lbPakageId"]);
                switch (Request.QueryString["Type"])
                {
                    case "1":
                        this.grdDocument.DataSource = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                        && t.PackageId.GetValueOrDefault() == lbPakageId
            && !string.IsNullOrEmpty(t.KDN_markup)
            && (t.KDN_markup == "I" || t.KDN_markup == "R" || t.KDN_markup == "A")
            && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
            && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
            && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();
                        // this.grdDocument.DataSource = docList;
                        break;
                    case "2":
                        this.grdDocument.DataSource = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                         && t.PackageId.GetValueOrDefault() == lbPakageId
           && !string.IsNullOrEmpty(t.NCS_markup)
           && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")
           && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
           && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
            && string.IsNullOrEmpty(t.NCS_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();

                        break;
                    case "3":
                        this.grdDocument.DataSource = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                       && t.PackageId.GetValueOrDefault() == lbPakageId
           && !string.IsNullOrEmpty(t.KDN_markup)
           && (t.KDN_markup == "I" || t.KDN_markup == "R" || t.KDN_markup == "A")
           && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
           && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))

           && (((t.KDN_markup == "R" || t.KDN_markup == "A") && t.KDN_Operator_FinalCode == "C") || (t.KDN_markup != "R" && t.KDN_markup != "A" && string.IsNullOrEmpty(t.KDN_Operator_FinalCode)))
           && (((t.NCS_markup == "R" || t.NCS_markup == "A") && t.NCS_Operator_FinalCode == "C") || (t.NCS_markup != "R" && t.NCS_markup != "A" && string.IsNullOrEmpty(t.NCS_Operator_FinalCode)))
           && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();
                        break;
                    case "4":
                        this.grdDocument.DataSource = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                      && t.PackageId.GetValueOrDefault() == lbPakageId
          && !string.IsNullOrEmpty(t.NCS_markup)
           && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")
          && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
          && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))

          && (((t.KDN_markup == "R" || t.KDN_markup == "A") && t.KDN_Operator_FinalCode == "C") || (t.KDN_markup != "R" && t.KDN_markup != "A" && string.IsNullOrEmpty(t.KDN_Operator_FinalCode)))
          && (((t.NCS_markup == "R" || t.NCS_markup == "A") && t.NCS_Operator_FinalCode == "C") || (t.NCS_markup != "R" && t.NCS_markup != "A" && string.IsNullOrEmpty(t.NCS_Operator_FinalCode)))
          && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();
                        break;

                }


            }
        }
        private void DocumentSendNCSROSNEFTPERENCO()
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGASTransmittalTemplate_NCS_Operator.xlsm");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            List<DocumentPackage> docList = new List<DocumentPackage>();
            if (this.grdDocument.SelectedItems.Count > 0)
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = new Guid(selectedItem.GetDataKeyValue("ID").ToString());
                    var docObj = this.documentPackageService.GetById(docId);
                    docList.Add(docObj);
                    selectedItem.Selected = false;
                }
            }
            else
            {
                var lbPakageId = Convert.ToInt32(Request.QueryString["lbPakageId"]);
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
                 docList = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                 && !string.IsNullOrEmpty(t.NCS_markup)
                   && t.PackageId.GetValueOrDefault() == lbPakageId
                 && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")
                 && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
                 && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
                  && string.IsNullOrEmpty(t.NCS_Operator_OutgoingTransNo)).OrderByDescending(t => t.CreatedDate.GetValueOrDefault()).ToList();
            }
           // dataSheet.Cells["C5"].PutValue(DateTime.Now.Date);
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                     new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String))});
            var countDoc = 0;

            foreach (var docobj in docList)
            {
                countDoc += 1;

                var dataRow = dtEngDoc.NewRow();
                dataRow["Index"] = countDoc;
                dataRow["DocTitle"] = docobj.DocTitle;
                dataRow["DocNo"] = docobj.DocNo;
                dataRow["RevisionName"] = docobj.RevisionName;


                dtEngDoc.Rows.Add(dataRow);

            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 20, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
            for (int i = 0; i < dtEngDoc.Rows.Count; i++)
            {
                dataSheet.Cells.Merge(20 + i, 1, 1, 2);
                dataSheet.Cells.Merge(20 + i, 3, 1, 2);

            }
            var filename = "PVGAS_Documents be Send To NCS ROSNEFT PERENCO _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentSendNCSOwner()
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGASTransmittalTemplate_NCS_Owner.xlsm");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            List<DocumentPackage> docList = new List<DocumentPackage>();
            if (this.grdDocument.SelectedItems.Count > 0)
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = new Guid(selectedItem.GetDataKeyValue("ID").ToString());
                    var docObj = this.documentPackageService.GetById(docId);
                    docList.Add(docObj);
                    selectedItem.Selected = false;
                }
            }
            else
            {
                var lbPakageId = Convert.ToInt32(Request.QueryString["lbPakageId"]);
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
                docList = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                      && t.PackageId.GetValueOrDefault() == lbPakageId
          && !string.IsNullOrEmpty(t.NCS_markup)
           && (t.NCS_markup == "I" || t.NCS_markup == "R" || t.NCS_markup == "A")
          && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
          && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))

          && (((t.KDN_markup == "R" || t.KDN_markup == "A") && t.KDN_Operator_FinalCode == "C") || (t.KDN_markup != "R" && t.KDN_markup != "A" && string.IsNullOrEmpty(t.KDN_Operator_FinalCode)))
          && (((t.NCS_markup == "R" || t.NCS_markup == "A") && t.NCS_Operator_FinalCode == "C") || (t.NCS_markup != "R" && t.NCS_markup != "A" && string.IsNullOrEmpty(t.NCS_Operator_FinalCode)))
          && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();
            }
            // dataSheet.Cells["C5"].PutValue(DateTime.Now.Date);
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                     new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String))});
            var countDoc = 0;

            foreach (var docobj in docList)
            {
                countDoc += 1;

                var dataRow = dtEngDoc.NewRow();
                dataRow["Index"] = countDoc;
                dataRow["DocTitle"] = docobj.DocTitle;
                dataRow["DocNo"] = docobj.DocNo;
                dataRow["RevisionName"] = docobj.RevisionName;


                dtEngDoc.Rows.Add(dataRow);

            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 20, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
            for (int i = 0; i < dtEngDoc.Rows.Count; i++)
            {
                dataSheet.Cells.Merge(20 + i, 1, 1, 2);
                dataSheet.Cells.Merge(20 + i, 3, 1, 2);

            }
            var filename = "PVGAS_Documents be Send To NCS ROSNEFT PERENCO _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            Response.ClearContent();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentSendKDNOperator()
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGASTransmittalTemplate_KDN_Operator.xlsm");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            List<DocumentPackage> docList = new List<DocumentPackage>();
            if (this.grdDocument.SelectedItems.Count > 0)
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = new Guid(selectedItem.GetDataKeyValue("ID").ToString());
                    var docObj = this.documentPackageService.GetById(docId);
                    docList.Add(docObj);
                    selectedItem.Selected = false;
                }
            }
            else
            {
                var lbPakageId = Convert.ToInt32(Request.QueryString["lbPakageId"]);
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
                docList = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                 && t.PackageId.GetValueOrDefault() == lbPakageId
                 && !string.IsNullOrEmpty(t.KDN_markup)
                 && (t.KDN_markup == "I" || t.KDN_markup == "R" || t.KDN_markup == "A")
                 && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
                 && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))
                 && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID.GetValueOrDefault()).ToList();
            }


            dataSheet.Cells["C5"].PutValue(DateTime.Now.Date);
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                     new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String))});
            var countDoc = 0;

            foreach (var docobj in docList)
            {
                countDoc += 1;

                var dataRow = dtEngDoc.NewRow();
                dataRow["Index"] = countDoc;
                dataRow["DocTitle"] = docobj.DocTitle;
                dataRow["DocNo"] = docobj.DocNo;
                dataRow["RevisionName"] = docobj.RevisionName;


                dtEngDoc.Rows.Add(dataRow);

            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 20, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
            for (int i = 0; i < dtEngDoc.Rows.Count; i++)
            {
                dataSheet.Cells.Merge(20 + i, 1, 1, 2);
                dataSheet.Cells.Merge(20 + i, 3, 1, 2);

            }
            var filename = "PVGAS_Documents be Send To KDN _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            //this.Download_File(serverTotalDocPackPath);
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }
        private void DocumentSendKDNOwner()
        {
            var filePath = Server.MapPath("~/Exports") + @"\";
            var workbook = new Workbook();
            workbook.Open(filePath + @"Template\PVGASTransmittalTemplate_KDN_Owner.xlsm");
            var sheets = workbook.Worksheets;
            var dataSheet = sheets[0];
            List<DocumentPackage> docList = new List<DocumentPackage>();
            if (this.grdDocument.SelectedItems.Count > 0)
            {
                foreach (GridDataItem selectedItem in this.grdDocument.SelectedItems)
                {
                    var docId = new Guid(selectedItem.GetDataKeyValue("ID").ToString());
                    var docObj = this.documentPackageService.GetById(docId);
                    docList.Add(docObj);
                    selectedItem.Selected = false;
                }
            }
            else
            {
                var lbPakageId = Convert.ToInt32(Request.QueryString["lbPakageId"]);
                var projectId = Convert.ToInt32(Request.QueryString["projectId"]);
                docList = this.documentPackageService.GetAllByProject(projectId, false).Where(t => t.IsEMDR.GetValueOrDefault()
                      && t.PackageId.GetValueOrDefault() == lbPakageId
          && !string.IsNullOrEmpty(t.KDN_markup)
          && (t.KDN_markup == "I" || t.KDN_markup == "R" || t.KDN_markup == "A")
          && (t.DNB_FinalCode == "Code 1" || t.DNB_FinalCode == "Code 4" || t.DNB_FinalCode == "Code 1*")
          && ((t.DNV_markup == "R" && t.DNV_FinalCode == "C") || (t.DNV_markup != "R" && string.IsNullOrEmpty(t.DNV_FinalCode)))

          && (((t.KDN_markup == "R" || t.KDN_markup == "A") && t.KDN_Operator_FinalCode == "C") || (t.KDN_markup != "R" && t.KDN_markup != "A" && string.IsNullOrEmpty(t.KDN_Operator_FinalCode)))
          && (((t.NCS_markup == "R" || t.NCS_markup == "A") && t.NCS_Operator_FinalCode == "C") || (t.NCS_markup != "R" && t.NCS_markup != "A" && string.IsNullOrEmpty(t.NCS_Operator_FinalCode)))
          && string.IsNullOrEmpty(t.KDN_Operator_OutgoingTransNo)).OrderBy(t => t.CategoryID).ToList();
            }


            dataSheet.Cells["C5"].PutValue(DateTime.Now.Date);
            var dtEngDoc = new DataTable();
            dtEngDoc.Columns.AddRange(new[]
                   {
                     new DataColumn("Index", typeof(String)),
                    new DataColumn("DocTitle", typeof(String)),
                    new DataColumn("Empty1", typeof(String)),
                    new DataColumn("DocNo", typeof(String)),
                    new DataColumn("Empty2", typeof(String)),
                    new DataColumn("RevisionName", typeof(String)),
                    new DataColumn("Empty3", typeof(String)),
                    new DataColumn("Empty4", typeof(String))});
            var countDoc = 0;

            foreach (var docobj in docList)
            {
                countDoc += 1;

                var dataRow = dtEngDoc.NewRow();
                dataRow["Index"] = countDoc;
                dataRow["DocTitle"] = docobj.DocTitle;
                dataRow["DocNo"] = docobj.DocNo;
                dataRow["RevisionName"] = docobj.RevisionName;


                dtEngDoc.Rows.Add(dataRow);

            }
            dataSheet.Cells.ImportDataTable(dtEngDoc, false, 20, 0, dtEngDoc.Rows.Count, dtEngDoc.Columns.Count, true);
            for (int i = 0; i < dtEngDoc.Rows.Count; i++)
            {
                dataSheet.Cells.Merge(20 + i, 1, 1, 2);
                dataSheet.Cells.Merge(20 + i, 3, 1, 2);

            }
            var filename = "PVGAS_Documents be Send ToBHP Owner _" + DateTime.Now.ToString("ddMMyyyy") + ".xlsm";

            var serverTotalDocPackPath = Server.MapPath("~/Exports/DocPack/" + filename);
            workbook.Save(serverTotalDocPackPath);
            //this.Download_File(serverTotalDocPackPath);
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ";");
            Response.TransmitFile(serverTotalDocPackPath);

            HttpCookie cookie = new HttpCookie("ExcelDownloadFlag");
            cookie.Value = "Flag";
            cookie.Expires = DateTime.Now.AddDays(1);
            Response.AppendCookie(cookie);

            Response.Flush();
            System.IO.File.Delete(serverTotalDocPackPath);
            Response.End();
        }

        protected void ajaxDocument_OnAjaxRequest(object sender, AjaxRequestEventArgs e)
        {
         
             if(e.Argument.Contains("DownloadFolder"))
            {
                if (Request.QueryString["Type"] != null)
                {
                    switch (Request.QueryString["Type"])
                    {
                        case "1":
                            this.DocumentSendKDNOperator();
                            break;
                        case "2":
                            this.DocumentSendNCSROSNEFTPERENCO();
                            break;
                        case "3":
                            this.DocumentSendKDNOwner();
                            break;
                        case "4":
                           this.DocumentSendNCSOwner();
                            break;
                    }
                }
            }

            this.grdDocument.Rebind();
        }

       
      
        protected void btnDownLoad_Click(object sender, EventArgs e)
        {
           
        }
    }
}