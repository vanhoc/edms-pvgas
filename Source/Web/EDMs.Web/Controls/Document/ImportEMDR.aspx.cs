﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportEMDR : Page
    {
        /// <summary>
        /// The revision service.
        /// </summary>
        private readonly RevisionService revisionService;

        /// <summary>
        /// The document type service.
        /// </summary>
        private readonly DocumentTypeService documentTypeService;

        /// <summary>
        /// The discipline service.
        /// </summary>
        private readonly DisciplineService disciplineService;

        /// <summary>
        /// The folder service.
        /// </summary>
        private readonly DocumentService documentService;

        private readonly PackageService packageService;

        private readonly RoleService roleService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ScopeProjectService scopeProjectService;
        private readonly WorkGroupService workGroupService;

        private readonly DocumentStatuService statusService;
        private readonly CodeService codeSerVice;
        private readonly TransmittalService transmittalService;
        private readonly AttachDocToTransmittalService attachDocToTransmittalService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>

        private readonly DepartmentService departmentService;
        private readonly PlatformService platformService;

        private readonly ProcessActualService processActualService;
        public ImportEMDR()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.packageService = new PackageService();
            this.roleService = new RoleService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new ScopeProjectService();
            this.workGroupService = new WorkGroupService();
            this.departmentService = new DepartmentService();
            this.platformService = new PlatformService();
            this.statusService = new DocumentStatuService();
            this.transmittalService = new TransmittalService();
            this.attachDocToTransmittalService = new AttachDocToTransmittalService();
            this.processActualService = new ProcessActualService();
            this.codeSerVice = new CodeService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentFileName = string.Empty;
            var currentDocumentNo = string.Empty;

            var documentPackageList = new List<DocumentPackage>();
            var newdocumentPackageList = new List<DocumentPackage>();
            var projectObj=new ScopeProject();;
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    currentFileName = docFile.FileName;
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") +
                                         "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        // Instantiate a new workbook
                        var workbook = new Workbook();
                        workbook.Open(importPath);
                        var wsData = workbook.Worksheets[2];

                        var projectId = Convert.ToInt32(wsData.Cells["A1"].Value);

                         projectObj = this.scopeProjectService.GetById(projectId);
                       

                        // Create a datatable
                        var dataTable = new DataTable();

                        // Export worksheet data to a DataTable object by calling either ExportDataTable or ExportDataTableAsString method of the Cells class	
                        var rowCount = Convert.ToInt32(wsData.Cells["A7"].Value);
                        dataTable = wsData.Cells.ExportDataTable(7, 0, rowCount, 46);
                        foreach (DataRow dataRow in dataTable.Rows)
                        {
                            if (!string.IsNullOrEmpty(dataRow["Column1"].ToString()) && (dataRow["Column1"].ToString()== "TRUE") && !string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                            {
                                currentDocumentNo = dataRow["Column4"].ToString();
                                var docId = new Guid(dataRow["Column2"].ToString());
                                var docObj = this.documentPackageService.GetById(docId);
                                if (docObj != null)
                                {
                                    docObj.DocTitle = dataRow["Column5"].ToString();
                                   // var revisionName = dataRow["Column6"].ToString();
                                   // var revisionObj = this.revisionService.GetByName(revisionName, projectId);
                                  
                                    #region Hiden1
                                    //if (revisionObj != null && revisionObj.ID > docObj.RevisionId)
                                    //{
                                    //    var docNewRev = new DocumentPackage()
                                    //    {
                                    //        ProjectId = docObj.ProjectId,
                                    //        ProjectName = docObj.ProjectName,
                                    //        ProjectFullName = docObj.ProjectFullName,
                                    //        DocNo = docObj.DocNo,
                                    //        DocTitle = docObj.DocTitle,

                                    //        RevisionId = revisionObj.ID,
                                    //        RevisionName = revisionObj.Name,

                                    //        OriginatorId = docObj.OriginatorId,
                                    //        OriginatorFullName = docObj.OriginatorFullName,
                                    //        OriginatorName = docObj.OriginatorName,

                                    //        DisciplineId = docObj.DisciplineId,
                                    //        DisciplineFullName = docObj.DisciplineFullName,
                                    //        DisciplineName = docObj.DisciplineName,

                                    //        DocTypeFullName = docObj.DocTypeFullName,
                                    //        DocumentTypeId = docObj.DocumentTypeId,
                                    //        DocumentTypeName = docObj.DocumentTypeName,

                                    //        SequencetialNumber = docObj.SequencetialNumber,
                                    //        DrawingSheetNumber = docObj.DrawingSheetNumber,

                                    //        Complete = docObj.Complete,
                                    //        RevisionReceiveTransNo = dataRow["Column15"].ToString(),
                                    //        FirstIssueTransNo = dataRow["Column18"].ToString(),
                                    //        FinalIssueTransNo = dataRow["Column21"].ToString(),

                                    //        StatusID = statusObj != null? statusObj.ID : 0,
                                    //        StatusFullName = statusObj != null?  statusObj.FullNameWithWeight : string.Empty,
                                    //        StatusName = statusObj != null ?statusObj.Name : string.Empty,

                                    //        IsDelete = false,
                                    //        IsLeaf = true,
                                    //        IsEMDR = true,
                                    //        ParentId = docObj.ParentId ?? docObj.ID,
                                    //        CreatedBy = UserSession.Current.User.Id,
                                    //        CreatedDate = DateTime.Now,
                                    //    };

                                    //    if (!string.IsNullOrEmpty(dataRow["Column23"].ToString()))
                                    //    {
                                    //        docNewRev.Weight = Math.Round( Convert.ToDouble(dataRow["Column23"])*100,2);
                                    //    }

                                    //    var strRevReceivedDate = dataRow["Column14"].ToString();
                                    //    var revReceivedDate = new DateTime();
                                    //    if (Utility.ConvertStringToDateTime(strRevReceivedDate, ref revReceivedDate))
                                    //    {
                                    //        docNewRev.RevisionActualDate = revReceivedDate;
                                    //    }

                                    //    var strFirstIssuePlanDate = dataRow["Column16"].ToString();
                                    //    var firstIssuePlanDate = new DateTime();
                                    //    if (Utility.ConvertStringToDateTime(strFirstIssuePlanDate, ref firstIssuePlanDate))
                                    //    {
                                    //        docNewRev.FirstIssuePlanDate = firstIssuePlanDate;
                                    //    }

                                    //    var strFirstIssueActualDate = dataRow["Column17"].ToString();
                                    //    var firstIssueActualDate = new DateTime();
                                    //    if (Utility.ConvertStringToDateTime(strFirstIssueActualDate, ref firstIssueActualDate))
                                    //    {
                                    //        docNewRev.FirstIssueActualDate = firstIssueActualDate;
                                    //    }

                                    //    var strFinalIssuePlanDate = dataRow["Column19"].ToString();
                                    //    var finalIssuePlanDate = new DateTime();
                                    //    if (Utility.ConvertStringToDateTime(strFinalIssuePlanDate, ref finalIssuePlanDate))
                                    //    {
                                    //        docNewRev.FinalIssuePlanDate = finalIssuePlanDate;
                                    //    }

                                    //    var strFinalIssueActualDate = dataRow["Column20"].ToString();
                                    //    var finalIssueActualDate = new DateTime();
                                    //    if (Utility.ConvertStringToDateTime(strFinalIssueActualDate, ref finalIssueActualDate))
                                    //    {
                                    //        docNewRev.FinalIssueActualDate = finalIssueActualDate;
                                    //    }

                                    //    if (statusObj != null)
                                    //    {
                                    //        docNewRev.Complete = statusObj.PercentCompleteDefault;
                                    //    }
                                    //    docNewRev.CompleteForProject = Math.Round(docNewRev.Complete.GetValueOrDefault() * docNewRev.Weight.GetValueOrDefault() / 100, 2);
                                    //    newdocumentPackageList.Add(docNewRev);

                                    //    docObj.IsLeaf = false;
                                    //    docObj.LastUpdatedBy = UserSession.Current.User.Id;
                                    //    docObj.LastUpdatedDate = DateTime.Now;
                                    //    documentPackageList.Add(docObj);
                                    //}
                                    //else
                                    //{
                                    #endregion
                                    var sartplan = dataRow["Column7"].ToString();
                                    var firstIssuePlanDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(sartplan, ref firstIssuePlanDate))
                                    {
                                        docObj.StartPlan = firstIssuePlanDate;
                                    }

                                    var sartrevised = dataRow["Column8"].ToString();
                                    var finalIssuePlanDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(sartrevised, ref finalIssuePlanDate))
                                    {
                                        docObj.StartRevised = finalIssuePlanDate;
                                    }
                                    var sartactual = dataRow["Column9"].ToString();
                                    var StartActual = new DateTime();
                                    if (Utility.ConvertStringToDateTime(sartactual, ref StartActual))
                                    {
                                        docObj.StartActual = StartActual;
                                    }

                                    var IDCplan = dataRow["Column10"].ToString();
                                    var IDCPlanDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IDCplan, ref IDCPlanDate))
                                    {
                                        docObj.IDCPlan = IDCPlanDate;
                                    }

                                    var IDCrevised = dataRow["Column11"].ToString();
                                    var IDCRevisedDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IDCrevised, ref IDCRevisedDate))
                                    {
                                        docObj.IDCRevised = IDCRevisedDate;
                                    }
                                    var IDCActual = dataRow["Column12"].ToString();
                                    var IDCActualDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IDCActual, ref IDCActualDate))
                                    {
                                        docObj.IDCACtual = IDCActualDate;
                                    }

                                    var IFRplan = dataRow["Column13"].ToString();
                                    var IFRPlanDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IFRplan, ref IFRPlanDate))
                                    {
                                        docObj.IFRPlan = IFRPlanDate;
                                    }

                                    var IFRrevised = dataRow["Column14"].ToString();
                                    var IFRrevisedDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IFRrevised, ref IFRrevisedDate))
                                    {
                                        docObj.IFRRevised = IFRrevisedDate;
                                    }

                                    var IFRactual = dataRow["Column15"].ToString();
                                    var IFRactualDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IFRactual, ref IFRactualDate))
                                    {
                                        docObj.IFRActual = IFRactualDate;
                                    }

                                    var reIFRplan = dataRow["Column16"].ToString();
                                    var reIFRPlanDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(reIFRplan, ref reIFRPlanDate))
                                    {
                                        docObj.ReIFRPlan = reIFRPlanDate;
                                    }

                                    var reifrrevised = dataRow["Column17"].ToString();
                                    var ReifrrevisedDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(reifrrevised, ref ReifrrevisedDate))
                                    {
                                        docObj.ReIFRRevised = ReifrrevisedDate;
                                    }
                                    var reifractual = dataRow["Column18"].ToString();
                                    var ReifractualDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(reifractual, ref ReifractualDate))
                                    {
                                        docObj.ReIFRActual = ReifractualDate;
                                    }
                                    var IFAplan = dataRow["Column19"].ToString();
                                    var IFAPlanDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IFAplan, ref IFAPlanDate))
                                    {
                                        docObj.IFAPlan = IFAPlanDate;
                                    }

                                    var IFARevised = dataRow["Column20"].ToString();
                                    var IFAreviseddate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IFARevised, ref IFAreviseddate))
                                    {
                                        docObj.IFARevised = IFAreviseddate;
                                    }
                                    var IFAactual = dataRow["Column21"].ToString();
                                    var IFAactualdate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(IFAactual, ref IFAactualdate))
                                    {
                                        docObj.IFAActual = IFAactualdate;
                                    }
                                    //AFC
                                    var AFCplan = dataRow["Column22"].ToString();
                                    var AFCPlanDate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(AFCplan, ref AFCPlanDate))
                                    {
                                        docObj.AFCPlan = AFCPlanDate;
                                    }
                                    var AFCRevised = dataRow["Column23"].ToString();
                                    var AFCreviseddate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(AFCRevised, ref AFCreviseddate))
                                    {
                                        docObj.AFCRevised = AFCreviseddate;
                                    }
                                    var AFCactual = dataRow["Column24"].ToString();
                                    var AFCactualdate = new DateTime();
                                    if (Utility.ConvertStringToDateTime(AFCactual, ref AFCactualdate))
                                    {
                                        docObj.AFCActual = AFCactualdate;
                                    }
                                    docObj.Manhours = !string.IsNullOrEmpty(dataRow["Column25"].ToString()) ? Convert.ToDouble(dataRow["Column25"]) : 0;
                                    var weight = !string.IsNullOrEmpty(dataRow["Column26"].ToString())
                                              ? (double?)
                                                  Math.Round(Convert.ToDouble(dataRow["Column26"].ToString()) * 100, 2)
                                              : null;
                                    docObj.Weight = weight;
                                    var statusName = dataRow["Column28"].ToString();
                                    var statusObj = this.statusService.GetByName(statusName, projectId);
                                    var stCodeReview = dataRow["Column29"].ToString();
                                    var CodeReview = this.codeSerVice.GetAll().Find(t=> t.Name.Contains(stCodeReview)&& t.ProjectID==projectId && t.TypeId==2);
                                    docObj.DocReviewStatusCodeID = CodeReview != null ? CodeReview.ID : 0;
                                    docObj.DocReviewStatusCode = CodeReview != null ? CodeReview.Name : string.Empty;
                                    docObj.Remarks = dataRow["Column30"].ToString();
                                    if (string.IsNullOrEmpty(docObj.IncomingTransNo_CA) && dataRow["Column33"].ToString().Trim()!="")
                                    {
                                        var stCodeCA = dataRow["Column31"].ToString().Trim();
                                        var CodeCA = this.codeSerVice.GetAll().Find(t => t.Name==stCodeCA && t.ProjectID == projectId && t.TypeId == 1);
                                        docObj.ResponseCAId = CodeCA != null ? CodeCA.ID : 0;
                                        docObj.ResponseCACode = CodeCA != null ? CodeCA.Name : string.Empty;
                                        docObj.DNV_FinalCode = CodeCA != null ? CodeCA.Name : string.Empty;
                                      

                                        //Trans DNV
                                        docObj.IncomingTransNo_CA = dataRow["Column33"].ToString();
                                        var DNV_Indate = dataRow["Column32"].ToString();
                                        var DNV_indate = new DateTime();
                                        if (Utility.ConvertStringToDateTime(DNV_Indate, ref DNV_indate))
                                        {
                                            docObj.IncomingTransDate_CA = DNV_indate;
                                        }
                                    }
                                    if (string.IsNullOrEmpty(docObj.OutgoingTransNo_CA) && dataRow["Column35"].ToString().Trim() != "" )
                                    {
                                        docObj.OutgoingTransNo_CA = dataRow["Column35"].ToString();
                                        var DNV_Uotdate = dataRow["Column34"].ToString();
                                        var DNV_outdate = new DateTime();
                                        if (Utility.ConvertStringToDateTime(DNV_Uotdate, ref DNV_outdate))
                                        {
                                            docObj.OutgoingTransDate_CA = DNV_outdate;
                                            if (!string.IsNullOrEmpty(docObj.DNV_markup) && (docObj.DNV_markup.ToUpper() == "2" || docObj.DNV_markup.ToUpper() == "3") && docObj.OutgoingTransDate_CA != null)
                                            {
                                                var Tempdate = docObj.OutgoingTransDate_CA.GetValueOrDefault();
                                                for (int i = 1; i <= 5; i++)
                                                {
                                                    Tempdate = this.GetNextWorkingDay(Tempdate);
                                                }
                                                docObj.DNV_DueDate = Tempdate;
                                            }
                                        }
                                    }

                                    ////KDN
                                    if (string.IsNullOrEmpty(docObj.KDN_Operator_IncomingTransNo) && dataRow["Column38"].ToString().Trim() != "")
                                    {
                                        var stCodeKDN = dataRow["Column36"].ToString().Trim();
                                        var CodeKDN = this.codeSerVice.GetAll().Find(t => t.Name==stCodeKDN && t.ProjectID == projectId && t.TypeId == 1);
                                        docObj.KDN_Operator_ResponseId = CodeKDN != null ? CodeKDN.ID : 0;
                                        docObj.KDN_Operator_ResponseCode = CodeKDN != null ? CodeKDN.Name : string.Empty;
                                        docObj.KDN_Operator_FinalCode = CodeKDN != null ? CodeKDN.Name : string.Empty;
                                        docObj.KDN_Operator_IncomingTransNo = dataRow["Column38"].ToString();
                                        var KDN_Indate = dataRow["Column37"].ToString();
                                        var KDN_indate = new DateTime();
                                        if (Utility.ConvertStringToDateTime(KDN_Indate, ref KDN_indate))
                                        {
                                            docObj.KDN_Operator_IncomingTransDate = KDN_indate;
                                        }
                                    }
                                    if (string.IsNullOrEmpty(docObj.KDN_Operator_OutgoingTransNo) && dataRow["Column40"].ToString().Trim() != "")
                                    {
                                        docObj.KDN_Operator_OutgoingTransNo = dataRow["Column40"].ToString();
                                        var KDN_Uotdate = dataRow["Column39"].ToString();
                                        var KDN_outdate = new DateTime();
                                        if (Utility.ConvertStringToDateTime(KDN_Uotdate, ref KDN_outdate))
                                        {
                                            docObj.KDN_Operator_OutgoingTransDate = KDN_outdate;
                                            if (!string.IsNullOrEmpty(docObj.KDN_markup) && (docObj.KDN_markup.ToUpper() == "2" || docObj.KDN_markup.ToUpper() == "3") && docObj.KDN_Operator_OutgoingTransDate != null)
                                            {
                                                var Tempdate = docObj.KDN_Operator_OutgoingTransDate.GetValueOrDefault();
                                                for (int i = 1; i <= 5; i++)
                                                {
                                                    Tempdate = this.GetNextWorkingDay(Tempdate);
                                                }
                                                docObj.KDN_Operator_DueDate = Tempdate;
                                            }
                                        }
                                    }

                                    ////NCS
                                    if (string.IsNullOrEmpty(docObj.NCS_Operator_IncomingTransNo) && dataRow["Column43"].ToString().Trim() != "")
                                    {
                                        var stCodeNCS = dataRow["Column41"].ToString();
                                        var CodeNCS = this.codeSerVice.GetAll().Find(t => t.Name==stCodeNCS && t.ProjectID == projectId && t.TypeId == 1);
                                        docObj.NCS_Operator_ResponseId = CodeNCS != null ? CodeNCS.ID : 0;
                                        docObj.NCS_Operator_ResponseCode = CodeNCS != null ? CodeNCS.Name : string.Empty;
                                        docObj.NCS_Operator_FinalCode = CodeNCS != null ? CodeNCS.Name : string.Empty;
                                        docObj.NCS_Operator_IncomingTransNo = dataRow["Column43"].ToString();
                                        var NCS_Indate = dataRow["Column42"].ToString();
                                        var NCS_indate = new DateTime();
                                        if (Utility.ConvertStringToDateTime(NCS_Indate, ref NCS_indate))
                                        {
                                            docObj.NCS_Operator_IncomingTransDate = NCS_indate;
                                        }
                                    }
                                    if (string.IsNullOrEmpty(docObj.NCS_Operator_OutgoingTransNo) && dataRow["Column45"].ToString().Trim() != "")
                                    {
                                        docObj.NCS_Operator_OutgoingTransNo = dataRow["Column45"].ToString();
                                        var NCS_Uotdate = dataRow["Column44"].ToString();
                                        var NCS_outdate = new DateTime();
                                        if (Utility.ConvertStringToDateTime(NCS_Uotdate, ref NCS_outdate))
                                        {
                                            docObj.NCS_Operator_OutgoingTransDate = NCS_outdate;
                                            if (!string.IsNullOrEmpty(docObj.NCS_markup) && (docObj.NCS_markup.ToUpper() == "2" || docObj.NCS_markup.ToUpper() == "3") && docObj.NCS_Operator_OutgoingTransDate != null)
                                            {
                                                var Tempdate = docObj.NCS_Operator_OutgoingTransDate.GetValueOrDefault();
                                                for (int i = 1; i <= 5; i++)
                                                {
                                                    Tempdate = this.GetNextWorkingDay(Tempdate);
                                                }
                                                docObj.NCS_Operator_DueDate = Tempdate;
                                            }
                                        }
                                    }

                                    docObj.LastUpdatedBy = UserSession.Current.User.Id;
                                        docObj.LastUpdatedDate = DateTime.Now;
                                        docObj.StatusID = statusObj != null ? statusObj.ID : 0;
                                        docObj.StatusFullName = statusObj != null ?statusObj.FullName : string.Empty;
                                        docObj.StatusName =statusObj != null ? statusObj.Name : string.Empty;

                                        documentPackageList.Add(docObj);
                                    //}
                                }
                            }
                        }
                    }
                }

                if (this.cbCheckValidFile.Checked)
                {
                    foreach (var documentPackage in documentPackageList)
                    {
                        this.documentPackageService.Update(documentPackage);
                    }

                    //foreach (var documentPackage in newdocumentPackageList)
                    //{
                    //    this.documentPackageService.Insert(documentPackage);
                    //}

                    //if (projectObj != null && projectObj.IsAutoCalculate.GetValueOrDefault())
                    //{
                    //    UpdateActualProgress(projectObj);
                    //}


                    this.blockError.Visible = true;
                    this.lblError.Text = "All CMDR data file is valid. Data import successfull.";
                }
                else
                {
                    this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);    
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at CMDR data file: '" + currentFileName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
        private bool IsWeekEnd(DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        private DateTime GetNextWorkingDay(DateTime date)
        {
          
            do
            {
                date = date.AddDays(1);
            }
            while (IsWeekEnd(date)  );

            return date;
        }
        /// <summary>
        /// Auto calculater
        /// </summary>
        /// <param name="objProject"></param>

        private void UpdateActualProgress(ScopeProject objProject)
        {
            var count = 0;
            for (var j = objProject.StartDate.GetValueOrDefault();
                j < objProject.EndDate.GetValueOrDefault();
                j = j.AddDays(objProject.FrequencyForProgressChart != null && objProject.FrequencyForProgressChart != 0 ? objProject.FrequencyForProgressChart.Value : 7))
            {
                if (DateTime.Now > j)
                {
                    count += 1;
                }
                else
                {
                    break;
                }
            }

            var listDiscipline = this.disciplineService.GetAllDisciplineOfProject(objProject.ID).OrderBy(t => t.ID).ToList();
            foreach (var discipline in listDiscipline)
            {
                var docList = this.documentPackageService.GetAllByDiscipline(discipline.ID).OrderBy(t => t.DocNo).ToList();
                double? complete = 0;
                complete = docList.Aggregate(complete, (current, t) => current + t.CompleteForProject.GetValueOrDefault());
                discipline.Complete =  complete ;
                var existProgressActual = this.processActualService.GetByProjectAndWorkgroup(objProject.ID, discipline.ID);
                if (existProgressActual != null)
                {
                    var arrActual = existProgressActual.Actual.Split('$');
                    if (arrActual.Count() >= count)
                    {
                        arrActual[count] = Math.Round(complete.GetValueOrDefault(), 2).ToString();

                        var newAtualProgress = string.Empty;
                        newAtualProgress = arrActual.Aggregate(newAtualProgress, (current, t) => current + t + "$");
                        newAtualProgress = newAtualProgress.Substring(0, newAtualProgress.Length - 1);

                        existProgressActual.Actual = newAtualProgress;

                        this.processActualService.Update(existProgressActual);
                    }
                  

                }
                else
                {
                    var progressActual = new ProcessActual();
                    progressActual.ProjectId = discipline.ProjectId;
                    progressActual.WorkgroupId = discipline.ID;
                    progressActual.Actual = Math.Round(complete.GetValueOrDefault(), 2).ToString();
                    this.processActualService.Insert(progressActual);
                }
                this.disciplineService.Update(discipline);
            }
        }


    }
}