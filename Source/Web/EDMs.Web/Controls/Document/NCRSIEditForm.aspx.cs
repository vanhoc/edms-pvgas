﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Linq;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;
    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class NCRSIEditForm : Page
    {

        /// <summary>
        /// The service name.
        /// </summary>
        protected const string ServiceName = "EDMSFolderWatcher";

        private readonly OrganizationCodeService organizationcodeService;
        private readonly ProjectCodeService projectcodeService;
        private readonly ConfidentialityService confidentialityService;
        private readonly GroupCodeService groupCodeService;
        private readonly ChangeRequestService changeRequestService;

        private readonly NCR_SIService ncrSiService;

        /// <summary>
        /// The document package service.
        /// </summary>
        private readonly PECC2DocumentsService pecc2DocumentService;

        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public NCRSIEditForm()
        {
            this.groupCodeService = new GroupCodeService();
            this.userService = new UserService();
            this.pecc2DocumentService = new PECC2DocumentsService();
            this.confidentialityService = new ConfidentialityService();
            this.organizationcodeService = new OrganizationCodeService();
            this.projectcodeService = new ProjectCodeService();
            this.changeRequestService = new ChangeRequestService();
            this.ncrSiService = new NCR_SIService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projectId"]);
                var projectObj = this.projectcodeService.GetById(projectId);
                this.LoadComboData(projectObj);
                
                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    this.CreatedInfo.Visible = true;
                    Guid objId;
                    Guid.TryParse(this.Request.QueryString["objId"],out objId);
                    var docObj = this.ncrSiService.GetById(objId);
                    if (docObj != null)
                    {
                        this.LoadDocInfo(docObj, projectObj);
                        var createdUser = this.userService.GetByID(docObj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + docObj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (docObj.LastUpdatedBy != null && docObj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(docObj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + docObj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    this.CreatedInfo.Visible = false;
                    var sequence = Utilities.Utility.ReturnSequenceString(this.ncrSiService.GetCurrentSequence(Convert.ToInt32(this.Request.QueryString["type"])), 3);
                    this.RegenerateNo(sequence);
                }
            }
        }

        private void RegenerateNo(string sequence)
        {
            switch (Convert.ToInt32(this.Request.QueryString["type"]))
            {
                case 1:
                    this.txtNumber.Text = "NCR-" + this.ddlGroup.SelectedItem.Text.Split(',')[0] + "-" + sequence;
                    break;
                case 2:
                    this.txtNumber.Text = "SI-" + this.ddlGroup.SelectedItem.Text.Split(',')[0] + "-" + sequence;
                    break;
                case 3:
                    this.txtNumber.Text = "CS-" + this.ddlGroup.SelectedItem.Text.Split(',')[0] + "-" + sequence;
                    break;
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid )
            {
                var projectId = Convert.ToInt32(this.Request.QueryString["projectId"]);
                var projectObj = this.projectcodeService.GetById(projectId);
                NCR_SI changeRequestObj;
                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    var changeRequestId = new Guid(this.Request.QueryString["objId"]);
                    changeRequestObj = this.ncrSiService.GetById(changeRequestId);
                    if (changeRequestObj != null)
                    {
                        
                        this.CollectData(ref changeRequestObj, projectObj);

                        changeRequestObj.LastUpdatedBy = UserSession.Current.User.Id;
                        changeRequestObj.LastUpdatedByName = UserSession.Current.User.FullName;
                        changeRequestObj.LastUpdatedDate = DateTime.Now;
                        this.ncrSiService.Update(changeRequestObj);
                    }
                }
                else
                {
                    changeRequestObj = new NCR_SI()
                    {
                        ID = Guid.NewGuid(),
                        GroupId = Convert.ToInt32(this.ddlGroup.SelectedValue),
                        GroupName = this.ddlGroup.SelectedItem.Text.Split(',')[0],
                        CreatedBy = UserSession.Current.User.Id,
                        CreatedByName = UserSession.Current.User.FullName,
                        CreatedDate = DateTime.Now,
                        IsDelete = false,
                        IsCompleteFinal = false,
                        IsInWFProcess = false,
                        IsWFComplete = false,
                        Type = Convert.ToInt32(this.Request.QueryString["type"]),
                    };

                    this.CollectData(ref changeRequestObj, projectObj);
                    if (!this.changeRequestService.IsExist(changeRequestObj.Number))
                    {
                        this.ncrSiService.Insert(changeRequestObj);
                    }
                    else
                    {
                        this.blockError.Visible = true;
                        this.lblError.Text = "NCR/SI. is already exist. ";
                        return;
                    }
                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "CloseWindow", "CloseAndRebind();", true);
            }
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            //if (this.txtDocNo.Text.Trim().Length == 0)
            //{
            //    this.fileNameValidator.ErrorMessage = "Please enter Document Number.";
            //    this.divDocNo.Style["margin-bottom"] = "-26px;";
            //    args.IsValid = false;
            //}
            //else if (!string.IsNullOrEmpty(Request.QueryString["objId"]))
            //{
            //    Guid objId;
            //    Guid.TryParse(this.Request.QueryString["objId"].ToString(), out objId);

            //    if (this._PECC2DocumentService.IsExistByDocNo(this.txtDocNumber.Text.Trim()) && objId == null)
            //    {
            //        this.fileNameValidator.ErrorMessage = "Document No. is already exist.";
            //        this.divDocNo.Style["margin-bottom"] = "-5px;";
            //        args.IsValid = false;
            //    }
            //}
        }

        /// <summary>
        /// The rad ajax manager 1_ ajax request.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
            if (e.Argument.Contains("CheckFileName"))
            {
                //var fileName = e.Argument.Split('$')[1];
                //var folderId = Convert.ToInt32(Request.QueryString["folId"]);
                
                
            }
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData(ProjectCode projectObj)
        {
            this.txtProjectCode.Text = projectObj.FullName;
            this.txtIssueDate.SelectedDate = DateTime.Now;

            //Confdentiality
            var Confdentialitylist = this.confidentialityService.GetAll(UserSession.Current.User.ConfidentialId.GetValueOrDefault());
            this.ddlConfidentiality.DataSource = Confdentialitylist;
            this.ddlConfidentiality.DataTextField = "Code";
            this.ddlConfidentiality.DataValueField = "ID";
            this.ddlConfidentiality.DataBind();

            var organizationList = this.organizationcodeService.GetAll();
            this.ddlOriginatingOrganisationOWNER.DataSource = organizationList.OrderBy(t => t.Code);
            this.ddlOriginatingOrganisationOWNER.DataTextField = "FullName";
            this.ddlOriginatingOrganisationOWNER.DataValueField = "ID";
            this.ddlOriginatingOrganisationOWNER.DataBind();

            this.ddlOriginatingOrganisationPMC.DataSource = organizationList.OrderBy(t => t.Code);
            this.ddlOriginatingOrganisationPMC.DataTextField = "FullName";
            this.ddlOriginatingOrganisationPMC.DataValueField = "ID";
            this.ddlOriginatingOrganisationPMC.DataBind();

            this.ddlReceivingOrganisationEPC.DataSource = organizationList.OrderBy(t => t.Code);
            this.ddlReceivingOrganisationEPC.DataTextField = "FullName";
            this.ddlReceivingOrganisationEPC.DataValueField = "ID";
            this.ddlReceivingOrganisationEPC.DataBind();

            var groupList = this.groupCodeService.GetAll();
            this.ddlGroup.DataSource = groupList.OrderBy(t => t.Code);
            this.ddlGroup.DataTextField = "FullName";
            this.ddlGroup.DataValueField = "ID";
            this.ddlGroup.DataBind();

            var docList = this.pecc2DocumentService.GetAllProjectCode(projectObj.ID);
            this.rtvRefDocNo.DataSource = docList.OrderBy(t => t.DocNo);
            this.rtvRefDocNo.DataTextField = "DocNoWithRev";
            this.rtvRefDocNo.DataValueField = "ID";
            this.rtvRefDocNo.DataBind();
        }

        private void CollectData(ref NCR_SI obj, ProjectCode projectObj)
        {
            obj.Number = this.txtNumber.Text.Trim();
            obj.Subject = this.txtSubject.Text.Trim();
            obj.Description = this.txtDescription.Text.Trim();
            obj.ConfidentialityId = this.ddlConfidentiality.SelectedItem != null ?
                                        Convert.ToInt32(this.ddlConfidentiality.SelectedValue)
                                        : 0;
            obj.ConfidentialityName = this.ddlConfidentiality.SelectedItem != null ?
                                        this.ddlConfidentiality.SelectedItem.Text.Split(',')[0]
                                        : string.Empty;

            obj.GroupId = this.ddlGroup.SelectedItem != null ?
                                        Convert.ToInt32(this.ddlGroup.SelectedValue)
                                        : 0;
            obj.GroupName = this.ddlGroup.SelectedItem != null ?
                                        this.ddlGroup.SelectedItem.Text.Split(',')[0]
                                        : string.Empty;
            obj.ProjectId = projectObj.ID;
            obj.ProjectName = projectObj.Code;

            obj.OriginatingOrganisationPMCId = this.ddlOriginatingOrganisationPMC.SelectedItem != null ?
                                        Convert.ToInt32(this.ddlOriginatingOrganisationPMC.SelectedValue)
                                        : 0;
            obj.OriginatingOrganisationPMCName = this.ddlOriginatingOrganisationPMC.SelectedItem != null ?
                                        this.ddlOriginatingOrganisationPMC.SelectedItem.Text.Split(',')[0]
                                        : string.Empty;
            obj.OriginatingOrganisationOwnerId = this.ddlOriginatingOrganisationOWNER.SelectedItem != null ?
                                        Convert.ToInt32(this.ddlOriginatingOrganisationOWNER.SelectedValue)
                                        : 0;
            obj.OriginatingOrganisationOwnerName = this.ddlOriginatingOrganisationOWNER.SelectedItem != null ?
                                        this.ddlOriginatingOrganisationOWNER.SelectedItem.Text.Split(',')[0]
                                        : string.Empty;
            obj.ReceivingOrganisationEPCId = this.ddlReceivingOrganisationEPC.SelectedItem != null ?
                                        Convert.ToInt32(this.ddlReceivingOrganisationEPC.SelectedValue)
                                        : 0;
            obj.ReceivingOrganisationEPCName = this.ddlReceivingOrganisationEPC.SelectedItem != null ?
                                        this.ddlReceivingOrganisationEPC.SelectedItem.Text.Split(',')[0]
                                        : string.Empty;

            obj.RefDocId = string.Empty;
            obj.RefDocNo = string.Empty;
            foreach (RadTreeNode actionNode in this.rtvRefDocNo.CheckedNodes.Where(t => !string.IsNullOrEmpty(t.Value)))
            {
                obj.RefDocId += actionNode.Value + ";";
                obj.RefDocNo += actionNode.Text + Environment.NewLine;
            }
            obj.ActionTake = this.txtActionTaken.Text.Trim();

            obj.IssuedDate = this.txtIssueDate.SelectedDate;
            obj.IssuedByOwner = this.txtIssuedByOWNER.Text.Trim();
            obj.IssuedByPMC = this.txtIssuedByPMC.Text.Trim();
            obj.ReceivedByEPC = this.txtReceivedByEPC.Text.Trim();
            obj.Status = this.ddlStatus.SelectedValue;
            obj.ClosedDate = this.txtClosedDate.SelectedDate;
            obj.ClosedByPMB = this.txtClosedByPMB.Text.Trim();
            obj.ClosedByPMC = this.txtIssuedByPMC.Text.Trim();
            obj.SequentialNumber = this.txtNumber.Text.Split('-')[2];
            obj.Sequence = Convert.ToInt32(this.txtNumber.Text.Split('-')[2]);
        }

        private void LoadDocInfo(NCR_SI obj, ProjectCode projectObj)
        {
            this.txtNumber.Text = obj.Number;
            this.txtSubject.Text = obj.Subject;
            this.txtDescription.Text = obj.Description;
            this.ddlConfidentiality.SelectedValue = obj.ConfidentialityId.ToString();
            this.txtProjectCode.Text = projectObj.FullName;
            this.ddlGroup.SelectedValue = obj.GroupId.ToString();
            this.ddlOriginatingOrganisationOWNER.SelectedValue = obj.OriginatingOrganisationOwnerId.ToString();
            this.ddlOriginatingOrganisationPMC.SelectedValue = obj.OriginatingOrganisationPMCId.ToString();
            this.ddlReceivingOrganisationEPC.SelectedValue = obj.ReceivingOrganisationEPCId.ToString();
            foreach (RadTreeNode refDocNode in this.rtvRefDocNo.Nodes)
            {
                refDocNode.Checked = !string.IsNullOrEmpty(obj.RefDocId) && obj.RefDocId.Split(';').ToList().Contains(refDocNode.Value);
            }
            this.txtActionTaken.Text = obj.ActionTake;
            this.txtIssueDate.SelectedDate = obj.IssuedDate;
            this.txtIssuedByPMC.Text = obj.IssuedByPMC;
            this.txtIssuedByOWNER.Text = obj.IssuedByOwner;
            this.txtReceivedByEPC.Text = obj.ReceivedByEPC;
            this.txtClosedByPMB.Text = obj.ClosedByPMB;
            this.txtClosedByPMC.Text = obj.ClosedByPMC;
            this.txtClosedDate.SelectedDate = obj.ClosedDate;
            this.ddlStatus.SelectedValue = obj.Status;
            
        }

        protected void ddlGroup_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var sequence = Utilities.Utility.ReturnSequenceString(this.changeRequestService.GetCurrentSequence(Convert.ToInt32(this.Request.QueryString["type"])), 3);
            this.RegenerateNo(sequence);
        }
    }
}