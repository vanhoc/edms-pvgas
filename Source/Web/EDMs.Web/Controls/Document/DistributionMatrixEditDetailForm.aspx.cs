﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Linq;
using System.Web.UI;
using EDMs.Business.Services.Document;
using EDMs.Business.Services.Library;
using EDMs.Business.Services.Security;
using EDMs.Data.Entities;
using EDMs.Web.Utilities.Sessions;
using Telerik.Web.UI;

namespace EDMs.Web.Controls.Document
{
    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class DistributionMatrixEditDetailForm : Page
    {

        private readonly DisciplineService disciplineService;
        private readonly DocumentTypeService documentTypeService;
        private readonly UserService userService;
        private readonly DistributionMatrixService dmService;
        private readonly DistributionMatrixDetailService dmDetailService;
        private readonly DocumentPackageService documenpackageservice;
        private int DistributionMatrixId
        {
            get
            {
                return Convert.ToInt32(this.Request.QueryString["disMatrixId"]);
            }
        }

        private DistributionMatrix DistributionMatrixObj
        {
            get { return this.dmService.GetById(this.DistributionMatrixId); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public DistributionMatrixEditDetailForm()
        {
            this.disciplineService = new DisciplineService();
            this.documentTypeService = new DocumentTypeService();
            this.userService = new UserService();
            this.dmService = new DistributionMatrixService();
            this.dmDetailService = new DistributionMatrixDetailService();
            this.documenpackageservice = new DocumentPackageService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();
                this.ClearData();
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["disMatrixId"])
                && this.ddlDiscipline.SelectedIndex != 0
               // && this.ddlDocType.SelectedIndex != 0
                && this.ddlUser.SelectedIndex != 0)
            {
                if (Session["EditingId"] != null)
                {
                    var dmDetailId = Convert.ToInt32(Session["EditingId"]);
                    var dmDetailObj = this.dmDetailService.GetById(dmDetailId);
                    if (dmDetailObj != null)
                    {
                        this.CollectData(ref dmDetailObj);
                        this.dmDetailService.Update(dmDetailObj);
                    }

                    Session.Remove("EditingId");
                }
                else
                {
                    var dmDetailObj = new DistributionMatrixDetail();

                    this.CollectData(ref dmDetailObj);

                    dmDetailObj.DistributionMatrixId = this.DistributionMatrixObj.ID;
                    dmDetailObj.DistributionMatrixName = this.DistributionMatrixObj.FullName;
                    this.dmDetailService.Insert(dmDetailObj);
                }
            }

            this.ClearData();
            this.grdDocument.Rebind();
        }



        protected void grdDocument_DeleteCommand(object sender, GridCommandEventArgs e)
        {
            var item = (GridDataItem)e.Item;
            var dmDetailId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());

            this.dmDetailService.Delete(dmDetailId);
            this.grdDocument.Rebind();
        }

        protected void grdDocument_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            var comResList = this.dmDetailService.GetAllByDM(this.DistributionMatrixId);
            this.grdDocument.DataSource = comResList;    
        }

        protected void ajaxDocument_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {
        }

        protected void grdDocument_OnItemCommand(object sender, GridCommandEventArgs e)
        {
           
            if (e.CommandName == "EditCmd")
            {
                var item = (GridDataItem)e.Item;
                var dmDetailId = Convert.ToInt32(item.GetDataKeyValue("ID").ToString());
                var dmDetailObj = this.dmDetailService.GetById(dmDetailId);
                if (dmDetailObj != null)
                {
                    Session.Add("EditingId", dmDetailObj.ID);
                    this.ddlDiscipline.SelectedValue = dmDetailObj.DisciplineId.GetValueOrDefault().ToString();
                  //  this.ddlDocType.SelectedValue = dmDetailObj.DocTypeId.GetValueOrDefault().ToString();
                    this.ddlActionType.SelectedValue = dmDetailObj.ActionTypeId.GetValueOrDefault().ToString();
                    this.ddlUser.SelectedValue = dmDetailObj.UserId.GetValueOrDefault().ToString();
                }
            }
        }

        private void ClearData()
        {
            Session.Remove("EditingId");
            this.ddlDiscipline.SelectedIndex = 0;
           // this.ddlDocType.SelectedIndex = 0;
            this.ddlUser.SelectedIndex = 0;
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearData();
        }

        private void LoadComboData()
        {
            var disciplineList = this.documenpackageservice.GetAllByProject(Convert.ToInt32(this.Request.QueryString["projectId"]), true); //this.disciplineService.GetAllDisciplineOfProject().OrderBy(t => t.Name).ToList();
            disciplineList.Insert(0, new  DocumentPackage() {DocNo= string.Empty});
            this.ddlDiscipline.DataTextField = "DocNo";
            this.ddlDiscipline.DataValueField = "ID";
            this.ddlDiscipline.DataSource = disciplineList;
            this.ddlDiscipline.DataBind();

            //var docTypeList = this.documentTypeService.GetAllByProject(Convert.ToInt32(this.Request.QueryString["projectId"])).OrderBy(t => t.Name).ToList();
            //docTypeList.Insert(0, new DocumentType() {ID = 0});
            //this.ddlDocType.DataTextField = "FullName";
            //this.ddlDocType.DataValueField = "ID";
            //this.ddlDocType.DataSource = docTypeList;
            //this.ddlDocType.DataBind();

            var userList = this.userService.GetAll().Where(t => t.Id != 1).OrderBy(t => t.UserNameWithFullName).ToList();
            userList.Insert(0, new User() {Id = 0});
            this.ddlUser.DataTextField = "UserNameWithFullName";
            this.ddlUser.DataValueField = "ID";
            this.ddlUser.DataSource = userList;
            this.ddlUser.DataBind();
        }

        private void CollectData(ref DistributionMatrixDetail dmDetailObj)
        {
            dmDetailObj.DocId = new Guid(this.ddlDiscipline.SelectedValue);
            dmDetailObj.DocNo = this.ddlDiscipline.SelectedItem.Text.Trim();
            dmDetailObj.DocTitle = this.ddlDiscipline.SelectedItem.Text;
           // dmDetailObj.DocTypeId = this.ddlDocType.SelectedIndex!= 0? Convert.ToInt32(this.ddlDocType.SelectedValue): 0;
          //  dmDetailObj.DocTypeName = this.ddlDocType.SelectedIndex!= 0? this.ddlDocType.SelectedItem.Text.Split('-')[0].Trim():string.Empty;
          //  dmDetailObj.DocTypeFullName = this.ddlDocType.SelectedItem.Text;
            dmDetailObj.UserId = Convert.ToInt32(this.ddlUser.SelectedValue);
            dmDetailObj.UserName = this.ddlUser.SelectedItem.Text;
            dmDetailObj.ActionTypeId = Convert.ToInt32(this.ddlActionType.SelectedValue);
            dmDetailObj.ActionTypeFullName = this.ddlActionType.SelectedItem.Text;
            dmDetailObj.ActionTypeName = this.ddlActionType.SelectedItem.Text.Split('-')[0].Trim();
        }
    }
}