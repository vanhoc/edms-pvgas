﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.IO;
using System.Linq;
using System.Web.Hosting;
using Telerik.Web.UI;
using System.Configuration;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Business.Services.Document;
    using Business.Services.Library;
    using Business.Services.Security;
    using Data.Entities;
    using Utilities.Sessions;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ContractorTransmittalEditForm : Page
    {
        /// <summary>
        /// The user service.
        /// </summary>
        private readonly UserService userService;

        /// <summary>
        /// The to list service.
        /// </summary>
        private readonly OrganizationCodeService organizationCodeService;

        /// <summary>
        /// The transmittal service.
        /// </summary>
        private readonly ContractorTransmittalService transmittalService;

        private readonly ProjectCodeService projectService;


        private readonly GroupCodeService groupCodeService;

        private readonly DocumentStatuService documentCodeServices;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentInfoEditForm"/> class.
        /// </summary>
        public ContractorTransmittalEditForm()
        {
            this.userService = new UserService();
            this.transmittalService = new ContractorTransmittalService();
            this.organizationCodeService = new OrganizationCodeService();
            this.projectService=new ProjectCodeService();
            this.groupCodeService = new GroupCodeService();
            this.documentCodeServices = new  DocumentStatuService();
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadComboData();

                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    this.CreatedInfo.Visible = true;

                    var obj = this.transmittalService.GetById(new Guid(this.Request.QueryString["objId"]));
                    if (obj != null)
                    {
                        this.ddlProjectCode.SelectedValue = obj.ProjectId.ToString();
                        this.txtTransNo.Text = obj.TransNo;
                        this.txtDayIssued.SelectedDate = obj.TransDate;
                        this.txtDescription.Text = obj.Description;
                        if (obj.OriginatingOrganizationId.ToString() != "")
                        { this.ddlOriginatingOrganization.SelectedValue = obj.OriginatingOrganizationId.ToString(); }
                        if (obj.ReceivingOrganizationId.ToString() != "")
                        { this.ddlReceivingOrganization.SelectedValue = obj.ReceivingOrganizationId.ToString(); }
                        if (obj.ProjectId.ToString() != "")
                        { this.ddlProjectCode.SelectedValue = obj.ProjectId.ToString(); }

                        this.ddlGroup.SelectedValue = obj.GroupId.ToString();
                        this.txtDueDate.SelectedDate = obj.DueDate;
                       this.ddlPurpose.SelectedValue = obj.PurposeId.ToString();
                        this.txtFrom.Text = obj.FromValue;
                        this.txtTo.Text = obj.ToValue;
                        this.txtCC.Text = obj.CCValue;
                        this.txtSubject.Text = obj.Subject;
                        this.ddlTransmittedByName.SelectedValue = obj.TransmittedById.ToString();
                        this.txtTransmittedByDesignation.Text = obj.TransmittedByDesignation;
                       // this.ddlAcknowledgedByName.SelectedValue = obj.AcknowledgedId.ToString();
                       // this.txtAcknowledgedByDesignation.Text = obj.AcknowledgedByDesignation;
                        this.txtRemark.Text = obj.Remark;
                        //this.ddlForSend.SelectedValue = obj.ForSentId.ToString();
                        //foreach (RadTreeNode actionNode in this.rtvCCOrganisation.Nodes)
                        //{
                        //    actionNode.Checked = !string.IsNullOrEmpty(obj.CCOrganizationId) && obj.CCOrganizationId.Split(';').ToList().Contains(actionNode.Value);
                        //}

                        var createdUser = this.userService.GetByID(obj.CreatedBy.GetValueOrDefault());

                        this.lblCreated.Text = "Created at " + obj.CreatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (createdUser != null ? createdUser.FullName : string.Empty);

                        if (obj.LastUpdatedBy != null && obj.LastUpdatedDate != null)
                        {
                            this.lblCreated.Text += "<br/>";
                            var lastUpdatedUser = this.userService.GetByID(obj.LastUpdatedBy.GetValueOrDefault());
                            this.lblUpdated.Text = "Last modified at " + obj.LastUpdatedDate.GetValueOrDefault().ToString("dd/MM/yyyy hh:mm tt") + " by " + (lastUpdatedUser != null ? lastUpdatedUser.FullName : string.Empty);
                        }
                        else
                        {
                            this.lblUpdated.Visible = false;
                        }
                    }
                }
                else
                {
                    //this.RegenerateTransNo();
                    this.ddlProjectCode.SelectedValue = this.Request.QueryString["projId"];
                    this.CreatedInfo.Visible = false;

                    this.txtDayIssued.SelectedDate = DateTime.Now;
                    this.txtDueDate.SelectedDate = GetDate(5, this.txtDayIssued.SelectedDate.GetValueOrDefault());
                }
            }
        }

        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Page.IsValid)
            {
                if (!string.IsNullOrEmpty(this.Request.QueryString["objId"]))
                {
                    var objId = new Guid(this.Request.QueryString["objId"]);
                    var obj = this.transmittalService.GetById(objId);
                    if (obj != null)
                    {
                        this.CollectData(obj);

                        obj.LastUpdatedBy = UserSession.Current.User.Id;
                        obj.LastUpdatedByName = UserSession.Current.User.FullName;
                        obj.LastUpdatedDate = DateTime.Now;

                        this.transmittalService.Update(obj);
                    }
                }
                else
                {
                    var obj = new ContractorTransmittal();
                    obj.ID = Guid.NewGuid();
                    this.CollectData(obj);

                    obj.Status = "Missing Doc File";
                    obj.IsValid = false;
                    obj.IsSend = false;
                    obj.ErrorMessage = "Missing Attach Document File.";

                    obj.CreatedBy = UserSession.Current.User.Id;
                    obj.CreatedByName = UserSession.Current.User.FullName;
                    obj.CreatedDate = DateTime.Now;

                    // Create store folder
                    var physicalStoreFolder = Server.MapPath("../../DocumentLibrary/ContractorTransmittal/" + Utilities.Utility.RemoveSpecialCharacter(obj.TransNo) + "_" + obj.CreatedDate.GetValueOrDefault().ToString("ddMMyyyyHHmmss"));
                    Directory.CreateDirectory(physicalStoreFolder);
                    Directory.CreateDirectory(physicalStoreFolder + @"\eTRM File");


                    var serverStoreFolder = (HostingEnvironment.ApplicationVirtualPath == "/" 
                        ? string.Empty 
                        : HostingEnvironment.ApplicationVirtualPath) + "/DocumentLibrary/ContractorTransmittal/" + Utilities.Utility.RemoveSpecialCharacter(obj.TransNo) + "_" + obj.CreatedDate.GetValueOrDefault().ToString("ddMMyyyyHHmmss");
                    obj.StoreFolderPath = serverStoreFolder;
                    // --------------------------------------------------------------------------

                    this.transmittalService.Insert(obj);
                }

                this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CloseAndRebind();", true);
            }
        }

        private void CollectData(ContractorTransmittal obj)
        {
            obj.ProjectId = Convert.ToInt32(this.ddlProjectCode.SelectedValue);
            obj.ProjectName = (this.ddlProjectCode.SelectedItem.Text.Split(','))[0].Trim();
            obj.TransNo = this.txtTransNo.Text;
            obj.TransDate = this.txtDayIssued.SelectedDate;
            obj.DueDate = this.txtDueDate.SelectedDate;
            obj.Description = this.txtDescription.Text.Trim();
            obj.OriginatingOrganizationId = Convert.ToInt32(this.ddlOriginatingOrganization.SelectedValue);
            obj.OriginatingOrganizationName = (this.ddlOriginatingOrganization.SelectedItem.Text.Trim());
            obj.ReceivingOrganizationId = Convert.ToInt32(this.ddlReceivingOrganization.SelectedValue);
            obj.ReceivingOrganizationName = (this.ddlReceivingOrganization.SelectedItem.Text.Trim());
            obj.PurposeId = Convert.ToInt32(this.ddlPurpose.SelectedValue);
            obj.PurposeName = this.ddlPurpose.SelectedItem.Text;
            obj.FromValue = this.txtFrom.Text;
            obj.ToValue = this.txtTo.Text;
            obj.CCValue = this.txtCC.Text;
            //obj.IssuedDate = this.txtDayIssued.SelectedDate;

            //obj.GroupId = Convert.ToInt32(this.ddlGroup.SelectedValue);
           // obj.GroupCode = this.ddlGroup.SelectedItem.Text.Split(',')[0];
            obj.Subject = this.txtSubject.Text;
            obj.TransmittedById = this.ddlTransmittedByName.SelectedItem != null
                                    ? Convert.ToInt32(this.ddlTransmittedByName.SelectedValue)
                                    : 0;
            obj.TransmittedByName = this.ddlTransmittedByName.SelectedItem != null
                                    ? this.ddlTransmittedByName.SelectedItem.Text
                                    : string.Empty;
            obj.TransmittedByDesignation = this.txtTransmittedByDesignation.Text;

            //obj.AcknowledgedId = this.ddlAcknowledgedByName.SelectedItem != null
            //                        ? Convert.ToInt32(this.ddlAcknowledgedByName.SelectedValue)
            //                        : 0;
            //obj.AcknowledgedByName = this.ddlAcknowledgedByName.SelectedItem != null
            //                        ? this.ddlAcknowledgedByName.SelectedItem.Text
            //                        : string.Empty;
            //obj.AcknowledgedByDesignation = this.txtAcknowledgedByDesignation.Text;
            obj.Remark = this.txtRemark.Text;
            obj.Year = DateTime.Now.Year;
          //  obj.Sequence = Convert.ToInt32(this.txtTransNo.Text.Trim().Split('-')[4]);
          //  obj.SequenceString = this.txtTransNo.Text.Trim().Split('-')[4];
            obj.TypeId = 2;
            obj.ForSentId = Convert.ToInt32(this.ddlForSend.SelectedValue);
            obj.ForSentName = this.ddlForSend.SelectedItem.Text;

            //obj.CCOrganizationId = string.Empty;
            //obj.CCOrganizationName = string.Empty;
            //foreach (RadTreeNode actionNode in this.rtvCCOrganisation.CheckedNodes.Where(t => !string.IsNullOrEmpty(t.Value)))
            //{
            //    obj.CCOrganizationId += actionNode.Value + ";";
            //    obj.CCOrganizationName += actionNode.Text + Environment.NewLine;
            //}
        }

        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }

        /// <summary>
        /// The server validation file name is exist.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <exception cref="NotImplementedException">
        /// </exception>
        protected void ServerValidationFileNameIsExist(object source, ServerValidateEventArgs args)
        {
            if(this.txtTransNo.Text.Trim().Length == 0)
            {
                this.fileNameValidator.ErrorMessage = "Please enter transmittal number.";
                this.divFileName.Style["margin-bottom"] = "-26px;";
                args.IsValid = false;
            }
            
        }

        /// <summary>
        /// Load all combo data
        /// </summary>
        private void LoadComboData()
        {
            var groupList = this.groupCodeService.GetAll();
            this.ddlGroup.DataSource = groupList;
            this.ddlGroup.DataTextField = "FullName";
            this.ddlGroup.DataValueField = "ID";
            this.ddlGroup.DataBind();

            var projectList = this.projectService.GetAll();
            this.ddlProjectCode.DataSource = projectList.OrderBy(t => t.Code);
            this.ddlProjectCode.DataTextField = "FullName";
            this.ddlProjectCode.DataValueField = "ID";
            this.ddlProjectCode.DataBind();

            var organizationList = this.organizationCodeService.GetAll().OrderBy(t => t.Code);

            this.ddlOriginatingOrganization.DataSource = organizationList;
            this.ddlOriginatingOrganization.DataTextField = "FullName";
            this.ddlOriginatingOrganization.DataValueField = "Id";
            this.ddlOriginatingOrganization.DataBind();
            this.ddlOriginatingOrganization.SelectedValue = UserSession.Current.User.Role.ContractorId.ToString();

            this.ddlReceivingOrganization.DataSource = organizationList;
            this.ddlReceivingOrganization.DataTextField = "FullName";
            this.ddlReceivingOrganization.DataValueField = "Id";
            this.ddlReceivingOrganization.DataBind();

            if (organizationList.Any(t => t.IsDefaultReceiveContractorOutgoingTrans.GetValueOrDefault()))
            {
                this.ddlReceivingOrganization.SelectedValue = organizationList.FirstOrDefault(t => t.IsDefaultReceiveContractorOutgoingTrans.GetValueOrDefault()).ID.ToString();
            }

            this.rtvCCOrganisation.DataSource = organizationList;
            this.rtvCCOrganisation.DataTextField = "FullName";
            this.rtvCCOrganisation.DataValueField = "Id";
            this.rtvCCOrganisation.DataBind();

            var purposeList = this.documentCodeServices.GetAll();
            purposeList.Insert(0, new DocumentStatu() {Name=string.Empty });
            this.ddlPurpose.DataSource = purposeList.OrderBy(t => t.Name);
            this.ddlPurpose.DataTextField = "FullName";
            this.ddlPurpose.DataValueField = "ID";
            this.ddlPurpose.DataBind();

            var userList = this.userService.GetAll();
            var trasmittedUser = userList.Where(t => t.Role.ContractorId == Convert.ToInt32(this.ddlOriginatingOrganization.SelectedValue)).OrderBy(t => t.UserNameWithFullName).ToList();
            this.ddlTransmittedByName.DataSource = trasmittedUser;
            this.ddlTransmittedByName.DataTextField = "UserNameWithFullName";
            this.ddlTransmittedByName.DataValueField = "ID";
            this.ddlTransmittedByName.DataBind();
            this.ddlTransmittedByName.SelectedValue = UserSession.Current.User.Id.ToString();


            if (this.ddlTransmittedByName.SelectedItem != null)
            {
                var transmittedUser = this.userService.GetByID(Convert.ToInt32(this.ddlTransmittedByName.SelectedValue));
                if (transmittedUser != null)
                {
                    this.txtTransmittedByDesignation.Text = transmittedUser.Position;
                }
            }

            //var acknowledgedUserList = userList.Where(t => t.Role.ContractorId == Convert.ToInt32(this.ddlReceivingOrganization.SelectedValue)).OrderBy(t => t.UserNameWithFullName).ToList();
            //acknowledgedUserList.Insert(0, new User() {Id = 0});
            //this.ddlAcknowledgedByName.DataSource = acknowledgedUserList;
            //this.ddlAcknowledgedByName.DataTextField = "UserNameWithFullName";
            //this.ddlAcknowledgedByName.DataValueField = "ID";
            //this.ddlAcknowledgedByName.DataBind();

            //if (this.ddlAcknowledgedByName.SelectedItem != null)
            //{
            //    var acknowledgedUser = this.userService.GetByID(Convert.ToInt32(this.ddlAcknowledgedByName.SelectedValue));
            //    if (acknowledgedUser != null)
            //    {
            //        this.txtAcknowledgedByDesignation.Text = acknowledgedUser.Position;
            //    }
            //}

        }
        private DateTime GetDate(int day, DateTime transdate)
        {
            var actualDeadline = transdate;
            for (int i = 1; i <= day; i++)
            {
                actualDeadline = this.GetNextWorkingDay(actualDeadline);
            }
            return actualDeadline;
        }
        private bool IsWeekEnd(DateTime date)
        {
            return ConfigurationManager.AppSettings["WeekendWork"] == "false" ? date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday : false;
        }
        private DateTime GetNextWorkingDay(DateTime date)
        {
            do
            {
                date = date.AddDays(1);
            }
            while (IsWeekEnd(date));

            return date;
        }
        protected void ddl_RegenerateTransNo(object sender, EventArgs e)
        {
            

            // Rebind user list of Transmitted and Acknowledged
            var userList = this.userService.GetAll();
            var trasmittedUserList = userList.Where(t => t.Role.ContractorId == Convert.ToInt32(this.ddlOriginatingOrganization.SelectedValue)).OrderBy(t => t.UserNameWithFullName).ToList();

            this.ddlTransmittedByName.DataSource = trasmittedUserList;
            this.ddlTransmittedByName.DataTextField = "UserNameWithFullName";
            this.ddlTransmittedByName.DataValueField = "ID";
            this.ddlTransmittedByName.DataBind();

            if (this.ddlTransmittedByName.SelectedItem != null)
            {
                var transmittedUser = this.userService.GetByID(Convert.ToInt32(this.ddlTransmittedByName.SelectedValue));
                if (transmittedUser != null)
                {
                    this.txtTransmittedByDesignation.Text = transmittedUser.Position;
                }
            }

            //var acknowledgedUserList = userList.Where(t => t.Role.ContractorId == Convert.ToInt32(this.ddlReceivingOrganization.SelectedValue)).OrderBy(t => t.UserNameWithFullName).ToList();
            //this.ddlAcknowledgedByName.DataSource = acknowledgedUserList;
            //this.ddlAcknowledgedByName.DataTextField = "UserNameWithFullName";
            //this.ddlAcknowledgedByName.DataValueField = "ID";
            //this.ddlAcknowledgedByName.DataBind();

            //if (this.ddlAcknowledgedByName.SelectedItem != null)
            //{
            //    var acknowledgedUser = this.userService.GetByID(Convert.ToInt32(this.ddlAcknowledgedByName.SelectedValue));
            //    if (acknowledgedUser != null)
            //    {
            //        this.txtAcknowledgedByDesignation.Text = acknowledgedUser.Position;
            //    }
            //}

            // --------------------------------------------------------------------------------------
        }

        private void RegenerateTransNo()
        {
            var fromId = Convert.ToInt32(this.ddlOriginatingOrganization.SelectedValue);
            var fromObj = this.organizationCodeService.GetById(fromId);

            var toId = Convert.ToInt32(this.ddlReceivingOrganization.SelectedValue);
            var toObj = this.organizationCodeService.GetById(toId);

            this.txtTransNo.Text = this.ddlProjectCode.SelectedItem.Text.Split(',')[0] + "-X-";

            if (fromObj != null)
            {
                this.txtTransNo.Text += fromObj.Code + "-";
                this.txtFrom.Text = fromObj.HeadOffice;
            }

            if (toObj != null)
            {
                this.txtTransNo.Text += toObj.Code + "-";
                this.txtTo.Text = toObj.HeadOffice;
            }

            //this.txtTransNo.Text += DateTime.Now.Year.ToString().Substring(2, 2) + "-";
           // this.txtTransNo.Text += this.ddlGroup.SelectedItem.Text.Split(',')[0] + "-";

            var sequence = Utilities.Utility.ReturnSequenceString(this.transmittalService.GetCurrentSequenceByContractor(fromObj.ID), 4);
            this.txtTransNo.Text += sequence;
        }

        protected void ddlTransmittedByName_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var transmittedUser = this.userService.GetByID(Convert.ToInt32(this.ddlTransmittedByName.SelectedValue));
            if (transmittedUser != null)
            {
                this.txtTransmittedByDesignation.Text = transmittedUser.Position;
            }
        }

        //protected void ddlAcknowledgedByName_OnSelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var acknowledgedUser = this.userService.GetByID(Convert.ToInt32(this.ddlAcknowledgedByName.SelectedValue));
        //    if (acknowledgedUser != null)
        //    {
        //        this.txtAcknowledgedByDesignation.Text = acknowledgedUser.Position;
        //    }
        //}

        protected void ddlOriginatingOrganization_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.RegenerateTransNo();
            // Rebind user list of Transmitted and Acknowledged
            var userList = this.userService.GetAll();
            var trasmittedUserList = userList.Where(t => t.Role.ContractorId == Convert.ToInt32(this.ddlOriginatingOrganization.SelectedValue)).OrderBy(t => t.UserNameWithFullName).ToList();

            this.ddlTransmittedByName.DataSource = trasmittedUserList;
            this.ddlTransmittedByName.DataTextField = "UserNameWithFullName";
            this.ddlTransmittedByName.DataValueField = "ID";
            this.ddlTransmittedByName.DataBind();

            if (this.ddlTransmittedByName.SelectedItem != null)
            {
                var transmittedUser = this.userService.GetByID(Convert.ToInt32(this.ddlTransmittedByName.SelectedValue));
                if (transmittedUser != null)
                {
                    this.txtTransmittedByDesignation.Text = transmittedUser.Position;
                }
            }
        }

        protected void ddlReceivingOrganization_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.RegenerateTransNo();
            var userList = this.userService.GetAll();
            userList.Insert(0, new User() {Id = 0});
            var acknowledgedUserList = userList.Where(t => t.Role.ContractorId == Convert.ToInt32(this.ddlReceivingOrganization.SelectedValue)).OrderBy(t => t.UserNameWithFullName).ToList();
            //this.ddlAcknowledgedByName.DataSource = acknowledgedUserList;
            //this.ddlAcknowledgedByName.DataTextField = "UserNameWithFullName";
            //this.ddlAcknowledgedByName.DataValueField = "ID";
            //this.ddlAcknowledgedByName.DataBind();

            //if (this.ddlAcknowledgedByName.SelectedItem != null)
            //{
            //    var acknowledgedUser = this.userService.GetByID(Convert.ToInt32(this.ddlAcknowledgedByName.SelectedValue));
            //    if (acknowledgedUser != null)
            //    {
            //        this.txtAcknowledgedByDesignation.Text = acknowledgedUser.Position;
            //    }
            //}
        }
    }
}