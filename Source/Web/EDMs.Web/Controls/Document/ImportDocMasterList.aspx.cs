﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerEditForm.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The customer edit form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using EDMs.Business.Services.Scope;
using EDMs.Web.Utilities;

namespace EDMs.Web.Controls.Document
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Web.UI;

    using Aspose.Cells;

    using EDMs.Business.Services.Document;
    using EDMs.Business.Services.Library;
    using EDMs.Business.Services.Security;
    using EDMs.Data.Entities;
    using EDMs.Web.Utilities.Sessions;

    using Telerik.Web.UI;

    /// <summary>
    /// The customer edit form.
    /// </summary>
    public partial class ImportDocMasterList : Page
    {
        private readonly RevisionService revisionService;

        private readonly DocumentTypeService documentTypeService;

        private readonly DisciplineService disciplineService;

        private readonly DocumentService documentService;

        private readonly OriginatorService originatorService;

        private readonly DocumentPackageService documentPackageService;

        private readonly ProjectCodeService scopeProjectService;

        private readonly DocumentSequenceManagementService documentSequenceManagementService;

        private readonly DocumentNumberingService documentNumberingService;

        private readonly PhaseService phaseService;

        private readonly AreaService areaService;

        private readonly SubAreaService subAreaService;

        private readonly DrawingDetailCodeService drawingDetailCodeService;

        private readonly DrawingCodeService drawingCodeService;

        private readonly SystemCodeService systemCodeService;

        private readonly ContractorService contractorService;

        private readonly CodeService codeService;

        public ImportDocMasterList()
        {
            this.revisionService = new RevisionService();
            this.documentTypeService = new DocumentTypeService();
            this.disciplineService = new DisciplineService();
            this.documentService = new DocumentService();
            this.originatorService = new OriginatorService();
            this.documentPackageService = new DocumentPackageService();
            this.scopeProjectService = new  ProjectCodeService();
            this.documentSequenceManagementService = new DocumentSequenceManagementService();
            this.documentNumberingService = new DocumentNumberingService();

            this.phaseService = new PhaseService();
            this.areaService = new AreaService();
            this.subAreaService = new SubAreaService();
            this.drawingDetailCodeService = new DrawingDetailCodeService();
            this.drawingCodeService = new DrawingCodeService();
            this.systemCodeService = new SystemCodeService();
            this.contractorService = new ContractorService();
            this.codeService = new CodeService();
        }


        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                
            }
        }


        /// <summary>
        /// The btn cap nhat_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            var currentSheetName = string.Empty;
            var currentDocumentNo = string.Empty;
            var documentPackageList = new List<DocumentPackage>();
            var invalidNumberingFormatList = new List<DocumentPackage>();
            try
            {
                foreach (UploadedFile docFile in this.docuploader.UploadedFiles)
                {
                    var extension = docFile.GetExtension();
                    if (extension == ".xls" || extension == ".xlsx" || extension == ".xlsm")
                    {
                        var importPath = Server.MapPath("../../Import") + "/" + DateTime.Now.ToString("ddMMyyyyhhmmss") + "_" + docFile.FileName;
                        docFile.SaveAs(importPath);

                        var workbook = new Workbook();
                        workbook.Open(importPath);

                        for (var i = 1; i < workbook.Worksheets.Count; i++)
                        {
                            var datasheet = workbook.Worksheets[i];

                            currentSheetName = datasheet.Name;
                            currentDocumentNo = string.Empty;

                            var projectID = !string.IsNullOrEmpty(datasheet.Cells["A1"].Value.ToString())
                                ? Convert.ToInt32(datasheet.Cells["A1"].Value.ToString())
                                : 0;
                            var projectObj = this.scopeProjectService.GetById(projectID);
                            if (projectObj != null)
                            {
                                var documentNumberingList = this.documentNumberingService.GetAllByProject(projectID);
                                var disciplineID = !string.IsNullOrEmpty(datasheet.Cells["A2"].Value.ToString())
                                    ? Convert.ToInt32(datasheet.Cells["A2"].Value.ToString())
                                    : 0;
                                //var disciplineObj = this.disciplineService.GetById(disciplineID);
                                //if (disciplineObj != null)
                                //{
                                    var dataTable = datasheet.Cells.ExportDataTable(5, 1, datasheet.Cells.MaxRow - 1, 31);
                                    foreach (DataRow dataRow in dataTable.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(dataRow["Column2"].ToString()))
                                        {
                                            currentDocumentNo = dataRow["Column2"].ToString();
                                            DocumentNumbering documentNumbering = new DocumentNumbering();
                                            //---------------------------------------------
                                            var partOfFileName = currentDocumentNo.Replace(" ", string.Empty).Split('-');
                                            if (partOfFileName.Length > 3)
                                            {
                                                var lenghtSegment = partOfFileName.Length;

                                                var segmentEnd = partOfFileName[lenghtSegment - 1].Length;
                                                //if (segmentEnd.Length == 3)
                                                //{
                                                //    documentNumbering = documentNumberingList.Find(t => t.Lenght == lenghtSegment && t.TypeID == 2);
                                                //}
                                                //else if (segmentEnd.Length == 4)
                                                //{
                                                //    documentNumbering = documentNumberingList.Find(t => t.Lenght == lenghtSegment && t.TypeID == 1);
                                                //}
                                                foreach(var fromat in documentNumberingList)
                                                { if (documentNumbering.Name == null)
                                                    {
                                                        var fromat_lenghtSegment = fromat.Format.Split('-').Length;

                                                        var fromat_segmentEnd = fromat.Format.Split('-')[fromat_lenghtSegment - 1].Length;
                                                        if(fromat_lenghtSegment== lenghtSegment && segmentEnd== fromat_segmentEnd) { documentNumbering = fromat; }
                                                    }
                                                }
                                               

                                            }
                                            var revObj = this.revisionService.GetByName(dataRow["Column4"].ToString(), projectObj.ID);
                                            var weight = !string.IsNullOrEmpty(dataRow["Column24"].ToString())
                                                ? (double?)
                                                    Math.Round(Convert.ToDouble(dataRow["Column24"].ToString()) * 100, 2)
                                                : null;
                                           
                                            var docObj = new DocumentPackage();
                                            docObj.ID = Guid.NewGuid();
                                            docObj.ProjectId = projectObj.ID;
                                            docObj.ProjectName = projectObj.Name;
                                            docObj.ProjectFullName = projectObj.FullName;
                                            docObj.Manhours = !string.IsNullOrEmpty(dataRow["Column23"].ToString()) ? Convert.ToDouble(dataRow["Column23"]) : 0;
                                            docObj.DocNo = dataRow["Column2"].ToString().Replace(" ", string.Empty);
                                            docObj.DocTitle = dataRow["Column3"].ToString();

                                            docObj.RevisionId = revObj != null ? revObj.ID : 0;
                                            docObj.RevisionName = revObj != null ? revObj.Name : dataRow["Column4"].ToString();

                                            //docObj.DisciplineId = disciplineObj.ID;
                                            //docObj.DisciplineName = disciplineObj.Name;
                                            //docObj.DisciplineFullName = disciplineObj.FullName;

                                            var sartplan = dataRow["Column5"].ToString();
                                            var firstIssuePlanDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(sartplan, ref firstIssuePlanDate))
                                            {
                                                docObj.StartPlan = firstIssuePlanDate;
                                            }

                                            var sartrevised = dataRow["Column6"].ToString();
                                            var finalIssuePlanDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(sartrevised, ref finalIssuePlanDate))
                                            {
                                                docObj.StartRevised = finalIssuePlanDate;
                                            }
                                            var sartactual = dataRow["Column7"].ToString();
                                            var StartActual = new DateTime();
                                            if (Utility.ConvertStringToDateTime(sartactual, ref StartActual))
                                            {
                                                docObj.StartActual = StartActual;
                                            }

                                            var IDCplan = dataRow["Column8"].ToString();
                                            var IDCPlanDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IDCplan, ref IDCPlanDate))
                                            {
                                                docObj.IDCPlan = IDCPlanDate;
                                            }

                                            var IDCrevised = dataRow["Column9"].ToString();
                                            var IDCRevisedDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IDCrevised, ref IDCRevisedDate))
                                            {
                                                docObj.IDCRevised = IDCRevisedDate;
                                            }
                                            var IDCActual = dataRow["Column10"].ToString();
                                            var IDCActualDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IDCActual, ref IDCActualDate))
                                            {
                                                docObj.IDCACtual = IDCActualDate;
                                            }

                                            var IFRplan = dataRow["Column11"].ToString();
                                            var IFRPlanDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IFRplan, ref IFRPlanDate))
                                            {
                                                docObj.IFRPlan = IFRPlanDate;
                                            }

                                            var IFRrevised = dataRow["Column12"].ToString();
                                            var IFRrevisedDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IFRrevised, ref IFRrevisedDate))
                                            {
                                                docObj.IFRRevised = IFRrevisedDate;
                                            }

                                            var IFRactual = dataRow["Column13"].ToString();
                                            var IFRactualDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IFRactual, ref IFRactualDate))
                                            {
                                                docObj.IFRActual = IFRactualDate;
                                            }

                                            var reIFRplan = dataRow["Column14"].ToString();
                                            var reIFRPlanDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(reIFRplan, ref reIFRPlanDate))
                                            {
                                                docObj.ReIFRPlan = reIFRPlanDate;
                                            }

                                            var reifrrevised = dataRow["Column15"].ToString();
                                            var ReifrrevisedDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(reifrrevised, ref ReifrrevisedDate))
                                            {
                                                docObj.ReIFRRevised = ReifrrevisedDate;
                                            }
                                            var reifractual = dataRow["Column16"].ToString();
                                            var ReifractualDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(reifractual, ref ReifractualDate))
                                            {
                                                docObj.ReIFRActual = ReifractualDate;
                                            }
                                            var IFAplan = dataRow["Column17"].ToString();
                                            var IFAPlanDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IFAplan, ref IFAPlanDate))
                                            {
                                                docObj.IFAPlan = IFAPlanDate;
                                            }

                                            var IFARevised = dataRow["Column18"].ToString();
                                            var IFAreviseddate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IFARevised, ref IFAreviseddate))
                                            {
                                                docObj.IFARevised = IFAreviseddate;
                                            }
                                            var IFAactual = dataRow["Column19"].ToString();
                                            var IFAactualdate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(IFAactual, ref IFAactualdate))
                                            {
                                                docObj.IFAActual = IFAactualdate;
                                            }
                                            //AFC
                                            var AFCplan = dataRow["Column20"].ToString();
                                            var AFCPlanDate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(AFCplan, ref AFCPlanDate))
                                            {
                                                docObj.AFCPlan = AFCPlanDate;
                                            }
                                            var AFCRevised = dataRow["Column21"].ToString();
                                            var AFCreviseddate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(AFCRevised, ref AFCreviseddate))
                                            {
                                                docObj.AFCRevised = AFCreviseddate;
                                            }
                                            var AFCactual = dataRow["Column22"].ToString();
                                            var AFCactualdate = new DateTime();
                                            if (Utility.ConvertStringToDateTime(AFCactual, ref AFCactualdate))
                                            {
                                                docObj.AFCActual = AFCactualdate;
                                            }

                                            docObj.Weight = weight;
                                            docObj.Remarks = dataRow["Column25"].ToString();
                                            docObj.KDN_markup = dataRow["Column26"].ToString().Trim().ToUpper();
                                            docObj.DNV_markup = dataRow["Column27"].ToString().Trim().ToUpper();
                                            docObj.NCS_markup = dataRow["Column28"].ToString().Trim().ToUpper();
                                            docObj.StatusID = 0;
                                            docObj.StatusName = string.Empty;
                                            docObj.StatusFullName = string.Empty;

                                            docObj.CreatedBy = UserSession.Current.User.Id;
                                            docObj.CreatedDate = DateTime.Now;
                                            docObj.IsLeaf = true;
                                            docObj.IsDelete = false;
                                            docObj.IsEMDR = true;

                                            // Detect document element base on Document numbering
                                            if (documentNumbering != null)
                                            {
                                                if (documentNumbering.Format.Split('-').Length == docObj.DocNo.Split('-').Length)
                                                {
                                                    var docNoFormatPart = documentNumbering.Format.Split('-');
                                                    var docNoPart = docObj.DocNo.Split('-');
                                                    for (int j = 0; j < docNoFormatPart.Length; j++)
                                                    {
                                                        switch (docNoFormatPart[j])
                                                        {
                                                            case "AAAA":
                                                                break;
                                                            case "BBB":
                                                                var contractorObj = this.contractorService.GetByName(docNoPart[j]);
                                                                docObj.ContractorId = contractorObj != null ? contractorObj.ID : 0;
                                                                docObj.ContractorName = contractorObj != null ? contractorObj.Name : string.Empty;
                                                                docObj.ContractorFullName = contractorObj != null ? contractorObj.Name+", "+ contractorObj.Description : string.Empty;
                                                                break;
                                                            case "CC":
                                                                var phaseObj = this.phaseService.GetByName(docNoPart[j], projectID);
                                                                docObj.PhaseId = phaseObj != null ? phaseObj.ID : 0;
                                                                docObj.PhaseName = phaseObj != null ? phaseObj.Name : string.Empty;
                                                                docObj.PhaseFullName = phaseObj != null ? phaseObj.FullName : string.Empty;
                                                                break;
                                                            case "D":
                                                                var areaObj = this.areaService.GetByName(docNoPart[j], projectID);
                                                                docObj.AreaId = areaObj != null ? areaObj.ID : 0;
                                                                docObj.AreaName = areaObj != null ? areaObj.Name : string.Empty;
                                                                docObj.AreaFullName = areaObj != null ? areaObj.FullName : string.Empty;
                                                                break;
                                                            case "EE":
                                                                var subAreaObj = this.subAreaService.GetByName(docNoPart[j], projectID, docObj.AreaId.GetValueOrDefault());
                                                                docObj.SubAreaId = subAreaObj != null ? subAreaObj.ID : 0;
                                                                docObj.SubAreaName = subAreaObj != null ? subAreaObj.Name : string.Empty;
                                                                docObj.SubAreaFullName = subAreaObj != null ? subAreaObj.FullName : string.Empty;
                                                                break;
                                                            case "FFF":
                                                                var originatorObj = this.originatorService.GetByName(docNoPart[j], projectID);
                                                                docObj.OriginatorId = originatorObj != null ? originatorObj.ID : 0;
                                                                docObj.OriginatorName = originatorObj != null ? originatorObj.Name : string.Empty;
                                                                docObj.OriginatorFullName = originatorObj != null ? originatorObj.FullName : string.Empty;
                                                                break;
                                                            case "GG":
                                                           var disciplineObj = this.disciplineService.GetByName(docNoPart[j]);
                                                            docObj.DisciplineId = disciplineObj.ID;
                                                            docObj.DisciplineName = disciplineObj.Name;
                                                            docObj.DisciplineFullName = disciplineObj.FullName;
                                                            break;
                                                            case "HH":
                                                                var docTypeObj = this.documentTypeService.GetByCode(docNoPart[j]);
                                                                docObj.DocumentTypeId = docTypeObj != null ? docTypeObj.ID : 0;
                                                                docObj.DocumentTypeName = docTypeObj != null ? docTypeObj.Code : string.Empty;
                                                                docObj.DocTypeFullName = docTypeObj != null ? docTypeObj.FullName : string.Empty;
                                                                break;
                                                            case "K":
                                                                var codeObj = this.codeService.GetByName(docNoPart[j], projectID);
                                                                docObj.CodeId = codeObj != null ? codeObj.ID : 0;
                                                                docObj.CodeName = codeObj != null ? codeObj.Name : string.Empty;
                                                                docObj.CodeFullName = codeObj != null ? codeObj.FullName : string.Empty;
                                                                break;
                                                            case "LM":
                                                            case "L":
                                                                if (docNoPart[j].Length == 1)
                                                                {
                                                                    var drawingCodeObj = this.drawingCodeService.GetByName(docNoPart[j], projectID);
                                                                    docObj.DrawingCodeId = drawingCodeObj != null ? drawingCodeObj.ID : 0;
                                                                    docObj.DrawingCodeName = drawingCodeObj != null ? drawingCodeObj.Name : string.Empty;
                                                                    docObj.DrawingCodeFullName = drawingCodeObj != null ? drawingCodeObj.FullName : string.Empty;
                                                                }
                                                                else
                                                                {
                                                                 var stL = docNoPart[j].Substring(0, 1);
                                                                var drawingCodeObj_ = this.drawingCodeService.GetByName(stL, projectID);
                                                                docObj.DrawingCodeId = drawingCodeObj_ != null ? drawingCodeObj_.ID : 0;
                                                                docObj.DrawingCodeName = drawingCodeObj_ != null ? drawingCodeObj_.Name : string.Empty;
                                                                docObj.DrawingCodeFullName = drawingCodeObj_ != null ? drawingCodeObj_.FullName : string.Empty;
                                                                var stM = docNoPart[j].Substring(1, 1);
                                                                var stMDetailCodeObj = this.drawingDetailCodeService.GetByName(stM, projectID);
                                                                docObj.DrawingDetailCodeId = stMDetailCodeObj != null ? stMDetailCodeObj.ID : 0;
                                                                docObj.DrawingDetailCodeName = stMDetailCodeObj != null ? stMDetailCodeObj.Name : string.Empty;
                                                                docObj.DrawingDetailCodeFullName = stMDetailCodeObj != null ? stMDetailCodeObj.FullName : string.Empty;
                                                                }
                                                               
                                                                break;
                                                            //case "L":
                                                            //    var drawingCodeObj = this.drawingCodeService.GetByName(docNoPart[j], projectID);
                                                            //    docObj.DrawingCodeId = drawingCodeObj != null ? drawingCodeObj.ID : 0;
                                                            //    docObj.DrawingCodeName = drawingCodeObj != null ? drawingCodeObj.Name : string.Empty;
                                                            //    docObj.DrawingCodeFullName = drawingCodeObj != null ? drawingCodeObj.FullName : string.Empty;
                                                            //    break;
                                                            case "M":
                                                                var drawingDetailCodeObj = this.drawingDetailCodeService.GetByName(docNoPart[j], projectID);
                                                                docObj.DrawingDetailCodeId = drawingDetailCodeObj != null ? drawingDetailCodeObj.ID : 0;
                                                                docObj.DrawingDetailCodeName = drawingDetailCodeObj != null ? drawingDetailCodeObj.Name : string.Empty;
                                                                docObj.DrawingDetailCodeFullName = drawingDetailCodeObj != null ? drawingDetailCodeObj.FullName : string.Empty;
                                                                break;
                                                            case "NNOO":
                                                               
                                                                    var systemCodeObj = this.systemCodeService.GetByName(docNoPart[j].Substring(0, 2), projectID);
                                                                    docObj.SystemCodeId = systemCodeObj != null ? systemCodeObj.ID : 0;
                                                                    docObj.SystemCodeName = systemCodeObj != null ? systemCodeObj.Name : string.Empty;
                                                                    docObj.SystemCodeFullName = systemCodeObj != null ? systemCodeObj.FullName : string.Empty;
                                                                    docObj.SequencetialNumber = docNoPart[j].Substring(2, 2);
                                                                break;
                                                            case "OOO":
                                                            case "OO":
                                                                docObj.SequencetialNumber = docNoPart[j];
                                                                break;
                                                        }
                                                    }

                                                    documentPackageList.Add(docObj);
                                                }
                                                else
                                                {
                                                    invalidNumberingFormatList.Add(docObj);
                                                }
                                            }
                                            else
                                            {
                                                invalidNumberingFormatList.Add(docObj);
                                            }
                                        }
                                    }
                                //}
                                //else
                                //{
                                //    this.blockError.Visible = true;
                                //    this.lblError.Text =
                                //        "Have Error: Project '" + projectObj.Name + "' don't have Discipline '" +
                                //        currentSheetName + "'. Please remove this sheet out of master file.";
                                //}



                            }
                            else
                            {
                                this.blockError.Visible = true;
                                this.lblError.Text =
                                    "Master list file is invalid. Can't find the project to import!";
                            }
                        }

                        // Show documents invalid document number format
                        if (invalidNumberingFormatList.Count > 0)
                        {
                            this.blockError.Visible = true;
                            this.lblError.Text = "Some documents are invalid document numbering format: </br>";
                            foreach (var documentPackage in invalidNumberingFormatList)
                            {
                                this.lblError.Text += documentPackage.DocNo + "</br>";
                            }

                            this.lblError.Text += "Please re-check and import again.";
                        }
                        // --------------------------------------------------------------------------------------

                        // Insert document if don't have any error
                        if (string.IsNullOrEmpty(this.lblError.Text))
                        {
                            foreach (var docObj in documentPackageList)
                            {
                                this.documentPackageService.Insert(docObj);
                            }

                            this.blockError.Visible = true;
                            this.lblError.Text =
                                "Data of document master list file is valid. System import successfull!";
                        }
                        // ---------------------------------------------------------------------------------------
                    }
                }
            }
            catch (Exception ex)
            {
                this.blockError.Visible = true;
                this.lblError.Text = "Have error at sheet: '" + currentSheetName + "', document: '" + currentDocumentNo + "', with error: '" + ex.Message + "'";
            }
        }


        /// <summary>
        /// The btncancel_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void btncancel_Click(object sender, EventArgs e)
        {
            this.ClientScript.RegisterStartupScript(this.Page.GetType(), "mykey", "CancelEdit();", true);
        }
    }
}