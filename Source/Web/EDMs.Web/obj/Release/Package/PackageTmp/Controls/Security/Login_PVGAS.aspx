﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login_PVGAS.aspx.cs" Inherits="EDMs.Web.Controls.Security.Login_PVGAS" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<!DOCTYPE html>
<!-- saved from url=(0108)http://backup-server.local/ttconfigsvc/spfauthentication/oauth/login?signin=927e2d821274c03b992d8c11cb07d3d1 -->
<html> <%--ng-app="app" ng-controller="LayoutCtrl" class="ng-scope">--%>
<head runat="server"> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}
</style>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../../CSS/Login_1.css" rel="stylesheet" type="text/css" />
     <link href="../../CSS/Ess-all_1.css" rel="stylesheet" type="text/css" />
     <link href="../../CSS/Ess-all_2.css" rel="stylesheet" type="text/css" />
     <link href="../../CSS/Ess-all_3.css" rel="stylesheet" type="text/css" />
   
   
</head>
<body style="overflow:hidden;">
    <div class="hi-wrapper">
        <div class="hi-branding"></div>
  

    <div style=" height:100%; width: 50%; " >
     
        <div class="row" style=" padding-top:30px; height:93%; padding-left:20px;">
            <div class="col-md-12 col-sm-12" ng-show="model.loginUrl">
                <div class="panel panel-heading" style=" padding-bottom:50px;height:20%; padding-top:20px; padding-left:10px;">
                      <div class="x-component x-box-item x-component-default" id="box-1013" style="right: auto; left: 0px; padding-bottom:10px; padding-left:20px; top: 0px; margin: 0px; height: 179px; width: 357px;"><div class="logo-login"><a class="logo"><img src="../../Images/logo_new.gif" alt=""></a></div></div>
                </div>
             
               <div class="panel-body" style=" height:70%; padding-left:100px; ">
            <form id="frmLogin" runat="server"  method="post" class="ng-pristine ng-invalid ng-invalid-required">
                  <input type="hidden" name="idsrv.xsrf" value="j3zUNhPiO28hmp19AD6gHnS7cHNwxHS46ypUJylGayLqjHX6PbGOLd5BC9NtmBgaBl84SxCmZs_hklmC4y9wr-jcpmRjdRNKUbyoJrRmtR8" token="model.antiForgery" class="ng-isolate-scope">
                <telerik:RadScriptManager ID="RadScriptManager2" runat="server"></telerik:RadScriptManager>
            <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel2" />
                 <div >
                          <h3 style="    font-size: 40px;   margin-left:-100px;  text-align: center; padding-bottom:20px;    color: #0093d9;    text-transform: uppercase;">Quản lý tài liệu</h3>
              
                    </div>
                <div style="padding-left: 13%;">
                      <div style="padding-bottom:20px;">
                                    <label for="username"  style="color: #333; width:100px; float:left; margin-left:0px;  font: 400 12px/17px helvetica, arial, verdana, sans-serif;    min-height: 28px;    padding-top: 6px;    padding-right: 5px;">Tên đăng nhập</label>
                          <div style="float: left; padding-left:30px; padding-top: 1px; font: 400 12px/17px helvetica, arial, verdana, sans-serif; ">
                            <asp:TextBox ID="txtUsername" Width="316px"  runat="server"  CssClass="form-control ng-dirty ng-valid ng-valid-required" onBlur="if(this.value=='')this.value='User name'" onFocus="if(this.value=='User name')this.value='' "></asp:TextBox></div> 
                    <div style="clear: both; font-size: 0;"></div>
                       </div>
                                <div class="form-group">
                               
                     <label for="username" style="color: #333; width:100px; float:left; margin-left:0px;  font: 400 12px/17px helvetica, arial, verdana, sans-serif;    min-height: 28px;    padding-top: 6px;    padding-right: 5px;">Mật khẩu</label>
                          <div style="float: left; padding-left:30px; padding-top: 1px; font: 400 12px/17px helvetica, arial, verdana, sans-serif; ">
                            <asp:TextBox ID="txtPassword"  Width="316px" runat="server" TextMode="Password" CssClass="form-control ng-dirty ng-valid ng-valid-required" onBlur="if(this.value=='')this.value='Password'" onFocus="if(this.value=='Password')this.value='' "></asp:TextBox></div> 
                    <div style="clear: both; font-size: 0;"></div>
                       </div>
                
                            <div >
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUsername" ErrorMessage="Please enter user name." />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPassword" ErrorMessage="Please enter password." />
                                <asp:Label ID="lblMessage" runat="server" CssClass="login-error"></asp:Label>
                            </div>
                         <div >
                              <label for="username" style="color: #333; width:100px; float:left; margin-left:0px;  font: 400 12px/17px helvetica, arial, verdana, sans-serif;    min-height: 28px;    padding-top: 6px;    padding-right: 5px;"></label>
                             <div style="float: left; padding-left:30px; padding-top: 1px; font: 400 12px/17px helvetica, arial, verdana, sans-serif; ">
                            <asp:Button ID="btnLogin" runat="server" Text="Đăng nhập"  CssClass="btn btn-primary"  OnClick="btnLogin_Click" OnClientClick="GetInfor()"/></div> 
                    <div style="clear: both; font-size: 0;"></div>
                        </div>
                               
    </div>
      
    <asp:HiddenField runat="server" ID="Browser"/>
    <asp:HiddenField runat="server" ID="TimeZone"/>
    <asp:HiddenField runat="server" ID="LocalTime"/>
    <asp:HiddenField runat="server" ID="LocalDate"/>
    <asp:HiddenField runat="server" ID="Domain"/>
    <asp:HiddenField runat="server" ID="Hostname"/>
    <asp:HiddenField runat="server" ID="Memory"/>
    <asp:HiddenField runat="server" ID="Os"/>
    <asp:HiddenField runat="server" ID="IpAddress"/>
     <telerik:RadScriptBlock runat="server">
           <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>  
          <script type="text/javascript">
                function pageLoad() {
                    GetInfor();
                }

                function GetInfor() {
                    var getbrowser = "";
                    var osystem = "";
                  
                    var offset = new Date().getTimezoneOffset(), o = Math.abs(offset);
                    offset = (offset < 0 ? "+" : "-") + ("00" + Math.floor(o / 60)).slice(-2) + ":" + ("00" + (o % 60)).slice(-2);
                    document.getElementById("<%= TimeZone.ClientID %>").value = offset;
                    var year = new Date().getFullYear();
                    var month = new Date().getMonth()+1;
                    var day = new Date().getDate();
                    var hh = new Date().getHours();
                    var m = new Date().getMinutes();
                    var s = new Date().getSeconds();
                    var ss = new Date().getMilliseconds();
                    document.getElementById("<%= LocalTime.ClientID %>").value = hh+ ":" +m+ ":" + s+ ":" +ss ;
                    document.getElementById("<%= LocalDate.ClientID %>").value = day + "/" + month + "/" + year ;
                    var unknown = '-';
                    var nVer = navigator.appVersion;
                    var nAgt = navigator.userAgent;
                    var browserName = navigator.appName;
                    var fullVersion = '' + parseFloat(navigator.appVersion);
                    var majorVersion = parseInt(navigator.appVersion, 10);
                    var nameOffset, verOffset, ix;

                    // In Opera, the true version is after "Opera" or after "Version"
                    if ((verOffset = nAgt.indexOf("Opera")) != -1) {
                        browserName = "Opera";
                        fullVersion = nAgt.substring(verOffset + 6);
                        if ((verOffset = nAgt.indexOf("Version")) != -1)
                            fullVersion = nAgt.substring(verOffset + 8);
                    }
                        // In MSIE, the true version is after "MSIE" in userAgent
                    else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
                        browserName = "Microsoft Internet Explorer";
                        fullVersion = nAgt.substring(verOffset + 5);
                    }
                    else if ((verOffset = nAgt.indexOf("coc_coc_browser")) != -1) {
                        browserName = "coc_coc";
                        fullVersion = nAgt.substring(verOffset + 16);
                    }
                        // In Chrome, the true version is after "Chrome" 
                    else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
                        browserName = "Chrome";
                        fullVersion = nAgt.substring(verOffset + 7);
                    }
                        // In Safari, the true version is after "Safari" or after "Version" 
                    else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
                        browserName = "Safari";
                        fullVersion = nAgt.substring(verOffset + 7);
                        if ((verOffset = nAgt.indexOf("Version")) != -1)
                            fullVersion = nAgt.substring(verOffset + 8);
                    }
                        // In Firefox, the true version is after "Firefox" 
                    else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
                        browserName = "Firefox";
                        fullVersion = nAgt.substring(verOffset + 8);
                    }
                        // In most other browsers, "name/version" is at the end of userAgent 
                    else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                        browserName = nAgt.substring(nameOffset, verOffset);
                        fullVersion = nAgt.substring(verOffset + 1);
                        if (browserName.toLowerCase() == browserName.toUpperCase()) {
                            browserName = navigator.appName;
                        }
                    }
                    // trim the fullVersion string at semicolon/space if present
                    if ((ix = fullVersion.indexOf(";")) != -1)
                        fullVersion = fullVersion.substring(0, ix);
                    if ((ix = fullVersion.indexOf(" ")) != -1)
                        fullVersion = fullVersion.substring(0, ix);

                    majorVersion = parseInt('' + fullVersion, 10);
                    if (isNaN(majorVersion)) {
                        fullVersion = '' + parseFloat(navigator.appVersion);
                        majorVersion = parseInt(navigator.appVersion, 10);
                    }
                    getbrowser = browserName + " (" + fullVersion + ")";


                    var os = unknown;
                    var clientStrings = [
                        { s: 'Windows 10', r: /(Windows 10.0|Windows NT 10.0)/ },
                        { s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/ },
                        { s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/ },
                        { s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/ },
                        { s: 'Windows Vista', r: /Windows NT 6.0/ },
                        { s: 'Windows Server 2003', r: /Windows NT 5.2/ },
                        { s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/ },
                        { s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/ },
                        { s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/ },
                        { s: 'Windows 98', r: /(Windows 98|Win98)/ },
                        { s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/ },
                        { s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/ },
                        { s: 'Windows CE', r: /Windows CE/ },
                        { s: 'Windows 3.11', r: /Win16/ },
                        { s: 'Android', r: /Android/ },
                        { s: 'Open BSD', r: /OpenBSD/ },
                        { s: 'Sun OS', r: /SunOS/ },
                        { s: 'Linux', r: /(Linux|X11)/ },
                        { s: 'iOS', r: /(iPhone|iPad|iPod)/ },
                        { s: 'Mac OS X', r: /Mac OS X/ },
                        { s: 'Mac OS', r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/ },
                        { s: 'QNX', r: /QNX/ },
                        { s: 'UNIX', r: /UNIX/ },
                        { s: 'BeOS', r: /BeOS/ },
                        { s: 'OS/2', r: /OS\/2/ },
                        { s: 'Search Bot', r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/ }
                    ];
                    for (var id in clientStrings) {
                        var cs = clientStrings[id];
                        if (cs.r.test(nAgt)) {
                            os = cs.s;
                            break;
                        }
                    }

                    var osVersion = unknown;

                    if (/Windows/.test(os)) {
                        osVersion = /Windows (.*)/.exec(os)[1];
                        os = 'Windows';
                    }

                    switch (os) {
                        case 'Mac OS X':
                            osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                            break;

                        case 'Android':
                            osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                            break;

                        case 'iOS':
                            osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                            osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                            break;
                    }

                    osystem = os + ' ' + osVersion;

                    document.getElementById("<%= Os.ClientID %>").value = osystem;
                    document.getElementById("<%= Browser.ClientID %>").value = getbrowser;
                   <%-- document.getElementById("<%= Hostname.ClientID %>").value = Environment.MachineNam;--%>
                    
                  
                    getIPAddress();
                    }

              var getIPAddress = function () {
                  $.getJSON("https://jsonip.com?callback=?", function (data) {
                      document.getElementById("<%= IpAddress.ClientID %>").value = data.ip;   
                  });
              };
            
                 </script>
        </telerik:RadScriptBlock>
            </form>
        </div>
               
            </div>
      <ul class="list-unstyled">
                       
                    </ul>
                </div>
          <div class="panel-footer" style="height:8%" >
                    <div id="footer-page-1021-innerCt" data-ref="innerCt" role="presentation" class="x-box-inner" style="width: 100%; align-content:center; text-align:center; height: 50px;"><div id="footer-page-1021-targetEl" data-ref="targetEl" class="x-box-target" role="presentation" style="width: 100%;"><label class="x-component x-box-item x-component-default" id="label-1022" for="" style="right: auto; text-align:center; left:unset !important;   top: 17px; margin: 0px;"><span style="font-weight: normal !important;">Copyright © 2019 </span></label></div></div>
                </div>   
            </div>
        
     
    </div>


      
           
    <div id="dummy"></div>
</body>
</html>
