﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Viewer.aspx.cs" Inherits="EDMs.Web.Viewer" %>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.Examples" Assembly="GleamTech.Common" %>
<%@ Register TagPrefix="GleamTech" Namespace="GleamTech.DocumentUltimate.AspNet.WebForms" Assembly="GleamTech.DocumentUltimate" %>

<!DOCTYPE html>

<html>
<head runat="server">
     <script type="text/javascript">
        function CloseAndRebind(args) {
            GetRadWindow().BrowserWindow.refreshGrid(args);
            GetRadWindow().close();
        }
        function CloseWindow() {
            window.close();
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow; //IE (and Moz as well)

            return oWindow;
        }

        function CancelEdit() {
            GetRadWindow().close();
        }
        function showWin() {
            $find("RadWindow1").show();
        }
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow)
                oWindow = window.radWindow;
            else if (window.frameElement && window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow;
            return oWindow;
        }
        function CloseModal(args) {
            var oWnd = GetRadWindow();
            if (oWnd) {
                oWnd.BrowserWindow.refreshGrid(args);
                setTimeout(function () { oWnd.close(); }, 0);
            }
        }

            </script>
    <style type="text/css">
/*#documentViewer
{

    height: 700px;
    width: 98% !important;
}*/
    </style>
    <title>Overview</title>
</head>
<body style="margin: 0px;">
   <%-- <GleamTech:ExampleFileSelectorControl ID="exampleFileSelector" runat="server"
        InitialFile="" />--%>

   <%-- <GleamTech:DocumentViewerControl ID="documentViewer" runat="server" 
        Width="800" 
        Height="600" 
        Resizable="True" 
        DeniedPermissions="Download, DownloadAsPdf, Print, SelectText" />--%>
     <GleamTech:DocumentViewerControl ID="documentViewer" Visible="false" runat="server" 
        Resizable="True"/>
    <div style=" background-color: white; width: 100%; height: 700px;">
    <iframe id="ViewPDF" runat="server" style=" width: 98%; height: 98%;"></iframe> 
</div>
</body>
</html>