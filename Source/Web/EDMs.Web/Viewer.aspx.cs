﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using GleamTech.AspNet;
using GleamTech.DocumentUltimate;
using GleamTech.Examples;
using GleamTech.IO;
using GleamTech.Zip;
using EDMs.Business.Services.Document;
using Aspose.Words;
using Aspose.Words.Saving;
using Aspose.Cells;
//using Aspose.Cells.Saving;
//using Aspose.Pdf;
namespace EDMs.Web
{
    
    public partial class Viewer : System.Web.UI.Page
    {
        protected string InputFormat;
        protected string ConvertHandlerUrl;
        private readonly PVGASDocumentAttachFileService _AttachfileService= new  PVGASDocumentAttachFileService();
        protected void Page_Load(object sender, EventArgs e)
        {
            var indexFile =  new Guid(Request.QueryString["IndexId"]);
            var fileObj = this._AttachfileService.GetById(indexFile);
            var inputDocument =Server.MapPath( fileObj.FilePath);// exampleFileSelector.SelectedFile;
            var fileInfo = new FileInfo(inputDocument);

          //  var inputFormat = DocumentFormatInfo.Get(inputDocument);
            //InputFormat = inputFormat != null ? inputFormat.Description : "(not supported)";
            ViewPDF.Attributes["name"] = fileObj.FileName;
            if (fileInfo.Extension.ToLower().Contains("pdf"))
            {
                ViewPDF.Attributes["src"] = fileObj.FilePath;
            }
            else if (!string.IsNullOrEmpty(fileObj.FilePathViewer))
            {
                ViewPDF.Attributes["src"] = fileObj.FilePathViewer;
            }
            else if (fileInfo.Extension.ToLower().Contains("doc"))
            {
                Aspose.Words.Document doc = new Aspose.Words.Document(inputDocument);
                Aspose.Words.Saving.PdfSaveOptions option = new Aspose.Words.Saving.PdfSaveOptions();
                var Output = "/DocumentLibrary/DocumentCache" + "/" + DateTime.Now.ToBinary() + ".pdf";
                doc.Save(Server.MapPath(Output), option);
                fileObj.FilePathViewer = Output;
                this._AttachfileService.Update(fileObj);
                ViewPDF.Attributes["src"] = Output;

            }
            else if (fileInfo.Extension.ToLower().Contains("xls"))
            {
                Workbook workbook = new Workbook(inputDocument);
                Aspose.Cells.PdfSaveOptions options = new Aspose.Cells.PdfSaveOptions(Aspose.Cells.SaveFormat.Pdf);
                var Output = "/DocumentLibrary/DocumentCache" + "/" + DateTime.Now.ToBinary() + ".pdf";
                options.OnePagePerSheet = true;
                // Save the document in PDF format
                workbook.Save(Server.MapPath(Output), options);
                fileObj.FilePathViewer = Output;
                this._AttachfileService.Update(fileObj);
                ViewPDF.Attributes["src"] = Output;
            }
               
            }

      

      
    }
}