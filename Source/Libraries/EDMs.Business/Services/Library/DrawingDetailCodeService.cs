﻿namespace EDMs.Business.Services.Library
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Library;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class DrawingDetailCodeService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly DrawingDetailCodeDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawingDetailCodeService"/> class.
        /// </summary>
        public DrawingDetailCodeService()
        {
            this.repo = new DrawingDetailCodeDAO();
        }

        #region Get (Advances)
        public List<DrawingDetailCode> GetAllDrawingDetailCodeOfProject(int projectId)
        {
            return this.repo.GetAll().Where(t => t.ProjectId == projectId).OrderBy(t => t.Name).ToList();
        }

        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<DrawingDetailCode> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public DrawingDetailCode GetById(int id)
        {
            return this.repo.GetById(id);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(DrawingDetailCode bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(DrawingDetailCode bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(DrawingDetailCode bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        public DrawingDetailCode GetByName(string name, int projectId)
        {
            return this.repo.GetByName(name, projectId);
        }
    }
}
