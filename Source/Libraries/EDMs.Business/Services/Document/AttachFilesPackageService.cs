﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AttachFilesPackageService.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class AttachFilesPackageService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly AttachFilesPackageDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttachFilesPackageService"/> class.
        /// </summary>
        public AttachFilesPackageService()
        {
            this.repo = new AttachFilesPackageDAO();
        }

        #region Get (Advances)

        /// <summary>
        /// The get specific.
        /// </summary>
        /// <param name="tranId">
        /// The tran id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachFilesPackage> GetSpecific(int tranId)
        {
            return this.repo.GetSpecific(tranId);
        }
        #endregion

        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<AttachFilesPackage> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// The get all by doc id.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachFilesPackage> GetAllDocumentFileByDocId(Guid docId)
        {
            return this.repo.GetAll().Where(t => t.DocumentPackageID == docId && t.AttachType == 1).ToList();
        }

        public List<AttachFilesPackage> GetAllCmtResByDocId(Guid docId)
        {
            return this.repo.GetAll().Where(t => t.DocumentPackageID == docId && t.AttachType != 1).ToList();
        }

        public List<AttachFilesPackage> GetAllDocId(Guid docId)
        {
            return this.repo.GetAll().Where(t => t.DocumentPackageID == docId).OrderBy(t => t.AttachType).ToList();
        }

        /// <summary>
        /// The get specific document.
        /// </summary>
        /// <param name="listDocId">
        /// The list doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<AttachFilesPackage> GetSpecificDocument(List<Guid> listDocId)
        {
            return this.repo.GetAll().Where(t => listDocId.Contains(t.DocumentPackageID.GetValueOrDefault()) && t.AttachType==1).ToList();
        }
        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public AttachFilesPackage GetById(int id)
        {
            return this.repo.GetById(id);
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="AttachFilesPackage"/>.
        /// </returns>
        public AttachFilesPackage GetByNameServer(string nameServer)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.FilePath.ToLower().Contains(nameServer.ToLower()));
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public int? Insert(AttachFilesPackage bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(AttachFilesPackage bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(AttachFilesPackage bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
