﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PVGASDocumentAttachFileService.cs" company="">
//   
// </copyright>
// <summary>
//   The category service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Business.Services.Document
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using EDMs.Data.DAO.Document;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category service.
    /// </summary>
    public class PVGASDocumentAttachFileService
    {
        /// <summary>
        /// The repo.
        /// </summary>
        private readonly PVGASDocumentAttachFileDAO repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="PVGASDocumentAttachFileService"/> class.
        /// </summary>
        public PVGASDocumentAttachFileService()
        {
            this.repo = new PVGASDocumentAttachFileDAO();
        }


        #region GET (Basic)
        /// <summary>
        /// Get All Categories
        /// </summary>
        /// <returns>
        /// The category
        /// </returns>
        public List<PVGASDocumentAttachFile> GetAll()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// The get all by doc id.
        /// </summary>
        /// <param name="docId">
        /// The doc id.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<PVGASDocumentAttachFile> GetAllDocumentFileByDocId(Guid docId)
        {
            return this.repo.GetAll().Where(t => t.ProjectDocumentId == docId && t.TypeId == 1).ToList();
        }

        public List<PVGASDocumentAttachFile> GetAllCmtResByDocId(Guid docId)
        {
            return this.repo.GetAll().Where(t => t.ProjectDocumentId == docId && (t.TypeId == 3 || t.TypeId == 4)).ToList();
        }
        /// <summary>
        /// Get all file markup;
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        public List<PVGASDocumentAttachFile> GetByUserUploadMarkupFile(int UserId, Guid docId)
        {
            return this.repo.GetAll().Where(t => t.ProjectDocumentId == docId && t.CreatedBy.GetValueOrDefault() == UserId && t.TypeId.GetValueOrDefault() == 2).ToList();
        }
        public List<PVGASDocumentAttachFile> GetAllDocId(Guid docId)
        {
            return this.repo.GetAll().Where(t => t.ProjectDocumentId == docId).OrderBy(t => t.TypeId).ToList();
        }

        /// <summary>
        /// Get Resource By ID
        /// </summary>
        /// <param name="id">
        /// ID of category
        /// </param>
        /// <returns>
        /// The category</returns>
        public PVGASDocumentAttachFile GetById(Guid id)
        {
            return this.repo.GetById(id);
        }

        /// <summary>
        /// The get by name.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <returns>
        /// The <see cref="PVGASDocumentAttachFile"/>.
        /// </returns>
        public PVGASDocumentAttachFile GetByNameServer(string nameServer)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.FilePath.ToLower().Contains(nameServer.ToLower()));
        }

        /// <summary>
        /// The get file consolidate by UserId
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PVGASDocumentAttachFile GetByUserUploadConsolidateFile(int UserId)
        {
            return this.repo.GetAll().FirstOrDefault(t => t.CreatedBy.GetValueOrDefault()==UserId && t.TypeId.GetValueOrDefault()==3);
        }
        #endregion

        #region Insert, Update, Delete
        /// <summary>
        /// Insert Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public Guid? Insert(PVGASDocumentAttachFile bo)
        {
            return this.repo.Insert(bo);
        }

        /// <summary>
        /// Update Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Update(PVGASDocumentAttachFile bo)
        {
            try
            {
                return this.repo.Update(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource
        /// </summary>
        /// <param name="bo"></param>
        /// <returns></returns>
        public bool Delete(PVGASDocumentAttachFile bo)
        {
            try
            {
                return this.repo.Delete(bo);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Resource By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            try
            {
                return this.repo.Delete(id);
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

    }
}
