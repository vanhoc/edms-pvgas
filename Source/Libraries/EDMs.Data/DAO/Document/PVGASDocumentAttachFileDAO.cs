﻿

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using EDMs.Data.Entities;

  public  class PVGASDocumentAttachFileDAO: BaseDAO
    {/// <summary>
     /// Initializes a new instance of the <see cref="PVGASDocumentAttachFileDAO"/> class.
     /// </summary>
        public PVGASDocumentAttachFileDAO() : base() { }

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<PVGASDocumentAttachFile> GetIQueryable()
        {
            return this.EDMsDataContext.PVGASDocumentAttachFiles;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<PVGASDocumentAttachFile> GetAll()
        {
            return this.EDMsDataContext.PVGASDocumentAttachFiles.ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public PVGASDocumentAttachFile GetById(Guid id)
        {
            return this.EDMsDataContext.PVGASDocumentAttachFiles.FirstOrDefault(ob => ob.ID == id);
        }



   

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see>
        ///       <cref>int?</cref>
        ///     </see> .
        /// </returns>
        public Guid? Insert(PVGASDocumentAttachFile ob)
        {
            try
            {
                this.EDMsDataContext.AddToPVGASDocumentAttachFiles(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(PVGASDocumentAttachFile src)
        {
            try
            {
                PVGASDocumentAttachFile des = (from rs in this.EDMsDataContext.PVGASDocumentAttachFiles
                                          where rs.ID == src.ID
                                          select rs).First();

                des.FileName = src.FileName;
                des.Extension = src.Extension;
                des.FilePath = src.FilePath;
                des.FileSize = src.FileSize;
                des.TypeId = src.TypeId;
                des.TypeName = src.TypeName;
                des.IsOnlyMarkupPage = des.IsOnlyMarkupPage;

                des.IsCheckOut = src.IsCheckOut;
                des.CheckoutBy = src.CheckoutBy;
                des.CheckoutByName = src.CheckoutByName;
                des.CheckoutDate = src.CheckoutDate;
                des.CheckinDate = src.CheckinDate;
                des.IsDefualtResponseContractor = src.IsDefualtResponseContractor;

                des.FilePathViewer = src.FilePathViewer;
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(PVGASDocumentAttachFile src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(Guid ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

    }
}
