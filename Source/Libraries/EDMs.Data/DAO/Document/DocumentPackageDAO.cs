﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DocumentPackageDAO.cs" company="">
//   
// </copyright>
// <summary>
//   The category dao.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace EDMs.Data.DAO.Document
{
    using System.Collections.Generic;
    using System.Linq;
    using System;
    using EDMs.Data.Entities;

    /// <summary>
    /// The category dao.
    /// </summary>
    public class DocumentPackageDAO : BaseDAO
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentPackageDAO"/> class.
        /// </summary>
        public DocumentPackageDAO() : base() { }

        #region GET (Basic)

        /// <summary>
        /// The get i queryable.
        /// </summary>
        /// <returns>
        /// The <see cref="IQueryable"/>.
        /// </returns>
        public IQueryable<DocumentPackage> GetIQueryable()
        {
            return this.EDMsDataContext.DocumentPackages;
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<DocumentPackage> GetAll()
        {
            return this.EDMsDataContext.DocumentPackages.OrderByDescending(t => t.ID).ToList();
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Resource"/>.
        /// </returns>
        public DocumentPackage GetById(Guid id)
        {
            return this.EDMsDataContext.DocumentPackages.FirstOrDefault(ob => ob.ID == id);
        }
       
        #endregion

        #region GET ADVANCE

        #endregion

        #region Insert, Update, Delete

        /// <summary>
        /// The insert.
        /// </summary>
        /// <param name="ob">
        /// The ob.
        /// </param>
        /// <returns>
        /// The <see cref="int?"/>.
        /// </returns>
        public Guid? Insert(DocumentPackage ob)
        {
            try
            {
                this.EDMsDataContext.AddToDocumentPackages(ob);
                this.EDMsDataContext.SaveChanges();
                return ob.ID;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="src">
        /// Entity for update
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if update success, false if not
        /// </returns>
        public bool Update(DocumentPackage src)
        {
            try
            {
                DocumentPackage des = (from rs in this.EDMsDataContext.DocumentPackages
                                where rs.ID == src.ID
                                select rs).First();

                des.DocNo = src.DocNo;
                des.DocTitle = src.DocTitle;
                des.WorkgroupId = src.WorkgroupId;
                des.WorkgroupName = src.WorkgroupName;
                des.DeparmentId = src.DeparmentId;
                des.DeparmentName = src.DeparmentName;
                des.StartDate = src.StartDate;
                des.PlanedDate = src.PlanedDate;
                des.RevisionId = src.RevisionId;
                des.RevisionName = src.RevisionName;
                des.RevisionActualDate = src.RevisionActualDate;
                des.RevisionPlanedDate = src.RevisionPlanedDate;
                des.RevisionCommentCode = src.RevisionCommentCode;
                des.RevisionReceiveTransNo = src.RevisionReceiveTransNo;
                des.Complete = src.Complete;
                des.Weight = src.Weight;
                des.OutgoingTransNo = src.OutgoingTransNo;
                des.OutgoingTransDate = src.OutgoingTransDate;
                des.IncomingTransDate = src.IncomingTransDate;
                des.IncomingTransNo = src.IncomingTransNo;
                des.ICAReviewCode = src.ICAReviewCode;
                des.ICAReviewOutTransNo = src.ICAReviewOutTransNo;
                des.ICAReviewReceivedDate = src.ICAReviewReceivedDate;
                des.Notes = src.Notes;
                des.DocumentTypeId = src.DocumentTypeId;
                des.DocumentTypeName = src.DocumentTypeName;
                des.DisciplineId = src.DisciplineId;
                des.DisciplineName = src.DisciplineName;
                des.PackageId = src.PackageId;
                des.PackageName = src.PackageName;
                des.ProjectId = src.ProjectId;
                des.ProjectName = src.ProjectName;
                des.IsLeaf = src.IsLeaf;
                des.IsDelete = src.IsDelete;
                des.IsEMDR = src.IsEMDR;
                des.IndexNumber = src.IndexNumber;

                des.IsCriticalDoc = src.IsCriticalDoc;
                des.IsPriorityDoc = src.IsPriorityDoc;
                des.IsVendorDoc = src.IsVendorDoc;

                des.ReferenceFromID = src.ReferenceFromID;
                des.ReferenceFromName = src.ReferenceFromName;
                des.StatusID = src.StatusID;
                des.StatusName = src.StatusName;
                des.FinalCodeID = src.FinalCodeID;
                des.FinalCodeName = src.FinalCodeName;
                des.CompleteForProject = src.CompleteForProject;

                des.ProjectFullName = src.ProjectFullName;
                des.DocTypeFullName = src.DocTypeFullName;
                des.DisciplineFullName = src.DisciplineFullName;
                des.OriginatorFullName = src.OriginatorFullName;
                des.OriginatorId = src.OriginatorId;
                des.OriginatorName = src.OriginatorName;
                des.SequencetialNumber = src.SequencetialNumber;
                des.DrawingSheetNumber = src.DrawingSheetNumber;

                des.FinalIssuePlanDate = src.FinalIssuePlanDate;
                des.FinalIssueActualDate = src.FinalIssueActualDate;
                des.FinalIssueTransNo = src.FinalIssueTransNo;

                des.FirstIssueActualDate = src.FirstIssueActualDate;
                des.FirstIssuePlanDate = src.FirstIssuePlanDate;
                des.FirstIssueTransNo = src.FirstIssueTransNo;

                des.RevisionActualReceiveCommentDate = src.RevisionActualReceiveCommentDate;
                des.RevisionPlanReceiveCommentDate = src.RevisionPlanReceiveCommentDate;
                des.IsInternalDocument = src.IsInternalDocument;
                des.LastUpdatedBy = src.LastUpdatedBy;
                des.LastUpdatedDate = src.LastUpdatedDate;
                des.LastUpdatedByName = src.LastUpdatedByName;

                des.StatusFullName = src.StatusFullName;
                des.PhaseId = src.PhaseId;
                des.PhaseFullName = src.PhaseFullName;
                des.PhaseName = src.PhaseName;

                des.AreaId = src.AreaId;
                des.AreaName = src.AreaName;
                des.AreaFullName = src.AreaFullName;

                des.SubAreaId = src.SubAreaId;
                des.SubAreaName = src.SubAreaName;
                des.SubAreaFullName = src.SubAreaFullName;

                des.DrawingCodeId = src.DrawingCodeId;
                des.DrawingCodeName = src.DrawingCodeName;
                des.DrawingCodeFullName = src.DrawingCodeFullName;

                des.DrawingDetailCodeId = src.DrawingDetailCodeId;
                des.DrawingDetailCodeName = src.DrawingDetailCodeName;
                des.DrawingDetailCodeFullName = src.DrawingDetailCodeFullName;

                des.SystemCodeId = src.SystemCodeId;
                des.SystemCodeName = src.SystemCodeName;
                des.SystemCodeFullName = src.SystemCodeFullName;

                des.ContractorId = src.ContractorId;
                des.ContractorName = src.ContractorName;
                des.ContractorFullName = src.ContractorFullName;

                des.CodeId = src.CodeId;
                des.CodeName = src.CodeName;
                des.CodeFullName = src.CodeFullName;
                des.OutgoingTransId = src.OutgoingTransId;
                des.IncomingTransId = src.IncomingTransId;
                des.IsCreateOutgoingTrans = src.IsCreateOutgoingTrans;
                des.IsHasAttachFile = src.IsHasAttachFile;
                des.IsInWFProcess = src.IsInWFProcess;
                des.IsWFComplete = src.IsWFComplete;
                des.IsCompleteFinal = src.IsCompleteFinal;
                des.CurrentWorkflowName = src.CurrentWorkflowName;
                des.CurrentWorkflowStepName = src.CurrentWorkflowStepName;
                des.CurrentAssignUserName = src.CurrentAssignUserName;
                des.StartPlan = src.StartPlan;
                des.StartRevised = src.StartRevised;
                des.StartActual = src.StartActual;
                des.IDCPlan = src.IDCPlan;
                des.IDCRevised = src.IDCRevised;
                des.IDCACtual = src.IDCACtual;
                des.IFRActual = src.IFRActual;
                des.IFRPlan = src.IFRPlan;
                des.IFRRevised = src.IFRRevised;
                des.ReIFRActual = src.ReIFRActual;
                des.ReIFRPlan = src.ReIFRPlan;
                des.ReIFRRevised = src.ReIFRRevised;
                des.IFAActual = src.IFAActual;
                des.IFAPlan = src.IFAPlan;
                des.IFARevised = src.IFARevised;
                des.AFCPlan = src.AFCPlan;
                des.AFCRevised = src.AFCRevised;
                des.AFCActual = src.AFCActual;
                des.IsAttachWorkflow = src.IsAttachWorkflow;
                des.Remarks = src.Remarks;
                des.DeadlineTransOut = src.DeadlineTransOut;

                des.IsUseCustomWfFromTrans = src.IsUseCustomWfFromTrans;
                des.IsUseIsUseCustomWfFromObj = src.IsUseIsUseCustomWfFromObj;
                des.DeadlineClientResponse = src.DeadlineClientResponse;
                des.DocReviewStatusCodeID = src.DocReviewStatusCodeID;
                des.DocReviewStatusCode = src.DocReviewStatusCode;
                des.ResponseCAId = src.ResponseCAId;
                des.ResponseCACode = src.ResponseCACode;
                des.OutgoingTransDate_CA = src.OutgoingTransDate_CA;
                des.OutgoingTransId_CA = src.OutgoingTransId_CA;
                des.OutgoingTransNo_CA = src.OutgoingTransNo_CA;
                des.IncomingTransDate_CA = src.IncomingTransDate_CA;
                des.IncomingTransId_CA = src.IncomingTransId_CA;
                des.IncomingTransNo_CA = src.IncomingTransNo_CA;
                des.DNV_DueDate = src.DNV_DueDate;
                des.DNV_markup = src.DNV_markup;
                des.KDN_markup = src.KDN_markup;
                des.NCS_markup = src.NCS_markup;

                des.DNV_FinalCode = src.DNV_FinalCode;
                des.DNB_FinalCode = src.DNB_FinalCode;
                des.KDN_Operator_FinalCode = src.KDN_Operator_FinalCode;
                des.NCS_Operator_FinalCode = src.NCS_Operator_FinalCode;

                des.KDN_Operator_ResponseId = src.KDN_Operator_ResponseId;
                des.KDN_Operator_ResponseCode = src.KDN_Operator_ResponseCode;
                des.KDN_Operator_OutgoingTransDate = src.KDN_Operator_OutgoingTransDate;
                des.KDN_Operator_OutgoingTransId = src.KDN_Operator_OutgoingTransId;
                des.KDN_Operator_OutgoingTransNo = src.KDN_Operator_OutgoingTransNo;
                des.KDN_Operator_IncomingTransDate = src.KDN_Operator_IncomingTransDate;
                des.KDN_Operator_IncomingTransId = src.KDN_Operator_IncomingTransId;
                des.KDN_Operator_IncomingTransNo = src.KDN_Operator_IncomingTransNo;
                des.KDN_Operator_DueDate = src.KDN_Operator_DueDate;
                des.KDN_Owner_ResponseId = src.KDN_Owner_ResponseId;
                des.KDN_Owner_ResponseCode = src.KDN_Owner_ResponseCode;
                des.KDN_Owner_OutgoingTransDate = src.KDN_Owner_OutgoingTransDate;
                des.KDN_Owner_OutgoingTransId = src.KDN_Owner_OutgoingTransId;
                des.KDN_Owner_OutgoingTransNo = src.KDN_Owner_OutgoingTransNo;
                des.KDN_Owner_IncomingTransDate = src.KDN_Owner_IncomingTransDate;
                des.KDN_Owner_IncomingTransId = src.KDN_Owner_IncomingTransId;
                des.KDN_Owner_IncomingTransNo = src.KDN_Owner_IncomingTransNo;
                des.KDN_Owner_DueDate = src.KDN_Owner_DueDate;

                des.NCS_Operator_ResponseId = src.NCS_Operator_ResponseId;
                des.NCS_Operator_ResponseCode = src.NCS_Operator_ResponseCode;
                des.NCS_Operator_OutgoingTransDate = src.NCS_Operator_OutgoingTransDate;
                des.NCS_Operator_OutgoingTransId = src.NCS_Operator_OutgoingTransId;
                des.NCS_Operator_OutgoingTransNo = src.NCS_Operator_OutgoingTransNo;
                des.NCS_Operator_IncomingTransDate = src.NCS_Operator_IncomingTransDate;
                des.NCS_Operator_IncomingTransId = src.NCS_Operator_IncomingTransId;
                des.NCS_Operator_IncomingTransNo = src.NCS_Operator_IncomingTransNo;
                des.NCS_Operator_DueDate = src.NCS_Operator_DueDate;
                des.NCS_Owner_ResponseId = src.NCS_Owner_ResponseId;
                des.NCS_Owner_ResponseCode = src.NCS_Owner_ResponseCode;
                des.NCS_Owner_OutgoingTransDate = src.NCS_Owner_OutgoingTransDate;
                des.NCS_Owner_OutgoingTransId = src.NCS_Owner_OutgoingTransId;
                des.NCS_Owner_OutgoingTransNo = src.NCS_Owner_OutgoingTransNo;
                des.NCS_Owner_IncomingTransDate = src.NCS_Owner_IncomingTransDate;
                des.NCS_Owner_IncomingTransId = src.NCS_Owner_IncomingTransId;
                des.NCS_Owner_IncomingTransNo = src.NCS_Owner_IncomingTransNo;
                des.NCS_Owner_DueDate = src.NCS_Owner_DueDate;

                des.IsSendNCSPOwner = src.IsSendNCSPOwner;
                des.CategoryID = src.CategoryID;
                des.CategoryName = src.CategoryName;
                des.KDN_Owner_FinalCode = src.KDN_Owner_FinalCode;
                des.NCS_Owner_FinalCode = src.NCS_Owner_FinalCode;
                this.EDMsDataContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="src">
        /// The src.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if delete success, false if not
        /// </returns>
        public bool Delete(DocumentPackage src)
        {
            try
            {
                var des = this.GetById(src.ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete By ID
        /// </summary>
        /// <param name="ID"></param>
        /// ID of entity
        /// <returns></returns>
        public bool Delete(Guid ID)
        {
            try
            {
                var des = this.GetById(ID);
                if (des != null)
                {
                    this.EDMsDataContext.DeleteObject(des);
                    this.EDMsDataContext.SaveChanges();
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
