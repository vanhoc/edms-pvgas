﻿namespace EDMs.Data.Entities
{
    public partial class DocumentNumbering
    {
        public string FullName
        {
            get
            {
                return !string.IsNullOrEmpty(this.Description)
                        ? this.Name + " - " + this.Description
                        : this.Name;
            }
        }
        public string FullNameLenght
        {
            get
            {
                return this.Name + "( " + this.Lenght + " segment)";
            }
        }
    }
}
